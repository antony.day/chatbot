module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 140);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * The main AWS namespace
 */
var AWS = { util: __webpack_require__(2) };

/**
 * @api private
 * @!macro [new] nobrowser
 *   @note This feature is not supported in the browser environment of the SDK.
 */
var _hidden = {}; _hidden.toString(); // hack to parse macro

/**
 * @api private
 */
module.exports = AWS;

AWS.util.update(AWS, {

  /**
   * @constant
   */
  VERSION: '2.612.0',

  /**
   * @api private
   */
  Signers: {},

  /**
   * @api private
   */
  Protocol: {
    Json: __webpack_require__(16),
    Query: __webpack_require__(25),
    Rest: __webpack_require__(13),
    RestJson: __webpack_require__(27),
    RestXml: __webpack_require__(28)
  },

  /**
   * @api private
   */
  XML: {
    Builder: __webpack_require__(58),
    Parser: null // conditionally set based on environment
  },

  /**
   * @api private
   */
  JSON: {
    Builder: __webpack_require__(17),
    Parser: __webpack_require__(18)
  },

  /**
   * @api private
   */
  Model: {
    Api: __webpack_require__(29),
    Operation: __webpack_require__(30),
    Shape: __webpack_require__(9),
    Paginator: __webpack_require__(31),
    ResourceWaiter: __webpack_require__(32)
  },

  /**
   * @api private
   */
  apiLoader: __webpack_require__(63),

  /**
   * @api private
   */
  EndpointCache: __webpack_require__(64).EndpointCache
});
__webpack_require__(34);
__webpack_require__(66);
__webpack_require__(69);
__webpack_require__(37);
__webpack_require__(70);
__webpack_require__(75);
__webpack_require__(77);
__webpack_require__(78);
__webpack_require__(79);
__webpack_require__(86);

/**
 * @readonly
 * @return [AWS.SequentialExecutor] a collection of global event listeners that
 *   are attached to every sent request.
 * @see AWS.Request AWS.Request for a list of events to listen for
 * @example Logging the time taken to send a request
 *   AWS.events.on('send', function startSend(resp) {
 *     resp.startTime = new Date().getTime();
 *   }).on('complete', function calculateTime(resp) {
 *     var time = (new Date().getTime() - resp.startTime) / 1000;
 *     console.log('Request took ' + time + ' seconds');
 *   });
 *
 *   new AWS.S3().listBuckets(); // prints 'Request took 0.285 seconds'
 */
AWS.events = new AWS.SequentialExecutor();

//create endpoint cache lazily
AWS.util.memoizedProperty(AWS, 'endpointCache', function() {
  return new AWS.EndpointCache(AWS.config.endpointCacheSize);
}, true);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process, setImmediate) {/* eslint guard-for-in:0 */
var AWS;

/**
 * A set of utility methods for use with the AWS SDK.
 *
 * @!attribute abort
 *   Return this value from an iterator function {each} or {arrayEach}
 *   to break out of the iteration.
 *   @example Breaking out of an iterator function
 *     AWS.util.each({a: 1, b: 2, c: 3}, function(key, value) {
 *       if (key == 'b') return AWS.util.abort;
 *     });
 *   @see each
 *   @see arrayEach
 * @api private
 */
var util = {
  environment: 'nodejs',
  engine: function engine() {
    if (util.isBrowser() && typeof navigator !== 'undefined') {
      return navigator.userAgent;
    } else {
      var engine = process.platform + '/' + process.version;
      if (Object({"NODE_ENV":undefined}).AWS_EXECUTION_ENV) {
        engine += ' exec-env/' + Object({"NODE_ENV":undefined}).AWS_EXECUTION_ENV;
      }
      return engine;
    }
  },

  userAgent: function userAgent() {
    var name = util.environment;
    var agent = 'aws-sdk-' + name + '/' + __webpack_require__(1).VERSION;
    if (name === 'nodejs') agent += ' ' + util.engine();
    return agent;
  },

  uriEscape: function uriEscape(string) {
    var output = encodeURIComponent(string);
    output = output.replace(/[^A-Za-z0-9_.~\-%]+/g, escape);

    // AWS percent-encodes some extra non-standard characters in a URI
    output = output.replace(/[*]/g, function(ch) {
      return '%' + ch.charCodeAt(0).toString(16).toUpperCase();
    });

    return output;
  },

  uriEscapePath: function uriEscapePath(string) {
    var parts = [];
    util.arrayEach(string.split('/'), function (part) {
      parts.push(util.uriEscape(part));
    });
    return parts.join('/');
  },

  urlParse: function urlParse(url) {
    return util.url.parse(url);
  },

  urlFormat: function urlFormat(url) {
    return util.url.format(url);
  },

  queryStringParse: function queryStringParse(qs) {
    return util.querystring.parse(qs);
  },

  queryParamsToString: function queryParamsToString(params) {
    var items = [];
    var escape = util.uriEscape;
    var sortedKeys = Object.keys(params).sort();

    util.arrayEach(sortedKeys, function(name) {
      var value = params[name];
      var ename = escape(name);
      var result = ename + '=';
      if (Array.isArray(value)) {
        var vals = [];
        util.arrayEach(value, function(item) { vals.push(escape(item)); });
        result = ename + '=' + vals.sort().join('&' + ename + '=');
      } else if (value !== undefined && value !== null) {
        result = ename + '=' + escape(value);
      }
      items.push(result);
    });

    return items.join('&');
  },

  readFileSync: function readFileSync(path) {
    if (util.isBrowser()) return null;
    return __webpack_require__(21).readFileSync(path, 'utf-8');
  },

  base64: {
    encode: function encode64(string) {
      if (typeof string === 'number') {
        throw util.error(new Error('Cannot base64 encode number ' + string));
      }
      if (string === null || typeof string === 'undefined') {
        return string;
      }
      var buf = util.buffer.toBuffer(string);
      return buf.toString('base64');
    },

    decode: function decode64(string) {
      if (typeof string === 'number') {
        throw util.error(new Error('Cannot base64 decode number ' + string));
      }
      if (string === null || typeof string === 'undefined') {
        return string;
      }
      return util.buffer.toBuffer(string, 'base64');
    }

  },

  buffer: {
    /**
     * Buffer constructor for Node buffer and buffer pollyfill
     */
    toBuffer: function(data, encoding) {
      return (typeof util.Buffer.from === 'function' && util.Buffer.from !== Uint8Array.from) ?
        util.Buffer.from(data, encoding) : new util.Buffer(data, encoding);
    },

    alloc: function(size, fill, encoding) {
      if (typeof size !== 'number') {
        throw new Error('size passed to alloc must be a number.');
      }
      if (typeof util.Buffer.alloc === 'function') {
        return util.Buffer.alloc(size, fill, encoding);
      } else {
        var buf = new util.Buffer(size);
        if (fill !== undefined && typeof buf.fill === 'function') {
          buf.fill(fill, undefined, undefined, encoding);
        }
        return buf;
      }
    },

    toStream: function toStream(buffer) {
      if (!util.Buffer.isBuffer(buffer)) buffer =  util.buffer.toBuffer(buffer);

      var readable = new (util.stream.Readable)();
      var pos = 0;
      readable._read = function(size) {
        if (pos >= buffer.length) return readable.push(null);

        var end = pos + size;
        if (end > buffer.length) end = buffer.length;
        readable.push(buffer.slice(pos, end));
        pos = end;
      };

      return readable;
    },

    /**
     * Concatenates a list of Buffer objects.
     */
    concat: function(buffers) {
      var length = 0,
          offset = 0,
          buffer = null, i;

      for (i = 0; i < buffers.length; i++) {
        length += buffers[i].length;
      }

      buffer = util.buffer.alloc(length);

      for (i = 0; i < buffers.length; i++) {
        buffers[i].copy(buffer, offset);
        offset += buffers[i].length;
      }

      return buffer;
    }
  },

  string: {
    byteLength: function byteLength(string) {
      if (string === null || string === undefined) return 0;
      if (typeof string === 'string') string = util.buffer.toBuffer(string);

      if (typeof string.byteLength === 'number') {
        return string.byteLength;
      } else if (typeof string.length === 'number') {
        return string.length;
      } else if (typeof string.size === 'number') {
        return string.size;
      } else if (typeof string.path === 'string') {
        return __webpack_require__(21).lstatSync(string.path).size;
      } else {
        throw util.error(new Error('Cannot determine length of ' + string),
          { object: string });
      }
    },

    upperFirst: function upperFirst(string) {
      return string[0].toUpperCase() + string.substr(1);
    },

    lowerFirst: function lowerFirst(string) {
      return string[0].toLowerCase() + string.substr(1);
    }
  },

  ini: {
    parse: function string(ini) {
      var currentSection, map = {};
      util.arrayEach(ini.split(/\r?\n/), function(line) {
        line = line.split(/(^|\s)[;#]/)[0]; // remove comments
        var section = line.match(/^\s*\[([^\[\]]+)\]\s*$/);
        if (section) {
          currentSection = section[1];
        } else if (currentSection) {
          var item = line.match(/^\s*(.+?)\s*=\s*(.+?)\s*$/);
          if (item) {
            map[currentSection] = map[currentSection] || {};
            map[currentSection][item[1]] = item[2];
          }
        }
      });

      return map;
    }
  },

  fn: {
    noop: function() {},
    callback: function (err) { if (err) throw err; },

    /**
     * Turn a synchronous function into as "async" function by making it call
     * a callback. The underlying function is called with all but the last argument,
     * which is treated as the callback. The callback is passed passed a first argument
     * of null on success to mimick standard node callbacks.
     */
    makeAsync: function makeAsync(fn, expectedArgs) {
      if (expectedArgs && expectedArgs <= fn.length) {
        return fn;
      }

      return function() {
        var args = Array.prototype.slice.call(arguments, 0);
        var callback = args.pop();
        var result = fn.apply(null, args);
        callback(result);
      };
    }
  },

  /**
   * Date and time utility functions.
   */
  date: {

    /**
     * @return [Date] the current JavaScript date object. Since all
     *   AWS services rely on this date object, you can override
     *   this function to provide a special time value to AWS service
     *   requests.
     */
    getDate: function getDate() {
      if (!AWS) AWS = __webpack_require__(1);
      if (AWS.config.systemClockOffset) { // use offset when non-zero
        return new Date(new Date().getTime() + AWS.config.systemClockOffset);
      } else {
        return new Date();
      }
    },

    /**
     * @return [String] the date in ISO-8601 format
     */
    iso8601: function iso8601(date) {
      if (date === undefined) { date = util.date.getDate(); }
      return date.toISOString().replace(/\.\d{3}Z$/, 'Z');
    },

    /**
     * @return [String] the date in RFC 822 format
     */
    rfc822: function rfc822(date) {
      if (date === undefined) { date = util.date.getDate(); }
      return date.toUTCString();
    },

    /**
     * @return [Integer] the UNIX timestamp value for the current time
     */
    unixTimestamp: function unixTimestamp(date) {
      if (date === undefined) { date = util.date.getDate(); }
      return date.getTime() / 1000;
    },

    /**
     * @param [String,number,Date] date
     * @return [Date]
     */
    from: function format(date) {
      if (typeof date === 'number') {
        return new Date(date * 1000); // unix timestamp
      } else {
        return new Date(date);
      }
    },

    /**
     * Given a Date or date-like value, this function formats the
     * date into a string of the requested value.
     * @param [String,number,Date] date
     * @param [String] formatter Valid formats are:
     #   * 'iso8601'
     #   * 'rfc822'
     #   * 'unixTimestamp'
     * @return [String]
     */
    format: function format(date, formatter) {
      if (!formatter) formatter = 'iso8601';
      return util.date[formatter](util.date.from(date));
    },

    parseTimestamp: function parseTimestamp(value) {
      if (typeof value === 'number') { // unix timestamp (number)
        return new Date(value * 1000);
      } else if (value.match(/^\d+$/)) { // unix timestamp
        return new Date(value * 1000);
      } else if (value.match(/^\d{4}/)) { // iso8601
        return new Date(value);
      } else if (value.match(/^\w{3},/)) { // rfc822
        return new Date(value);
      } else {
        throw util.error(
          new Error('unhandled timestamp format: ' + value),
          {code: 'TimestampParserError'});
      }
    }

  },

  crypto: {
    crc32Table: [
     0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419,
     0x706AF48F, 0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4,
     0xE0D5E91E, 0x97D2D988, 0x09B64C2B, 0x7EB17CBD, 0xE7B82D07,
     0x90BF1D91, 0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
     0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7, 0x136C9856,
     0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
     0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4,
     0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
     0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3,
     0x45DF5C75, 0xDCD60DCF, 0xABD13D59, 0x26D930AC, 0x51DE003A,
     0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599,
     0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
     0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190,
     0x01DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F,
     0x9FBFE4A5, 0xE8B8D433, 0x7807C9A2, 0x0F00F934, 0x9609A88E,
     0xE10E9818, 0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
     0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED,
     0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
     0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3,
     0xFBD44C65, 0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
     0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A,
     0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5,
     0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA, 0xBE0B1010,
     0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
     0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17,
     0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6,
     0x03B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x04DB2615,
     0x73DC1683, 0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
     0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1, 0xF00F9344,
     0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
     0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A,
     0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
     0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1,
     0xA6BC5767, 0x3FB506DD, 0x48B2364B, 0xD80D2BDA, 0xAF0A1B4C,
     0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF,
     0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
     0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE,
     0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31,
     0x2CD99E8B, 0x5BDEAE1D, 0x9B64C2B0, 0xEC63F226, 0x756AA39C,
     0x026D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
     0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38, 0x92D28E9B,
     0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
     0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1,
     0x18B74777, 0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
     0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45, 0xA00AE278,
     0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7,
     0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC, 0x40DF0B66,
     0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
     0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605,
     0xCDD70693, 0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8,
     0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B,
     0x2D02EF8D],

    crc32: function crc32(data) {
      var tbl = util.crypto.crc32Table;
      var crc = 0 ^ -1;

      if (typeof data === 'string') {
        data = util.buffer.toBuffer(data);
      }

      for (var i = 0; i < data.length; i++) {
        var code = data.readUInt8(i);
        crc = (crc >>> 8) ^ tbl[(crc ^ code) & 0xFF];
      }
      return (crc ^ -1) >>> 0;
    },

    hmac: function hmac(key, string, digest, fn) {
      if (!digest) digest = 'binary';
      if (digest === 'buffer') { digest = undefined; }
      if (!fn) fn = 'sha256';
      if (typeof string === 'string') string = util.buffer.toBuffer(string);
      return util.crypto.lib.createHmac(fn, key).update(string).digest(digest);
    },

    md5: function md5(data, digest, callback) {
      return util.crypto.hash('md5', data, digest, callback);
    },

    sha256: function sha256(data, digest, callback) {
      return util.crypto.hash('sha256', data, digest, callback);
    },

    hash: function(algorithm, data, digest, callback) {
      var hash = util.crypto.createHash(algorithm);
      if (!digest) { digest = 'binary'; }
      if (digest === 'buffer') { digest = undefined; }
      if (typeof data === 'string') data = util.buffer.toBuffer(data);
      var sliceFn = util.arraySliceFn(data);
      var isBuffer = util.Buffer.isBuffer(data);
      //Identifying objects with an ArrayBuffer as buffers
      if (util.isBrowser() && typeof ArrayBuffer !== 'undefined' && data && data.buffer instanceof ArrayBuffer) isBuffer = true;

      if (callback && typeof data === 'object' &&
          typeof data.on === 'function' && !isBuffer) {
        data.on('data', function(chunk) { hash.update(chunk); });
        data.on('error', function(err) { callback(err); });
        data.on('end', function() { callback(null, hash.digest(digest)); });
      } else if (callback && sliceFn && !isBuffer &&
                 typeof FileReader !== 'undefined') {
        // this might be a File/Blob
        var index = 0, size = 1024 * 512;
        var reader = new FileReader();
        reader.onerror = function() {
          callback(new Error('Failed to read data.'));
        };
        reader.onload = function() {
          var buf = new util.Buffer(new Uint8Array(reader.result));
          hash.update(buf);
          index += buf.length;
          reader._continueReading();
        };
        reader._continueReading = function() {
          if (index >= data.size) {
            callback(null, hash.digest(digest));
            return;
          }

          var back = index + size;
          if (back > data.size) back = data.size;
          reader.readAsArrayBuffer(sliceFn.call(data, index, back));
        };

        reader._continueReading();
      } else {
        if (util.isBrowser() && typeof data === 'object' && !isBuffer) {
          data = new util.Buffer(new Uint8Array(data));
        }
        var out = hash.update(data).digest(digest);
        if (callback) callback(null, out);
        return out;
      }
    },

    toHex: function toHex(data) {
      var out = [];
      for (var i = 0; i < data.length; i++) {
        out.push(('0' + data.charCodeAt(i).toString(16)).substr(-2, 2));
      }
      return out.join('');
    },

    createHash: function createHash(algorithm) {
      return util.crypto.lib.createHash(algorithm);
    }

  },

  /** @!ignore */

  /* Abort constant */
  abort: {},

  each: function each(object, iterFunction) {
    for (var key in object) {
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        var ret = iterFunction.call(this, key, object[key]);
        if (ret === util.abort) break;
      }
    }
  },

  arrayEach: function arrayEach(array, iterFunction) {
    for (var idx in array) {
      if (Object.prototype.hasOwnProperty.call(array, idx)) {
        var ret = iterFunction.call(this, array[idx], parseInt(idx, 10));
        if (ret === util.abort) break;
      }
    }
  },

  update: function update(obj1, obj2) {
    util.each(obj2, function iterator(key, item) {
      obj1[key] = item;
    });
    return obj1;
  },

  merge: function merge(obj1, obj2) {
    return util.update(util.copy(obj1), obj2);
  },

  copy: function copy(object) {
    if (object === null || object === undefined) return object;
    var dupe = {};
    // jshint forin:false
    for (var key in object) {
      dupe[key] = object[key];
    }
    return dupe;
  },

  isEmpty: function isEmpty(obj) {
    for (var prop in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, prop)) {
        return false;
      }
    }
    return true;
  },

  arraySliceFn: function arraySliceFn(obj) {
    var fn = obj.slice || obj.webkitSlice || obj.mozSlice;
    return typeof fn === 'function' ? fn : null;
  },

  isType: function isType(obj, type) {
    // handle cross-"frame" objects
    if (typeof type === 'function') type = util.typeName(type);
    return Object.prototype.toString.call(obj) === '[object ' + type + ']';
  },

  typeName: function typeName(type) {
    if (Object.prototype.hasOwnProperty.call(type, 'name')) return type.name;
    var str = type.toString();
    var match = str.match(/^\s*function (.+)\(/);
    return match ? match[1] : str;
  },

  error: function error(err, options) {
    var originalError = null;
    if (typeof err.message === 'string' && err.message !== '') {
      if (typeof options === 'string' || (options && options.message)) {
        originalError = util.copy(err);
        originalError.message = err.message;
      }
    }
    err.message = err.message || null;

    if (typeof options === 'string') {
      err.message = options;
    } else if (typeof options === 'object' && options !== null) {
      util.update(err, options);
      if (options.message)
        err.message = options.message;
      if (options.code || options.name)
        err.code = options.code || options.name;
      if (options.stack)
        err.stack = options.stack;
    }

    if (typeof Object.defineProperty === 'function') {
      Object.defineProperty(err, 'name', {writable: true, enumerable: false});
      Object.defineProperty(err, 'message', {enumerable: true});
    }

    err.name = String(options && options.name || err.name || err.code || 'Error');
    err.time = new Date();

    if (originalError) err.originalError = originalError;

    return err;
  },

  /**
   * @api private
   */
  inherit: function inherit(klass, features) {
    var newObject = null;
    if (features === undefined) {
      features = klass;
      klass = Object;
      newObject = {};
    } else {
      var ctor = function ConstructorWrapper() {};
      ctor.prototype = klass.prototype;
      newObject = new ctor();
    }

    // constructor not supplied, create pass-through ctor
    if (features.constructor === Object) {
      features.constructor = function() {
        if (klass !== Object) {
          return klass.apply(this, arguments);
        }
      };
    }

    features.constructor.prototype = newObject;
    util.update(features.constructor.prototype, features);
    features.constructor.__super__ = klass;
    return features.constructor;
  },

  /**
   * @api private
   */
  mixin: function mixin() {
    var klass = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
      // jshint forin:false
      for (var prop in arguments[i].prototype) {
        var fn = arguments[i].prototype[prop];
        if (prop !== 'constructor') {
          klass.prototype[prop] = fn;
        }
      }
    }
    return klass;
  },

  /**
   * @api private
   */
  hideProperties: function hideProperties(obj, props) {
    if (typeof Object.defineProperty !== 'function') return;

    util.arrayEach(props, function (key) {
      Object.defineProperty(obj, key, {
        enumerable: false, writable: true, configurable: true });
    });
  },

  /**
   * @api private
   */
  property: function property(obj, name, value, enumerable, isValue) {
    var opts = {
      configurable: true,
      enumerable: enumerable !== undefined ? enumerable : true
    };
    if (typeof value === 'function' && !isValue) {
      opts.get = value;
    }
    else {
      opts.value = value; opts.writable = true;
    }

    Object.defineProperty(obj, name, opts);
  },

  /**
   * @api private
   */
  memoizedProperty: function memoizedProperty(obj, name, get, enumerable) {
    var cachedValue = null;

    // build enumerable attribute for each value with lazy accessor.
    util.property(obj, name, function() {
      if (cachedValue === null) {
        cachedValue = get();
      }
      return cachedValue;
    }, enumerable);
  },

  /**
   * TODO Remove in major version revision
   * This backfill populates response data without the
   * top-level payload name.
   *
   * @api private
   */
  hoistPayloadMember: function hoistPayloadMember(resp) {
    var req = resp.request;
    var operationName = req.operation;
    var operation = req.service.api.operations[operationName];
    var output = operation.output;
    if (output.payload && !operation.hasEventOutput) {
      var payloadMember = output.members[output.payload];
      var responsePayload = resp.data[output.payload];
      if (payloadMember.type === 'structure') {
        util.each(responsePayload, function(key, value) {
          util.property(resp.data, key, value, false);
        });
      }
    }
  },

  /**
   * Compute SHA-256 checksums of streams
   *
   * @api private
   */
  computeSha256: function computeSha256(body, done) {
    if (util.isNode()) {
      var Stream = util.stream.Stream;
      var fs = __webpack_require__(21);
      if (typeof Stream === 'function' && body instanceof Stream) {
        if (typeof body.path === 'string') { // assume file object
          var settings = {};
          if (typeof body.start === 'number') {
            settings.start = body.start;
          }
          if (typeof body.end === 'number') {
            settings.end = body.end;
          }
          body = fs.createReadStream(body.path, settings);
        } else { // TODO support other stream types
          return done(new Error('Non-file stream objects are ' +
                                'not supported with SigV4'));
        }
      }
    }

    util.crypto.sha256(body, 'hex', function(err, sha) {
      if (err) done(err);
      else done(null, sha);
    });
  },

  /**
   * @api private
   */
  isClockSkewed: function isClockSkewed(serverTime) {
    if (serverTime) {
      util.property(AWS.config, 'isClockSkewed',
        Math.abs(new Date().getTime() - serverTime) >= 300000, false);
      return AWS.config.isClockSkewed;
    }
  },

  applyClockOffset: function applyClockOffset(serverTime) {
    if (serverTime)
      AWS.config.systemClockOffset = serverTime - new Date().getTime();
  },

  /**
   * @api private
   */
  extractRequestId: function extractRequestId(resp) {
    var requestId = resp.httpResponse.headers['x-amz-request-id'] ||
                     resp.httpResponse.headers['x-amzn-requestid'];

    if (!requestId && resp.data && resp.data.ResponseMetadata) {
      requestId = resp.data.ResponseMetadata.RequestId;
    }

    if (requestId) {
      resp.requestId = requestId;
    }

    if (resp.error) {
      resp.error.requestId = requestId;
    }
  },

  /**
   * @api private
   */
  addPromises: function addPromises(constructors, PromiseDependency) {
    var deletePromises = false;
    if (PromiseDependency === undefined && AWS && AWS.config) {
      PromiseDependency = AWS.config.getPromisesDependency();
    }
    if (PromiseDependency === undefined && typeof Promise !== 'undefined') {
      PromiseDependency = Promise;
    }
    if (typeof PromiseDependency !== 'function') deletePromises = true;
    if (!Array.isArray(constructors)) constructors = [constructors];

    for (var ind = 0; ind < constructors.length; ind++) {
      var constructor = constructors[ind];
      if (deletePromises) {
        if (constructor.deletePromisesFromClass) {
          constructor.deletePromisesFromClass();
        }
      } else if (constructor.addPromisesToClass) {
        constructor.addPromisesToClass(PromiseDependency);
      }
    }
  },

  /**
   * @api private
   * Return a function that will return a promise whose fate is decided by the
   * callback behavior of the given method with `methodName`. The method to be
   * promisified should conform to node.js convention of accepting a callback as
   * last argument and calling that callback with error as the first argument
   * and success value on the second argument.
   */
  promisifyMethod: function promisifyMethod(methodName, PromiseDependency) {
    return function promise() {
      var self = this;
      var args = Array.prototype.slice.call(arguments);
      return new PromiseDependency(function(resolve, reject) {
        args.push(function(err, data) {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
        self[methodName].apply(self, args);
      });
    };
  },

  /**
   * @api private
   */
  isDualstackAvailable: function isDualstackAvailable(service) {
    if (!service) return false;
    var metadata = __webpack_require__(33);
    if (typeof service !== 'string') service = service.serviceIdentifier;
    if (typeof service !== 'string' || !metadata.hasOwnProperty(service)) return false;
    return !!metadata[service].dualstackAvailable;
  },

  /**
   * @api private
   */
  calculateRetryDelay: function calculateRetryDelay(retryCount, retryDelayOptions, err) {
    if (!retryDelayOptions) retryDelayOptions = {};
    var customBackoff = retryDelayOptions.customBackoff || null;
    if (typeof customBackoff === 'function') {
      return customBackoff(retryCount, err);
    }
    var base = typeof retryDelayOptions.base === 'number' ? retryDelayOptions.base : 100;
    var delay = Math.random() * (Math.pow(2, retryCount) * base);
    return delay;
  },

  /**
   * @api private
   */
  handleRequestWithRetries: function handleRequestWithRetries(httpRequest, options, cb) {
    if (!options) options = {};
    var http = AWS.HttpClient.getInstance();
    var httpOptions = options.httpOptions || {};
    var retryCount = 0;

    var errCallback = function(err) {
      var maxRetries = options.maxRetries || 0;
      if (err && err.code === 'TimeoutError') err.retryable = true;
      var delay = util.calculateRetryDelay(retryCount, options.retryDelayOptions, err);
      if (err && err.retryable && retryCount < maxRetries && delay >= 0) {
        retryCount++;
        setTimeout(sendRequest, delay + (err.retryAfter || 0));
      } else {
        cb(err);
      }
    };

    var sendRequest = function() {
      var data = '';
      http.handleRequest(httpRequest, httpOptions, function(httpResponse) {
        httpResponse.on('data', function(chunk) { data += chunk.toString(); });
        httpResponse.on('end', function() {
          var statusCode = httpResponse.statusCode;
          if (statusCode < 300) {
            cb(null, data);
          } else {
            var retryAfter = parseInt(httpResponse.headers['retry-after'], 10) * 1000 || 0;
            var err = util.error(new Error(),
              {
                statusCode: statusCode,
                retryable: statusCode >= 500 || statusCode === 429
              }
            );
            if (retryAfter && err.retryable) err.retryAfter = retryAfter;
            errCallback(err);
          }
        });
      }, errCallback);
    };

    AWS.util.defer(sendRequest);
  },

  /**
   * @api private
   */
  uuid: {
    v4: function uuidV4() {
      return __webpack_require__(87).v4();
    }
  },

  /**
   * @api private
   */
  convertPayloadToString: function convertPayloadToString(resp) {
    var req = resp.request;
    var operation = req.operation;
    var rules = req.service.api.operations[operation].output || {};
    if (rules.payload && resp.data[rules.payload]) {
      resp.data[rules.payload] = resp.data[rules.payload].toString();
    }
  },

  /**
   * @api private
   */
  defer: function defer(callback) {
    if (typeof process === 'object' && typeof process.nextTick === 'function') {
      process.nextTick(callback);
    } else if (typeof setImmediate === 'function') {
      setImmediate(callback);
    } else {
      setTimeout(callback, 0);
    }
  },

  /**
   * @api private
   */
  getRequestPayloadShape: function getRequestPayloadShape(req) {
    var operations = req.service.api.operations;
    if (!operations) return undefined;
    var operation = (operations || {})[req.operation];
    if (!operation || !operation.input || !operation.input.payload) return undefined;
    return operation.input.members[operation.input.payload];
  },

  getProfilesFromSharedConfig: function getProfilesFromSharedConfig(iniLoader, filename) {
    var profiles = {};
    var profilesFromConfig = {};
    if (Object({"NODE_ENV":undefined})[util.configOptInEnv]) {
      var profilesFromConfig = iniLoader.loadFrom({
        isConfig: true,
        filename: Object({"NODE_ENV":undefined})[util.sharedConfigFileEnv]
      });
    }
    var profilesFromCreds = iniLoader.loadFrom({
      filename: filename ||
        (Object({"NODE_ENV":undefined})[util.configOptInEnv] && Object({"NODE_ENV":undefined})[util.sharedCredentialsFileEnv])
    });
    for (var i = 0, profileNames = Object.keys(profilesFromConfig); i < profileNames.length; i++) {
      profiles[profileNames[i]] = profilesFromConfig[profileNames[i]];
    }
    for (var i = 0, profileNames = Object.keys(profilesFromCreds); i < profileNames.length; i++) {
      profiles[profileNames[i]] = profilesFromCreds[profileNames[i]];
    }
    return profiles;
  },

  /**
   * @api private
   */
  ARN: {
    validate: function validateARN(str) {
      return str && str.indexOf('arn:') === 0 && str.split(':').length >= 6;
    },
    parse: function parseARN(arn) {
      var matched = arn.split(':');
      return {
        partition: matched[1],
        service: matched[2],
        region: matched[3],
        accountId: matched[4],
        resource: matched.slice(5).join(':')
      };
    },
    build: function buildARN(arnObject) {
      if (
        arnObject.service === undefined ||
        arnObject.region === undefined ||
        arnObject.accountId === undefined ||
        arnObject.resource === undefined
      ) throw util.error(new Error('Input ARN object is invalid'));
      return 'arn:'+ (arnObject.partition || 'aws') + ':' + arnObject.service +
        ':' + arnObject.region + ':' + arnObject.accountId + ':' + arnObject.resource;
    }
  },

  /**
   * @api private
   */
  defaultProfile: 'default',

  /**
   * @api private
   */
  configOptInEnv: 'AWS_SDK_LOAD_CONFIG',

  /**
   * @api private
   */
  sharedCredentialsFileEnv: 'AWS_SHARED_CREDENTIALS_FILE',

  /**
   * @api private
   */
  sharedConfigFileEnv: 'AWS_CONFIG_FILE',

  /**
   * @api private
   */
  imdsDisabledEnv: 'AWS_EC2_METADATA_DISABLED'
};

/**
 * @api private
 */
module.exports = util;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(7), __webpack_require__(55).setImmediate))

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (undefined !== 'production') {
  var ReactIs = __webpack_require__(42);

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = __webpack_require__(136)(ReactIs.isElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = __webpack_require__(139)();
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(12);

var AWS = __webpack_require__(1);
if (typeof window !== 'undefined') window.AWS = AWS;
if (true) module.exports = AWS;
if (typeof self !== 'undefined') self.AWS = AWS;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),
/* 7 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 8 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var Collection = __webpack_require__(26);

var util = __webpack_require__(2);

function property(obj, name, value) {
  if (value !== null && value !== undefined) {
    util.property.apply(this, arguments);
  }
}

function memoizedProperty(obj, name) {
  if (!obj.constructor.prototype[name]) {
    util.memoizedProperty.apply(this, arguments);
  }
}

function Shape(shape, options, memberName) {
  options = options || {};

  property(this, 'shape', shape.shape);
  property(this, 'api', options.api, false);
  property(this, 'type', shape.type);
  property(this, 'enum', shape.enum);
  property(this, 'min', shape.min);
  property(this, 'max', shape.max);
  property(this, 'pattern', shape.pattern);
  property(this, 'location', shape.location || this.location || 'body');
  property(this, 'name', this.name || shape.xmlName || shape.queryName ||
    shape.locationName || memberName);
  property(this, 'isStreaming', shape.streaming || this.isStreaming || false);
  property(this, 'requiresLength', shape.requiresLength, false);
  property(this, 'isComposite', shape.isComposite || false);
  property(this, 'isShape', true, false);
  property(this, 'isQueryName', Boolean(shape.queryName), false);
  property(this, 'isLocationName', Boolean(shape.locationName), false);
  property(this, 'isIdempotent', shape.idempotencyToken === true);
  property(this, 'isJsonValue', shape.jsonvalue === true);
  property(this, 'isSensitive', shape.sensitive === true || shape.prototype && shape.prototype.sensitive === true);
  property(this, 'isEventStream', Boolean(shape.eventstream), false);
  property(this, 'isEvent', Boolean(shape.event), false);
  property(this, 'isEventPayload', Boolean(shape.eventpayload), false);
  property(this, 'isEventHeader', Boolean(shape.eventheader), false);
  property(this, 'isTimestampFormatSet', Boolean(shape.timestampFormat) || shape.prototype && shape.prototype.isTimestampFormatSet === true, false);
  property(this, 'endpointDiscoveryId', Boolean(shape.endpointdiscoveryid), false);
  property(this, 'hostLabel', Boolean(shape.hostLabel), false);

  if (options.documentation) {
    property(this, 'documentation', shape.documentation);
    property(this, 'documentationUrl', shape.documentationUrl);
  }

  if (shape.xmlAttribute) {
    property(this, 'isXmlAttribute', shape.xmlAttribute || false);
  }

  // type conversion and parsing
  property(this, 'defaultValue', null);
  this.toWireFormat = function(value) {
    if (value === null || value === undefined) return '';
    return value;
  };
  this.toType = function(value) { return value; };
}

/**
 * @api private
 */
Shape.normalizedTypes = {
  character: 'string',
  double: 'float',
  long: 'integer',
  short: 'integer',
  biginteger: 'integer',
  bigdecimal: 'float',
  blob: 'binary'
};

/**
 * @api private
 */
Shape.types = {
  'structure': StructureShape,
  'list': ListShape,
  'map': MapShape,
  'boolean': BooleanShape,
  'timestamp': TimestampShape,
  'float': FloatShape,
  'integer': IntegerShape,
  'string': StringShape,
  'base64': Base64Shape,
  'binary': BinaryShape
};

Shape.resolve = function resolve(shape, options) {
  if (shape.shape) {
    var refShape = options.api.shapes[shape.shape];
    if (!refShape) {
      throw new Error('Cannot find shape reference: ' + shape.shape);
    }

    return refShape;
  } else {
    return null;
  }
};

Shape.create = function create(shape, options, memberName) {
  if (shape.isShape) return shape;

  var refShape = Shape.resolve(shape, options);
  if (refShape) {
    var filteredKeys = Object.keys(shape);
    if (!options.documentation) {
      filteredKeys = filteredKeys.filter(function(name) {
        return !name.match(/documentation/);
      });
    }

    // create an inline shape with extra members
    var InlineShape = function() {
      refShape.constructor.call(this, shape, options, memberName);
    };
    InlineShape.prototype = refShape;
    return new InlineShape();
  } else {
    // set type if not set
    if (!shape.type) {
      if (shape.members) shape.type = 'structure';
      else if (shape.member) shape.type = 'list';
      else if (shape.key) shape.type = 'map';
      else shape.type = 'string';
    }

    // normalize types
    var origType = shape.type;
    if (Shape.normalizedTypes[shape.type]) {
      shape.type = Shape.normalizedTypes[shape.type];
    }

    if (Shape.types[shape.type]) {
      return new Shape.types[shape.type](shape, options, memberName);
    } else {
      throw new Error('Unrecognized shape type: ' + origType);
    }
  }
};

function CompositeShape(shape) {
  Shape.apply(this, arguments);
  property(this, 'isComposite', true);

  if (shape.flattened) {
    property(this, 'flattened', shape.flattened || false);
  }
}

function StructureShape(shape, options) {
  var self = this;
  var requiredMap = null, firstInit = !this.isShape;

  CompositeShape.apply(this, arguments);

  if (firstInit) {
    property(this, 'defaultValue', function() { return {}; });
    property(this, 'members', {});
    property(this, 'memberNames', []);
    property(this, 'required', []);
    property(this, 'isRequired', function() { return false; });
  }

  if (shape.members) {
    property(this, 'members', new Collection(shape.members, options, function(name, member) {
      return Shape.create(member, options, name);
    }));
    memoizedProperty(this, 'memberNames', function() {
      return shape.xmlOrder || Object.keys(shape.members);
    });

    if (shape.event) {
      memoizedProperty(this, 'eventPayloadMemberName', function() {
        var members = self.members;
        var memberNames = self.memberNames;
        // iterate over members to find ones that are event payloads
        for (var i = 0, iLen = memberNames.length; i < iLen; i++) {
          if (members[memberNames[i]].isEventPayload) {
            return memberNames[i];
          }
        }
      });

      memoizedProperty(this, 'eventHeaderMemberNames', function() {
        var members = self.members;
        var memberNames = self.memberNames;
        var eventHeaderMemberNames = [];
        // iterate over members to find ones that are event headers
        for (var i = 0, iLen = memberNames.length; i < iLen; i++) {
          if (members[memberNames[i]].isEventHeader) {
            eventHeaderMemberNames.push(memberNames[i]);
          }
        }
        return eventHeaderMemberNames;
      });
    }
  }

  if (shape.required) {
    property(this, 'required', shape.required);
    property(this, 'isRequired', function(name) {
      if (!requiredMap) {
        requiredMap = {};
        for (var i = 0; i < shape.required.length; i++) {
          requiredMap[shape.required[i]] = true;
        }
      }

      return requiredMap[name];
    }, false, true);
  }

  property(this, 'resultWrapper', shape.resultWrapper || null);

  if (shape.payload) {
    property(this, 'payload', shape.payload);
  }

  if (typeof shape.xmlNamespace === 'string') {
    property(this, 'xmlNamespaceUri', shape.xmlNamespace);
  } else if (typeof shape.xmlNamespace === 'object') {
    property(this, 'xmlNamespacePrefix', shape.xmlNamespace.prefix);
    property(this, 'xmlNamespaceUri', shape.xmlNamespace.uri);
  }
}

function ListShape(shape, options) {
  var self = this, firstInit = !this.isShape;
  CompositeShape.apply(this, arguments);

  if (firstInit) {
    property(this, 'defaultValue', function() { return []; });
  }

  if (shape.member) {
    memoizedProperty(this, 'member', function() {
      return Shape.create(shape.member, options);
    });
  }

  if (this.flattened) {
    var oldName = this.name;
    memoizedProperty(this, 'name', function() {
      return self.member.name || oldName;
    });
  }
}

function MapShape(shape, options) {
  var firstInit = !this.isShape;
  CompositeShape.apply(this, arguments);

  if (firstInit) {
    property(this, 'defaultValue', function() { return {}; });
    property(this, 'key', Shape.create({type: 'string'}, options));
    property(this, 'value', Shape.create({type: 'string'}, options));
  }

  if (shape.key) {
    memoizedProperty(this, 'key', function() {
      return Shape.create(shape.key, options);
    });
  }
  if (shape.value) {
    memoizedProperty(this, 'value', function() {
      return Shape.create(shape.value, options);
    });
  }
}

function TimestampShape(shape) {
  var self = this;
  Shape.apply(this, arguments);

  if (shape.timestampFormat) {
    property(this, 'timestampFormat', shape.timestampFormat);
  } else if (self.isTimestampFormatSet && this.timestampFormat) {
    property(this, 'timestampFormat', this.timestampFormat);
  } else if (this.location === 'header') {
    property(this, 'timestampFormat', 'rfc822');
  } else if (this.location === 'querystring') {
    property(this, 'timestampFormat', 'iso8601');
  } else if (this.api) {
    switch (this.api.protocol) {
      case 'json':
      case 'rest-json':
        property(this, 'timestampFormat', 'unixTimestamp');
        break;
      case 'rest-xml':
      case 'query':
      case 'ec2':
        property(this, 'timestampFormat', 'iso8601');
        break;
    }
  }

  this.toType = function(value) {
    if (value === null || value === undefined) return null;
    if (typeof value.toUTCString === 'function') return value;
    return typeof value === 'string' || typeof value === 'number' ?
           util.date.parseTimestamp(value) : null;
  };

  this.toWireFormat = function(value) {
    return util.date.format(value, self.timestampFormat);
  };
}

function StringShape() {
  Shape.apply(this, arguments);

  var nullLessProtocols = ['rest-xml', 'query', 'ec2'];
  this.toType = function(value) {
    value = this.api && nullLessProtocols.indexOf(this.api.protocol) > -1 ?
      value || '' : value;
    if (this.isJsonValue) {
      return JSON.parse(value);
    }

    return value && typeof value.toString === 'function' ?
      value.toString() : value;
  };

  this.toWireFormat = function(value) {
    return this.isJsonValue ? JSON.stringify(value) : value;
  };
}

function FloatShape() {
  Shape.apply(this, arguments);

  this.toType = function(value) {
    if (value === null || value === undefined) return null;
    return parseFloat(value);
  };
  this.toWireFormat = this.toType;
}

function IntegerShape() {
  Shape.apply(this, arguments);

  this.toType = function(value) {
    if (value === null || value === undefined) return null;
    return parseInt(value, 10);
  };
  this.toWireFormat = this.toType;
}

function BinaryShape() {
  Shape.apply(this, arguments);
  this.toType = function(value) {
    var buf = util.base64.decode(value);
    if (this.isSensitive && util.isNode() && typeof util.Buffer.alloc === 'function') {
  /* Node.js can create a Buffer that is not isolated.
   * i.e. buf.byteLength !== buf.buffer.byteLength
   * This means that the sensitive data is accessible to anyone with access to buf.buffer.
   * If this is the node shared Buffer, then other code within this process _could_ find this secret.
   * Copy sensitive data to an isolated Buffer and zero the sensitive data.
   * While this is safe to do here, copying this code somewhere else may produce unexpected results.
   */
      var secureBuf = util.Buffer.alloc(buf.length, buf);
      buf.fill(0);
      buf = secureBuf;
    }
    return buf;
  };
  this.toWireFormat = util.base64.encode;
}

function Base64Shape() {
  BinaryShape.apply(this, arguments);
}

function BooleanShape() {
  Shape.apply(this, arguments);

  this.toType = function(value) {
    if (typeof value === 'boolean') return value;
    if (value === null || value === undefined) return null;
    return value === 'true';
  };
}

/**
 * @api private
 */
Shape.shapes = {
  StructureShape: StructureShape,
  ListShape: ListShape,
  MapShape: MapShape,
  StringShape: StringShape,
  BooleanShape: BooleanShape,
  Base64Shape: Base64Shape
};

/**
 * @api private
 */
module.exports = Shape;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <http://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */



var base64 = __webpack_require__(92)
var ieee754 = __webpack_require__(93)
var isArray = __webpack_require__(94)

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

/*
 * Export kMaxLength after typed array support is determined.
 */
exports.kMaxLength = kMaxLength()

function typedArraySupport () {
  try {
    var arr = new Uint8Array(1)
    arr.__proto__ = {__proto__: Uint8Array.prototype, foo: function () { return 42 }}
    return arr.foo() === 42 && // typed array instances can be augmented
        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
        arr.subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

function createBuffer (that, length) {
  if (kMaxLength() < length) {
    throw new RangeError('Invalid typed array length')
  }
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(length)
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    if (that === null) {
      that = new Buffer(length)
    }
    that.length = length
  }

  return that
}

/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */

function Buffer (arg, encodingOrOffset, length) {
  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
    return new Buffer(arg, encodingOrOffset, length)
  }

  // Common case.
  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new Error(
        'If encoding is specified then the first argument must be a string'
      )
    }
    return allocUnsafe(this, arg)
  }
  return from(this, arg, encodingOrOffset, length)
}

Buffer.poolSize = 8192 // not used by this implementation

// TODO: Legacy, not needed anymore. Remove in next major version.
Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype
  return arr
}

function from (that, value, encodingOrOffset, length) {
  if (typeof value === 'number') {
    throw new TypeError('"value" argument must not be a number')
  }

  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
    return fromArrayBuffer(that, value, encodingOrOffset, length)
  }

  if (typeof value === 'string') {
    return fromString(that, value, encodingOrOffset)
  }

  return fromObject(that, value)
}

/**
 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
 * if value is a number.
 * Buffer.from(str[, encoding])
 * Buffer.from(array)
 * Buffer.from(buffer)
 * Buffer.from(arrayBuffer[, byteOffset[, length]])
 **/
Buffer.from = function (value, encodingOrOffset, length) {
  return from(null, value, encodingOrOffset, length)
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
  if (typeof Symbol !== 'undefined' && Symbol.species &&
      Buffer[Symbol.species] === Buffer) {
    // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
    Object.defineProperty(Buffer, Symbol.species, {
      value: null,
      configurable: true
    })
  }
}

function assertSize (size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be a number')
  } else if (size < 0) {
    throw new RangeError('"size" argument must not be negative')
  }
}

function alloc (that, size, fill, encoding) {
  assertSize(size)
  if (size <= 0) {
    return createBuffer(that, size)
  }
  if (fill !== undefined) {
    // Only pay attention to encoding if it's a string. This
    // prevents accidentally sending in a number that would
    // be interpretted as a start offset.
    return typeof encoding === 'string'
      ? createBuffer(that, size).fill(fill, encoding)
      : createBuffer(that, size).fill(fill)
  }
  return createBuffer(that, size)
}

/**
 * Creates a new filled Buffer instance.
 * alloc(size[, fill[, encoding]])
 **/
Buffer.alloc = function (size, fill, encoding) {
  return alloc(null, size, fill, encoding)
}

function allocUnsafe (that, size) {
  assertSize(size)
  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < size; ++i) {
      that[i] = 0
    }
  }
  return that
}

/**
 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
 * */
Buffer.allocUnsafe = function (size) {
  return allocUnsafe(null, size)
}
/**
 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
 */
Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(null, size)
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8'
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('"encoding" must be a valid string encoding')
  }

  var length = byteLength(string, encoding) | 0
  that = createBuffer(that, length)

  var actual = that.write(string, encoding)

  if (actual !== length) {
    // Writing a hex string, for example, that contains invalid characters will
    // cause everything after the first invalid character to be ignored. (e.g.
    // 'abxxcd' will be treated as 'ab')
    that = that.slice(0, actual)
  }

  return that
}

function fromArrayLike (that, array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0
  that = createBuffer(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array, byteOffset, length) {
  array.byteLength // this throws if `array` is not a valid ArrayBuffer

  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('\'offset\' is out of bounds')
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('\'length\' is out of bounds')
  }

  if (byteOffset === undefined && length === undefined) {
    array = new Uint8Array(array)
  } else if (length === undefined) {
    array = new Uint8Array(array, byteOffset)
  } else {
    array = new Uint8Array(array, byteOffset, length)
  }

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = array
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that = fromArrayLike(that, array)
  }
  return that
}

function fromObject (that, obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0
    that = createBuffer(that, len)

    if (that.length === 0) {
      return that
    }

    obj.copy(that, 0, 0, len)
    return that
  }

  if (obj) {
    if ((typeof ArrayBuffer !== 'undefined' &&
        obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
      if (typeof obj.length !== 'number' || isnan(obj.length)) {
        return createBuffer(that, 0)
      }
      return fromArrayLike(that, obj)
    }

    if (obj.type === 'Buffer' && isArray(obj.data)) {
      return fromArrayLike(that, obj.data)
    }
  }

  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
}

function checked (length) {
  // Note: cannot use `length < kMaxLength()` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (length) {
  if (+length != length) { // eslint-disable-line eqeqeq
    length = 0
  }
  return Buffer.alloc(+length)
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i]
      y = b[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers')
  }

  if (list.length === 0) {
    return Buffer.alloc(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; ++i) {
      length += list[i].length
    }
  }

  var buffer = Buffer.allocUnsafe(length)
  var pos = 0
  for (i = 0; i < list.length; ++i) {
    var buf = list[i]
    if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers')
    }
    buf.copy(buffer, pos)
    pos += buf.length
  }
  return buffer
}

function byteLength (string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length
  }
  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
      (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
    return string.byteLength
  }
  if (typeof string !== 'string') {
    string = '' + string
  }

  var len = string.length
  if (len === 0) return 0

  // Use a for loop to avoid recursion
  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len
      case 'utf8':
      case 'utf-8':
      case undefined:
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length // assume utf8
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false

  // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
  // property of a typed array.

  // This behaves neither like String nor Uint8Array in that we set start/end
  // to their upper/lower bounds if the value passed is out of range.
  // undefined is handled specially as per ECMA-262 6th Edition,
  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
  if (start === undefined || start < 0) {
    start = 0
  }
  // Return early if start > this.length. Done here to prevent potential uint32
  // coercion fail below.
  if (start > this.length) {
    return ''
  }

  if (end === undefined || end > this.length) {
    end = this.length
  }

  if (end <= 0) {
    return ''
  }

  // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
  end >>>= 0
  start >>>= 0

  if (end <= start) {
    return ''
  }

  if (!encoding) encoding = 'utf8'

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
// Buffer instances.
Buffer.prototype._isBuffer = true

function swap (b, n, m) {
  var i = b[n]
  b[n] = b[m]
  b[m] = i
}

Buffer.prototype.swap16 = function swap16 () {
  var len = this.length
  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits')
  }
  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1)
  }
  return this
}

Buffer.prototype.swap32 = function swap32 () {
  var len = this.length
  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits')
  }
  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3)
    swap(this, i + 1, i + 2)
  }
  return this
}

Buffer.prototype.swap64 = function swap64 () {
  var len = this.length
  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits')
  }
  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7)
    swap(this, i + 1, i + 6)
    swap(this, i + 2, i + 5)
    swap(this, i + 3, i + 4)
  }
  return this
}

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
  if (!Buffer.isBuffer(target)) {
    throw new TypeError('Argument must be a Buffer')
  }

  if (start === undefined) {
    start = 0
  }
  if (end === undefined) {
    end = target ? target.length : 0
  }
  if (thisStart === undefined) {
    thisStart = 0
  }
  if (thisEnd === undefined) {
    thisEnd = this.length
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index')
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0
  }
  if (thisStart >= thisEnd) {
    return -1
  }
  if (start >= end) {
    return 1
  }

  start >>>= 0
  end >>>= 0
  thisStart >>>= 0
  thisEnd >>>= 0

  if (this === target) return 0

  var x = thisEnd - thisStart
  var y = end - start
  var len = Math.min(x, y)

  var thisCopy = this.slice(thisStart, thisEnd)
  var targetCopy = target.slice(start, end)

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i]
      y = targetCopy[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

// Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
//
// Arguments:
// - buffer - a Buffer to search
// - val - a string, Buffer, or number
// - byteOffset - an index into `buffer`; will be clamped to an int32
// - encoding - an optional encoding, relevant is val is a string
// - dir - true for indexOf, false for lastIndexOf
function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
  // Empty buffer means no match
  if (buffer.length === 0) return -1

  // Normalize byteOffset
  if (typeof byteOffset === 'string') {
    encoding = byteOffset
    byteOffset = 0
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000
  }
  byteOffset = +byteOffset  // Coerce to Number.
  if (isNaN(byteOffset)) {
    // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
    byteOffset = dir ? 0 : (buffer.length - 1)
  }

  // Normalize byteOffset: negative offsets start from the end of the buffer
  if (byteOffset < 0) byteOffset = buffer.length + byteOffset
  if (byteOffset >= buffer.length) {
    if (dir) return -1
    else byteOffset = buffer.length - 1
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0
    else return -1
  }

  // Normalize val
  if (typeof val === 'string') {
    val = Buffer.from(val, encoding)
  }

  // Finally, search either indexOf (if dir is true) or lastIndexOf
  if (Buffer.isBuffer(val)) {
    // Special case: looking for empty string/buffer always fails
    if (val.length === 0) {
      return -1
    }
    return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
  } else if (typeof val === 'number') {
    val = val & 0xFF // Search for a byte value [0-255]
    if (Buffer.TYPED_ARRAY_SUPPORT &&
        typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
      }
    }
    return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
  }

  throw new TypeError('val must be string, number or Buffer')
}

function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
  var indexSize = 1
  var arrLength = arr.length
  var valLength = val.length

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase()
    if (encoding === 'ucs2' || encoding === 'ucs-2' ||
        encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1
      }
      indexSize = 2
      arrLength /= 2
      valLength /= 2
      byteOffset /= 2
    }
  }

  function read (buf, i) {
    if (indexSize === 1) {
      return buf[i]
    } else {
      return buf.readUInt16BE(i * indexSize)
    }
  }

  var i
  if (dir) {
    var foundIndex = -1
    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
      } else {
        if (foundIndex !== -1) i -= i - foundIndex
        foundIndex = -1
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength
    for (i = byteOffset; i >= 0; i--) {
      var found = true
      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false
          break
        }
      }
      if (found) return i
    }
  }

  return -1
}

Buffer.prototype.includes = function includes (val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
}

Buffer.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  // must be an even number of digits
  var strLen = string.length
  if (strLen % 2 !== 0) throw new TypeError('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) return i
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function latin1Write (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  // legacy write(string, encoding, offset, length) - remove in v0.13
  } else {
    throw new Error(
      'Buffer.write(string, encoding, offset[, length]) is no longer supported'
    )
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'latin1':
      case 'binary':
        return latin1Write(this, string, offset, length)

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
  }

  // Decode in chunks to avoid "call stack size exceeded".
  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function latin1Slice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; ++i) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end)
    newBuf.__proto__ = Buffer.prototype
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; ++i) {
      newBuf[i] = this[i + start]
    }
  }

  return newBuf
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
  if (offset < 0) throw new RangeError('Index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  // Copy 0 bytes; we're done
  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  // Fatal error conditions
  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (i = len - 1; i >= 0; --i) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    // ascending copy from start
    for (i = 0; i < len; ++i) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, start + len),
      targetStart
    )
  }

  return len
}

// Usage:
//    buffer.fill(number[, offset[, end]])
//    buffer.fill(buffer[, offset[, end]])
//    buffer.fill(string[, offset[, end]][, encoding])
Buffer.prototype.fill = function fill (val, start, end, encoding) {
  // Handle string cases:
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start
      start = 0
      end = this.length
    } else if (typeof end === 'string') {
      encoding = end
      end = this.length
    }
    if (val.length === 1) {
      var code = val.charCodeAt(0)
      if (code < 256) {
        val = code
      }
    }
    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string')
    }
    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding)
    }
  } else if (typeof val === 'number') {
    val = val & 255
  }

  // Invalid ranges are not set to a default, so can range check early.
  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index')
  }

  if (end <= start) {
    return this
  }

  start = start >>> 0
  end = end === undefined ? this.length : end >>> 0

  if (!val) val = 0

  var i
  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val
    }
  } else {
    var bytes = Buffer.isBuffer(val)
      ? val
      : utf8ToBytes(new Buffer(val, encoding).toString())
    var len = bytes.length
    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len]
    }
  }

  return this
}

// HELPER FUNCTIONS
// ================

var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  // Node converts strings with length < 2 to ''
  if (str.length < 2) return ''
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i)

    // is surrogate component
    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        // valid lead
        leadSurrogate = codePoint

        continue
      }

      // 2 leads in a row
      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      // valid surrogate pair
      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    // encode utf8
    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; ++i) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

function isnan (val) {
  return val !== val // eslint-disable-line no-self-compare
}

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(8)))

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(12);
var AWS = __webpack_require__(1);
var Service = AWS.Service;
var apiLoader = AWS.apiLoader;

apiLoader.services['sts'] = {};
AWS.STS = Service.defineService('sts', ['2011-06-15']);
__webpack_require__(112);
Object.defineProperty(apiLoader.services['sts'], '2011-06-15', {
  get: function get() {
    var model = __webpack_require__(114);
    model.paginators = __webpack_require__(115).pagination;
    return model;
  },
  enumerable: true,
  configurable: true
});

module.exports = AWS.STS;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);

// browser specific modules
util.crypto.lib = __webpack_require__(90);
util.Buffer = __webpack_require__(10).Buffer;
util.url = __webpack_require__(98);
util.querystring = __webpack_require__(41);
util.realClock = __webpack_require__(104);
util.environment = 'js';
util.createEventStream = __webpack_require__(105).createEventStream;
util.isBrowser = function() { return true; };
util.isNode = function() { return false; };

var AWS = __webpack_require__(1);

/**
 * @api private
 */
module.exports = AWS;

__webpack_require__(35);
__webpack_require__(36);
__webpack_require__(111);
__webpack_require__(116);
__webpack_require__(117);
__webpack_require__(118);
__webpack_require__(123);

// Load the DOMParser XML parser
AWS.XML.Parser = __webpack_require__(124);

// Load the XHR HttpClient
__webpack_require__(125);

if (typeof process === 'undefined') {
  var process = {
    browser: true
  };
}


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var populateHostPrefix = __webpack_require__(19).populateHostPrefix;

function populateMethod(req) {
  req.httpRequest.method = req.service.api.operations[req.operation].httpMethod;
}

function generateURI(endpointPath, operationPath, input, params) {
  var uri = [endpointPath, operationPath].join('/');
  uri = uri.replace(/\/+/g, '/');

  var queryString = {}, queryStringSet = false;
  util.each(input.members, function (name, member) {
    var paramValue = params[name];
    if (paramValue === null || paramValue === undefined) return;
    if (member.location === 'uri') {
      var regex = new RegExp('\\{' + member.name + '(\\+)?\\}');
      uri = uri.replace(regex, function(_, plus) {
        var fn = plus ? util.uriEscapePath : util.uriEscape;
        return fn(String(paramValue));
      });
    } else if (member.location === 'querystring') {
      queryStringSet = true;

      if (member.type === 'list') {
        queryString[member.name] = paramValue.map(function(val) {
          return util.uriEscape(member.member.toWireFormat(val).toString());
        });
      } else if (member.type === 'map') {
        util.each(paramValue, function(key, value) {
          if (Array.isArray(value)) {
            queryString[key] = value.map(function(val) {
              return util.uriEscape(String(val));
            });
          } else {
            queryString[key] = util.uriEscape(String(value));
          }
        });
      } else {
        queryString[member.name] = util.uriEscape(member.toWireFormat(paramValue).toString());
      }
    }
  });

  if (queryStringSet) {
    uri += (uri.indexOf('?') >= 0 ? '&' : '?');
    var parts = [];
    util.arrayEach(Object.keys(queryString).sort(), function(key) {
      if (!Array.isArray(queryString[key])) {
        queryString[key] = [queryString[key]];
      }
      for (var i = 0; i < queryString[key].length; i++) {
        parts.push(util.uriEscape(String(key)) + '=' + queryString[key][i]);
      }
    });
    uri += parts.join('&');
  }

  return uri;
}

function populateURI(req) {
  var operation = req.service.api.operations[req.operation];
  var input = operation.input;

  var uri = generateURI(req.httpRequest.endpoint.path, operation.httpPath, input, req.params);
  req.httpRequest.path = uri;
}

function populateHeaders(req) {
  var operation = req.service.api.operations[req.operation];
  util.each(operation.input.members, function (name, member) {
    var value = req.params[name];
    if (value === null || value === undefined) return;

    if (member.location === 'headers' && member.type === 'map') {
      util.each(value, function(key, memberValue) {
        req.httpRequest.headers[member.name + key] = memberValue;
      });
    } else if (member.location === 'header') {
      value = member.toWireFormat(value).toString();
      if (member.isJsonValue) {
        value = util.base64.encode(value);
      }
      req.httpRequest.headers[member.name] = value;
    }
  });
}

function buildRequest(req) {
  populateMethod(req);
  populateURI(req);
  populateHeaders(req);
  populateHostPrefix(req);
}

function extractError() {
}

function extractData(resp) {
  var req = resp.request;
  var data = {};
  var r = resp.httpResponse;
  var operation = req.service.api.operations[req.operation];
  var output = operation.output;

  // normalize headers names to lower-cased keys for matching
  var headers = {};
  util.each(r.headers, function (k, v) {
    headers[k.toLowerCase()] = v;
  });

  util.each(output.members, function(name, member) {
    var header = (member.name || name).toLowerCase();
    if (member.location === 'headers' && member.type === 'map') {
      data[name] = {};
      var location = member.isLocationName ? member.name : '';
      var pattern = new RegExp('^' + location + '(.+)', 'i');
      util.each(r.headers, function (k, v) {
        var result = k.match(pattern);
        if (result !== null) {
          data[name][result[1]] = v;
        }
      });
    } else if (member.location === 'header') {
      if (headers[header] !== undefined) {
        var value = member.isJsonValue ?
          util.base64.decode(headers[header]) :
          headers[header];
        data[name] = member.toType(value);
      }
    } else if (member.location === 'statusCode') {
      data[name] = parseInt(r.statusCode, 10);
    }
  });

  resp.data = data;
}

/**
 * @api private
 */
module.exports = {
  buildRequest: buildRequest,
  extractError: extractError,
  extractData: extractData,
  generateURI: generateURI
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var Buffer = __webpack_require__(10).Buffer;

/**
 * This is a polyfill for the static method `isView` of `ArrayBuffer`, which is
 * e.g. missing in IE 10.
 *
 * @api private
 */
if (
    typeof ArrayBuffer !== 'undefined' &&
    typeof ArrayBuffer.isView === 'undefined'
) {
    ArrayBuffer.isView = function(arg) {
        return viewStrings.indexOf(Object.prototype.toString.call(arg)) > -1;
    };
}

/**
 * @api private
 */
var viewStrings = [
    '[object Int8Array]',
    '[object Uint8Array]',
    '[object Uint8ClampedArray]',
    '[object Int16Array]',
    '[object Uint16Array]',
    '[object Int32Array]',
    '[object Uint32Array]',
    '[object Float32Array]',
    '[object Float64Array]',
    '[object DataView]',
];

/**
 * @api private
 */
function isEmptyData(data) {
    if (typeof data === 'string') {
        return data.length === 0;
    }
    return data.byteLength === 0;
}

/**
 * @api private
 */
function convertToBuffer(data) {
    if (typeof data === 'string') {
        data = new Buffer(data, 'utf8');
    }

    if (ArrayBuffer.isView(data)) {
        return new Uint8Array(data.buffer, data.byteOffset, data.byteLength / Uint8Array.BYTES_PER_ELEMENT);
    }

    return new Uint8Array(data);
}

/**
 * @api private
 */
module.exports = exports = {
    isEmptyData: isEmptyData,
    convertToBuffer: convertToBuffer,
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (url, options) {
  if (!options) {
    // eslint-disable-next-line no-param-reassign
    options = {};
  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign


  url = url && url.__esModule ? url.default : url;

  if (typeof url !== 'string') {
    return url;
  } // If url is already wrapped in quotes, remove them


  if (/^['"].*['"]$/.test(url)) {
    // eslint-disable-next-line no-param-reassign
    url = url.slice(1, -1);
  }

  if (options.hash) {
    // eslint-disable-next-line no-param-reassign
    url += options.hash;
  } // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls


  if (/["'() \t\n]/.test(url) || options.needQuotes) {
    return "\"".concat(url.replace(/"/g, '\\"').replace(/\n/g, '\\n'), "\"");
  }

  return url;
};

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var JsonBuilder = __webpack_require__(17);
var JsonParser = __webpack_require__(18);
var populateHostPrefix = __webpack_require__(19).populateHostPrefix;

function buildRequest(req) {
  var httpRequest = req.httpRequest;
  var api = req.service.api;
  var target = api.targetPrefix + '.' + api.operations[req.operation].name;
  var version = api.jsonVersion || '1.0';
  var input = api.operations[req.operation].input;
  var builder = new JsonBuilder();

  if (version === 1) version = '1.0';
  httpRequest.body = builder.build(req.params || {}, input);
  httpRequest.headers['Content-Type'] = 'application/x-amz-json-' + version;
  httpRequest.headers['X-Amz-Target'] = target;

  populateHostPrefix(req);
}

function extractError(resp) {
  var error = {};
  var httpResponse = resp.httpResponse;

  error.code = httpResponse.headers['x-amzn-errortype'] || 'UnknownError';
  if (typeof error.code === 'string') {
    error.code = error.code.split(':')[0];
  }

  if (httpResponse.body.length > 0) {
    try {
      var e = JSON.parse(httpResponse.body.toString());
      if (e.__type || e.code) {
        error.code = (e.__type || e.code).split('#').pop();
      }
      if (error.code === 'RequestEntityTooLarge') {
        error.message = 'Request body must be less than 1 MB';
      } else {
        error.message = (e.message || e.Message || null);
      }
    } catch (e) {
      error.statusCode = httpResponse.statusCode;
      error.message = httpResponse.statusMessage;
    }
  } else {
    error.statusCode = httpResponse.statusCode;
    error.message = httpResponse.statusCode.toString();
  }

  resp.error = util.error(new Error(), error);
}

function extractData(resp) {
  var body = resp.httpResponse.body.toString() || '{}';
  if (resp.request.service.config.convertResponseTypes === false) {
    resp.data = JSON.parse(body);
  } else {
    var operation = resp.request.service.api.operations[resp.request.operation];
    var shape = operation.output || {};
    var parser = new JsonParser();
    resp.data = parser.parse(body, shape);
  }
}

/**
 * @api private
 */
module.exports = {
  buildRequest: buildRequest,
  extractError: extractError,
  extractData: extractData
};


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);

function JsonBuilder() { }

JsonBuilder.prototype.build = function(value, shape) {
  return JSON.stringify(translate(value, shape));
};

function translate(value, shape) {
  if (!shape || value === undefined || value === null) return undefined;

  switch (shape.type) {
    case 'structure': return translateStructure(value, shape);
    case 'map': return translateMap(value, shape);
    case 'list': return translateList(value, shape);
    default: return translateScalar(value, shape);
  }
}

function translateStructure(structure, shape) {
  var struct = {};
  util.each(structure, function(name, value) {
    var memberShape = shape.members[name];
    if (memberShape) {
      if (memberShape.location !== 'body') return;
      var locationName = memberShape.isLocationName ? memberShape.name : name;
      var result = translate(value, memberShape);
      if (result !== undefined) struct[locationName] = result;
    }
  });
  return struct;
}

function translateList(list, shape) {
  var out = [];
  util.arrayEach(list, function(value) {
    var result = translate(value, shape.member);
    if (result !== undefined) out.push(result);
  });
  return out;
}

function translateMap(map, shape) {
  var out = {};
  util.each(map, function(key, value) {
    var result = translate(value, shape.value);
    if (result !== undefined) out[key] = result;
  });
  return out;
}

function translateScalar(value, shape) {
  return shape.toWireFormat(value);
}

/**
 * @api private
 */
module.exports = JsonBuilder;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);

function JsonParser() { }

JsonParser.prototype.parse = function(value, shape) {
  return translate(JSON.parse(value), shape);
};

function translate(value, shape) {
  if (!shape || value === undefined) return undefined;

  switch (shape.type) {
    case 'structure': return translateStructure(value, shape);
    case 'map': return translateMap(value, shape);
    case 'list': return translateList(value, shape);
    default: return translateScalar(value, shape);
  }
}

function translateStructure(structure, shape) {
  if (structure == null) return undefined;

  var struct = {};
  var shapeMembers = shape.members;
  util.each(shapeMembers, function(name, memberShape) {
    var locationName = memberShape.isLocationName ? memberShape.name : name;
    if (Object.prototype.hasOwnProperty.call(structure, locationName)) {
      var value = structure[locationName];
      var result = translate(value, memberShape);
      if (result !== undefined) struct[name] = result;
    }
  });
  return struct;
}

function translateList(list, shape) {
  if (list == null) return undefined;

  var out = [];
  util.arrayEach(list, function(value) {
    var result = translate(value, shape.member);
    if (result === undefined) out.push(null);
    else out.push(result);
  });
  return out;
}

function translateMap(map, shape) {
  if (map == null) return undefined;

  var out = {};
  util.each(map, function(key, value) {
    var result = translate(value, shape.value);
    if (result === undefined) out[key] = null;
    else out[key] = result;
  });
  return out;
}

function translateScalar(value, shape) {
  return shape.toType(value);
}

/**
 * @api private
 */
module.exports = JsonParser;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

var util =  __webpack_require__(2);
var AWS = __webpack_require__(1);

/**
 * Prepend prefix defined by API model to endpoint that's already
 * constructed. This feature does not apply to operations using
 * endpoint discovery and can be disabled.
 * @api private
 */
function populateHostPrefix(request)  {
  var enabled = request.service.config.hostPrefixEnabled;
  if (!enabled) return request;
  var operationModel = request.service.api.operations[request.operation];
  //don't marshal host prefix when operation has endpoint discovery traits
  if (hasEndpointDiscover(request)) return request;
  if (operationModel.endpoint && operationModel.endpoint.hostPrefix) {
    var hostPrefixNotation = operationModel.endpoint.hostPrefix;
    var hostPrefix = expandHostPrefix(hostPrefixNotation, request.params, operationModel.input);
    prependEndpointPrefix(request.httpRequest.endpoint, hostPrefix);
    validateHostname(request.httpRequest.endpoint.hostname);
  }
  return request;
}

/**
 * @api private
 */
function hasEndpointDiscover(request) {
  var api = request.service.api;
  var operationModel = api.operations[request.operation];
  var isEndpointOperation = api.endpointOperation && (api.endpointOperation === util.string.lowerFirst(operationModel.name));
  return (operationModel.endpointDiscoveryRequired !== 'NULL' || isEndpointOperation === true);
}

/**
 * @api private
 */
function expandHostPrefix(hostPrefixNotation, params, shape) {
  util.each(shape.members, function(name, member) {
    if (member.hostLabel === true) {
      if (typeof params[name] !== 'string' || params[name] === '') {
        throw util.error(new Error(), {
          message: 'Parameter ' + name + ' should be a non-empty string.',
          code: 'InvalidParameter'
        });
      }
      var regex = new RegExp('\\{' + name + '\\}', 'g');
      hostPrefixNotation = hostPrefixNotation.replace(regex, params[name]);
    }
  });
  return hostPrefixNotation;
}

/**
 * @api private
 */
function prependEndpointPrefix(endpoint, prefix) {
  if (endpoint.host) {
    endpoint.host = prefix + endpoint.host;
  }
  if (endpoint.hostname) {
    endpoint.hostname = prefix + endpoint.hostname;
  }
}

/**
 * @api private
 */
function validateHostname(hostname) {
  var labels = hostname.split('.');
  //Reference: https://tools.ietf.org/html/rfc1123#section-2
  var hostPattern = /^[a-zA-Z0-9]{1}$|^[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]$/;
  util.arrayEach(labels, function(label) {
    if (!label.length || label.length < 1 || label.length > 63) {
      throw util.error(new Error(), {
        code: 'ValidationError',
        message: 'Hostname label length should be between 1 to 63 characters, inclusive.'
      });
    }
    if (!hostPattern.test(label)) {
      throw AWS.util.error(new Error(),
        {code: 'ValidationError', message: label + ' is not hostname compatible.'});
    }
  });
}

module.exports = {
  populateHostPrefix: populateHostPrefix
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

(function(exports) {
  "use strict";

  function isArray(obj) {
    if (obj !== null) {
      return Object.prototype.toString.call(obj) === "[object Array]";
    } else {
      return false;
    }
  }

  function isObject(obj) {
    if (obj !== null) {
      return Object.prototype.toString.call(obj) === "[object Object]";
    } else {
      return false;
    }
  }

  function strictDeepEqual(first, second) {
    // Check the scalar case first.
    if (first === second) {
      return true;
    }

    // Check if they are the same type.
    var firstType = Object.prototype.toString.call(first);
    if (firstType !== Object.prototype.toString.call(second)) {
      return false;
    }
    // We know that first and second have the same type so we can just check the
    // first type from now on.
    if (isArray(first) === true) {
      // Short circuit if they're not the same length;
      if (first.length !== second.length) {
        return false;
      }
      for (var i = 0; i < first.length; i++) {
        if (strictDeepEqual(first[i], second[i]) === false) {
          return false;
        }
      }
      return true;
    }
    if (isObject(first) === true) {
      // An object is equal if it has the same key/value pairs.
      var keysSeen = {};
      for (var key in first) {
        if (hasOwnProperty.call(first, key)) {
          if (strictDeepEqual(first[key], second[key]) === false) {
            return false;
          }
          keysSeen[key] = true;
        }
      }
      // Now check that there aren't any keys in second that weren't
      // in first.
      for (var key2 in second) {
        if (hasOwnProperty.call(second, key2)) {
          if (keysSeen[key2] !== true) {
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }

  function isFalse(obj) {
    // From the spec:
    // A false value corresponds to the following values:
    // Empty list
    // Empty object
    // Empty string
    // False boolean
    // null value

    // First check the scalar values.
    if (obj === "" || obj === false || obj === null) {
        return true;
    } else if (isArray(obj) && obj.length === 0) {
        // Check for an empty array.
        return true;
    } else if (isObject(obj)) {
        // Check for an empty object.
        for (var key in obj) {
            // If there are any keys, then
            // the object is not empty so the object
            // is not false.
            if (obj.hasOwnProperty(key)) {
              return false;
            }
        }
        return true;
    } else {
        return false;
    }
  }

  function objValues(obj) {
    var keys = Object.keys(obj);
    var values = [];
    for (var i = 0; i < keys.length; i++) {
      values.push(obj[keys[i]]);
    }
    return values;
  }

  function merge(a, b) {
      var merged = {};
      for (var key in a) {
          merged[key] = a[key];
      }
      for (var key2 in b) {
          merged[key2] = b[key2];
      }
      return merged;
  }

  var trimLeft;
  if (typeof String.prototype.trimLeft === "function") {
    trimLeft = function(str) {
      return str.trimLeft();
    };
  } else {
    trimLeft = function(str) {
      return str.match(/^\s*(.*)/)[1];
    };
  }

  // Type constants used to define functions.
  var TYPE_NUMBER = 0;
  var TYPE_ANY = 1;
  var TYPE_STRING = 2;
  var TYPE_ARRAY = 3;
  var TYPE_OBJECT = 4;
  var TYPE_BOOLEAN = 5;
  var TYPE_EXPREF = 6;
  var TYPE_NULL = 7;
  var TYPE_ARRAY_NUMBER = 8;
  var TYPE_ARRAY_STRING = 9;

  var TOK_EOF = "EOF";
  var TOK_UNQUOTEDIDENTIFIER = "UnquotedIdentifier";
  var TOK_QUOTEDIDENTIFIER = "QuotedIdentifier";
  var TOK_RBRACKET = "Rbracket";
  var TOK_RPAREN = "Rparen";
  var TOK_COMMA = "Comma";
  var TOK_COLON = "Colon";
  var TOK_RBRACE = "Rbrace";
  var TOK_NUMBER = "Number";
  var TOK_CURRENT = "Current";
  var TOK_EXPREF = "Expref";
  var TOK_PIPE = "Pipe";
  var TOK_OR = "Or";
  var TOK_AND = "And";
  var TOK_EQ = "EQ";
  var TOK_GT = "GT";
  var TOK_LT = "LT";
  var TOK_GTE = "GTE";
  var TOK_LTE = "LTE";
  var TOK_NE = "NE";
  var TOK_FLATTEN = "Flatten";
  var TOK_STAR = "Star";
  var TOK_FILTER = "Filter";
  var TOK_DOT = "Dot";
  var TOK_NOT = "Not";
  var TOK_LBRACE = "Lbrace";
  var TOK_LBRACKET = "Lbracket";
  var TOK_LPAREN= "Lparen";
  var TOK_LITERAL= "Literal";

  // The "&", "[", "<", ">" tokens
  // are not in basicToken because
  // there are two token variants
  // ("&&", "[?", "<=", ">=").  This is specially handled
  // below.

  var basicTokens = {
    ".": TOK_DOT,
    "*": TOK_STAR,
    ",": TOK_COMMA,
    ":": TOK_COLON,
    "{": TOK_LBRACE,
    "}": TOK_RBRACE,
    "]": TOK_RBRACKET,
    "(": TOK_LPAREN,
    ")": TOK_RPAREN,
    "@": TOK_CURRENT
  };

  var operatorStartToken = {
      "<": true,
      ">": true,
      "=": true,
      "!": true
  };

  var skipChars = {
      " ": true,
      "\t": true,
      "\n": true
  };


  function isAlpha(ch) {
      return (ch >= "a" && ch <= "z") ||
             (ch >= "A" && ch <= "Z") ||
             ch === "_";
  }

  function isNum(ch) {
      return (ch >= "0" && ch <= "9") ||
             ch === "-";
  }
  function isAlphaNum(ch) {
      return (ch >= "a" && ch <= "z") ||
             (ch >= "A" && ch <= "Z") ||
             (ch >= "0" && ch <= "9") ||
             ch === "_";
  }

  function Lexer() {
  }
  Lexer.prototype = {
      tokenize: function(stream) {
          var tokens = [];
          this._current = 0;
          var start;
          var identifier;
          var token;
          while (this._current < stream.length) {
              if (isAlpha(stream[this._current])) {
                  start = this._current;
                  identifier = this._consumeUnquotedIdentifier(stream);
                  tokens.push({type: TOK_UNQUOTEDIDENTIFIER,
                               value: identifier,
                               start: start});
              } else if (basicTokens[stream[this._current]] !== undefined) {
                  tokens.push({type: basicTokens[stream[this._current]],
                              value: stream[this._current],
                              start: this._current});
                  this._current++;
              } else if (isNum(stream[this._current])) {
                  token = this._consumeNumber(stream);
                  tokens.push(token);
              } else if (stream[this._current] === "[") {
                  // No need to increment this._current.  This happens
                  // in _consumeLBracket
                  token = this._consumeLBracket(stream);
                  tokens.push(token);
              } else if (stream[this._current] === "\"") {
                  start = this._current;
                  identifier = this._consumeQuotedIdentifier(stream);
                  tokens.push({type: TOK_QUOTEDIDENTIFIER,
                               value: identifier,
                               start: start});
              } else if (stream[this._current] === "'") {
                  start = this._current;
                  identifier = this._consumeRawStringLiteral(stream);
                  tokens.push({type: TOK_LITERAL,
                               value: identifier,
                               start: start});
              } else if (stream[this._current] === "`") {
                  start = this._current;
                  var literal = this._consumeLiteral(stream);
                  tokens.push({type: TOK_LITERAL,
                               value: literal,
                               start: start});
              } else if (operatorStartToken[stream[this._current]] !== undefined) {
                  tokens.push(this._consumeOperator(stream));
              } else if (skipChars[stream[this._current]] !== undefined) {
                  // Ignore whitespace.
                  this._current++;
              } else if (stream[this._current] === "&") {
                  start = this._current;
                  this._current++;
                  if (stream[this._current] === "&") {
                      this._current++;
                      tokens.push({type: TOK_AND, value: "&&", start: start});
                  } else {
                      tokens.push({type: TOK_EXPREF, value: "&", start: start});
                  }
              } else if (stream[this._current] === "|") {
                  start = this._current;
                  this._current++;
                  if (stream[this._current] === "|") {
                      this._current++;
                      tokens.push({type: TOK_OR, value: "||", start: start});
                  } else {
                      tokens.push({type: TOK_PIPE, value: "|", start: start});
                  }
              } else {
                  var error = new Error("Unknown character:" + stream[this._current]);
                  error.name = "LexerError";
                  throw error;
              }
          }
          return tokens;
      },

      _consumeUnquotedIdentifier: function(stream) {
          var start = this._current;
          this._current++;
          while (this._current < stream.length && isAlphaNum(stream[this._current])) {
              this._current++;
          }
          return stream.slice(start, this._current);
      },

      _consumeQuotedIdentifier: function(stream) {
          var start = this._current;
          this._current++;
          var maxLength = stream.length;
          while (stream[this._current] !== "\"" && this._current < maxLength) {
              // You can escape a double quote and you can escape an escape.
              var current = this._current;
              if (stream[current] === "\\" && (stream[current + 1] === "\\" ||
                                               stream[current + 1] === "\"")) {
                  current += 2;
              } else {
                  current++;
              }
              this._current = current;
          }
          this._current++;
          return JSON.parse(stream.slice(start, this._current));
      },

      _consumeRawStringLiteral: function(stream) {
          var start = this._current;
          this._current++;
          var maxLength = stream.length;
          while (stream[this._current] !== "'" && this._current < maxLength) {
              // You can escape a single quote and you can escape an escape.
              var current = this._current;
              if (stream[current] === "\\" && (stream[current + 1] === "\\" ||
                                               stream[current + 1] === "'")) {
                  current += 2;
              } else {
                  current++;
              }
              this._current = current;
          }
          this._current++;
          var literal = stream.slice(start + 1, this._current - 1);
          return literal.replace("\\'", "'");
      },

      _consumeNumber: function(stream) {
          var start = this._current;
          this._current++;
          var maxLength = stream.length;
          while (isNum(stream[this._current]) && this._current < maxLength) {
              this._current++;
          }
          var value = parseInt(stream.slice(start, this._current));
          return {type: TOK_NUMBER, value: value, start: start};
      },

      _consumeLBracket: function(stream) {
          var start = this._current;
          this._current++;
          if (stream[this._current] === "?") {
              this._current++;
              return {type: TOK_FILTER, value: "[?", start: start};
          } else if (stream[this._current] === "]") {
              this._current++;
              return {type: TOK_FLATTEN, value: "[]", start: start};
          } else {
              return {type: TOK_LBRACKET, value: "[", start: start};
          }
      },

      _consumeOperator: function(stream) {
          var start = this._current;
          var startingChar = stream[start];
          this._current++;
          if (startingChar === "!") {
              if (stream[this._current] === "=") {
                  this._current++;
                  return {type: TOK_NE, value: "!=", start: start};
              } else {
                return {type: TOK_NOT, value: "!", start: start};
              }
          } else if (startingChar === "<") {
              if (stream[this._current] === "=") {
                  this._current++;
                  return {type: TOK_LTE, value: "<=", start: start};
              } else {
                  return {type: TOK_LT, value: "<", start: start};
              }
          } else if (startingChar === ">") {
              if (stream[this._current] === "=") {
                  this._current++;
                  return {type: TOK_GTE, value: ">=", start: start};
              } else {
                  return {type: TOK_GT, value: ">", start: start};
              }
          } else if (startingChar === "=") {
              if (stream[this._current] === "=") {
                  this._current++;
                  return {type: TOK_EQ, value: "==", start: start};
              }
          }
      },

      _consumeLiteral: function(stream) {
          this._current++;
          var start = this._current;
          var maxLength = stream.length;
          var literal;
          while(stream[this._current] !== "`" && this._current < maxLength) {
              // You can escape a literal char or you can escape the escape.
              var current = this._current;
              if (stream[current] === "\\" && (stream[current + 1] === "\\" ||
                                               stream[current + 1] === "`")) {
                  current += 2;
              } else {
                  current++;
              }
              this._current = current;
          }
          var literalString = trimLeft(stream.slice(start, this._current));
          literalString = literalString.replace("\\`", "`");
          if (this._looksLikeJSON(literalString)) {
              literal = JSON.parse(literalString);
          } else {
              // Try to JSON parse it as "<literal>"
              literal = JSON.parse("\"" + literalString + "\"");
          }
          // +1 gets us to the ending "`", +1 to move on to the next char.
          this._current++;
          return literal;
      },

      _looksLikeJSON: function(literalString) {
          var startingChars = "[{\"";
          var jsonLiterals = ["true", "false", "null"];
          var numberLooking = "-0123456789";

          if (literalString === "") {
              return false;
          } else if (startingChars.indexOf(literalString[0]) >= 0) {
              return true;
          } else if (jsonLiterals.indexOf(literalString) >= 0) {
              return true;
          } else if (numberLooking.indexOf(literalString[0]) >= 0) {
              try {
                  JSON.parse(literalString);
                  return true;
              } catch (ex) {
                  return false;
              }
          } else {
              return false;
          }
      }
  };

      var bindingPower = {};
      bindingPower[TOK_EOF] = 0;
      bindingPower[TOK_UNQUOTEDIDENTIFIER] = 0;
      bindingPower[TOK_QUOTEDIDENTIFIER] = 0;
      bindingPower[TOK_RBRACKET] = 0;
      bindingPower[TOK_RPAREN] = 0;
      bindingPower[TOK_COMMA] = 0;
      bindingPower[TOK_RBRACE] = 0;
      bindingPower[TOK_NUMBER] = 0;
      bindingPower[TOK_CURRENT] = 0;
      bindingPower[TOK_EXPREF] = 0;
      bindingPower[TOK_PIPE] = 1;
      bindingPower[TOK_OR] = 2;
      bindingPower[TOK_AND] = 3;
      bindingPower[TOK_EQ] = 5;
      bindingPower[TOK_GT] = 5;
      bindingPower[TOK_LT] = 5;
      bindingPower[TOK_GTE] = 5;
      bindingPower[TOK_LTE] = 5;
      bindingPower[TOK_NE] = 5;
      bindingPower[TOK_FLATTEN] = 9;
      bindingPower[TOK_STAR] = 20;
      bindingPower[TOK_FILTER] = 21;
      bindingPower[TOK_DOT] = 40;
      bindingPower[TOK_NOT] = 45;
      bindingPower[TOK_LBRACE] = 50;
      bindingPower[TOK_LBRACKET] = 55;
      bindingPower[TOK_LPAREN] = 60;

  function Parser() {
  }

  Parser.prototype = {
      parse: function(expression) {
          this._loadTokens(expression);
          this.index = 0;
          var ast = this.expression(0);
          if (this._lookahead(0) !== TOK_EOF) {
              var t = this._lookaheadToken(0);
              var error = new Error(
                  "Unexpected token type: " + t.type + ", value: " + t.value);
              error.name = "ParserError";
              throw error;
          }
          return ast;
      },

      _loadTokens: function(expression) {
          var lexer = new Lexer();
          var tokens = lexer.tokenize(expression);
          tokens.push({type: TOK_EOF, value: "", start: expression.length});
          this.tokens = tokens;
      },

      expression: function(rbp) {
          var leftToken = this._lookaheadToken(0);
          this._advance();
          var left = this.nud(leftToken);
          var currentToken = this._lookahead(0);
          while (rbp < bindingPower[currentToken]) {
              this._advance();
              left = this.led(currentToken, left);
              currentToken = this._lookahead(0);
          }
          return left;
      },

      _lookahead: function(number) {
          return this.tokens[this.index + number].type;
      },

      _lookaheadToken: function(number) {
          return this.tokens[this.index + number];
      },

      _advance: function() {
          this.index++;
      },

      nud: function(token) {
        var left;
        var right;
        var expression;
        switch (token.type) {
          case TOK_LITERAL:
            return {type: "Literal", value: token.value};
          case TOK_UNQUOTEDIDENTIFIER:
            return {type: "Field", name: token.value};
          case TOK_QUOTEDIDENTIFIER:
            var node = {type: "Field", name: token.value};
            if (this._lookahead(0) === TOK_LPAREN) {
                throw new Error("Quoted identifier not allowed for function names.");
            } else {
                return node;
            }
            break;
          case TOK_NOT:
            right = this.expression(bindingPower.Not);
            return {type: "NotExpression", children: [right]};
          case TOK_STAR:
            left = {type: "Identity"};
            right = null;
            if (this._lookahead(0) === TOK_RBRACKET) {
                // This can happen in a multiselect,
                // [a, b, *]
                right = {type: "Identity"};
            } else {
                right = this._parseProjectionRHS(bindingPower.Star);
            }
            return {type: "ValueProjection", children: [left, right]};
          case TOK_FILTER:
            return this.led(token.type, {type: "Identity"});
          case TOK_LBRACE:
            return this._parseMultiselectHash();
          case TOK_FLATTEN:
            left = {type: TOK_FLATTEN, children: [{type: "Identity"}]};
            right = this._parseProjectionRHS(bindingPower.Flatten);
            return {type: "Projection", children: [left, right]};
          case TOK_LBRACKET:
            if (this._lookahead(0) === TOK_NUMBER || this._lookahead(0) === TOK_COLON) {
                right = this._parseIndexExpression();
                return this._projectIfSlice({type: "Identity"}, right);
            } else if (this._lookahead(0) === TOK_STAR &&
                       this._lookahead(1) === TOK_RBRACKET) {
                this._advance();
                this._advance();
                right = this._parseProjectionRHS(bindingPower.Star);
                return {type: "Projection",
                        children: [{type: "Identity"}, right]};
            } else {
                return this._parseMultiselectList();
            }
            break;
          case TOK_CURRENT:
            return {type: TOK_CURRENT};
          case TOK_EXPREF:
            expression = this.expression(bindingPower.Expref);
            return {type: "ExpressionReference", children: [expression]};
          case TOK_LPAREN:
            var args = [];
            while (this._lookahead(0) !== TOK_RPAREN) {
              if (this._lookahead(0) === TOK_CURRENT) {
                expression = {type: TOK_CURRENT};
                this._advance();
              } else {
                expression = this.expression(0);
              }
              args.push(expression);
            }
            this._match(TOK_RPAREN);
            return args[0];
          default:
            this._errorToken(token);
        }
      },

      led: function(tokenName, left) {
        var right;
        switch(tokenName) {
          case TOK_DOT:
            var rbp = bindingPower.Dot;
            if (this._lookahead(0) !== TOK_STAR) {
                right = this._parseDotRHS(rbp);
                return {type: "Subexpression", children: [left, right]};
            } else {
                // Creating a projection.
                this._advance();
                right = this._parseProjectionRHS(rbp);
                return {type: "ValueProjection", children: [left, right]};
            }
            break;
          case TOK_PIPE:
            right = this.expression(bindingPower.Pipe);
            return {type: TOK_PIPE, children: [left, right]};
          case TOK_OR:
            right = this.expression(bindingPower.Or);
            return {type: "OrExpression", children: [left, right]};
          case TOK_AND:
            right = this.expression(bindingPower.And);
            return {type: "AndExpression", children: [left, right]};
          case TOK_LPAREN:
            var name = left.name;
            var args = [];
            var expression, node;
            while (this._lookahead(0) !== TOK_RPAREN) {
              if (this._lookahead(0) === TOK_CURRENT) {
                expression = {type: TOK_CURRENT};
                this._advance();
              } else {
                expression = this.expression(0);
              }
              if (this._lookahead(0) === TOK_COMMA) {
                this._match(TOK_COMMA);
              }
              args.push(expression);
            }
            this._match(TOK_RPAREN);
            node = {type: "Function", name: name, children: args};
            return node;
          case TOK_FILTER:
            var condition = this.expression(0);
            this._match(TOK_RBRACKET);
            if (this._lookahead(0) === TOK_FLATTEN) {
              right = {type: "Identity"};
            } else {
              right = this._parseProjectionRHS(bindingPower.Filter);
            }
            return {type: "FilterProjection", children: [left, right, condition]};
          case TOK_FLATTEN:
            var leftNode = {type: TOK_FLATTEN, children: [left]};
            var rightNode = this._parseProjectionRHS(bindingPower.Flatten);
            return {type: "Projection", children: [leftNode, rightNode]};
          case TOK_EQ:
          case TOK_NE:
          case TOK_GT:
          case TOK_GTE:
          case TOK_LT:
          case TOK_LTE:
            return this._parseComparator(left, tokenName);
          case TOK_LBRACKET:
            var token = this._lookaheadToken(0);
            if (token.type === TOK_NUMBER || token.type === TOK_COLON) {
                right = this._parseIndexExpression();
                return this._projectIfSlice(left, right);
            } else {
                this._match(TOK_STAR);
                this._match(TOK_RBRACKET);
                right = this._parseProjectionRHS(bindingPower.Star);
                return {type: "Projection", children: [left, right]};
            }
            break;
          default:
            this._errorToken(this._lookaheadToken(0));
        }
      },

      _match: function(tokenType) {
          if (this._lookahead(0) === tokenType) {
              this._advance();
          } else {
              var t = this._lookaheadToken(0);
              var error = new Error("Expected " + tokenType + ", got: " + t.type);
              error.name = "ParserError";
              throw error;
          }
      },

      _errorToken: function(token) {
          var error = new Error("Invalid token (" +
                                token.type + "): \"" +
                                token.value + "\"");
          error.name = "ParserError";
          throw error;
      },


      _parseIndexExpression: function() {
          if (this._lookahead(0) === TOK_COLON || this._lookahead(1) === TOK_COLON) {
              return this._parseSliceExpression();
          } else {
              var node = {
                  type: "Index",
                  value: this._lookaheadToken(0).value};
              this._advance();
              this._match(TOK_RBRACKET);
              return node;
          }
      },

      _projectIfSlice: function(left, right) {
          var indexExpr = {type: "IndexExpression", children: [left, right]};
          if (right.type === "Slice") {
              return {
                  type: "Projection",
                  children: [indexExpr, this._parseProjectionRHS(bindingPower.Star)]
              };
          } else {
              return indexExpr;
          }
      },

      _parseSliceExpression: function() {
          // [start:end:step] where each part is optional, as well as the last
          // colon.
          var parts = [null, null, null];
          var index = 0;
          var currentToken = this._lookahead(0);
          while (currentToken !== TOK_RBRACKET && index < 3) {
              if (currentToken === TOK_COLON) {
                  index++;
                  this._advance();
              } else if (currentToken === TOK_NUMBER) {
                  parts[index] = this._lookaheadToken(0).value;
                  this._advance();
              } else {
                  var t = this._lookahead(0);
                  var error = new Error("Syntax error, unexpected token: " +
                                        t.value + "(" + t.type + ")");
                  error.name = "Parsererror";
                  throw error;
              }
              currentToken = this._lookahead(0);
          }
          this._match(TOK_RBRACKET);
          return {
              type: "Slice",
              children: parts
          };
      },

      _parseComparator: function(left, comparator) {
        var right = this.expression(bindingPower[comparator]);
        return {type: "Comparator", name: comparator, children: [left, right]};
      },

      _parseDotRHS: function(rbp) {
          var lookahead = this._lookahead(0);
          var exprTokens = [TOK_UNQUOTEDIDENTIFIER, TOK_QUOTEDIDENTIFIER, TOK_STAR];
          if (exprTokens.indexOf(lookahead) >= 0) {
              return this.expression(rbp);
          } else if (lookahead === TOK_LBRACKET) {
              this._match(TOK_LBRACKET);
              return this._parseMultiselectList();
          } else if (lookahead === TOK_LBRACE) {
              this._match(TOK_LBRACE);
              return this._parseMultiselectHash();
          }
      },

      _parseProjectionRHS: function(rbp) {
          var right;
          if (bindingPower[this._lookahead(0)] < 10) {
              right = {type: "Identity"};
          } else if (this._lookahead(0) === TOK_LBRACKET) {
              right = this.expression(rbp);
          } else if (this._lookahead(0) === TOK_FILTER) {
              right = this.expression(rbp);
          } else if (this._lookahead(0) === TOK_DOT) {
              this._match(TOK_DOT);
              right = this._parseDotRHS(rbp);
          } else {
              var t = this._lookaheadToken(0);
              var error = new Error("Sytanx error, unexpected token: " +
                                    t.value + "(" + t.type + ")");
              error.name = "ParserError";
              throw error;
          }
          return right;
      },

      _parseMultiselectList: function() {
          var expressions = [];
          while (this._lookahead(0) !== TOK_RBRACKET) {
              var expression = this.expression(0);
              expressions.push(expression);
              if (this._lookahead(0) === TOK_COMMA) {
                  this._match(TOK_COMMA);
                  if (this._lookahead(0) === TOK_RBRACKET) {
                    throw new Error("Unexpected token Rbracket");
                  }
              }
          }
          this._match(TOK_RBRACKET);
          return {type: "MultiSelectList", children: expressions};
      },

      _parseMultiselectHash: function() {
        var pairs = [];
        var identifierTypes = [TOK_UNQUOTEDIDENTIFIER, TOK_QUOTEDIDENTIFIER];
        var keyToken, keyName, value, node;
        for (;;) {
          keyToken = this._lookaheadToken(0);
          if (identifierTypes.indexOf(keyToken.type) < 0) {
            throw new Error("Expecting an identifier token, got: " +
                            keyToken.type);
          }
          keyName = keyToken.value;
          this._advance();
          this._match(TOK_COLON);
          value = this.expression(0);
          node = {type: "KeyValuePair", name: keyName, value: value};
          pairs.push(node);
          if (this._lookahead(0) === TOK_COMMA) {
            this._match(TOK_COMMA);
          } else if (this._lookahead(0) === TOK_RBRACE) {
            this._match(TOK_RBRACE);
            break;
          }
        }
        return {type: "MultiSelectHash", children: pairs};
      }
  };


  function TreeInterpreter(runtime) {
    this.runtime = runtime;
  }

  TreeInterpreter.prototype = {
      search: function(node, value) {
          return this.visit(node, value);
      },

      visit: function(node, value) {
          var matched, current, result, first, second, field, left, right, collected, i;
          switch (node.type) {
            case "Field":
              if (value === null ) {
                  return null;
              } else if (isObject(value)) {
                  field = value[node.name];
                  if (field === undefined) {
                      return null;
                  } else {
                      return field;
                  }
              } else {
                return null;
              }
              break;
            case "Subexpression":
              result = this.visit(node.children[0], value);
              for (i = 1; i < node.children.length; i++) {
                  result = this.visit(node.children[1], result);
                  if (result === null) {
                      return null;
                  }
              }
              return result;
            case "IndexExpression":
              left = this.visit(node.children[0], value);
              right = this.visit(node.children[1], left);
              return right;
            case "Index":
              if (!isArray(value)) {
                return null;
              }
              var index = node.value;
              if (index < 0) {
                index = value.length + index;
              }
              result = value[index];
              if (result === undefined) {
                result = null;
              }
              return result;
            case "Slice":
              if (!isArray(value)) {
                return null;
              }
              var sliceParams = node.children.slice(0);
              var computed = this.computeSliceParams(value.length, sliceParams);
              var start = computed[0];
              var stop = computed[1];
              var step = computed[2];
              result = [];
              if (step > 0) {
                  for (i = start; i < stop; i += step) {
                      result.push(value[i]);
                  }
              } else {
                  for (i = start; i > stop; i += step) {
                      result.push(value[i]);
                  }
              }
              return result;
            case "Projection":
              // Evaluate left child.
              var base = this.visit(node.children[0], value);
              if (!isArray(base)) {
                return null;
              }
              collected = [];
              for (i = 0; i < base.length; i++) {
                current = this.visit(node.children[1], base[i]);
                if (current !== null) {
                  collected.push(current);
                }
              }
              return collected;
            case "ValueProjection":
              // Evaluate left child.
              base = this.visit(node.children[0], value);
              if (!isObject(base)) {
                return null;
              }
              collected = [];
              var values = objValues(base);
              for (i = 0; i < values.length; i++) {
                current = this.visit(node.children[1], values[i]);
                if (current !== null) {
                  collected.push(current);
                }
              }
              return collected;
            case "FilterProjection":
              base = this.visit(node.children[0], value);
              if (!isArray(base)) {
                return null;
              }
              var filtered = [];
              var finalResults = [];
              for (i = 0; i < base.length; i++) {
                matched = this.visit(node.children[2], base[i]);
                if (!isFalse(matched)) {
                  filtered.push(base[i]);
                }
              }
              for (var j = 0; j < filtered.length; j++) {
                current = this.visit(node.children[1], filtered[j]);
                if (current !== null) {
                  finalResults.push(current);
                }
              }
              return finalResults;
            case "Comparator":
              first = this.visit(node.children[0], value);
              second = this.visit(node.children[1], value);
              switch(node.name) {
                case TOK_EQ:
                  result = strictDeepEqual(first, second);
                  break;
                case TOK_NE:
                  result = !strictDeepEqual(first, second);
                  break;
                case TOK_GT:
                  result = first > second;
                  break;
                case TOK_GTE:
                  result = first >= second;
                  break;
                case TOK_LT:
                  result = first < second;
                  break;
                case TOK_LTE:
                  result = first <= second;
                  break;
                default:
                  throw new Error("Unknown comparator: " + node.name);
              }
              return result;
            case TOK_FLATTEN:
              var original = this.visit(node.children[0], value);
              if (!isArray(original)) {
                return null;
              }
              var merged = [];
              for (i = 0; i < original.length; i++) {
                current = original[i];
                if (isArray(current)) {
                  merged.push.apply(merged, current);
                } else {
                  merged.push(current);
                }
              }
              return merged;
            case "Identity":
              return value;
            case "MultiSelectList":
              if (value === null) {
                return null;
              }
              collected = [];
              for (i = 0; i < node.children.length; i++) {
                  collected.push(this.visit(node.children[i], value));
              }
              return collected;
            case "MultiSelectHash":
              if (value === null) {
                return null;
              }
              collected = {};
              var child;
              for (i = 0; i < node.children.length; i++) {
                child = node.children[i];
                collected[child.name] = this.visit(child.value, value);
              }
              return collected;
            case "OrExpression":
              matched = this.visit(node.children[0], value);
              if (isFalse(matched)) {
                  matched = this.visit(node.children[1], value);
              }
              return matched;
            case "AndExpression":
              first = this.visit(node.children[0], value);

              if (isFalse(first) === true) {
                return first;
              }
              return this.visit(node.children[1], value);
            case "NotExpression":
              first = this.visit(node.children[0], value);
              return isFalse(first);
            case "Literal":
              return node.value;
            case TOK_PIPE:
              left = this.visit(node.children[0], value);
              return this.visit(node.children[1], left);
            case TOK_CURRENT:
              return value;
            case "Function":
              var resolvedArgs = [];
              for (i = 0; i < node.children.length; i++) {
                  resolvedArgs.push(this.visit(node.children[i], value));
              }
              return this.runtime.callFunction(node.name, resolvedArgs);
            case "ExpressionReference":
              var refNode = node.children[0];
              // Tag the node with a specific attribute so the type
              // checker verify the type.
              refNode.jmespathType = TOK_EXPREF;
              return refNode;
            default:
              throw new Error("Unknown node type: " + node.type);
          }
      },

      computeSliceParams: function(arrayLength, sliceParams) {
        var start = sliceParams[0];
        var stop = sliceParams[1];
        var step = sliceParams[2];
        var computed = [null, null, null];
        if (step === null) {
          step = 1;
        } else if (step === 0) {
          var error = new Error("Invalid slice, step cannot be 0");
          error.name = "RuntimeError";
          throw error;
        }
        var stepValueNegative = step < 0 ? true : false;

        if (start === null) {
            start = stepValueNegative ? arrayLength - 1 : 0;
        } else {
            start = this.capSliceRange(arrayLength, start, step);
        }

        if (stop === null) {
            stop = stepValueNegative ? -1 : arrayLength;
        } else {
            stop = this.capSliceRange(arrayLength, stop, step);
        }
        computed[0] = start;
        computed[1] = stop;
        computed[2] = step;
        return computed;
      },

      capSliceRange: function(arrayLength, actualValue, step) {
          if (actualValue < 0) {
              actualValue += arrayLength;
              if (actualValue < 0) {
                  actualValue = step < 0 ? -1 : 0;
              }
          } else if (actualValue >= arrayLength) {
              actualValue = step < 0 ? arrayLength - 1 : arrayLength;
          }
          return actualValue;
      }

  };

  function Runtime(interpreter) {
    this._interpreter = interpreter;
    this.functionTable = {
        // name: [function, <signature>]
        // The <signature> can be:
        //
        // {
        //   args: [[type1, type2], [type1, type2]],
        //   variadic: true|false
        // }
        //
        // Each arg in the arg list is a list of valid types
        // (if the function is overloaded and supports multiple
        // types.  If the type is "any" then no type checking
        // occurs on the argument.  Variadic is optional
        // and if not provided is assumed to be false.
        abs: {_func: this._functionAbs, _signature: [{types: [TYPE_NUMBER]}]},
        avg: {_func: this._functionAvg, _signature: [{types: [TYPE_ARRAY_NUMBER]}]},
        ceil: {_func: this._functionCeil, _signature: [{types: [TYPE_NUMBER]}]},
        contains: {
            _func: this._functionContains,
            _signature: [{types: [TYPE_STRING, TYPE_ARRAY]},
                        {types: [TYPE_ANY]}]},
        "ends_with": {
            _func: this._functionEndsWith,
            _signature: [{types: [TYPE_STRING]}, {types: [TYPE_STRING]}]},
        floor: {_func: this._functionFloor, _signature: [{types: [TYPE_NUMBER]}]},
        length: {
            _func: this._functionLength,
            _signature: [{types: [TYPE_STRING, TYPE_ARRAY, TYPE_OBJECT]}]},
        map: {
            _func: this._functionMap,
            _signature: [{types: [TYPE_EXPREF]}, {types: [TYPE_ARRAY]}]},
        max: {
            _func: this._functionMax,
            _signature: [{types: [TYPE_ARRAY_NUMBER, TYPE_ARRAY_STRING]}]},
        "merge": {
            _func: this._functionMerge,
            _signature: [{types: [TYPE_OBJECT], variadic: true}]
        },
        "max_by": {
          _func: this._functionMaxBy,
          _signature: [{types: [TYPE_ARRAY]}, {types: [TYPE_EXPREF]}]
        },
        sum: {_func: this._functionSum, _signature: [{types: [TYPE_ARRAY_NUMBER]}]},
        "starts_with": {
            _func: this._functionStartsWith,
            _signature: [{types: [TYPE_STRING]}, {types: [TYPE_STRING]}]},
        min: {
            _func: this._functionMin,
            _signature: [{types: [TYPE_ARRAY_NUMBER, TYPE_ARRAY_STRING]}]},
        "min_by": {
          _func: this._functionMinBy,
          _signature: [{types: [TYPE_ARRAY]}, {types: [TYPE_EXPREF]}]
        },
        type: {_func: this._functionType, _signature: [{types: [TYPE_ANY]}]},
        keys: {_func: this._functionKeys, _signature: [{types: [TYPE_OBJECT]}]},
        values: {_func: this._functionValues, _signature: [{types: [TYPE_OBJECT]}]},
        sort: {_func: this._functionSort, _signature: [{types: [TYPE_ARRAY_STRING, TYPE_ARRAY_NUMBER]}]},
        "sort_by": {
          _func: this._functionSortBy,
          _signature: [{types: [TYPE_ARRAY]}, {types: [TYPE_EXPREF]}]
        },
        join: {
            _func: this._functionJoin,
            _signature: [
                {types: [TYPE_STRING]},
                {types: [TYPE_ARRAY_STRING]}
            ]
        },
        reverse: {
            _func: this._functionReverse,
            _signature: [{types: [TYPE_STRING, TYPE_ARRAY]}]},
        "to_array": {_func: this._functionToArray, _signature: [{types: [TYPE_ANY]}]},
        "to_string": {_func: this._functionToString, _signature: [{types: [TYPE_ANY]}]},
        "to_number": {_func: this._functionToNumber, _signature: [{types: [TYPE_ANY]}]},
        "not_null": {
            _func: this._functionNotNull,
            _signature: [{types: [TYPE_ANY], variadic: true}]
        }
    };
  }

  Runtime.prototype = {
    callFunction: function(name, resolvedArgs) {
      var functionEntry = this.functionTable[name];
      if (functionEntry === undefined) {
          throw new Error("Unknown function: " + name + "()");
      }
      this._validateArgs(name, resolvedArgs, functionEntry._signature);
      return functionEntry._func.call(this, resolvedArgs);
    },

    _validateArgs: function(name, args, signature) {
        // Validating the args requires validating
        // the correct arity and the correct type of each arg.
        // If the last argument is declared as variadic, then we need
        // a minimum number of args to be required.  Otherwise it has to
        // be an exact amount.
        var pluralized;
        if (signature[signature.length - 1].variadic) {
            if (args.length < signature.length) {
                pluralized = signature.length === 1 ? " argument" : " arguments";
                throw new Error("ArgumentError: " + name + "() " +
                                "takes at least" + signature.length + pluralized +
                                " but received " + args.length);
            }
        } else if (args.length !== signature.length) {
            pluralized = signature.length === 1 ? " argument" : " arguments";
            throw new Error("ArgumentError: " + name + "() " +
                            "takes " + signature.length + pluralized +
                            " but received " + args.length);
        }
        var currentSpec;
        var actualType;
        var typeMatched;
        for (var i = 0; i < signature.length; i++) {
            typeMatched = false;
            currentSpec = signature[i].types;
            actualType = this._getTypeName(args[i]);
            for (var j = 0; j < currentSpec.length; j++) {
                if (this._typeMatches(actualType, currentSpec[j], args[i])) {
                    typeMatched = true;
                    break;
                }
            }
            if (!typeMatched) {
                throw new Error("TypeError: " + name + "() " +
                                "expected argument " + (i + 1) +
                                " to be type " + currentSpec +
                                " but received type " + actualType +
                                " instead.");
            }
        }
    },

    _typeMatches: function(actual, expected, argValue) {
        if (expected === TYPE_ANY) {
            return true;
        }
        if (expected === TYPE_ARRAY_STRING ||
            expected === TYPE_ARRAY_NUMBER ||
            expected === TYPE_ARRAY) {
            // The expected type can either just be array,
            // or it can require a specific subtype (array of numbers).
            //
            // The simplest case is if "array" with no subtype is specified.
            if (expected === TYPE_ARRAY) {
                return actual === TYPE_ARRAY;
            } else if (actual === TYPE_ARRAY) {
                // Otherwise we need to check subtypes.
                // I think this has potential to be improved.
                var subtype;
                if (expected === TYPE_ARRAY_NUMBER) {
                  subtype = TYPE_NUMBER;
                } else if (expected === TYPE_ARRAY_STRING) {
                  subtype = TYPE_STRING;
                }
                for (var i = 0; i < argValue.length; i++) {
                    if (!this._typeMatches(
                            this._getTypeName(argValue[i]), subtype,
                                             argValue[i])) {
                        return false;
                    }
                }
                return true;
            }
        } else {
            return actual === expected;
        }
    },
    _getTypeName: function(obj) {
        switch (Object.prototype.toString.call(obj)) {
            case "[object String]":
              return TYPE_STRING;
            case "[object Number]":
              return TYPE_NUMBER;
            case "[object Array]":
              return TYPE_ARRAY;
            case "[object Boolean]":
              return TYPE_BOOLEAN;
            case "[object Null]":
              return TYPE_NULL;
            case "[object Object]":
              // Check if it's an expref.  If it has, it's been
              // tagged with a jmespathType attr of 'Expref';
              if (obj.jmespathType === TOK_EXPREF) {
                return TYPE_EXPREF;
              } else {
                return TYPE_OBJECT;
              }
        }
    },

    _functionStartsWith: function(resolvedArgs) {
        return resolvedArgs[0].lastIndexOf(resolvedArgs[1]) === 0;
    },

    _functionEndsWith: function(resolvedArgs) {
        var searchStr = resolvedArgs[0];
        var suffix = resolvedArgs[1];
        return searchStr.indexOf(suffix, searchStr.length - suffix.length) !== -1;
    },

    _functionReverse: function(resolvedArgs) {
        var typeName = this._getTypeName(resolvedArgs[0]);
        if (typeName === TYPE_STRING) {
          var originalStr = resolvedArgs[0];
          var reversedStr = "";
          for (var i = originalStr.length - 1; i >= 0; i--) {
              reversedStr += originalStr[i];
          }
          return reversedStr;
        } else {
          var reversedArray = resolvedArgs[0].slice(0);
          reversedArray.reverse();
          return reversedArray;
        }
    },

    _functionAbs: function(resolvedArgs) {
      return Math.abs(resolvedArgs[0]);
    },

    _functionCeil: function(resolvedArgs) {
        return Math.ceil(resolvedArgs[0]);
    },

    _functionAvg: function(resolvedArgs) {
        var sum = 0;
        var inputArray = resolvedArgs[0];
        for (var i = 0; i < inputArray.length; i++) {
            sum += inputArray[i];
        }
        return sum / inputArray.length;
    },

    _functionContains: function(resolvedArgs) {
        return resolvedArgs[0].indexOf(resolvedArgs[1]) >= 0;
    },

    _functionFloor: function(resolvedArgs) {
        return Math.floor(resolvedArgs[0]);
    },

    _functionLength: function(resolvedArgs) {
       if (!isObject(resolvedArgs[0])) {
         return resolvedArgs[0].length;
       } else {
         // As far as I can tell, there's no way to get the length
         // of an object without O(n) iteration through the object.
         return Object.keys(resolvedArgs[0]).length;
       }
    },

    _functionMap: function(resolvedArgs) {
      var mapped = [];
      var interpreter = this._interpreter;
      var exprefNode = resolvedArgs[0];
      var elements = resolvedArgs[1];
      for (var i = 0; i < elements.length; i++) {
          mapped.push(interpreter.visit(exprefNode, elements[i]));
      }
      return mapped;
    },

    _functionMerge: function(resolvedArgs) {
      var merged = {};
      for (var i = 0; i < resolvedArgs.length; i++) {
        var current = resolvedArgs[i];
        for (var key in current) {
          merged[key] = current[key];
        }
      }
      return merged;
    },

    _functionMax: function(resolvedArgs) {
      if (resolvedArgs[0].length > 0) {
        var typeName = this._getTypeName(resolvedArgs[0][0]);
        if (typeName === TYPE_NUMBER) {
          return Math.max.apply(Math, resolvedArgs[0]);
        } else {
          var elements = resolvedArgs[0];
          var maxElement = elements[0];
          for (var i = 1; i < elements.length; i++) {
              if (maxElement.localeCompare(elements[i]) < 0) {
                  maxElement = elements[i];
              }
          }
          return maxElement;
        }
      } else {
          return null;
      }
    },

    _functionMin: function(resolvedArgs) {
      if (resolvedArgs[0].length > 0) {
        var typeName = this._getTypeName(resolvedArgs[0][0]);
        if (typeName === TYPE_NUMBER) {
          return Math.min.apply(Math, resolvedArgs[0]);
        } else {
          var elements = resolvedArgs[0];
          var minElement = elements[0];
          for (var i = 1; i < elements.length; i++) {
              if (elements[i].localeCompare(minElement) < 0) {
                  minElement = elements[i];
              }
          }
          return minElement;
        }
      } else {
        return null;
      }
    },

    _functionSum: function(resolvedArgs) {
      var sum = 0;
      var listToSum = resolvedArgs[0];
      for (var i = 0; i < listToSum.length; i++) {
        sum += listToSum[i];
      }
      return sum;
    },

    _functionType: function(resolvedArgs) {
        switch (this._getTypeName(resolvedArgs[0])) {
          case TYPE_NUMBER:
            return "number";
          case TYPE_STRING:
            return "string";
          case TYPE_ARRAY:
            return "array";
          case TYPE_OBJECT:
            return "object";
          case TYPE_BOOLEAN:
            return "boolean";
          case TYPE_EXPREF:
            return "expref";
          case TYPE_NULL:
            return "null";
        }
    },

    _functionKeys: function(resolvedArgs) {
        return Object.keys(resolvedArgs[0]);
    },

    _functionValues: function(resolvedArgs) {
        var obj = resolvedArgs[0];
        var keys = Object.keys(obj);
        var values = [];
        for (var i = 0; i < keys.length; i++) {
            values.push(obj[keys[i]]);
        }
        return values;
    },

    _functionJoin: function(resolvedArgs) {
        var joinChar = resolvedArgs[0];
        var listJoin = resolvedArgs[1];
        return listJoin.join(joinChar);
    },

    _functionToArray: function(resolvedArgs) {
        if (this._getTypeName(resolvedArgs[0]) === TYPE_ARRAY) {
            return resolvedArgs[0];
        } else {
            return [resolvedArgs[0]];
        }
    },

    _functionToString: function(resolvedArgs) {
        if (this._getTypeName(resolvedArgs[0]) === TYPE_STRING) {
            return resolvedArgs[0];
        } else {
            return JSON.stringify(resolvedArgs[0]);
        }
    },

    _functionToNumber: function(resolvedArgs) {
        var typeName = this._getTypeName(resolvedArgs[0]);
        var convertedValue;
        if (typeName === TYPE_NUMBER) {
            return resolvedArgs[0];
        } else if (typeName === TYPE_STRING) {
            convertedValue = +resolvedArgs[0];
            if (!isNaN(convertedValue)) {
                return convertedValue;
            }
        }
        return null;
    },

    _functionNotNull: function(resolvedArgs) {
        for (var i = 0; i < resolvedArgs.length; i++) {
            if (this._getTypeName(resolvedArgs[i]) !== TYPE_NULL) {
                return resolvedArgs[i];
            }
        }
        return null;
    },

    _functionSort: function(resolvedArgs) {
        var sortedArray = resolvedArgs[0].slice(0);
        sortedArray.sort();
        return sortedArray;
    },

    _functionSortBy: function(resolvedArgs) {
        var sortedArray = resolvedArgs[0].slice(0);
        if (sortedArray.length === 0) {
            return sortedArray;
        }
        var interpreter = this._interpreter;
        var exprefNode = resolvedArgs[1];
        var requiredType = this._getTypeName(
            interpreter.visit(exprefNode, sortedArray[0]));
        if ([TYPE_NUMBER, TYPE_STRING].indexOf(requiredType) < 0) {
            throw new Error("TypeError");
        }
        var that = this;
        // In order to get a stable sort out of an unstable
        // sort algorithm, we decorate/sort/undecorate (DSU)
        // by creating a new list of [index, element] pairs.
        // In the cmp function, if the evaluated elements are
        // equal, then the index will be used as the tiebreaker.
        // After the decorated list has been sorted, it will be
        // undecorated to extract the original elements.
        var decorated = [];
        for (var i = 0; i < sortedArray.length; i++) {
          decorated.push([i, sortedArray[i]]);
        }
        decorated.sort(function(a, b) {
          var exprA = interpreter.visit(exprefNode, a[1]);
          var exprB = interpreter.visit(exprefNode, b[1]);
          if (that._getTypeName(exprA) !== requiredType) {
              throw new Error(
                  "TypeError: expected " + requiredType + ", received " +
                  that._getTypeName(exprA));
          } else if (that._getTypeName(exprB) !== requiredType) {
              throw new Error(
                  "TypeError: expected " + requiredType + ", received " +
                  that._getTypeName(exprB));
          }
          if (exprA > exprB) {
            return 1;
          } else if (exprA < exprB) {
            return -1;
          } else {
            // If they're equal compare the items by their
            // order to maintain relative order of equal keys
            // (i.e. to get a stable sort).
            return a[0] - b[0];
          }
        });
        // Undecorate: extract out the original list elements.
        for (var j = 0; j < decorated.length; j++) {
          sortedArray[j] = decorated[j][1];
        }
        return sortedArray;
    },

    _functionMaxBy: function(resolvedArgs) {
      var exprefNode = resolvedArgs[1];
      var resolvedArray = resolvedArgs[0];
      var keyFunction = this.createKeyFunction(exprefNode, [TYPE_NUMBER, TYPE_STRING]);
      var maxNumber = -Infinity;
      var maxRecord;
      var current;
      for (var i = 0; i < resolvedArray.length; i++) {
        current = keyFunction(resolvedArray[i]);
        if (current > maxNumber) {
          maxNumber = current;
          maxRecord = resolvedArray[i];
        }
      }
      return maxRecord;
    },

    _functionMinBy: function(resolvedArgs) {
      var exprefNode = resolvedArgs[1];
      var resolvedArray = resolvedArgs[0];
      var keyFunction = this.createKeyFunction(exprefNode, [TYPE_NUMBER, TYPE_STRING]);
      var minNumber = Infinity;
      var minRecord;
      var current;
      for (var i = 0; i < resolvedArray.length; i++) {
        current = keyFunction(resolvedArray[i]);
        if (current < minNumber) {
          minNumber = current;
          minRecord = resolvedArray[i];
        }
      }
      return minRecord;
    },

    createKeyFunction: function(exprefNode, allowedTypes) {
      var that = this;
      var interpreter = this._interpreter;
      var keyFunc = function(x) {
        var current = interpreter.visit(exprefNode, x);
        if (allowedTypes.indexOf(that._getTypeName(current)) < 0) {
          var msg = "TypeError: expected one of " + allowedTypes +
                    ", received " + that._getTypeName(current);
          throw new Error(msg);
        }
        return current;
      };
      return keyFunc;
    }

  };

  function compile(stream) {
    var parser = new Parser();
    var ast = parser.parse(stream);
    return ast;
  }

  function tokenize(stream) {
      var lexer = new Lexer();
      return lexer.tokenize(stream);
  }

  function search(data, expression) {
      var parser = new Parser();
      // This needs to be improved.  Both the interpreter and runtime depend on
      // each other.  The runtime needs the interpreter to support exprefs.
      // There's likely a clean way to avoid the cyclic dependency.
      var runtime = new Runtime();
      var interpreter = new TreeInterpreter(runtime);
      runtime._interpreter = interpreter;
      var node = parser.parse(expression);
      return interpreter.search(node, data);
  }

  exports.tokenize = tokenize;
  exports.compile = compile;
  exports.search = search;
  exports.strictDeepEqual = strictDeepEqual;
})( false ? undefined : exports);


/***/ }),
/* 21 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

module.exports = ReactPropTypesSecret;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(12);
var AWS = __webpack_require__(1);
var Service = AWS.Service;
var apiLoader = AWS.apiLoader;

apiLoader.services['lexruntime'] = {};
AWS.LexRuntime = Service.defineService('lexruntime', ['2016-11-28']);
Object.defineProperty(apiLoader.services['lexruntime'], '2016-11-28', {
  get: function get() {
    var model = __webpack_require__(127);
    model.paginators = __webpack_require__(128).pagination;
    return model;
  },
  enumerable: true,
  configurable: true
});

module.exports = AWS.LexRuntime;


/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("data:font/ttf;base64,AAEAAAALAIAAAwAwT1MvMg8SDssAAAC8AAAAYGNtYXC8QrYGAAABHAAAAIxnYXNwAAAAEAAAAagAAAAIZ2x5ZgCmvr4AAAGwAAAJ5GhlYWQYZPdkAAALlAAAADZoaGVhB8IDzgAAC8wAAAAkaG10eCTdAMcAAAvwAAAAMGxvY2ELPglWAAAMIAAAABptYXhwAB4AzgAADDwAAAAgbmFtZZlKCfsAAAxcAAABhnBvc3QAAwAAAAAN5AAAACAAAwOnAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADx+gPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAcAAAABgAEAADAAgAAQAg6UPpR+nJ8APwRPCY8azx+v/9//8AAAAAACDpQ+lH6cnwA/BE8JjxrPH6//3//wAB/+MWwRa+Fj0QBA/ED3EOXg4RAAMAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAH//wAPAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEABwDFA/kCgABCAAABHgEHBgcGJicmNTQ2Jy4BIyIGBwYWFRQHDgEnJicmNjc+ATc4ATE2Nz4BNzYzMToBMzoBMzEyFx4BFxYXOAExHgEXA/kGARIQMTFpKikhHx9mPT1mHx8hKSppMTEQEgEGBB4lHCUkZkREWwECAQECAVtERGYkJRwlHgQBgCphFRIJCAkTEyQkOCQjDQ0jJDgkJBMTCQgJEhVhKhw+JhoXFyMKCwsKIxcXGiY+HAAAAgDA/8ADQAPAABsAJwAAASIHDgEHBhUUFx4BFxYxMDc+ATc2NTQnLgEnJgMiJjU0NjMyFhUUBgIAQjs6VxkZMjJ4MjIyMngyMhkZVzo7QlBwcFBQcHADwBkZVzo7Qnh9fcxBQUFBzH19eEI7OlcZGf4AcFBQcHBQUHAAABEAAP/AA8ADgAAbACQALQA0AEAATABTAFsAYQBsAHcAfQCGAJEAmwClALAAAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYTPgE3Mw4BByMBDgEHIz4BNzMhHgEXIzUzJzUeARceARceARcjJz4BNz4BNxUjPgE3FxUjPgE3MwEuASczHgEXNzMVIy4BFxUuAScuAScuAScFDgEHDgEHNTMOASc1Mw4BBzcuASczHgEXIzcjLgEnHgEXHgEXJT4BNw4BByM+AQMzHgEXLgEnLgEFDgEHPgE3Mw4BBwHgY1hXgyUmJiWDV1hjY1hXgyUmJiWDV1itBggBgAMPDXD94AYIAYADDw1wAd4HCQG/rq4LFgoUJhALEwibtRAmFAoWC5sIEwt1vwEJB67+oA0PA4ABCAYxv64HCb4LFgoUJhALEwgBUBAmFAoWC5sIE4C/AQkHUQEIBnANDwOAQGAOJhggOhkQHQz9exk6IBgmDmAMHSlgDiYYIDoZEB0CeRk6IBgmDmAMHRADgCYlg1dYY2NYV4MlJiYlg1dYY2NYV4MlJv2AHkEhIUAfAUAeQSEhQB8fQCGAQLsDCwcOKxsSKRdSGysOBwsDuxcpEpKAIUAf/sAfQCEhQR6AgB9An7sDCwcOKxsSKRdSGysOBwsDuxcpgIAhQB/AIUEeH0AhwCxNHg8pGRAjE0YZKQ8eTSwTI/4KLE0eDykZECMzGSkPHk0sEyMQAAMAAAAABAADJQAcADcARwAAJREOAQcOAQcOASsBIiYnLgEnLgEnERQWMyEyNjURNCYjISIGFRQWFx4BFx4BOwEyNjc+ATc+ATU3ERQGIyEiJjURNDYzITIWA7cJFAs9ejwgTywCLE8gPHo9CxQJCwcDSgcLAhD8tgcLLiY6cjkXSB4CHkgXOXI6HDhJNiX8tiU2NiUDSiU2WwG3ChMIMGAyGzU1GzJgMAgTCv5JBwsLBwJZCxwLBzFTHi1bLRM6OhMtWy0WUyQV/ZIlNjYlAm4mNjYABQAAAEkD6wNuAAcAFQBKAE8AWgAAATcnBxUzFTMTJgYPAQYUFxY2PwE+ARMVFAYjISImNRE0NjMhMhYXHgEXFgYPAQ4BJy4BIyEiBhURFBYzITI2PQE0Nj8BPgEXHgEVAxcBIzUBByc3NjIfARYUBwH7Q1dCNiD8BAsEyAQDBAsEyAQBKmFE/iVFYGBFAdsRIhAEBQEBAwMcBAoEBw0G/iUmNjYmAdsmNQMDJAQLBQUHN6T+gKQCfTSlNRAuEFYQEAESQ1ZCIDcBnAQBBMgECwQEAQTIBAv+sGxEYWFEAdtEYQcIAQcFBAkDHAQDAgICNib+JSY2NiZIAwcCJQQCAgIJBgGmpf6ApQE1NaU0EBBXEC4PAAAAAAIAAAAAA24DbgA6AEoAAAE8AScuAScuASMiBiMiJicuAScuATU0NjU0JicuAScmIiMiBgcOARUUFhcWFx4BFxYXHgEzMjY3PgE1ExEUBiMhIiY1ETQ2MyEyFgLbAQJZDAgTChMxEAkUBzlRIAQMSw4FBy8IAgUCCyQLHCUTChEgIVMvMC8aNhweVg0ECJNhRP3cRWBgRQIkRGEBDQIFAggvBwUOSwwEIFE5BxQJEDETChMIDFkCAQgEDVYeHDYaLzAvUyEgEQoTJRwLJAsBvP3cRWBgRQIkRGFhAAAMAAD/twQAA7cAEAApADoASwBcAG0AfgCPAKAAsQDBAMsAABMyFhURFAYrASImNRE0NjsBBR4BFREUBiMhIiY1ETQ2MyEyFh8BHgEdAQE1NCYrASIGHQEUFjsBMjY1PQE0JisBIgYdARQWOwEyNjU9ATQmKwEiBh0BFBY7ATI2NRM1NCYrASIGHQEUFjsBMjY1PQE0JisBIgYdARQWOwEyNjU9ATQmKwEiBh0BFBY7ATI2NRM1NCYrASIGHQEUFjsBMjY1PQE0JisBIgYdARQWOwEyNjU9ATQmKwEiBh0BFBY7ATI2NzUjIiY9ASERIaUlNjYlSiU2NiVKAxIgKVY8/hImNSAWAYAXNxBXEBf+WwoISQgKCghJCAoKCEkICgoISQgKCghJCAoKCEkICpMLCEkICgoISQgLCwhJCAoKCEkICwsISQgKCghJCAuSCwdKBwsLB0oHCwsHSgcLCwdKBwsLB0oHCwsHSgcLN1wWIf6TAgAC2zUm/ZIlNjYlAm4mNV0TQyj+STxWNiUDbhcgFxBXEDcXXf3LSQgLCwhJCAoKCJJKBwsLB0oHCwsHk0kICgoISQgLCwj+20kICwsISQgKCgiSSgcLCwdKBwsLB5NJCAoKCEkICwsI/ttJCAsLCEkICgoIkkoHCwsHSgcLCweTSQgKCghJCAsL45IhFlz+2wACAAAAAANuA24AFACFAAABNCYjIgcOAQcGFRQWMzI3PgE3NjUFFAcOAQcGByIGIyImJy4BJw4BIyImNTQ3PgE3NjMyFhc/ATQ2OwEyFhceARUDDgEVFBYzPgE1NCcuAScmIyIHDgEHBhUUFx4BFxYzMjY3NhYfAR4BBxQGBw4BIyInLgEnJjU0Nz4BNzYzMhceARcWFQIrPDYkIyM4EhE9OCsjJDQODgFDFhVEKiopBQcGGykNCAoBG1k6XGUWFkwzMzoyThUBBwUDRAIEAQEBRQECDxIdiBoaYENEU0xCQ2MdHR0dY0NCTD93MQYPBRcDAgEDAzuPTFtQUHciIyMid1BQW2JRUXMgIAH8PkUREUAtLjlARhYXRywtLEVDLi87Dg4BARAPCRgOITdtZEQ8PFoaGikkCyADBwUBAgUD/qIJDQYXDgE4dlNEQ2AaGh0dY0NDS0xCQ2MdHSsnBgIGHAMHBAMHAjAzIyJ3UFBbW09QeCIjICBzUVFiAAEAAAAAAADS5KnfXw889QALBAAAAAAA2qNZeAAAAADao1l4AAD/twQAA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABAEAAAAABAAAAQAAAAAAAAAAAAAAAAAAAAwEAAAAAAAAAAAAAAACAAAABAAABwQAAMAEAAAABAAAAAQBAAADbgAABAAAAANuAAAAAAAAAAoAFAAeAHwAugHOAjgCxAMyBDQE8gAAAAEAAAAMAMwAEQAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQAHAAAAAQAAAAAAAgAHAGAAAQAAAAAAAwAHADYAAQAAAAAABAAHAHUAAQAAAAAABQALABUAAQAAAAAABgAHAEsAAQAAAAAACgAaAIoAAwABBAkAAQAOAAcAAwABBAkAAgAOAGcAAwABBAkAAwAOAD0AAwABBAkABAAOAHwAAwABBAkABQAWACAAAwABBAkABgAOAFIAAwABBAkACgA0AKRpY29tb29uAGkAYwBvAG0AbwBvAG5WZXJzaW9uIDEuMABWAGUAcgBzAGkAbwBuACAAMQAuADBpY29tb29uAGkAYwBvAG0AbwBvAG5pY29tb29uAGkAYwBvAG0AbwBvAG5SZWd1bGFyAFIAZQBnAHUAbABhAHJpY29tb29uAGkAYwBvAG0AbwBvAG5Gb250IGdlbmVyYXRlZCBieSBJY29Nb29uLgBGAG8AbgB0ACAAZwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAC4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var util = __webpack_require__(2);
var QueryParamSerializer = __webpack_require__(57);
var Shape = __webpack_require__(9);
var populateHostPrefix = __webpack_require__(19).populateHostPrefix;

function buildRequest(req) {
  var operation = req.service.api.operations[req.operation];
  var httpRequest = req.httpRequest;
  httpRequest.headers['Content-Type'] =
    'application/x-www-form-urlencoded; charset=utf-8';
  httpRequest.params = {
    Version: req.service.api.apiVersion,
    Action: operation.name
  };

  // convert the request parameters into a list of query params,
  // e.g. Deeply.NestedParam.0.Name=value
  var builder = new QueryParamSerializer();
  builder.serialize(req.params, operation.input, function(name, value) {
    httpRequest.params[name] = value;
  });
  httpRequest.body = util.queryParamsToString(httpRequest.params);

  populateHostPrefix(req);
}

function extractError(resp) {
  var data, body = resp.httpResponse.body.toString();
  if (body.match('<UnknownOperationException')) {
    data = {
      Code: 'UnknownOperation',
      Message: 'Unknown operation ' + resp.request.operation
    };
  } else {
    try {
      data = new AWS.XML.Parser().parse(body);
    } catch (e) {
      data = {
        Code: resp.httpResponse.statusCode,
        Message: resp.httpResponse.statusMessage
      };
    }
  }

  if (data.requestId && !resp.requestId) resp.requestId = data.requestId;
  if (data.Errors) data = data.Errors;
  if (data.Error) data = data.Error;
  if (data.Code) {
    resp.error = util.error(new Error(), {
      code: data.Code,
      message: data.Message
    });
  } else {
    resp.error = util.error(new Error(), {
      code: resp.httpResponse.statusCode,
      message: null
    });
  }
}

function extractData(resp) {
  var req = resp.request;
  var operation = req.service.api.operations[req.operation];
  var shape = operation.output || {};
  var origRules = shape;

  if (origRules.resultWrapper) {
    var tmp = Shape.create({type: 'structure'});
    tmp.members[origRules.resultWrapper] = shape;
    tmp.memberNames = [origRules.resultWrapper];
    util.property(shape, 'name', shape.resultWrapper);
    shape = tmp;
  }

  var parser = new AWS.XML.Parser();

  // TODO: Refactor XML Parser to parse RequestId from response.
  if (shape && shape.members && !shape.members._XAMZRequestId) {
    var requestIdShape = Shape.create(
      { type: 'string' },
      { api: { protocol: 'query' } },
      'requestId'
    );
    shape.members._XAMZRequestId = requestIdShape;
  }

  var data = parser.parse(resp.httpResponse.body.toString(), shape);
  resp.requestId = data._XAMZRequestId || data.requestId;

  if (data._XAMZRequestId) delete data._XAMZRequestId;

  if (origRules.resultWrapper) {
    if (data[origRules.resultWrapper]) {
      util.update(data, data[origRules.resultWrapper]);
      delete data[origRules.resultWrapper];
    }
  }

  resp.data = data;
}

/**
 * @api private
 */
module.exports = {
  buildRequest: buildRequest,
  extractError: extractError,
  extractData: extractData
};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

var memoizedProperty = __webpack_require__(2).memoizedProperty;

function memoize(name, value, factory, nameTr) {
  memoizedProperty(this, nameTr(name), function() {
    return factory(name, value);
  });
}

function Collection(iterable, options, factory, nameTr, callback) {
  nameTr = nameTr || String;
  var self = this;

  for (var id in iterable) {
    if (Object.prototype.hasOwnProperty.call(iterable, id)) {
      memoize.call(self, id, iterable[id], factory, nameTr);
      if (callback) callback(id, iterable[id]);
    }
  }
}

/**
 * @api private
 */
module.exports = Collection;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var Rest = __webpack_require__(13);
var Json = __webpack_require__(16);
var JsonBuilder = __webpack_require__(17);
var JsonParser = __webpack_require__(18);

function populateBody(req) {
  var builder = new JsonBuilder();
  var input = req.service.api.operations[req.operation].input;

  if (input.payload) {
    var params = {};
    var payloadShape = input.members[input.payload];
    params = req.params[input.payload];
    if (params === undefined) return;

    if (payloadShape.type === 'structure') {
      req.httpRequest.body = builder.build(params, payloadShape);
      applyContentTypeHeader(req);
    } else { // non-JSON payload
      req.httpRequest.body = params;
      if (payloadShape.type === 'binary' || payloadShape.isStreaming) {
        applyContentTypeHeader(req, true);
      }
    }
  } else {
    var body = builder.build(req.params, input);
    if (body !== '{}' || req.httpRequest.method !== 'GET') { //don't send empty body for GET method
      req.httpRequest.body = body;
    }
    applyContentTypeHeader(req);
  }
}

function applyContentTypeHeader(req, isBinary) {
  var operation = req.service.api.operations[req.operation];
  var input = operation.input;

  if (!req.httpRequest.headers['Content-Type']) {
    var type = isBinary ? 'binary/octet-stream' : 'application/json';
    req.httpRequest.headers['Content-Type'] = type;
  }
}

function buildRequest(req) {
  Rest.buildRequest(req);

  // never send body payload on HEAD/DELETE
  if (['HEAD', 'DELETE'].indexOf(req.httpRequest.method) < 0) {
    populateBody(req);
  }
}

function extractError(resp) {
  Json.extractError(resp);
}

function extractData(resp) {
  Rest.extractData(resp);

  var req = resp.request;
  var operation = req.service.api.operations[req.operation];
  var rules = req.service.api.operations[req.operation].output || {};
  var parser;
  var hasEventOutput = operation.hasEventOutput;

  if (rules.payload) {
    var payloadMember = rules.members[rules.payload];
    var body = resp.httpResponse.body;
    if (payloadMember.isEventStream) {
      parser = new JsonParser();
      resp.data[payload] = util.createEventStream(
        AWS.HttpClient.streamsApiVersion === 2 ? resp.httpResponse.stream : body,
        parser,
        payloadMember
      );
    } else if (payloadMember.type === 'structure' || payloadMember.type === 'list') {
      var parser = new JsonParser();
      resp.data[rules.payload] = parser.parse(body, payloadMember);
    } else if (payloadMember.type === 'binary' || payloadMember.isStreaming) {
      resp.data[rules.payload] = body;
    } else {
      resp.data[rules.payload] = payloadMember.toType(body);
    }
  } else {
    var data = resp.data;
    Json.extractData(resp);
    resp.data = util.merge(data, resp.data);
  }
}

/**
 * @api private
 */
module.exports = {
  buildRequest: buildRequest,
  extractError: extractError,
  extractData: extractData
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var util = __webpack_require__(2);
var Rest = __webpack_require__(13);

function populateBody(req) {
  var input = req.service.api.operations[req.operation].input;
  var builder = new AWS.XML.Builder();
  var params = req.params;

  var payload = input.payload;
  if (payload) {
    var payloadMember = input.members[payload];
    params = params[payload];
    if (params === undefined) return;

    if (payloadMember.type === 'structure') {
      var rootElement = payloadMember.name;
      req.httpRequest.body = builder.toXML(params, payloadMember, rootElement, true);
    } else { // non-xml payload
      req.httpRequest.body = params;
    }
  } else {
    req.httpRequest.body = builder.toXML(params, input, input.name ||
      input.shape || util.string.upperFirst(req.operation) + 'Request');
  }
}

function buildRequest(req) {
  Rest.buildRequest(req);

  // never send body payload on GET/HEAD
  if (['GET', 'HEAD'].indexOf(req.httpRequest.method) < 0) {
    populateBody(req);
  }
}

function extractError(resp) {
  Rest.extractError(resp);

  var data;
  try {
    data = new AWS.XML.Parser().parse(resp.httpResponse.body.toString());
  } catch (e) {
    data = {
      Code: resp.httpResponse.statusCode,
      Message: resp.httpResponse.statusMessage
    };
  }

  if (data.Errors) data = data.Errors;
  if (data.Error) data = data.Error;
  if (data.Code) {
    resp.error = util.error(new Error(), {
      code: data.Code,
      message: data.Message
    });
  } else {
    resp.error = util.error(new Error(), {
      code: resp.httpResponse.statusCode,
      message: null
    });
  }
}

function extractData(resp) {
  Rest.extractData(resp);

  var parser;
  var req = resp.request;
  var body = resp.httpResponse.body;
  var operation = req.service.api.operations[req.operation];
  var output = operation.output;

  var hasEventOutput = operation.hasEventOutput;

  var payload = output.payload;
  if (payload) {
    var payloadMember = output.members[payload];
    if (payloadMember.isEventStream) {
      parser = new AWS.XML.Parser();
      resp.data[payload] = util.createEventStream(
        AWS.HttpClient.streamsApiVersion === 2 ? resp.httpResponse.stream : resp.httpResponse.body,
        parser,
        payloadMember
      );
    } else if (payloadMember.type === 'structure') {
      parser = new AWS.XML.Parser();
      resp.data[payload] = parser.parse(body.toString(), payloadMember);
    } else if (payloadMember.type === 'binary' || payloadMember.isStreaming) {
      resp.data[payload] = body;
    } else {
      resp.data[payload] = payloadMember.toType(body);
    }
  } else if (body.length > 0) {
    parser = new AWS.XML.Parser();
    var data = parser.parse(body.toString(), output);
    util.update(resp.data, data);
  }
}

/**
 * @api private
 */
module.exports = {
  buildRequest: buildRequest,
  extractError: extractError,
  extractData: extractData
};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

var Collection = __webpack_require__(26);
var Operation = __webpack_require__(30);
var Shape = __webpack_require__(9);
var Paginator = __webpack_require__(31);
var ResourceWaiter = __webpack_require__(32);
var metadata = __webpack_require__(33);

var util = __webpack_require__(2);
var property = util.property;
var memoizedProperty = util.memoizedProperty;

function Api(api, options) {
  var self = this;
  api = api || {};
  options = options || {};
  options.api = this;

  api.metadata = api.metadata || {};

  var serviceIdentifier = options.serviceIdentifier;
  delete options.serviceIdentifier;

  property(this, 'isApi', true, false);
  property(this, 'apiVersion', api.metadata.apiVersion);
  property(this, 'endpointPrefix', api.metadata.endpointPrefix);
  property(this, 'signingName', api.metadata.signingName);
  property(this, 'globalEndpoint', api.metadata.globalEndpoint);
  property(this, 'signatureVersion', api.metadata.signatureVersion);
  property(this, 'jsonVersion', api.metadata.jsonVersion);
  property(this, 'targetPrefix', api.metadata.targetPrefix);
  property(this, 'protocol', api.metadata.protocol);
  property(this, 'timestampFormat', api.metadata.timestampFormat);
  property(this, 'xmlNamespaceUri', api.metadata.xmlNamespace);
  property(this, 'abbreviation', api.metadata.serviceAbbreviation);
  property(this, 'fullName', api.metadata.serviceFullName);
  property(this, 'serviceId', api.metadata.serviceId);
  if (serviceIdentifier) {
      property(this, 'xmlNoDefaultLists', metadata[serviceIdentifier].xmlNoDefaultLists, false);
  }

  memoizedProperty(this, 'className', function() {
    var name = api.metadata.serviceAbbreviation || api.metadata.serviceFullName;
    if (!name) return null;

    name = name.replace(/^Amazon|AWS\s*|\(.*|\s+|\W+/g, '');
    if (name === 'ElasticLoadBalancing') name = 'ELB';
    return name;
  });

  function addEndpointOperation(name, operation) {
    if (operation.endpointoperation === true) {
      property(self, 'endpointOperation', util.string.lowerFirst(name));
    }
  }

  property(this, 'operations', new Collection(api.operations, options, function(name, operation) {
    return new Operation(name, operation, options);
  }, util.string.lowerFirst, addEndpointOperation));

  property(this, 'shapes', new Collection(api.shapes, options, function(name, shape) {
    return Shape.create(shape, options);
  }));

  property(this, 'paginators', new Collection(api.paginators, options, function(name, paginator) {
    return new Paginator(name, paginator, options);
  }));

  property(this, 'waiters', new Collection(api.waiters, options, function(name, waiter) {
    return new ResourceWaiter(name, waiter, options);
  }, util.string.lowerFirst));

  if (options.documentation) {
    property(this, 'documentation', api.documentation);
    property(this, 'documentationUrl', api.documentationUrl);
  }
}

/**
 * @api private
 */
module.exports = Api;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var Shape = __webpack_require__(9);

var util = __webpack_require__(2);
var property = util.property;
var memoizedProperty = util.memoizedProperty;

function Operation(name, operation, options) {
  var self = this;
  options = options || {};

  property(this, 'name', operation.name || name);
  property(this, 'api', options.api, false);

  operation.http = operation.http || {};
  property(this, 'endpoint', operation.endpoint);
  property(this, 'httpMethod', operation.http.method || 'POST');
  property(this, 'httpPath', operation.http.requestUri || '/');
  property(this, 'authtype', operation.authtype || '');
  property(
    this,
    'endpointDiscoveryRequired',
    operation.endpointdiscovery ?
      (operation.endpointdiscovery.required ? 'REQUIRED' : 'OPTIONAL') :
    'NULL'
  );

  memoizedProperty(this, 'input', function() {
    if (!operation.input) {
      return new Shape.create({type: 'structure'}, options);
    }
    return Shape.create(operation.input, options);
  });

  memoizedProperty(this, 'output', function() {
    if (!operation.output) {
      return new Shape.create({type: 'structure'}, options);
    }
    return Shape.create(operation.output, options);
  });

  memoizedProperty(this, 'errors', function() {
    var list = [];
    if (!operation.errors) return null;

    for (var i = 0; i < operation.errors.length; i++) {
      list.push(Shape.create(operation.errors[i], options));
    }

    return list;
  });

  memoizedProperty(this, 'paginator', function() {
    return options.api.paginators[name];
  });

  if (options.documentation) {
    property(this, 'documentation', operation.documentation);
    property(this, 'documentationUrl', operation.documentationUrl);
  }

  // idempotentMembers only tracks top-level input shapes
  memoizedProperty(this, 'idempotentMembers', function() {
    var idempotentMembers = [];
    var input = self.input;
    var members = input.members;
    if (!input.members) {
      return idempotentMembers;
    }
    for (var name in members) {
      if (!members.hasOwnProperty(name)) {
        continue;
      }
      if (members[name].isIdempotent === true) {
        idempotentMembers.push(name);
      }
    }
    return idempotentMembers;
  });

  memoizedProperty(this, 'hasEventOutput', function() {
    var output = self.output;
    return hasEventStream(output);
  });
}

function hasEventStream(topLevelShape) {
  var members = topLevelShape.members;
  var payload = topLevelShape.payload;

  if (!topLevelShape.members) {
    return false;
  }

  if (payload) {
    var payloadMember = members[payload];
    return payloadMember.isEventStream;
  }

  // check if any member is an event stream
  for (var name in members) {
    if (!members.hasOwnProperty(name)) {
      if (members[name].isEventStream === true) {
        return true;
      }
    }
  }
  return false;
}

/**
 * @api private
 */
module.exports = Operation;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

var property = __webpack_require__(2).property;

function Paginator(name, paginator) {
  property(this, 'inputToken', paginator.input_token);
  property(this, 'limitKey', paginator.limit_key);
  property(this, 'moreResults', paginator.more_results);
  property(this, 'outputToken', paginator.output_token);
  property(this, 'resultKey', paginator.result_key);
}

/**
 * @api private
 */
module.exports = Paginator;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var property = util.property;

function ResourceWaiter(name, waiter, options) {
  options = options || {};
  property(this, 'name', name);
  property(this, 'api', options.api, false);

  if (waiter.operation) {
    property(this, 'operation', util.string.lowerFirst(waiter.operation));
  }

  var self = this;
  var keys = [
    'type',
    'description',
    'delay',
    'maxAttempts',
    'acceptors'
  ];

  keys.forEach(function(key) {
    var value = waiter[key];
    if (value) {
      property(self, key, value);
    }
  });
}

/**
 * @api private
 */
module.exports = ResourceWaiter;


/***/ }),
/* 33 */
/***/ (function(module) {

module.exports = JSON.parse("{\"acm\":{\"name\":\"ACM\",\"cors\":true},\"apigateway\":{\"name\":\"APIGateway\",\"cors\":true},\"applicationautoscaling\":{\"prefix\":\"application-autoscaling\",\"name\":\"ApplicationAutoScaling\",\"cors\":true},\"appstream\":{\"name\":\"AppStream\"},\"autoscaling\":{\"name\":\"AutoScaling\",\"cors\":true},\"batch\":{\"name\":\"Batch\"},\"budgets\":{\"name\":\"Budgets\"},\"clouddirectory\":{\"name\":\"CloudDirectory\",\"versions\":[\"2016-05-10*\"]},\"cloudformation\":{\"name\":\"CloudFormation\",\"cors\":true},\"cloudfront\":{\"name\":\"CloudFront\",\"versions\":[\"2013-05-12*\",\"2013-11-11*\",\"2014-05-31*\",\"2014-10-21*\",\"2014-11-06*\",\"2015-04-17*\",\"2015-07-27*\",\"2015-09-17*\",\"2016-01-13*\",\"2016-01-28*\",\"2016-08-01*\",\"2016-08-20*\",\"2016-09-07*\",\"2016-09-29*\",\"2016-11-25*\",\"2017-03-25*\",\"2017-10-30*\",\"2018-06-18*\",\"2018-11-05*\"],\"cors\":true},\"cloudhsm\":{\"name\":\"CloudHSM\",\"cors\":true},\"cloudsearch\":{\"name\":\"CloudSearch\"},\"cloudsearchdomain\":{\"name\":\"CloudSearchDomain\"},\"cloudtrail\":{\"name\":\"CloudTrail\",\"cors\":true},\"cloudwatch\":{\"prefix\":\"monitoring\",\"name\":\"CloudWatch\",\"cors\":true},\"cloudwatchevents\":{\"prefix\":\"events\",\"name\":\"CloudWatchEvents\",\"versions\":[\"2014-02-03*\"],\"cors\":true},\"cloudwatchlogs\":{\"prefix\":\"logs\",\"name\":\"CloudWatchLogs\",\"cors\":true},\"codebuild\":{\"name\":\"CodeBuild\",\"cors\":true},\"codecommit\":{\"name\":\"CodeCommit\",\"cors\":true},\"codedeploy\":{\"name\":\"CodeDeploy\",\"cors\":true},\"codepipeline\":{\"name\":\"CodePipeline\",\"cors\":true},\"cognitoidentity\":{\"prefix\":\"cognito-identity\",\"name\":\"CognitoIdentity\",\"cors\":true},\"cognitoidentityserviceprovider\":{\"prefix\":\"cognito-idp\",\"name\":\"CognitoIdentityServiceProvider\",\"cors\":true},\"cognitosync\":{\"prefix\":\"cognito-sync\",\"name\":\"CognitoSync\",\"cors\":true},\"configservice\":{\"prefix\":\"config\",\"name\":\"ConfigService\",\"cors\":true},\"cur\":{\"name\":\"CUR\",\"cors\":true},\"datapipeline\":{\"name\":\"DataPipeline\"},\"devicefarm\":{\"name\":\"DeviceFarm\",\"cors\":true},\"directconnect\":{\"name\":\"DirectConnect\",\"cors\":true},\"directoryservice\":{\"prefix\":\"ds\",\"name\":\"DirectoryService\"},\"discovery\":{\"name\":\"Discovery\"},\"dms\":{\"name\":\"DMS\"},\"dynamodb\":{\"name\":\"DynamoDB\",\"cors\":true},\"dynamodbstreams\":{\"prefix\":\"streams.dynamodb\",\"name\":\"DynamoDBStreams\",\"cors\":true},\"ec2\":{\"name\":\"EC2\",\"versions\":[\"2013-06-15*\",\"2013-10-15*\",\"2014-02-01*\",\"2014-05-01*\",\"2014-06-15*\",\"2014-09-01*\",\"2014-10-01*\",\"2015-03-01*\",\"2015-04-15*\",\"2015-10-01*\",\"2016-04-01*\",\"2016-09-15*\"],\"cors\":true},\"ecr\":{\"name\":\"ECR\",\"cors\":true},\"ecs\":{\"name\":\"ECS\",\"cors\":true},\"efs\":{\"prefix\":\"elasticfilesystem\",\"name\":\"EFS\",\"cors\":true},\"elasticache\":{\"name\":\"ElastiCache\",\"versions\":[\"2012-11-15*\",\"2014-03-24*\",\"2014-07-15*\",\"2014-09-30*\"],\"cors\":true},\"elasticbeanstalk\":{\"name\":\"ElasticBeanstalk\",\"cors\":true},\"elb\":{\"prefix\":\"elasticloadbalancing\",\"name\":\"ELB\",\"cors\":true},\"elbv2\":{\"prefix\":\"elasticloadbalancingv2\",\"name\":\"ELBv2\",\"cors\":true},\"emr\":{\"prefix\":\"elasticmapreduce\",\"name\":\"EMR\",\"cors\":true},\"es\":{\"name\":\"ES\"},\"elastictranscoder\":{\"name\":\"ElasticTranscoder\",\"cors\":true},\"firehose\":{\"name\":\"Firehose\",\"cors\":true},\"gamelift\":{\"name\":\"GameLift\",\"cors\":true},\"glacier\":{\"name\":\"Glacier\"},\"health\":{\"name\":\"Health\"},\"iam\":{\"name\":\"IAM\",\"cors\":true},\"importexport\":{\"name\":\"ImportExport\"},\"inspector\":{\"name\":\"Inspector\",\"versions\":[\"2015-08-18*\"],\"cors\":true},\"iot\":{\"name\":\"Iot\",\"cors\":true},\"iotdata\":{\"prefix\":\"iot-data\",\"name\":\"IotData\",\"cors\":true},\"kinesis\":{\"name\":\"Kinesis\",\"cors\":true},\"kinesisanalytics\":{\"name\":\"KinesisAnalytics\"},\"kms\":{\"name\":\"KMS\",\"cors\":true},\"lambda\":{\"name\":\"Lambda\",\"cors\":true},\"lexruntime\":{\"prefix\":\"runtime.lex\",\"name\":\"LexRuntime\",\"cors\":true},\"lightsail\":{\"name\":\"Lightsail\"},\"machinelearning\":{\"name\":\"MachineLearning\",\"cors\":true},\"marketplacecommerceanalytics\":{\"name\":\"MarketplaceCommerceAnalytics\",\"cors\":true},\"marketplacemetering\":{\"prefix\":\"meteringmarketplace\",\"name\":\"MarketplaceMetering\"},\"mturk\":{\"prefix\":\"mturk-requester\",\"name\":\"MTurk\",\"cors\":true},\"mobileanalytics\":{\"name\":\"MobileAnalytics\",\"cors\":true},\"opsworks\":{\"name\":\"OpsWorks\",\"cors\":true},\"opsworkscm\":{\"name\":\"OpsWorksCM\"},\"organizations\":{\"name\":\"Organizations\"},\"pinpoint\":{\"name\":\"Pinpoint\"},\"polly\":{\"name\":\"Polly\",\"cors\":true},\"rds\":{\"name\":\"RDS\",\"versions\":[\"2014-09-01*\"],\"cors\":true},\"redshift\":{\"name\":\"Redshift\",\"cors\":true},\"rekognition\":{\"name\":\"Rekognition\",\"cors\":true},\"resourcegroupstaggingapi\":{\"name\":\"ResourceGroupsTaggingAPI\"},\"route53\":{\"name\":\"Route53\",\"cors\":true},\"route53domains\":{\"name\":\"Route53Domains\",\"cors\":true},\"s3\":{\"name\":\"S3\",\"dualstackAvailable\":true,\"cors\":true},\"s3control\":{\"name\":\"S3Control\",\"dualstackAvailable\":true,\"xmlNoDefaultLists\":true},\"servicecatalog\":{\"name\":\"ServiceCatalog\",\"cors\":true},\"ses\":{\"prefix\":\"email\",\"name\":\"SES\",\"cors\":true},\"shield\":{\"name\":\"Shield\"},\"simpledb\":{\"prefix\":\"sdb\",\"name\":\"SimpleDB\"},\"sms\":{\"name\":\"SMS\"},\"snowball\":{\"name\":\"Snowball\"},\"sns\":{\"name\":\"SNS\",\"cors\":true},\"sqs\":{\"name\":\"SQS\",\"cors\":true},\"ssm\":{\"name\":\"SSM\",\"cors\":true},\"storagegateway\":{\"name\":\"StorageGateway\",\"cors\":true},\"stepfunctions\":{\"prefix\":\"states\",\"name\":\"StepFunctions\"},\"sts\":{\"name\":\"STS\",\"cors\":true},\"support\":{\"name\":\"Support\"},\"swf\":{\"name\":\"SWF\"},\"xray\":{\"name\":\"XRay\",\"cors\":true},\"waf\":{\"name\":\"WAF\",\"cors\":true},\"wafregional\":{\"prefix\":\"waf-regional\",\"name\":\"WAFRegional\"},\"workdocs\":{\"name\":\"WorkDocs\",\"cors\":true},\"workspaces\":{\"name\":\"WorkSpaces\"},\"codestar\":{\"name\":\"CodeStar\"},\"lexmodelbuildingservice\":{\"prefix\":\"lex-models\",\"name\":\"LexModelBuildingService\",\"cors\":true},\"marketplaceentitlementservice\":{\"prefix\":\"entitlement.marketplace\",\"name\":\"MarketplaceEntitlementService\"},\"athena\":{\"name\":\"Athena\"},\"greengrass\":{\"name\":\"Greengrass\"},\"dax\":{\"name\":\"DAX\"},\"migrationhub\":{\"prefix\":\"AWSMigrationHub\",\"name\":\"MigrationHub\"},\"cloudhsmv2\":{\"name\":\"CloudHSMV2\"},\"glue\":{\"name\":\"Glue\"},\"mobile\":{\"name\":\"Mobile\"},\"pricing\":{\"name\":\"Pricing\",\"cors\":true},\"costexplorer\":{\"prefix\":\"ce\",\"name\":\"CostExplorer\",\"cors\":true},\"mediaconvert\":{\"name\":\"MediaConvert\"},\"medialive\":{\"name\":\"MediaLive\"},\"mediapackage\":{\"name\":\"MediaPackage\"},\"mediastore\":{\"name\":\"MediaStore\"},\"mediastoredata\":{\"prefix\":\"mediastore-data\",\"name\":\"MediaStoreData\",\"cors\":true},\"appsync\":{\"name\":\"AppSync\"},\"guardduty\":{\"name\":\"GuardDuty\"},\"mq\":{\"name\":\"MQ\"},\"comprehend\":{\"name\":\"Comprehend\",\"cors\":true},\"iotjobsdataplane\":{\"prefix\":\"iot-jobs-data\",\"name\":\"IoTJobsDataPlane\"},\"kinesisvideoarchivedmedia\":{\"prefix\":\"kinesis-video-archived-media\",\"name\":\"KinesisVideoArchivedMedia\",\"cors\":true},\"kinesisvideomedia\":{\"prefix\":\"kinesis-video-media\",\"name\":\"KinesisVideoMedia\",\"cors\":true},\"kinesisvideo\":{\"name\":\"KinesisVideo\",\"cors\":true},\"sagemakerruntime\":{\"prefix\":\"runtime.sagemaker\",\"name\":\"SageMakerRuntime\"},\"sagemaker\":{\"name\":\"SageMaker\"},\"translate\":{\"name\":\"Translate\",\"cors\":true},\"resourcegroups\":{\"prefix\":\"resource-groups\",\"name\":\"ResourceGroups\",\"cors\":true},\"alexaforbusiness\":{\"name\":\"AlexaForBusiness\"},\"cloud9\":{\"name\":\"Cloud9\"},\"serverlessapplicationrepository\":{\"prefix\":\"serverlessrepo\",\"name\":\"ServerlessApplicationRepository\"},\"servicediscovery\":{\"name\":\"ServiceDiscovery\"},\"workmail\":{\"name\":\"WorkMail\"},\"autoscalingplans\":{\"prefix\":\"autoscaling-plans\",\"name\":\"AutoScalingPlans\"},\"transcribeservice\":{\"prefix\":\"transcribe\",\"name\":\"TranscribeService\"},\"connect\":{\"name\":\"Connect\",\"cors\":true},\"acmpca\":{\"prefix\":\"acm-pca\",\"name\":\"ACMPCA\"},\"fms\":{\"name\":\"FMS\"},\"secretsmanager\":{\"name\":\"SecretsManager\",\"cors\":true},\"iotanalytics\":{\"name\":\"IoTAnalytics\",\"cors\":true},\"iot1clickdevicesservice\":{\"prefix\":\"iot1click-devices\",\"name\":\"IoT1ClickDevicesService\"},\"iot1clickprojects\":{\"prefix\":\"iot1click-projects\",\"name\":\"IoT1ClickProjects\"},\"pi\":{\"name\":\"PI\"},\"neptune\":{\"name\":\"Neptune\"},\"mediatailor\":{\"name\":\"MediaTailor\"},\"eks\":{\"name\":\"EKS\"},\"macie\":{\"name\":\"Macie\"},\"dlm\":{\"name\":\"DLM\"},\"signer\":{\"name\":\"Signer\"},\"chime\":{\"name\":\"Chime\"},\"pinpointemail\":{\"prefix\":\"pinpoint-email\",\"name\":\"PinpointEmail\"},\"ram\":{\"name\":\"RAM\"},\"route53resolver\":{\"name\":\"Route53Resolver\"},\"pinpointsmsvoice\":{\"prefix\":\"sms-voice\",\"name\":\"PinpointSMSVoice\"},\"quicksight\":{\"name\":\"QuickSight\"},\"rdsdataservice\":{\"prefix\":\"rds-data\",\"name\":\"RDSDataService\"},\"amplify\":{\"name\":\"Amplify\"},\"datasync\":{\"name\":\"DataSync\"},\"robomaker\":{\"name\":\"RoboMaker\"},\"transfer\":{\"name\":\"Transfer\"},\"globalaccelerator\":{\"name\":\"GlobalAccelerator\"},\"comprehendmedical\":{\"name\":\"ComprehendMedical\",\"cors\":true},\"kinesisanalyticsv2\":{\"name\":\"KinesisAnalyticsV2\"},\"mediaconnect\":{\"name\":\"MediaConnect\"},\"fsx\":{\"name\":\"FSx\"},\"securityhub\":{\"name\":\"SecurityHub\"},\"appmesh\":{\"name\":\"AppMesh\",\"versions\":[\"2018-10-01*\"]},\"licensemanager\":{\"prefix\":\"license-manager\",\"name\":\"LicenseManager\"},\"kafka\":{\"name\":\"Kafka\"},\"apigatewaymanagementapi\":{\"name\":\"ApiGatewayManagementApi\"},\"apigatewayv2\":{\"name\":\"ApiGatewayV2\"},\"docdb\":{\"name\":\"DocDB\"},\"backup\":{\"name\":\"Backup\"},\"worklink\":{\"name\":\"WorkLink\"},\"textract\":{\"name\":\"Textract\"},\"managedblockchain\":{\"name\":\"ManagedBlockchain\"},\"mediapackagevod\":{\"prefix\":\"mediapackage-vod\",\"name\":\"MediaPackageVod\"},\"groundstation\":{\"name\":\"GroundStation\"},\"iotthingsgraph\":{\"name\":\"IoTThingsGraph\"},\"iotevents\":{\"name\":\"IoTEvents\"},\"ioteventsdata\":{\"prefix\":\"iotevents-data\",\"name\":\"IoTEventsData\"},\"personalize\":{\"name\":\"Personalize\",\"cors\":true},\"personalizeevents\":{\"prefix\":\"personalize-events\",\"name\":\"PersonalizeEvents\",\"cors\":true},\"personalizeruntime\":{\"prefix\":\"personalize-runtime\",\"name\":\"PersonalizeRuntime\",\"cors\":true},\"applicationinsights\":{\"prefix\":\"application-insights\",\"name\":\"ApplicationInsights\"},\"servicequotas\":{\"prefix\":\"service-quotas\",\"name\":\"ServiceQuotas\"},\"ec2instanceconnect\":{\"prefix\":\"ec2-instance-connect\",\"name\":\"EC2InstanceConnect\"},\"eventbridge\":{\"name\":\"EventBridge\"},\"lakeformation\":{\"name\":\"LakeFormation\"},\"forecastservice\":{\"prefix\":\"forecast\",\"name\":\"ForecastService\",\"cors\":true},\"forecastqueryservice\":{\"prefix\":\"forecastquery\",\"name\":\"ForecastQueryService\",\"cors\":true},\"qldb\":{\"name\":\"QLDB\"},\"qldbsession\":{\"prefix\":\"qldb-session\",\"name\":\"QLDBSession\"},\"workmailmessageflow\":{\"name\":\"WorkMailMessageFlow\"},\"codestarnotifications\":{\"prefix\":\"codestar-notifications\",\"name\":\"CodeStarNotifications\"},\"savingsplans\":{\"name\":\"SavingsPlans\"},\"sso\":{\"name\":\"SSO\"},\"ssooidc\":{\"prefix\":\"sso-oidc\",\"name\":\"SSOOIDC\"},\"marketplacecatalog\":{\"prefix\":\"marketplace-catalog\",\"name\":\"MarketplaceCatalog\"},\"dataexchange\":{\"name\":\"DataExchange\"},\"sesv2\":{\"name\":\"SESV2\"},\"migrationhubconfig\":{\"prefix\":\"migrationhub-config\",\"name\":\"MigrationHubConfig\"},\"connectparticipant\":{\"name\":\"ConnectParticipant\"},\"appconfig\":{\"name\":\"AppConfig\"},\"iotsecuretunneling\":{\"name\":\"IoTSecureTunneling\"},\"wafv2\":{\"name\":\"WAFV2\"},\"elasticinference\":{\"prefix\":\"elastic-inference\",\"name\":\"ElasticInference\"},\"imagebuilder\":{\"name\":\"Imagebuilder\"},\"schemas\":{\"name\":\"Schemas\"},\"accessanalyzer\":{\"name\":\"AccessAnalyzer\"},\"codegurureviewer\":{\"prefix\":\"codeguru-reviewer\",\"name\":\"CodeGuruReviewer\"},\"codeguruprofiler\":{\"name\":\"CodeGuruProfiler\"},\"computeoptimizer\":{\"prefix\":\"compute-optimizer\",\"name\":\"ComputeOptimizer\"},\"frauddetector\":{\"name\":\"FraudDetector\"},\"kendra\":{\"name\":\"Kendra\"},\"networkmanager\":{\"name\":\"NetworkManager\"},\"outposts\":{\"name\":\"Outposts\"},\"augmentedairuntime\":{\"prefix\":\"sagemaker-a2i-runtime\",\"name\":\"AugmentedAIRuntime\"},\"ebs\":{\"name\":\"EBS\"},\"kinesisvideosignalingchannels\":{\"prefix\":\"kinesis-video-signaling\",\"name\":\"KinesisVideoSignalingChannels\",\"cors\":true},\"detective\":{\"name\":\"Detective\"},\"codestarconnections\":{\"prefix\":\"codestar-connections\",\"name\":\"CodeStarconnections\"}}");

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

/**
 * @api private
 * @!method on(eventName, callback)
 *   Registers an event listener callback for the event given by `eventName`.
 *   Parameters passed to the callback function depend on the individual event
 *   being triggered. See the event documentation for those parameters.
 *
 *   @param eventName [String] the event name to register the listener for
 *   @param callback [Function] the listener callback function
 *   @param toHead [Boolean] attach the listener callback to the head of callback array if set to true.
 *     Default to be false.
 *   @return [AWS.SequentialExecutor] the same object for chaining
 */
AWS.SequentialExecutor = AWS.util.inherit({

  constructor: function SequentialExecutor() {
    this._events = {};
  },

  /**
   * @api private
   */
  listeners: function listeners(eventName) {
    return this._events[eventName] ? this._events[eventName].slice(0) : [];
  },

  on: function on(eventName, listener, toHead) {
    if (this._events[eventName]) {
      toHead ?
        this._events[eventName].unshift(listener) :
        this._events[eventName].push(listener);
    } else {
      this._events[eventName] = [listener];
    }
    return this;
  },

  onAsync: function onAsync(eventName, listener, toHead) {
    listener._isAsync = true;
    return this.on(eventName, listener, toHead);
  },

  removeListener: function removeListener(eventName, listener) {
    var listeners = this._events[eventName];
    if (listeners) {
      var length = listeners.length;
      var position = -1;
      for (var i = 0; i < length; ++i) {
        if (listeners[i] === listener) {
          position = i;
        }
      }
      if (position > -1) {
        listeners.splice(position, 1);
      }
    }
    return this;
  },

  removeAllListeners: function removeAllListeners(eventName) {
    if (eventName) {
      delete this._events[eventName];
    } else {
      this._events = {};
    }
    return this;
  },

  /**
   * @api private
   */
  emit: function emit(eventName, eventArgs, doneCallback) {
    if (!doneCallback) doneCallback = function() { };
    var listeners = this.listeners(eventName);
    var count = listeners.length;
    this.callListeners(listeners, eventArgs, doneCallback);
    return count > 0;
  },

  /**
   * @api private
   */
  callListeners: function callListeners(listeners, args, doneCallback, prevError) {
    var self = this;
    var error = prevError || null;

    function callNextListener(err) {
      if (err) {
        error = AWS.util.error(error || new Error(), err);
        if (self._haltHandlersOnError) {
          return doneCallback.call(self, error);
        }
      }
      self.callListeners(listeners, args, doneCallback, error);
    }

    while (listeners.length > 0) {
      var listener = listeners.shift();
      if (listener._isAsync) { // asynchronous listener
        listener.apply(self, args.concat([callNextListener]));
        return; // stop here, callNextListener will continue
      } else { // synchronous listener
        try {
          listener.apply(self, args);
        } catch (err) {
          error = AWS.util.error(error || new Error(), err);
        }
        if (error && self._haltHandlersOnError) {
          doneCallback.call(self, error);
          return;
        }
      }
    }
    doneCallback.call(self, error);
  },

  /**
   * Adds or copies a set of listeners from another list of
   * listeners or SequentialExecutor object.
   *
   * @param listeners [map<String,Array<Function>>, AWS.SequentialExecutor]
   *   a list of events and callbacks, or an event emitter object
   *   containing listeners to add to this emitter object.
   * @return [AWS.SequentialExecutor] the emitter object, for chaining.
   * @example Adding listeners from a map of listeners
   *   emitter.addListeners({
   *     event1: [function() { ... }, function() { ... }],
   *     event2: [function() { ... }]
   *   });
   *   emitter.emit('event1'); // emitter has event1
   *   emitter.emit('event2'); // emitter has event2
   * @example Adding listeners from another emitter object
   *   var emitter1 = new AWS.SequentialExecutor();
   *   emitter1.on('event1', function() { ... });
   *   emitter1.on('event2', function() { ... });
   *   var emitter2 = new AWS.SequentialExecutor();
   *   emitter2.addListeners(emitter1);
   *   emitter2.emit('event1'); // emitter2 has event1
   *   emitter2.emit('event2'); // emitter2 has event2
   */
  addListeners: function addListeners(listeners) {
    var self = this;

    // extract listeners if parameter is an SequentialExecutor object
    if (listeners._events) listeners = listeners._events;

    AWS.util.each(listeners, function(event, callbacks) {
      if (typeof callbacks === 'function') callbacks = [callbacks];
      AWS.util.arrayEach(callbacks, function(callback) {
        self.on(event, callback);
      });
    });

    return self;
  },

  /**
   * Registers an event with {on} and saves the callback handle function
   * as a property on the emitter object using a given `name`.
   *
   * @param name [String] the property name to set on this object containing
   *   the callback function handle so that the listener can be removed in
   *   the future.
   * @param (see on)
   * @return (see on)
   * @example Adding a named listener DATA_CALLBACK
   *   var listener = function() { doSomething(); };
   *   emitter.addNamedListener('DATA_CALLBACK', 'data', listener);
   *
   *   // the following prints: true
   *   console.log(emitter.DATA_CALLBACK == listener);
   */
  addNamedListener: function addNamedListener(name, eventName, callback, toHead) {
    this[name] = callback;
    this.addListener(eventName, callback, toHead);
    return this;
  },

  /**
   * @api private
   */
  addNamedAsyncListener: function addNamedAsyncListener(name, eventName, callback, toHead) {
    callback._isAsync = true;
    return this.addNamedListener(name, eventName, callback, toHead);
  },

  /**
   * Helper method to add a set of named listeners using
   * {addNamedListener}. The callback contains a parameter
   * with a handle to the `addNamedListener` method.
   *
   * @callback callback function(add)
   *   The callback function is called immediately in order to provide
   *   the `add` function to the block. This simplifies the addition of
   *   a large group of named listeners.
   *   @param add [Function] the {addNamedListener} function to call
   *     when registering listeners.
   * @example Adding a set of named listeners
   *   emitter.addNamedListeners(function(add) {
   *     add('DATA_CALLBACK', 'data', function() { ... });
   *     add('OTHER', 'otherEvent', function() { ... });
   *     add('LAST', 'lastEvent', function() { ... });
   *   });
   *
   *   // these properties are now set:
   *   emitter.DATA_CALLBACK;
   *   emitter.OTHER;
   *   emitter.LAST;
   */
  addNamedListeners: function addNamedListeners(callback) {
    var self = this;
    callback(
      function() {
        self.addNamedListener.apply(self, arguments);
      },
      function() {
        self.addNamedAsyncListener.apply(self, arguments);
      }
    );
    return this;
  }
});

/**
 * {on} is the prefered method.
 * @api private
 */
AWS.SequentialExecutor.prototype.addListener = AWS.SequentialExecutor.prototype.on;

/**
 * @api private
 */
module.exports = AWS.SequentialExecutor;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

/**
 * Represents your AWS security credentials, specifically the
 * {accessKeyId}, {secretAccessKey}, and optional {sessionToken}.
 * Creating a `Credentials` object allows you to pass around your
 * security information to configuration and service objects.
 *
 * Note that this class typically does not need to be constructed manually,
 * as the {AWS.Config} and {AWS.Service} classes both accept simple
 * options hashes with the three keys. These structures will be converted
 * into Credentials objects automatically.
 *
 * ## Expiring and Refreshing Credentials
 *
 * Occasionally credentials can expire in the middle of a long-running
 * application. In this case, the SDK will automatically attempt to
 * refresh the credentials from the storage location if the Credentials
 * class implements the {refresh} method.
 *
 * If you are implementing a credential storage location, you
 * will want to create a subclass of the `Credentials` class and
 * override the {refresh} method. This method allows credentials to be
 * retrieved from the backing store, be it a file system, database, or
 * some network storage. The method should reset the credential attributes
 * on the object.
 *
 * @!attribute expired
 *   @return [Boolean] whether the credentials have been expired and
 *     require a refresh. Used in conjunction with {expireTime}.
 * @!attribute expireTime
 *   @return [Date] a time when credentials should be considered expired. Used
 *     in conjunction with {expired}.
 * @!attribute accessKeyId
 *   @return [String] the AWS access key ID
 * @!attribute secretAccessKey
 *   @return [String] the AWS secret access key
 * @!attribute sessionToken
 *   @return [String] an optional AWS session token
 */
AWS.Credentials = AWS.util.inherit({
  /**
   * A credentials object can be created using positional arguments or an options
   * hash.
   *
   * @overload AWS.Credentials(accessKeyId, secretAccessKey, sessionToken=null)
   *   Creates a Credentials object with a given set of credential information
   *   as positional arguments.
   *   @param accessKeyId [String] the AWS access key ID
   *   @param secretAccessKey [String] the AWS secret access key
   *   @param sessionToken [String] the optional AWS session token
   *   @example Create a credentials object with AWS credentials
   *     var creds = new AWS.Credentials('akid', 'secret', 'session');
   * @overload AWS.Credentials(options)
   *   Creates a Credentials object with a given set of credential information
   *   as an options hash.
   *   @option options accessKeyId [String] the AWS access key ID
   *   @option options secretAccessKey [String] the AWS secret access key
   *   @option options sessionToken [String] the optional AWS session token
   *   @example Create a credentials object with AWS credentials
   *     var creds = new AWS.Credentials({
   *       accessKeyId: 'akid', secretAccessKey: 'secret', sessionToken: 'session'
   *     });
   */
  constructor: function Credentials() {
    // hide secretAccessKey from being displayed with util.inspect
    AWS.util.hideProperties(this, ['secretAccessKey']);

    this.expired = false;
    this.expireTime = null;
    this.refreshCallbacks = [];
    if (arguments.length === 1 && typeof arguments[0] === 'object') {
      var creds = arguments[0].credentials || arguments[0];
      this.accessKeyId = creds.accessKeyId;
      this.secretAccessKey = creds.secretAccessKey;
      this.sessionToken = creds.sessionToken;
    } else {
      this.accessKeyId = arguments[0];
      this.secretAccessKey = arguments[1];
      this.sessionToken = arguments[2];
    }
  },

  /**
   * @return [Integer] the number of seconds before {expireTime} during which
   *   the credentials will be considered expired.
   */
  expiryWindow: 15,

  /**
   * @return [Boolean] whether the credentials object should call {refresh}
   * @note Subclasses should override this method to provide custom refresh
   *   logic.
   */
  needsRefresh: function needsRefresh() {
    var currentTime = AWS.util.date.getDate().getTime();
    var adjustedTime = new Date(currentTime + this.expiryWindow * 1000);

    if (this.expireTime && adjustedTime > this.expireTime) {
      return true;
    } else {
      return this.expired || !this.accessKeyId || !this.secretAccessKey;
    }
  },

  /**
   * Gets the existing credentials, refreshing them if they are not yet loaded
   * or have expired. Users should call this method before using {refresh},
   * as this will not attempt to reload credentials when they are already
   * loaded into the object.
   *
   * @callback callback function(err)
   *   When this callback is called with no error, it means either credentials
   *   do not need to be refreshed or refreshed credentials information has
   *   been loaded into the object (as the `accessKeyId`, `secretAccessKey`,
   *   and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   */
  get: function get(callback) {
    var self = this;
    if (this.needsRefresh()) {
      this.refresh(function(err) {
        if (!err) self.expired = false; // reset expired flag
        if (callback) callback(err);
      });
    } else if (callback) {
      callback();
    }
  },

  /**
   * @!method  getPromise()
   *   Returns a 'thenable' promise.
   *   Gets the existing credentials, refreshing them if they are not yet loaded
   *   or have expired. Users should call this method before using {refresh},
   *   as this will not attempt to reload credentials when they are already
   *   loaded into the object.
   *
   *   Two callbacks can be provided to the `then` method on the returned promise.
   *   The first callback will be called if the promise is fulfilled, and the second
   *   callback will be called if the promise is rejected.
   *   @callback fulfilledCallback function()
   *     Called if the promise is fulfilled. When this callback is called, it
   *     means either credentials do not need to be refreshed or refreshed
   *     credentials information has been loaded into the object (as the
   *     `accessKeyId`, `secretAccessKey`, and `sessionToken` properties).
   *   @callback rejectedCallback function(err)
   *     Called if the promise is rejected.
   *     @param err [Error] if an error occurred, this value will be filled
   *   @return [Promise] A promise that represents the state of the `get` call.
   *   @example Calling the `getPromise` method.
   *     var promise = credProvider.getPromise();
   *     promise.then(function() { ... }, function(err) { ... });
   */

  /**
   * @!method  refreshPromise()
   *   Returns a 'thenable' promise.
   *   Refreshes the credentials. Users should call {get} before attempting
   *   to forcibly refresh credentials.
   *
   *   Two callbacks can be provided to the `then` method on the returned promise.
   *   The first callback will be called if the promise is fulfilled, and the second
   *   callback will be called if the promise is rejected.
   *   @callback fulfilledCallback function()
   *     Called if the promise is fulfilled. When this callback is called, it
   *     means refreshed credentials information has been loaded into the object
   *     (as the `accessKeyId`, `secretAccessKey`, and `sessionToken` properties).
   *   @callback rejectedCallback function(err)
   *     Called if the promise is rejected.
   *     @param err [Error] if an error occurred, this value will be filled
   *   @return [Promise] A promise that represents the state of the `refresh` call.
   *   @example Calling the `refreshPromise` method.
   *     var promise = credProvider.refreshPromise();
   *     promise.then(function() { ... }, function(err) { ... });
   */

  /**
   * Refreshes the credentials. Users should call {get} before attempting
   * to forcibly refresh credentials.
   *
   * @callback callback function(err)
   *   When this callback is called with no error, it means refreshed
   *   credentials information has been loaded into the object (as the
   *   `accessKeyId`, `secretAccessKey`, and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   * @note Subclasses should override this class to reset the
   *   {accessKeyId}, {secretAccessKey} and optional {sessionToken}
   *   on the credentials object and then call the callback with
   *   any error information.
   * @see get
   */
  refresh: function refresh(callback) {
    this.expired = false;
    callback();
  },

  /**
   * @api private
   * @param callback
   */
  coalesceRefresh: function coalesceRefresh(callback, sync) {
    var self = this;
    if (self.refreshCallbacks.push(callback) === 1) {
      self.load(function onLoad(err) {
        AWS.util.arrayEach(self.refreshCallbacks, function(callback) {
          if (sync) {
            callback(err);
          } else {
            // callback could throw, so defer to ensure all callbacks are notified
            AWS.util.defer(function () {
              callback(err);
            });
          }
        });
        self.refreshCallbacks.length = 0;
      });
    }
  },

  /**
   * @api private
   * @param callback
   */
  load: function load(callback) {
    callback();
  }
});

/**
 * @api private
 */
AWS.Credentials.addPromisesToClass = function addPromisesToClass(PromiseDependency) {
  this.prototype.getPromise = AWS.util.promisifyMethod('get', PromiseDependency);
  this.prototype.refreshPromise = AWS.util.promisifyMethod('refresh', PromiseDependency);
};

/**
 * @api private
 */
AWS.Credentials.deletePromisesFromClass = function deletePromisesFromClass() {
  delete this.prototype.getPromise;
  delete this.prototype.refreshPromise;
};

AWS.util.addPromises(AWS.Credentials);


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

/**
 * Creates a credential provider chain that searches for AWS credentials
 * in a list of credential providers specified by the {providers} property.
 *
 * By default, the chain will use the {defaultProviders} to resolve credentials.
 * These providers will look in the environment using the
 * {AWS.EnvironmentCredentials} class with the 'AWS' and 'AMAZON' prefixes.
 *
 * ## Setting Providers
 *
 * Each provider in the {providers} list should be a function that returns
 * a {AWS.Credentials} object, or a hardcoded credentials object. The function
 * form allows for delayed execution of the credential construction.
 *
 * ## Resolving Credentials from a Chain
 *
 * Call {resolve} to return the first valid credential object that can be
 * loaded by the provider chain.
 *
 * For example, to resolve a chain with a custom provider that checks a file
 * on disk after the set of {defaultProviders}:
 *
 * ```javascript
 * var diskProvider = new AWS.FileSystemCredentials('./creds.json');
 * var chain = new AWS.CredentialProviderChain();
 * chain.providers.push(diskProvider);
 * chain.resolve();
 * ```
 *
 * The above code will return the `diskProvider` object if the
 * file contains credentials and the `defaultProviders` do not contain
 * any credential settings.
 *
 * @!attribute providers
 *   @return [Array<AWS.Credentials, Function>]
 *     a list of credentials objects or functions that return credentials
 *     objects. If the provider is a function, the function will be
 *     executed lazily when the provider needs to be checked for valid
 *     credentials. By default, this object will be set to the
 *     {defaultProviders}.
 *   @see defaultProviders
 */
AWS.CredentialProviderChain = AWS.util.inherit(AWS.Credentials, {

  /**
   * Creates a new CredentialProviderChain with a default set of providers
   * specified by {defaultProviders}.
   */
  constructor: function CredentialProviderChain(providers) {
    if (providers) {
      this.providers = providers;
    } else {
      this.providers = AWS.CredentialProviderChain.defaultProviders.slice(0);
    }
    this.resolveCallbacks = [];
  },

  /**
   * @!method  resolvePromise()
   *   Returns a 'thenable' promise.
   *   Resolves the provider chain by searching for the first set of
   *   credentials in {providers}.
   *
   *   Two callbacks can be provided to the `then` method on the returned promise.
   *   The first callback will be called if the promise is fulfilled, and the second
   *   callback will be called if the promise is rejected.
   *   @callback fulfilledCallback function(credentials)
   *     Called if the promise is fulfilled and the provider resolves the chain
   *     to a credentials object
   *     @param credentials [AWS.Credentials] the credentials object resolved
   *       by the provider chain.
   *   @callback rejectedCallback function(error)
   *     Called if the promise is rejected.
   *     @param err [Error] the error object returned if no credentials are found.
   *   @return [Promise] A promise that represents the state of the `resolve` method call.
   *   @example Calling the `resolvePromise` method.
   *     var promise = chain.resolvePromise();
   *     promise.then(function(credentials) { ... }, function(err) { ... });
   */

  /**
   * Resolves the provider chain by searching for the first set of
   * credentials in {providers}.
   *
   * @callback callback function(err, credentials)
   *   Called when the provider resolves the chain to a credentials object
   *   or null if no credentials can be found.
   *
   *   @param err [Error] the error object returned if no credentials are
   *     found.
   *   @param credentials [AWS.Credentials] the credentials object resolved
   *     by the provider chain.
   * @return [AWS.CredentialProviderChain] the provider, for chaining.
   */
  resolve: function resolve(callback) {
    var self = this;
    if (self.providers.length === 0) {
      callback(new Error('No providers'));
      return self;
    }

    if (self.resolveCallbacks.push(callback) === 1) {
      var index = 0;
      var providers = self.providers.slice(0);

      function resolveNext(err, creds) {
        if ((!err && creds) || index === providers.length) {
          AWS.util.arrayEach(self.resolveCallbacks, function (callback) {
            callback(err, creds);
          });
          self.resolveCallbacks.length = 0;
          return;
        }

        var provider = providers[index++];
        if (typeof provider === 'function') {
          creds = provider.call();
        } else {
          creds = provider;
        }

        if (creds.get) {
          creds.get(function (getErr) {
            resolveNext(getErr, getErr ? null : creds);
          });
        } else {
          resolveNext(null, creds);
        }
      }

      resolveNext();
    }

    return self;
  }
});

/**
 * The default set of providers used by a vanilla CredentialProviderChain.
 *
 * In the browser:
 *
 * ```javascript
 * AWS.CredentialProviderChain.defaultProviders = []
 * ```
 *
 * In Node.js:
 *
 * ```javascript
 * AWS.CredentialProviderChain.defaultProviders = [
 *   function () { return new AWS.EnvironmentCredentials('AWS'); },
 *   function () { return new AWS.EnvironmentCredentials('AMAZON'); },
 *   function () { return new AWS.SharedIniFileCredentials(); },
 *   function () { return new AWS.ECSCredentials(); },
 *   function () { return new AWS.ProcessCredentials(); },
 *   function () { return new AWS.TokenFileWebIdentityCredentials(); },
 *   function () { return new AWS.EC2MetadataCredentials() }
 * ]
 * ```
 */
AWS.CredentialProviderChain.defaultProviders = [];

/**
 * @api private
 */
AWS.CredentialProviderChain.addPromisesToClass = function addPromisesToClass(PromiseDependency) {
  this.prototype.resolvePromise = AWS.util.promisifyMethod('resolve', PromiseDependency);
};

/**
 * @api private
 */
AWS.CredentialProviderChain.deletePromisesFromClass = function deletePromisesFromClass() {
  delete this.prototype.resolvePromise;
};

AWS.util.addPromises(AWS.CredentialProviderChain);


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;

/**
 * The endpoint that a service will talk to, for example,
 * `'https://ec2.ap-southeast-1.amazonaws.com'`. If
 * you need to override an endpoint for a service, you can
 * set the endpoint on a service by passing the endpoint
 * object with the `endpoint` option key:
 *
 * ```javascript
 * var ep = new AWS.Endpoint('awsproxy.example.com');
 * var s3 = new AWS.S3({endpoint: ep});
 * s3.service.endpoint.hostname == 'awsproxy.example.com'
 * ```
 *
 * Note that if you do not specify a protocol, the protocol will
 * be selected based on your current {AWS.config} configuration.
 *
 * @!attribute protocol
 *   @return [String] the protocol (http or https) of the endpoint
 *     URL
 * @!attribute hostname
 *   @return [String] the host portion of the endpoint, e.g.,
 *     example.com
 * @!attribute host
 *   @return [String] the host portion of the endpoint including
 *     the port, e.g., example.com:80
 * @!attribute port
 *   @return [Integer] the port of the endpoint
 * @!attribute href
 *   @return [String] the full URL of the endpoint
 */
AWS.Endpoint = inherit({

  /**
   * @overload Endpoint(endpoint)
   *   Constructs a new endpoint given an endpoint URL. If the
   *   URL omits a protocol (http or https), the default protocol
   *   set in the global {AWS.config} will be used.
   *   @param endpoint [String] the URL to construct an endpoint from
   */
  constructor: function Endpoint(endpoint, config) {
    AWS.util.hideProperties(this, ['slashes', 'auth', 'hash', 'search', 'query']);

    if (typeof endpoint === 'undefined' || endpoint === null) {
      throw new Error('Invalid endpoint: ' + endpoint);
    } else if (typeof endpoint !== 'string') {
      return AWS.util.copy(endpoint);
    }

    if (!endpoint.match(/^http/)) {
      var useSSL = config && config.sslEnabled !== undefined ?
        config.sslEnabled : AWS.config.sslEnabled;
      endpoint = (useSSL ? 'https' : 'http') + '://' + endpoint;
    }

    AWS.util.update(this, AWS.util.urlParse(endpoint));

    // Ensure the port property is set as an integer
    if (this.port) {
      this.port = parseInt(this.port, 10);
    } else {
      this.port = this.protocol === 'https:' ? 443 : 80;
    }
  }

});

/**
 * The low level HTTP request object, encapsulating all HTTP header
 * and body data sent by a service request.
 *
 * @!attribute method
 *   @return [String] the HTTP method of the request
 * @!attribute path
 *   @return [String] the path portion of the URI, e.g.,
 *     "/list/?start=5&num=10"
 * @!attribute headers
 *   @return [map<String,String>]
 *     a map of header keys and their respective values
 * @!attribute body
 *   @return [String] the request body payload
 * @!attribute endpoint
 *   @return [AWS.Endpoint] the endpoint for the request
 * @!attribute region
 *   @api private
 *   @return [String] the region, for signing purposes only.
 */
AWS.HttpRequest = inherit({

  /**
   * @api private
   */
  constructor: function HttpRequest(endpoint, region) {
    endpoint = new AWS.Endpoint(endpoint);
    this.method = 'POST';
    this.path = endpoint.path || '/';
    this.headers = {};
    this.body = '';
    this.endpoint = endpoint;
    this.region = region;
    this._userAgent = '';
    this.setUserAgent();
  },

  /**
   * @api private
   */
  setUserAgent: function setUserAgent() {
    this._userAgent = this.headers[this.getUserAgentHeaderName()] = AWS.util.userAgent();
  },

  getUserAgentHeaderName: function getUserAgentHeaderName() {
    var prefix = AWS.util.isBrowser() ? 'X-Amz-' : '';
    return prefix + 'User-Agent';
  },

  /**
   * @api private
   */
  appendToUserAgent: function appendToUserAgent(agentPartial) {
    if (typeof agentPartial === 'string' && agentPartial) {
      this._userAgent += ' ' + agentPartial;
    }
    this.headers[this.getUserAgentHeaderName()] = this._userAgent;
  },

  /**
   * @api private
   */
  getUserAgent: function getUserAgent() {
    return this._userAgent;
  },

  /**
   * @return [String] the part of the {path} excluding the
   *   query string
   */
  pathname: function pathname() {
    return this.path.split('?', 1)[0];
  },

  /**
   * @return [String] the query string portion of the {path}
   */
  search: function search() {
    var query = this.path.split('?', 2)[1];
    if (query) {
      query = AWS.util.queryStringParse(query);
      return AWS.util.queryParamsToString(query);
    }
    return '';
  },

  /**
   * @api private
   * update httpRequest endpoint with endpoint string
   */
  updateEndpoint: function updateEndpoint(endpointStr) {
    var newEndpoint = new AWS.Endpoint(endpointStr);
    this.endpoint = newEndpoint;
    this.path = newEndpoint.path || '/';
  }
});

/**
 * The low level HTTP response object, encapsulating all HTTP header
 * and body data returned from the request.
 *
 * @!attribute statusCode
 *   @return [Integer] the HTTP status code of the response (e.g., 200, 404)
 * @!attribute headers
 *   @return [map<String,String>]
 *      a map of response header keys and their respective values
 * @!attribute body
 *   @return [String] the response body payload
 * @!attribute [r] streaming
 *   @return [Boolean] whether this response is being streamed at a low-level.
 *     Defaults to `false` (buffered reads). Do not modify this manually, use
 *     {createUnbufferedStream} to convert the stream to unbuffered mode
 *     instead.
 */
AWS.HttpResponse = inherit({

  /**
   * @api private
   */
  constructor: function HttpResponse() {
    this.statusCode = undefined;
    this.headers = {};
    this.body = undefined;
    this.streaming = false;
    this.stream = null;
  },

  /**
   * Disables buffering on the HTTP response and returns the stream for reading.
   * @return [Stream, XMLHttpRequest, null] the underlying stream object.
   *   Use this object to directly read data off of the stream.
   * @note This object is only available after the {AWS.Request~httpHeaders}
   *   event has fired. This method must be called prior to
   *   {AWS.Request~httpData}.
   * @example Taking control of a stream
   *   request.on('httpHeaders', function(statusCode, headers) {
   *     if (statusCode < 300) {
   *       if (headers.etag === 'xyz') {
   *         // pipe the stream, disabling buffering
   *         var stream = this.response.httpResponse.createUnbufferedStream();
   *         stream.pipe(process.stdout);
   *       } else { // abort this request and set a better error message
   *         this.abort();
   *         this.response.error = new Error('Invalid ETag');
   *       }
   *     }
   *   }).send(console.log);
   */
  createUnbufferedStream: function createUnbufferedStream() {
    this.streaming = true;
    return this.stream;
  }
});


AWS.HttpClient = inherit({});

/**
 * @api private
 */
AWS.HttpClient.getInstance = function getInstance() {
  if (this.singleton === undefined) {
    this.singleton = new this();
  }
  return this.singleton;
};


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;

/**
 * @api private
 */
AWS.Signers.V3 = inherit(AWS.Signers.RequestSigner, {
  addAuthorization: function addAuthorization(credentials, date) {

    var datetime = AWS.util.date.rfc822(date);

    this.request.headers['X-Amz-Date'] = datetime;

    if (credentials.sessionToken) {
      this.request.headers['x-amz-security-token'] = credentials.sessionToken;
    }

    this.request.headers['X-Amzn-Authorization'] =
      this.authorization(credentials, datetime);

  },

  authorization: function authorization(credentials) {
    return 'AWS3 ' +
      'AWSAccessKeyId=' + credentials.accessKeyId + ',' +
      'Algorithm=HmacSHA256,' +
      'SignedHeaders=' + this.signedHeaders() + ',' +
      'Signature=' + this.signature(credentials);
  },

  signedHeaders: function signedHeaders() {
    var headers = [];
    AWS.util.arrayEach(this.headersToSign(), function iterator(h) {
      headers.push(h.toLowerCase());
    });
    return headers.sort().join(';');
  },

  canonicalHeaders: function canonicalHeaders() {
    var headers = this.request.headers;
    var parts = [];
    AWS.util.arrayEach(this.headersToSign(), function iterator(h) {
      parts.push(h.toLowerCase().trim() + ':' + String(headers[h]).trim());
    });
    return parts.sort().join('\n') + '\n';
  },

  headersToSign: function headersToSign() {
    var headers = [];
    AWS.util.each(this.request.headers, function iterator(k) {
      if (k === 'Host' || k === 'Content-Encoding' || k.match(/^X-Amz/i)) {
        headers.push(k);
      }
    });
    return headers;
  },

  signature: function signature(credentials) {
    return AWS.util.crypto.hmac(credentials.secretAccessKey, this.stringToSign(), 'base64');
  },

  stringToSign: function stringToSign() {
    var parts = [];
    parts.push(this.request.method);
    parts.push('/');
    parts.push('');
    parts.push(this.canonicalHeaders());
    parts.push(this.request.body);
    return AWS.util.crypto.sha256(parts.join('\n'));
  }

});

/**
 * @api private
 */
module.exports = AWS.Signers.V3;


/***/ }),
/* 39 */
/***/ (function(module, exports) {

// Unique ID creation requires a high quality random # generator.  In the
// browser this is a little complicated due to unknown quality of Math.random()
// and inconsistent support for the `crypto` API.  We do the best we can via
// feature-detection

// getRandomValues needs to be invoked in a context where "this" is a Crypto
// implementation. Also, find the complete implementation of crypto on IE11.
var getRandomValues = (typeof(crypto) != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto)) ||
                      (typeof(msCrypto) != 'undefined' && typeof window.msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto));

if (getRandomValues) {
  // WHATWG crypto RNG - http://wiki.whatwg.org/wiki/Crypto
  var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

  module.exports = function whatwgRNG() {
    getRandomValues(rnds8);
    return rnds8;
  };
} else {
  // Math.random()-based (RNG)
  //
  // If all else fails, use Math.random().  It's fast, but is of unspecified
  // quality.
  var rnds = new Array(16);

  module.exports = function mathRNG() {
    for (var i = 0, r; i < 16; i++) {
      if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
      rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
    }

    return rnds;
  };
}


/***/ }),
/* 40 */
/***/ (function(module, exports) {

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
var byteToHex = [];
for (var i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
}

function bytesToUuid(buf, offset) {
  var i = offset || 0;
  var bth = byteToHex;
  // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
  return ([bth[buf[i++]], bth[buf[i++]], 
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]]]).join('');
}

module.exports = bytesToUuid;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.decode = exports.parse = __webpack_require__(102);
exports.encode = exports.stringify = __webpack_require__(103);


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (undefined === 'production') {
  module.exports = __webpack_require__(134);
} else {
  module.exports = __webpack_require__(135);
}


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(129);


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(5);
            var content = __webpack_require__(45);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(46);
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "@font-face {\r\n    font-family: 'Heebo';\r\n    src: local('Heebo'), url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") format('truetype');\r\n}\r\n\r\n.chat-bot-container {\r\n    position: fixed;\r\n    font-weight: 300;\r\n    font-size: .875rem;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    font-family: Heebo, Arial, Helvetica, sans-serif;\r\n    z-index: 10001;\r\n    border-top-right-radius: 5px;\r\n    border-top-left-radius: 5px;\r\n    -webkit-touch-callout: none;\r\n    -webkit-user-select: none;\r\n}\r\n\r\n/* Extra small devices (phones, 600px and down) */\r\n\r\n@media only screen and (max-width: 600px) {\r\n    .chat-bot-container .chatbot {\r\n        bottom: 1em;\r\n        right: 1em;\r\n        height: 80vh;\r\n        width: 95vw;\r\n    }\r\n}\r\n\r\n/* Small devices (portrait tablets and large phones, 600px and up) */\r\n\r\n@media only screen and (min-width: 600px) {\r\n    .chat-bot-container .chatbot {\r\n        bottom: 1em;\r\n        right: 1em;\r\n        height: 80vh;\r\n        width: 90vw;\r\n    }\r\n}\r\n\r\n/* Medium devices (landscape tablets, 768px and up) */\r\n\r\n@media only screen and (min-width: 768px) {\r\n    .chat-bot-container .chatbot {\r\n        bottom: 1em;\r\n        right: 1em;\r\n        height: 80vh;\r\n        width: 40vw;\r\n    }\r\n}\r\n\r\n/* Large devices (laptops/desktops, 992px and up) */\r\n\r\n@media only screen and (min-width: 992px) {\r\n    .chat-bot-container .chatbot {\r\n        bottom: 1em;\r\n        right: 1em;\r\n        height: 70vh;\r\n        width: 30vw;\r\n    }\r\n}\r\n\r\n/* Extra large devices (large laptops and desktops, 1200px and up) */\r\n\r\n@media only screen and (min-width: 1200px) {\r\n    .chat-bot-container .chatbot {\r\n        bottom: 1em;\r\n        right: 1em;\r\n        height: 70vh;\r\n        width: 30vw;\r\n    }\r\n}\r\n\r\n@media only screen and (min-height: 1024px) {\r\n    .chat-bot-container .chatbot {\r\n        bottom: 1em;\r\n        right: 1em;\r\n        height: 60vh;\r\n        width: 40vw;\r\n    }\r\n}\r\n\r\n@media print {\r\n    .chat-bot-container .no-print {\r\n        display: none;\r\n    }\r\n}\r\n\r\n.chat-bot-container .chat-bot-container-minimised {\r\n    position: fixed;\r\n    bottom: 1em;\r\n    right: 1em;\r\n}\r\n\r\n.chat-bot-container .chat-icon {\r\n    margin-left: auto;\r\n    width: fit-content;\r\n    display: -ms-flexbox;\r\n    justify-content: flex-end;\r\n    position: fixed;\r\n    bottom: 1em;\r\n    right: 0em;\r\n}\r\n\r\n.chat-bot-container .chat-icon:hover {\r\n    cursor: pointer;\r\n    cursor: hand;\r\n}\r\n\r\n.chat-bot-container .chat-icon.hidden {\r\n    visibility: hidden;\r\n}\r\n\r\n.chat-bot-container .chat-icon-svg {\r\n    fill: #d14210;\r\n}\r\n\r\n.chat-bot-container .chat-icon-svg.hover {\r\n    fill: #d54614;\r\n}\r\n\r\n.chat-bot-container .chatbot {\r\n    display: flex;\r\n    flex-direction: column;\r\n    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n    background-color: #FFF;\r\n    border-top-right-radius: 5px;\r\n    border-top-left-radius: 5px;\r\n    position: fixed;\r\n    bottom: 1em;\r\n    right: 1em;\r\n}\r\n\r\n.chat-bot-container .chatbot.minimised {\r\n    display: none;\r\n}\r\n\r\n.chat-bot-container .header {\r\n    background-color: #2D2F32;\r\n    height: 3em;\r\n    color: #FFF;\r\n    border-top-right-radius: 5px;\r\n    border-top-left-radius: 5px;\r\n    display: grid;\r\n    display: -ms-grid;\r\n    grid-template-columns: 10% 80% 10%;\r\n    -ms-grid-columns: 10% 80% 10%;\r\n    -ms-grid-rows: 100%;\r\n    align-items: center;\r\n}\r\n\r\n.chat-bot-container .header-text-outer {\r\n    display: flex;\r\n    flex-direction: row;\r\n}\r\n\r\n.chat-bot-container .header-text {\r\n    grid-column: 2;\r\n    -ms-grid-column: 2;\r\n    display: flex;\r\n    justify-content: center;\r\n    -ms-grid-row-align: center;\r\n}\r\n\r\n.chat-bot-container .minimise-icon {\r\n    display: flex;\r\n    justify-self: flex-end;\r\n    grid-column: 3;\r\n    -ms-grid-column: 3;\r\n    margin-right: 5px;\r\n    -ms-grid-row-align: center;\r\n    stroke: #FFF;\r\n}\r\n\r\n.chat-bot-container .minimise-icon:focus {\r\n    outline-color: #d14210;\r\n    outline-style: dotted;\r\n    outline-width: 1px;\r\n}\r\n\r\n.chat-bot-container .minimise-icon:hover {\r\n    cursor: pointer;\r\n    cursor: hand;\r\n    stroke: #d14210;\r\n}\r\n\r\n.chat-bot-container .chat-feed {\r\n    overflow-y: scroll;\r\n    overflow-x: hidden;\r\n    flex: 85%;\r\n    background-color: #FFF;\r\n    border-bottom: 1px solid black;\r\n}\r\n\r\n.chat-bot-container .text-input-panel {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    background-color: #FFF;\r\n    flex-grow: 1;\r\n    margin: 10px;\r\n    align-items: center;\r\n}\r\n\r\n.chat-bot-container .button-input-panel {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    flex: 1;\r\n    border-bottom-right-radius: 10px;\r\n    border-bottom-left-radius: 10px;\r\n}\r\n\r\n.chat-bot-container .input-control {\r\n    width: 100%;\r\n    margin-left: 10px;\r\n    border: solid 1px #585c62;\r\n    background-color: #FFFF;\r\n    color: black;\r\n    resize: none;\r\n    font-family: heebo;\r\n}\r\n\r\n.chat-bot-container .input-control:focus {\r\n    border: solid 1px #d14210;\r\n    outline: 0;\r\n}\r\n\r\n.chat-bot-container .messagecontainer {\r\n    display: flex;\r\n    flex-direction: column;\r\n    padding: 10px;\r\n    max-width: 85%;\r\n    width: fit-content;\r\n    width: -moz-fit-content;\r\n    align-items: center;\r\n}\r\n\r\n.chat-bot-container .messagecontent {\r\n    border-radius: 10px;\r\n    padding: 10px;\r\n    text-align: left;\r\n    width: fit-content;\r\n    grid-column: 2;\r\n    -ms-grid-column: 2;\r\n    grid-row: 1;\r\n    -ms-grid-row: 1;\r\n}\r\n\r\n.chat-bot-container .messagecontainer.in.bottyping {\r\n    display: flex;\r\n    flex-direction: row;\r\n    height: 36px;\r\n}\r\n\r\n.chat-bot-container .messagecontainer.in.error {\r\n    color: red;\r\n}\r\n\r\n.chat-bot-container .dot {\r\n    height: 5px;\r\n    width: 5px;\r\n    margin: 0 2px;\r\n    border-radius: 50%;\r\n    background-color: #9E9EA1;\r\n    opacity: 0.1;\r\n    padding: 2px;\r\n    align-self: center;\r\n    float: left;\r\n    display: block;\r\n}\r\n\r\n.chat-bot-container .dot.one {\r\n    animation: 1s blink infinite .3333s;\r\n}\r\n\r\n.chat-bot-container .dot.two {\r\n    animation: 1s blink infinite .6666s;\r\n}\r\n\r\n.chat-bot-container .dot.three {\r\n    animation: 1s blink infinite 1s;\r\n}\r\n\r\n@keyframes blink {\r\n    50% {\r\n        opacity: 1;\r\n    }\r\n}\r\n\r\n.chat-bot-container .messagecontainer.in {\r\n    margin-right: auto;\r\n    display: grid;\r\n    grid-template-rows: auto auto;\r\n    grid-template-columns: auto auto;\r\n    display: -ms-grid;\r\n    -ms-grid-rows: auto auto;\r\n    -ms-grid-columns: auto auto;\r\n    max-width: 85%;\r\n}\r\n\r\n.chat-bot-container .messagecontainer.out {\r\n    margin-left: auto;\r\n    max-width: 70%;\r\n}\r\n\r\n.chat-bot-container .messagecontent.in {\r\n    background-color: #d0d0d0;\r\n    margin-left: 10px;\r\n}\r\n\r\n.chat-bot-container .messagecontent.out {\r\n    background-color: #d14210;\r\n    color: #FFF;\r\n    margin-left: auto;\r\n    white-space: pre-line;\r\n}\r\n\r\n.chat-bot-container .send-button {\r\n    margin-left: 10px;\r\n    align-content: center;\r\n    width: 24px;\r\n    height: 24px;\r\n    stroke: currentColor;\r\n}\r\n\r\n.chat-bot-container .send-button:focus, .chat-bot-container .send-button:active {\r\n    outline-color: #d14210;\r\n    outline-style: dotted;\r\n    outline-width: 1px;\r\n}\r\n\r\n.chat-bot-container .send-button:hover {\r\n    cursor: pointer;\r\n    cursor: hand;\r\n    stroke: #d14210;\r\n}\r\n\r\n.chat-bot-container .icon-sender {\r\n    margin-top: auto;\r\n    width: 30;\r\n    height: 30;\r\n    /* IE 11 */\r\n    display: -ms-flexbox;\r\n    -ms-flex-direction: column;\r\n    justify-content: flex-end;\r\n}", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("data:font/ttf;base64,AAEAAAASAQAABAAgRFNJRwAAAAEAATjUAAAACEdERUYIDgjjAAABLAAAAEBHUE9TizWd1wAAAWwAABX8R1NVQgABAAAAABdoAAAACk9TLzKCd853AAAXdAAAAGBjbWFwA+TKsQAAF9QAAAWcY3Z0IAAeZPMAASokAAAAmmZwZ22Bq+NsAAEqwAAADV5nYXNwAAAAEAABKhwAAAAIZ2x5ZsUmwO8AAB1wAADuQmhlYWQJPR78AAELtAAAADZoaGVhDEcAlgABC+wAAAAkaG10eCczohsAAQwQAAAHPmxvY2EJts9kAAETUAAAA6JtYXhwA80OcgABFvQAAAAgbmFtZZpRdPoAARcUAAAGYXBvc3SkjYflAAEdeAAADKRwcmVwZN0tZgABOCAAAACxAAEAAAAMAAAAAAAAAAIACAADAI0AAQCOAJIAAgCTAM8AAQDQANMAAgDUAR0AAQGPAa8AAQGwAbIAAwG7AcsAAwABAAAACgAgAEAAAURGTFQACAAEAAAAAP//AAIAAAABAAJrZXJuAA5tYXJrABYAAAACAAAAAQAAAAMAAgADAAQABQAMABQAHAAkACwAAgAAAAEAKAACAAAAAQZ6AAQAAAABBpQABAABAAEHDAAEAAEAAQ2iAAEUmAAEAAAAMABqAHAAygDYAOoBFAFCAVQBhgHcAgYCOAJOAlwC7gL4A0IDiAO2BGQElgSgBLIEuATGBNwE4gToBPIFCAUyBTwFVgVcBWYFcAWWBaAFqgXIBdoF7AX+BggGJgYwBjoGSAABADH/xgAWACP/7AAx/4cAMv/vADf/sgA4/9YAOv9vAGT/1ABm//QAdf/vAHb/9QB7/84AfP/fAIEADACm//UAs//1AMD/xwDB//EAxv/NAMf/3QDJ/8QBbf+xAXD/iAADADH/5QA3/+gAOv/JAAQAMf/jAXX/7wF3//QBef/mAAoAMQAUAE7/7QBX/+4AZv/tAHb/7wB7/+YAfP/qAMb/8ADH/+0Ayf/wAAsAA/9CAB3/KwAxABQARf/eAE7/6wBm/+sAcf/mAHb/6gB7/+gAk//AAWz+8gAEAAMAEgAx/+MAOQARADr/5AAMACP/4QBO/+YAZP/+AGb/5QB2/+kAe//YAHz/wQCc/+gApv/mALP/5wC8/+cBfP9lABUAAwATACP/wAAx/y0AMv/lADf/NQA4/54AOv7tAHb/7wB7/4QAfP/IAJMAEwCc//MApv/xALP/8gC8//IAwP+xAMH/7ADG/68Ax//RAMn/rAFw/twACgAD/+sACv/fADH/sAA3/+oAOf/qADr/1QA8/+kAk//tAMj/9QFs/4gADAAD/1IACv7pAB3/RwA5/88APP/dAEX/9QBO//MAZv/zAHUADgB7AA8Ak/+GAWz+cwAFADH/3gA3/+QAOP/sADr/3QCTAA4AAwAx/8sAN//tADr/0AAkAAL/xgAD/4wACv9EAB3/JgAj/+QAL//wADEAEAA3ABAAOAAPADoAEABF/10ATP+pAE7/pgBk/6UAZv86AGv/oQBx/74Acv+wAHb/vAB7/68AfP/QAH3/swCB/8QAk/+vAJz/uQCm/7kAs/+5ALz/uQC+/7wAwP/xAMf/8QDI/+0BbP8CAXz+9AF+/1wBf/9oAAIAA//qAJP/6wASAAP/tQAj//MARf/SAE7/1ABm/9IAcf/iAHb/5AB7//UAk/+0AJz/2QCm/9kAs//ZALz/2QFs/ygBdQATAXcAEQF5ABQBfP9uABEAA//VADEADgBF/98ATv/hAGb/4QBx/+sAdv/tAJP/ywCc/+kApv/nALP/5wC8/+cBbP9zAXUADgF3AAwBeQAPAXz/xAALACP/5wA3AA4ATv/mAGb/6wB2/+sAe//hAJz/6QCm/+cAs//nALz/6QF8/24AKwAD/28ACv+gAB3/oAAj/+MAL//wADEAEQAy/6AANwASADgAEQA5AA0AOgASAEX/wABM/8EATv+/AFf/6gBk/9gAZv+/AGv/wABx/9gAcv/GAHX/6gB2/9kAe//sAH3/6QCB/+IAk/+rAJz/zQCm/8sAs//LALz/ywDA//MAx//zAMj/7wFi/88BZf/TAWz/GwF1ABMBdwASAXkAFAF8/3EBfv+uAX//zQGj/+IADAADAA0AI//mAE7/6wBm/+sAdv/tAHv/5QB8/+UAkwANAJz/7QCm/+sAs//sALz/7AACAHv/8QFw/+oABAB7//UAff/xAIH/8QFw/+MAAQFw//UAAwB7//MBcP/yAYb/vwAFAE7/6AFwABABdQATAXcAEgF5ABQAAQGG/6EAAQBO/+wAAgFw/64Bhv+MAAUAe//xAH3/6wCB//ABcP+kAYb/kwAKAEX/4QBO/+0AVwAUAGb/3QB1ADIAewASAHwAEQFs/1cBcAAQAYYADwACAGb/4wGGABYABgBF//EATv/zAFcADQBm//EBbP9eAXAADwABAWz/hAACAE7/7ABm/9oAAgBO//AAZv/wAAkAnP/yAKb/8gCz//IAvP/yAMD/wADB/+wAxv/HAMf/2ADJ/78AAgDG/+4Ax//1AAIAs//rALz/6wAHALP/7wC8//AAwP+7AMH/7ADG/7cAx//VAMn/tAAEAMD/7gDG//EAyP/sAMn/6gAEAMD/6QDG/+sAx//xAMn/5QAEAMD/8gDG//EAx//1AMn/7gACAMYADQDJAA0ABwAD/4gARf/OAE7/xQBk/+wAZv+oAHL/pQB8AAsAAgAd/+wAMv/sAAIAHf/uADL/7gADADcAFAA4ACQAOgAWAAQATv+hAGIAAwBm/3EAcv8jAAEOogAEAAAAAwAQABYAHAABAXD+7QABAXD/1QABAXL/CgABDooOpgACAAwASgAPAAEAUAABAFYAAQBcAAAARAAAAEQAAABEAAAARAAAAEQAAABEAAAARAAAAEQAAABKAAAARAAAAEQAAQBiAAEAKgAwAAECVgAAAAECVwAAAAH97wUKAAH9ygUKAAH96gUGAAEB5ASaAAECJf/2AAECcQUKAAEOLA48AAUADABeABQAAgNKAAIDUAACA1YAAAM4AAADOAAAAzgAAAM4AAADOAAAAzgAAAM4AAADOAAAAz4AAAM4AAMDYgADA2IAAAM4AAEDRAACA1wABANoAAMDaABKAxwDIgMoAy4GOgM0AzoDQANGBjoDTANSA1gDXgY6A2QDagNwA3YGOgLsA3wDggOIBjoDjgOUA5oDoAY6A6YDrAOyA3YGOgO4A74DxAPKBjoD0APWA9wD4gY6A+gD7gP0A/oGOgQABAYEDAN2BjoEEgQYBB4DdgY6BCQEKgQwBDYGOgQ8BEIESAROBjoEVARaAygEYAY6BGYEbARyBHgGOgR+BIQEigSQBjoElgScBKIETgY6BKgErgS0BLoGOgTABMYEzAPKBjoE0gTYBN4DXgY6BOQE6gTwBPYGOgT8BQIFCASQBjoFDgO+BDAEYAY6BRQFGgUgBSYGOgUsBTIFOAU+BUQFSgVQBVYFXAY6A+gD7gP0A/oGOgViBWgFbgNGBjoFdAV6BYADXgY6BYYFjAWSA0YGOgWYBZ4FpAWqBjoFsAW2BbwFwgY6BcgFzgXUBdoGOgXgBeYF7AXyBjoF+AX+BgQGCgY6BhAGFgYcBiIGOgUsBTIFOAU+BUQFLAUyBTgFPgVEBSwFMgU4BT4FRAUsBTIFOAU+BUQDHAMiAygDLgY6AxwDIgMoAy4GOgMcAyIDKAMuBjoDNAM6A0ADRgY6A0wDUgNYA14GOgNkA2oDcAN2BjoC7AN8A4IDiAY6A44DlAOaA6AGOgOmA6wDsgN2BjoD0APWA9wD4gY6A+gD7gP0A/oGOgQABAYEDAN2BjoEEgQYBB4DdgY6BCQEKgQwBDYGOgRUBFoDKARgBjoEfgSEBIoEkAY6BJYEnASiBE4GOgTABMYEzAPKBjoE0gTYBN4DXgY6BPwFAgUIBJAGOgUOA74EMARgBjoFFAUaBSAFJgY6BSwFMgU4BT4FRAVKBVAFVgVcBjoDjgOUA5oDoAY6AzQDOgNAA0YGOgQSBBgEHgN2BjoE0gTYBN4DXgY6A0wDUgNYA14GOgYoBi4GNAWqBjoGKAYuBjQFqgY6BigGLgY0BaoGOgYoBi4GNAWqBjoAAQJWAAAAAQJXAAAAAQE8AlcAAf3vBQoAAf3KBQoAAf3qBQYAAQHkBJoAAQEFBJoAAQAABJoAAQKpAAAAAQJHAN4AAQKqBJoAAQAoBJoAAQIVAAAAAQF9AlsAAQHjBJoAAQAQBJoAAQF7AAAAAQDJAlsAAQF3BJoAAQAtBJoAAQLKAAAAAQFCAlsAAQIUBJoAAQACBJoAAQJXAlsAAQJQBJoAAQAKBJoAAQFMAAAAAQBHAlsAAQEMBJoAAQEHBJoAAQGYAAAAAQI1AlsAAQFhBJoAAQJ+AAAAAQJ+AlsAAQJyBJoAAQAuBJoAAQKMAAAAAQKMAlsAAQJ9BJUAAQArBJoAAQFyAAAAAQBcAxYAAQEVBJoAAf/6BJoAAQEMAhwAAQFJAlsAAQHBBJoAAQHtAAAAAQGZAlsAAQHeBJoAAQHFAAAAAQE2AqYAAQJ8BJoAAf/cBV4AAQKLAAAAAQKLAlsAAQKLBJoAAQAjBJoAAQKcAAAAAQKwAlsAAQAnBJoAAf+CAkEAAQAzAlsAAQD9BJoAAQABBJoAAQE9AAAAAQCsAlsAAQFpBJoAAQARBJoAAQKmAAAAAQKdAlsAAQJ+BJoAAQIyAAAAAQJqApIAAQI5BJoAAQATBJoAAQGaAcAAAQJpAwwAAQJWBJoAAQKBAAAAAQKKAxkAAQKGBJoAAQDYAWcAAQFHAX8AAQItBJoAAf//BJoAAQIvAAAAAQFDAWkAAQIoBJoAAQLxAAAAAQL5AAAAAQFVAlsAAQHIBJoAAf/1BJoAAQMfAAAAAQQ6AlsAAQMoBJoAAQAaBJoAAQAVBJoAAQKJAAAAAQKrAlsAAQKKBJoAAQAgBJoAAQIgAAAAAQJtAuwAAQJKBJoAAQNzAAAAAQK1ARwAAQOuBJoAAQO6AAAAAQICAlsAAQKIBJoAAQMgAAAAAQMgAlsAAQMTBJoAAQADBJoAAQKtAAAAAQKWAlsAAQLNBJoAAQAFBJoAAQJ9AAAAAQHaAlsAAQMWBJoAAf/JBTkAAQNhAAAAAQNhAlsAAQNhBJoAAf/hBJoAAQSlAAAAAQH4AlsAAQJiBJoAAf/3BJoAAQOBAAAAAQOeAlsAAQN9BJoAAQAvBJoAAQMHAAAAAQFjAlsAAQHVBJoAAQAAAAAAAQdsB6gAAgAMAEoADwABAvwAAQMCAAEDCAAAAvAAAALwAAAC8AAAAvAAAALwAAAC8AAAAvAAAALwAAAC9gAAAvAAAALwAAEDDgCsAtYC3ALWAtwC1gLcAtYC3ALWAtwC1gLcAtYC3AauAuIC6ALuAvQC+gL0AvoDAAMGAwADBgMMAxIDDAMSAwwDEgMMAxIDDAMSAxgDEgMeAyQDKgMwAzYDPAM2AzwDNgM8AzYDPAM2AzwDQgNIA04DVANaA2ADZgNsA3IDeANyA3gDfgOEA34DhAN+A4QDfgOEA34DhAN+A4oDfgOEA5ADlgOcAu4DogOoA64DtAO6A8ADxgPMA9ID2APSA9gD0gPYA9ID2APSA9gD3gPkA+oD8AP2A/wEAgQIBAIECAMABA4DWgQUBBoEIAQaBCAEGgQgBBoEIAQaBCAEGgQgBBoEIAauBCYELAQyBDgEPgQ4BD4ERARKBFAEVgRQBFYEUARWBFAEVgRQBFYEXARiBGgEbgR0BHoEgASGBIAEjASABIwEgASMBIAEjASABIwEkgSYBJ4EpASABKoEsAS2BLwEwgS8BMIEyATOBMgEzgTIBM4EyATOBMgEzgTIBNQEyATOBNoE4ATmBOwE8gT4BP4FBAUKBRAFFgUcBSIFKAUuBTQFLgU0BS4FNAUuBTQFLgU0BToFQAVGBUwFUgVYBV4FZAVeBWQFXgVkBWoFcASeBKQFdgV8BXYFfAV2BXwFdgV8BXYFfAV2BXwFdgV8BYIFiAWOBZQFmgWgBZoFoAWmBawFpgWsBbIFuAWyBbgFsgW4BbIFuAWyBbgFvgXEBcoF0AXWBdwF4gXoBeIF6AXiBegF4gXoBeIF6AXuBfQF+gYABgYGDAYSBhgGHgYkBh4GJAYqBjAGKgYwBioGMAYqBjAGKgYwBioGMAYqBjAFpgY2BjwGQgQsBkgGTgZUBloGYAZmBmwGcgZ4BnIGeAZyBngGcgZ4BnIGeAZ+BoQGigaQBpYGnAaiBqgGogaoBqIGqAABAlYAAAABAlcAAAAB/e8FCgAB/coFCgAB/eoFBgABAeQEmgABAq4AAAABAqEGQQABA6oGTAABAmYACgABAl4GTAABAqj/9gABAqgGYQABAmUACgABAmEGTAABAmsACgABAmsGTAABAhMACgABAtr/9wABAp8GYQABAsoACgABAsgGTAABARcACQABARcGTAABAgD/9gABA1MGPwABApQAWgABAl0GOgABAmwACgABAQgGOwABA34ACgABA3oGQQABAtAACgABAtYGQQABAsQAAAABAsMGQwABAssGigABA7kAAAABA7kGQwABAhIACgABArr/9gABArwGQAABAmMACgABAmIGQQABAnT//wABAm4GQwABAmYAAAABAmQGQAABApoAAAABApsGQQABApAACgABApAGTAABA5oACgABA5wGQQABAoUACgABAo0GTAABAmcAAQABAmoGQAABAmgGQQABAmwGTAABAfYAAAABAkYFCgABA1sFCwABAlf/9gABAnoGSwABAhT/9gABAhQFCgABAmkAAAABAiUGTAABAjgAAAABAjUFCgABAUoACgABAfYG2QABAir+QQABAiwFCgABAkwACgABAfUGSwABAPkACgABAPwGQAABAP0FCAABAL7+NQABAN8GQAABAhEARwABAiUGSwABAPkGoQABA4EACgABA48FCgABAjMACgABAj0FCgABAkj//wABAj8FCgABAhkFCQABA7H//wABA7EFCgABAOf+aQABAnUFAAABA5T+aQABAhkFCgABAPYACwABAZgFCgABAjP/9gABAjMFCgABAPgACgABAa0G2QABAcUAAAABATgFygABAjMAAAABAjcFCgABAfgACgABAfkFAAABAv4ACgABAvsFCgABAf8ACgABAfwFAAABAsj/YgABAfwFCgABAg0ACgABAgMFCgABAlAAAAABAkYFKAABA4AAAAABA4AFCgABAhgACgABAhEFKAABAkr//AABAksFKAABAfQACgABAfQFKAABAhkACAABAhkFKAABAekACgABAh0FKAABAk7//AABAkgFKAABAmcACgABAmcFKAABAPUABAABAPUFKAABAcn/9gABAt4FKAABAhMABgABAgYFKAABAg8ACAABAOkFKAABAv0ACgABAv0FKAABAmsABAABAnAFKAABAncAAAABAl8FKAABAkAFKAABAcIACgABAg4FKAABAlcFKAABAg0ACAABAggFKAABAij//wABAh8FKAABAhMAAwABAhIFKAABAj4AAAABAj8FKAABAjIACgABAjIFKAABAxIACgABAxIFKAABAiYACAABAiYFKAABAhUACgABAhUFKAABAAAAAAABADAAAgADAAsADAAQABUAFwAeAB8AIwArAC0ALgAxADIANwA4ADkAOgA8AEUATQBOAFIAVwBZAGEAZABmAHEAdQB7AHwAfQCBAJMAngCuAK8AswC8AL0AwAFwAXQBdgF4AYYAAQADAWwBcAFyAAIABAGwAbIAAAG7AcQAAwHHAccADQHJAckADgABAAEAzgACAAIBsAGyAAABuwHLAAMAAgABANQBHQAAAAIACwADACsAAAAtAC8AKQAxADwALAA+AD4AOABFAFAAOQBSAG4ARQBwAHIAYgB0AIEAZQCDAIMAcwCTAL4AdADAAMsAoAABAAAAAAAAAAAAAAADBC4BkAAFAAAFMwTMAAAAmQUzBMwAAALMAGQCwgAAAAAFAAAAAAAAAAAACANAAAABAAAAAAAAAABVS1dOAEAAIPtPCGL8ogAACGIDXiAAACEAAAAABJoFsAAAACAAAgAAAAMAAAADAAACDgABAAAAAAAcAAMAAQAAAg4ABgHyAAAACQD0AAIAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAWkBbwFrAZIBnQGjAXABeAF5AWIBngFnAXwBbAFyAR4BHwEgASEBIgEjASQBJQEmAScBZgFxAZkBlwGYAW0BogADAAsADAAOABAAFQAWABcAGAAdAB4AHwAgACEAIwArAC0ALgAvADEAMgA3ADgAOQA6ADwBdgFjAXcBqQFzAbcARQBNAE4AUABSAFcAWABZAFoAYABhAGIAYwBkAGYAbgBwAHEAcgB1AHYAewB8AH0AfgCBAXQBoAF1AZUAAAAGAAgADQARACIAJgA1AEYASQBHAEgASwBKAE8AUwBWAFQAVQBcAF8AXQBeAGUAZwBqAGgAaQBsAHcAegB4AHkAAAGoAZABkwGnAWUBpABzAaYBpQAAAbMBtgAAAAoAKAAAAZ8AAAAAAZQAAAAAAAAAAAAAAAAAzADNAAAATABrAW4BagGaAAAAAAAAAAABfgF/AWgAAAAHAAkAKQAqAG0BewF6AYMBhAGFAYYBlgAAAIAAAAFDAY8BgAGBAJIAAAAAAWQBhwGCAAAABQASAAQAEwAUABkAGgAbABwAJAAlAAAAJwAzADQANgBbAbUBugG4AAAAAAG5AbQABAOOAAAAZgBAAAUAJgAvAEUASQBVAGUAaQB1AH4AtAC7AP8BMQFTAX8CxgLaAtwDAQMDA7EFtwW8Bb8FwgXHBeoF9B6eIA8gFCAaIB4gIiAmIDogRCBwIHkgiSCsIhL7AfsG+yn7Nvs8+z77QftE+0///wAAACAAMABGAEoAVgBmAGoAdgChALYAvwExAVIBfwLGAtoC3AMAAwMDsQWwBbgFvgXBBccF0AXwHp4gDCATIBggHCAiICYgOSBEIHAgdCCAIKwiEvsB+wX7H/sq+zj7PvtA+0P7Rv//AAAAAP/PAAAAAP/xAAAAAAAAAAAAAP8qAAD+9f7v/t/+3v6w/q/9HfwL/AwAAPwJ+/z7BAAA4ZIAAAAA4W0AAOFD4ULhR+D/4N7g3uDE4OPfiQAAAAAAAAAAAAAAAAAAAAAAAAABAGYAhAAAAKwAwgAAAN4A9AEEASoBNAAAAbIAAAAAAAAAAAAAAAAAAAAAAAABogAAAAAAAAGeAAABpAGqAAABqgAAAAAAAAAAAAAAAAAAAAAAAAGcAZwBngGyAcoB0gHSAdQB1gAAAAIBaQFvAWsBkgGdAaMBcAF4AXkBYgGeAWcBfAFsAXIBHgEfASABIQEiASMBJAElASYBJwFmAXEBmQGXAZgBbQGiAAMACwAMAA4AEAAdAB4AHwAgACEAIwArAC0ALgAvADEAMgA3ADgAOQA6ADwBdgFjAXcBqQFzAbcARQBNAE4AUABSAGAAYQBiAGMAZABmAG4AcABxAHIAdQB2AHsAfAB9AH4AgQF0AaABdQGVAWoBkAGTAZEBlAGhAacBtgGlAMwBfgGaAX0BpgG4AagBnwFQAVEBswGkAWQBtAFPAM0BfwFuAAcABAAFAAkABgAIAAoADQAUABEAEgATABwAGQAaABsADwAiACcAJAAlACkAJgGcACgANgAzADQANQA7ACwAcwBJAEYARwBLAEgASgBMAE8AVgBTAFQAVQBfAFwAXQBeAFEAZQBqAGcAaABsAGkBlgBrAHoAdwB4AHkAfwBvAIAAKgBtAYoByQDQANEA0gGIAYkBjgGNAYwBiwF7AXoBgwGEAYIAkgHMAc0A7wDwAPEA8gDzAPQA9QD2APcA+AGvAPkA+gD7APwA/QD+AP8BAAEBAQIBAwEEAQUBBgEHAQgBCQEKAQsBDAENAQ4BDwEQAREBEgETARQBFQEWARcBGADTAAoA+/3VBFMIYgADAA8AFQAZACMAKQA1ADkAPQBIAiO0QQEhAUlLsBRQWECJABYYFRUWcAYBAgUBAwQCA2UABCUBCgwECmUACCYBEQ0IEWUQAQ0nFAIODw0OZQAPABITDxJlABMoGgIYFhMYZQAVABcZFRdmABkpARweGRxlAB4AHRseHWUAGyoBIx8bI2UiAR8AISAfIWUAIAAAIABhJAEHBwFdAAEBK0sLAQkJDF0ADAwnCUwbS7AqUFhAjgAWGBUVFnAGAQIFAQMEAgNlAAQlAQoMBAplAAgmARENCBFlJwEUDg0UVRABDQAODw0OZQAPABITDxJlABMoGgIYFhMYZQAVABcZFRdmABkpARweGRxlAB4AHRseHWUAGyoBIx8bI2UiAR8AISAfIWUAIAAAIABhJAEHBwFdAAEBK0sLAQkJDF0ADAwnCUwbQI8AFhgVGBYVfgYBAgUBAwQCA2UABCUBCgwECmUACCYBEQ0IEWUnARQODRRVEAENAA4PDQ5lAA8AEhMPEmUAEygaAhgWExhlABUAFxkVF2YAGSkBHB4ZHGUAHgAdGx4dZQAbKgEjHxsjZSIBHwAhIB8hZQAgAAAgAGEkAQcHAV0AAQErSwsBCQkMXQAMDCcJTFlZQGA+PjY2KiokJBoaEBAEBD5IPkhHRkVEQ0JAPz08Ozo2OTY5ODcqNSo1NDMyMTAvLi0sKyQpJCkoJyYlGiMaIyIhIB8eHRwbGRgXFhAVEBUUExIRBA8EDxERERESERArCBsrASERIQUVMxUjFSE1IzUzNQERITUjNQcjNTMDFTMVIxUhNTM1BxUhFSERARUzNTMVITUjESERAREhEQchNSEBFTMHFSE1ITczNQRT/KgDWP1zr7IBwLKy/kABwLJcWVmysrIBDrJZ/pkBwP7yXFn+8lkBwP5AAcBZ/vIBDv6Zvb0BwP7tvVb91QqNtVlkWVlkWf6k/udcvb1h/vpZZFm9WaKzWwEO/raXPn/Y/s8BMf4t/s8BMdh//upaflpafloAAAACABwAAAUfBbAACAALACtAKAoBBAIBSgUBBAAAAQQAZgACAiVLAwEBASYBTAkJCQsJCxEhERAGCBgrASEDIwEzMwEjCwIDzf2hisgCLIAqAi3HxPf2AX3+gwWw+lACGgKo/Vj//wAcAAAFHwc3ACIBzxwAACIAAwAAAQcBsQTXATcAPkA7CwEEAgFKCAEGBQaDAAUCBYMHAQQAAAEEAGYAAgIlSwMBAQEmAUwNDQoKDRANEA8OCgwKDBEhEREJCCMrAAD//wAcAAAFHwc3ACIBzxwAACIAAwAAAQcBtQDJATcARUBCDwEFBwsBBAICSgAHBQeDBgkCBQIFgwgBBAAAAQQAZgACAiVLAwEBASYBTA4NCgoUExEQDRUOFQoMCgwRIRERCggjKwAAAP//ABwAAAUfBv0AIgHPHAAAIgADAAABBwG2APoBNwBHQEQLAQQCAUoLCAoDBgcBBQIGBWcJAQQAAAEEAGYAAgIlSwMBAQEmAUwZGQ0NCgoZJBkjHx0NGA0XExEKDAoMESEREQwIIysA//8AHAAABR8HNwAiAc8cAAAiAAMAAAEHAbAEsgE3AD5AOwsBBAIBSgAFBgWDCAEGAgaDBwEEAAABBABmAAICJUsDAQEBJgFMDQ0KCg0QDRAPDgoMCgwRIRERCQgjKwAA//8AHAAABR8HkgAiAc8cAAAiAAMAAAEHAbkBUAFCAE1ASgsBBAIBSgoBBgAHCAYHZwsBCAAFAggFZwkBBAAAAQQAZgACAiVLAwEBASYBTB0dDQ0KCh0pHSgjIQ0cDRsVEwoMCgwRIRERDAgjKwAAAP//ABwAAAUfByMAIgHPHAAAIgADAAABBwGyBLcBOwCFQAsLAQQCAUooJwIISEuwI1BYQCYACAAGBQgGZwAJBwEFAgkFZwoBBAAAAQQAZgACAiVLAwEBASYBTBtALQAHBQIFBwJ+AAgABgUIBmcACQAFBwkFZwoBBAAAAQQAZgACAiVLAwEBASYBTFlAFwoKJSMfHRoZFxURDwoMCgwRIRERCwgjKwAAAAAC//AAAAdZBbAAEgAVAEdARBQBBQQBSgAFAAYIBQZlCgEIAAEHCAFlAAQEA10AAwMlSwkBBwcAXQIBAAAmAEwTEwAAExUTFQASABIRERExEREhCwgbKyUVISMDIQMjATMzIRUhEyEVIRMDAwEHWf0jmA/9zc3lA3F4iQK5/U0UAk79uBfLH/5XlpYBYv6eBbCW/ieW/esBeQLe/SIAAAAAAwCpAAAEiAWwABAAGgAjADxAORABBAIBSgACAAQFAgRlBgEDAwFdAAEBJUsHAQUFAF0AAAAmAEwbGxERGyMbIiEfERoRGSchNQgIFysAFhUUBgYjISMRITIWFRQGBwERITI2NjU0JiMSNjU0JiMhESEEAoZ22ZL+TEoB3uX2c2b94gEhUn1Fio+nmIiO/rsBPALfvX6GvWEFsL7GZJ4rAhT+MDhqSXdu+4mNeX6K/fIAAAAAAQB3/+wE2QXEACIAMEAtAAIDBQMCBX4ABQQDBQR8AAMDAV8AAQEtSwAEBABfAAAALgBMEyciEyciBggaKwAGBiMiJAI1NTQSJDMyFhYXIyYmIyIGBhUVFBYWMzI2NjczBMmC7aqp/v6OjwEKsqHmgQ/CF56gf7BZUqh8d5NLEsIBQNl7pwEyyZPKATKneduTo6eA6JyVlOqIS45tAAAAAAEAd/5DBNkFxAAxAEVAQhYIAgIGAUoABAUABQQAfgAABgUABnwHAQYCBQYCfAAFBQNfAAMDLUsAAgIBYAABATIBTAAAADEAMCITLxEZEwgIGiskNjY3MwYCBwcWFhUUBiMnMjY1NCYmJzcmJgI1NTQSJDMyFhYXIyYmIyIGBhUVFBYWMwMnk0sSwhf23gpDVp6SB0pcIUQ8Hpfheo8BCrKh5oEPwheeoH+wWVKofIhLjm3N/v4RLQtSUGBxajEyIiYSBYERrwEju5PKATKneduTo6eA6JyVlOqIAAACAKkAAATHBbAADQAZACdAJAACAgFdBAEBASVLAAMDAF0AAAAmAEwAABYUExEADQALNwUIFSsABBIVFRQCBCMhIxEzMwE0JiYjIxEzMjY2NQMDASOhof7Wyf7QWsLZAcNpy4/ZyJjSagWwp/7Kz1jR/sumBbD9VqbrfPuJge2iAAIABgAABMcFsAARACEAN0A0BQECBgEBBwIBZQAEBANdCAEDAyVLAAcHAF0AAAAmAEwAAB4cGxoZGBcVABEADxERNwkIFysABBIVFRQCBCMhIxEjNTMRMzMBNCYmIyMRIRUhETMyNjY1AwMBI6Gh/tbJ/tBao6PC2QHDacuP2QEZ/ufImNJqBbCn/srPWNH+y6YCmpYCgP1Wput8/h2W/gKB7aIAAQCpAAAERgWwAA0AL0AsAAMABAUDBGUAAgIBXQABASVLBgEFBQBdAAAAJgBMAAAADQANERERISEHCBkrJRUhIxEzIRUhESEVIREERvz/nMIC0f0vAnX9i5ycBbCd/iuc/foA//8AqQAABEYHQgAjAc8AqQAAACIAEAAAAQcBsQShAUIAQkA/CQEHBgeDAAYBBoMAAwAEBQMEZQACAgFdAAEBJUsIAQUFAF0AAAAmAEwPDwEBDxIPEhEQAQ4BDhERESEiCggkK///AKkAAARGB0IAIwHPAKkAAAAiABAAAAEHAbUAkwFCAEtASBEBBggBSgAIBgiDBwoCBgEGgwADAAQFAwRlAAICAV0AAQElSwkBBQUAXQAAACYATBAPAQEWFRMSDxcQFwEOAQ4REREhIgsIJCsAAAD//wCpAAAERgcIACMBzwCpAAAAIgAQAAABBwG2AMQBQgBLQEgMCQsDBwgBBgEHBmcAAwAEBQMEZQACAgFdAAEBJUsKAQUFAF0AAAAmAEwbGw8PAQEbJhslIR8PGg8ZFRMBDgEOERERISINCCQrAAAA//8AqQAABEYHQgAjAc8AqQAAACIAEAAAAQcBsAR8AUIAQkA/AAYHBoMJAQcBB4MAAwAEBQMEZQACAgFdAAEBJUsIAQUFAF0AAAAmAEwPDwEBDxIPEhEQAQ4BDhERESEiCggkKwABAKkAAAQvBbAACgApQCYAAAABAgABZQUBBAQDXQADAyVLAAICJgJMAAAACgAKIREREQYIGCsBESEVIREjETMhFQFrAmH9n8LCAsQFE/4NnP18BbCdAAAAAQB6/+wE3QXEACIAOUA2Ih0CBAUBSgACAwYDAgZ+AAYABQQGBWUAAwMBXwABAS1LAAQEAF8AAAAuAEwREyYiEichBwgbKyQGIyIkAjU1NBIkMzIEFyMmJiMiAhUVFBYWMzI2NxEhNSERBKPx2LL+7ZuIAQe68AENHcIWpZzFwme7fImQKP6wAhFrf6UBOdVz2QE1pPLMgp/+6v11pfKAPS0BSJv96gAAAQCpAAAFCQWwAAsAJ0AkAAQAAQAEAWUGBQIDAyVLAgEAACYATAAAAAsACxERERERBwgZKwERIxEhESMRMxEhEQUJwv0kwsIC3AWw+lACov1eBbD9jgJyAAEAtwAAAXkFsAADABlAFgAAACVLAgEBASYBTAAAAAMAAxEDCBUrMxEzEbfCBbD6UAAAAP//ALcAAAJdB0IAIwHPALcAAAAiABgAAAEHAbEDTQFCACxAKQUBAwIDgwACAAKDAAAAJUsEAQEBJgFMBQUBAQUIBQgHBgEEAQQSBgggKwAA////6QAAAkcHQgAiAc8AAAAiABgAAAEHAbX/PwFCADVAMgcBAgQBSgAEAgSDAwYCAgACgwAAACVLBQEBASYBTAYFAQEMCwkIBQ0GDQEEAQQSBwggKwAAAP///9QAAAJfBwgAIgHPAAAAIgAYAAABBwG2/3ABQgA1QDIIBQcDAwQBAgADAmcAAAAlSwYBAQEmAUwREQUFAQERHBEbFxUFEAUPCwkBBAEEEgkIICsAAAD////NAAABeQdCACIBzwAAACIAGAAAAQcBsAMoAUIALEApAAIDAoMFAQMAA4MAAAAlSwQBAQEmAUwFBQEBBQgFCAcGAQQBBBIGCCArAAEANf/sA80FsAASAChAJQABAwIDAQJ+BAEDAyVLAAICAF8AAAAuAEwAAAASABIiEyQFCBcrAREUBgYjIiYmNTMUFjMyNjY1EQPNddCHic90wpB6TXhFBbD7+o3JaF69jIWGQ4NcBAYAAQCpAAAFBgWwAAsAH0AcCQYBAwABAUoCAQEBJUsDAQAAJgBMEhIREgQIGCsBBxEjETMRATMBASMCHbLCwgKF6v3DAmnoAqa4/hIFsP0yAs79fvzSAAAAAAEAqQAABB0FsAAGAB9AHAABASVLAwECAgBeAAAAJgBMAAAABgAGESEECBYrJRUhIxEzEQQd/SicwpycBbD67AABAKkAAAZTBbAADwAhQB4NBgMDAAMBSgQBAwMlSwIBAgAAJgBMEiETExAFCBkrISMREwEjARMRIxEzMwEBMwZTwRL+I5P+JBLBplQB2wHb+gI5Al/7aASX/aL9xwWw+1oEpgAAAQCpAAAFCQWwAAkAJEAhBgECAAEBSgIBAQElSwQDAgAAJgBMAAAACQAJEhESBQgXKyEBESMRMwERMxEERv0lwsIC3sAEZvuaBbD7lwRp+lAAAAD//wCpAAAFCQcjACMBzwCpAAAAIgAhAAABBwGyBOwBOwB3QAwHAgIAAQFKJiUCB0hLsCNQWEAfAAcABQQHBWcACAYBBAEIBGcCAQEBJUsJAwIAACYATBtAJgAGBAEEBgF+AAcABQQHBWcACAAEBggEZwIBAQElSwkDAgAAJgBMWUAWAQEjIR0bGBcVEw8NAQoBChIREwoIIisAAAAAAgB2/+wFCgXEABEAIgAfQBwAAgIBXwABAS1LAAMDAF8AAAAuAEwnJyciBAgYKwACBCMiJAI1NTQSJDMyBBIVFQImJiMiBgYVFRQWFjMyEhE1BQqP/vixrP71lZQBCqyxAQmQwF2xfHeyYGGyeLzMAdH+xKmqAT3WXdYBPqqq/sPXXQEL8Xx98atfrPJ9ARYBBV8A//8Adv/sBQoHOQAiAc92AAAiACMAAAEHAbEE+QE5ADFALgYBBQQFgwAEAQSDAAICAV8AAQEtSwADAwBfAAAALgBMJCQkJyQnFScnJyMHCCQrAAAA//8Adv/sBQoHOQAiAc92AAAiACMAAAEHAbUA6wE5ADtAOCYBBAYBSgAGBAaDBQcCBAEEgwACAgFfAAEBLUsAAwMAXwAAAC4ATCUkKyooJyQsJSwnJycjCAgjKwD//wB2/+wFCgb/ACIBz3YAACIAIwAAAQcBtgEcATkAOkA3CQcIAwUGAQQBBQRnAAICAV8AAQEtSwADAwBfAAAALgBMMDAkJDA7MDo2NCQvJC4oJycnIwoIJCsAAP//AHb/7AUKBzkAIgHPdgAAIgAjAAABBwGwBNQBOQAxQC4ABAUEgwYBBQEFgwACAgFfAAEBLUsAAwMAXwAAAC4ATCQkJCckJxUnJycjBwgkKwAAAAADAHb/owUeBewAGgAkAC0APUA6GAEEAicmHRwEBQQNCgIABQNKAAEAAYQAAwMnSwAEBAJfAAICLUsABQUAXwAAAC4ATCgkEygSJwYIGisBFhYVFRQCBCMiJwcjNyYCNTU0EiQzMhYXNzMAFwEmIyIGBhUVACcBFjMyEhE1BH9ER4/++LGnh2CQkVxjlAEKrGu3SGeN/BlhAjRkqHeyYAMTN/3cWXq8zATtWveWXdj+xKlSm+hcARSuXdYBPqo/PaT7y44DiHB98atfARh//I9BARYBBV8AAP//AHb/7AUKByUAIgHPdgAAIgAjAAABBwGyBNkBPQBztD8+AgdIS7AjUFhAJgAHAAUEBwVnAAgGAQQBCARnAAICAV8AAQEtSwADAwBfAAAALgBMG0AtAAYEAQQGAX4ABwAFBAcFZwAIAAQGCARnAAICAV8AAQEtSwADAwBfAAAALgBMWUAMJCMSJCYnJycjCQgoKwAAAgBp/+sHCQXFABoAKADUS7AUUFhACiEBBAIgAQAHAkobQAohAQQJIAEIBwJKWUuwFFBYQCIABQAGBwUGZQkBBAQCXwMBAgItSwgKAgcHAF8BAQAAJgBMG0uwF1BYQDYABQAGBwUGZQAJCQJfAwECAi1LAAQEAl8DAQICLUsKAQcHAF8BAQAAJksACAgAXwEBAAAmAEwbQDIABQAGBwUGZQAJCQJfAAICLUsABAQDXQADAyVLCgEHBwBdAAAAJksACAgBXwABAS4BTFlZQBQAACQiHx0AGgAaERERISciIQsIGyslFSEjBgYjIiYCNRE0EjYzMhczIRUhESEVIREAFhYzMjcRJiMiBgYVEQcJ/P5NXIREpP2Mi/2jeqx0AtH9LwJ1/Yv89lqmcmZwam5ypVmcnAoLlwEPrwEwrwEPlxWd/iuc/foBFclnDwSQDmbHj/7OAAIAqQAABMIFsAAMABYAMEAtBgEEAAABBABlAAMDAl0FAQICJUsAAQEmAUwNDQAADRYNFRQSAAwACxEmBwgWKwAWFhUUBgYjIREjESESNjU0JiYjIREhA2Tnd3blpP6owgIaoptDj2v+qAFYBbBxyIOMxmf9xQWw/SeYg0+DT/3EAAAAAAIApgAABF4FsAAOABkANEAxBgEDAAQFAwRlBwEFAAABBQBlAAICJUsAAQEmAUwPDwAADxkPGBcVAA4ADRERJggIFysAFhYVFAYGIyERIxEzESESNjY1NCYmIyERIQMQ3HJy3Jr+67u7ARVmh0BAh2b+6wEVBIttwHx7wG3+xgWw/tv9RUx8SEp+Tf3bAAAAAAIAbf8JBQcFxAAVACYAKUAmFAEAAgFKFQEARwADAwFfAAEBLUsAAgIAXwAAAC4ATCYrJyIECBgrBSUGIyIkAjU1NBIkMzIEEhUVFAIHBQAWFjMyEhE1NCYmIyIGBhUVBIT+zEZRrP71lZQBCqyxAQmRhXoBBPwoYbJ3vMxdsXx3sWD39RKqAT3WXdYBPqqp/sLXXc/+y1fNAnzyfQEWAQVfrPF8ffGrXwAAAAACAKkAAATKBbAADgAYADJALw4BAQQBSgYBBAABAAQBZQAFBQNdAAMDJUsCAQAAJgBMEA8XFQ8YEBghEREgBwgYKyUVIwEhESMRITIEFRQGBycyNjY1NCYjIREEytD+xP6twgHi9wEIk4PkZIxHnKD+4AwMAk79sgWw4dSHyzN0SHxNhJT91wAAAQBR/+wEcwXEAC0AMEAtAAECBAIBBH4ABAUCBAV8AAICAF8AAAAtSwAFBQNfAAMDLgNMIxMtIhMlBggaKwAmNTQ2NjMyFhYVIzQmIyIGFRQWFhceAhUUBgYjIiYmNTMUFhYzMjY1NCYmJwFv8Hvhkp/pe8OinpOYRpR5qNFlfeaZi/2ewmGhYpWkO5SLAs7JoXG0Z3rKdICbgmo+XkwiL3qlcXWxYGfIi2CAPnxsR19NKAAAAQCP/+sEzwXEACYAikuwF1BYQBAlFhQDBgMJAQECCAEAAQNKG0AQJRYUAwYDCQEBAggBBAEDSllLsBdQWEAfBwEGAAIBBgJlAAMDBV8ABQUtSwABAQBfBAEAAC4ATBtAIwcBBgACAQYCZQADAwVfAAUFLUsABAQmSwABAQBfAAAALgBMWUAPAAAAJgAmIxMlJSMlCAgaKwAWFRQGBiMiJzcWMzI2NjU0JiMjNTUBJiYjIgYVESMREBIzMhYXAQPc833ekZOGNmx3X4pJpKaTATM6hFyXcbrf45fybv60A0PiyYbBZjKYM0V8UZmQkgQBdS83w6v8QAPCAQEBAXdp/moAAAABADIAAASXBbAACAAbQBgCAQAAA10AAwMlSwABASYBTCERERAECBgrASERIxEhNSEhBJf+LcD+LgKSAdMFE/rtBROdAAEAjP/sBKsFsAATABtAGAMBAQElSwACAgBfAAAALgBMEyMUIwQIGCsBFAYGIyImJjURMxEUFjMyNjURMwSrj/GRmO2JwLObnLLDAdee3m9v3Z8D2fwnpqmppgPZAAAA//8AjP/sBKsHNwAjAc8AjAAAACIAMgAAAQcBsQTRATcALUAqBgEFBAWDAAQBBIMDAQEBJUsAAgIAXwAAAC4ATBUVFRgVGBITIxQkBwgkKwD//wCM/+wEqwc3ACMBzwCMAAAAIgAyAAABBwG1AMMBNwA3QDQXAQQGAUoABgQGgwUHAgQBBIMDAQEBJUsAAgIAXwAAAC4ATBYVHBsZGBUdFh0TIxQkCAgjKwAAAP//AIz/7ASrBv0AIwHPAIwAAAAiADIAAAEHAbYA9AE3ADZAMwkHCAMFBgEEAQUEZwMBAQElSwACAgBfAAAALgBMISEVFSEsISsnJRUgFR8lEyMUJAoIJCv//wCM/+wEqwc3ACMBzwCMAAAAIgAyAAABBwGwBKwBNwAtQCoABAUEgwYBBQEFgwMBAQElSwACAgBfAAAALgBMFRUVGBUYEhMjFCQHCCQrAAABAB0AAAT+BbAABwAhQB4GAQABAUoDAgIBASVLAAAAJgBMAAAABwAHESEECBYrAQEjIwEzAQEE/v3llhb95tMBnAGfBbD6UAWw+1AEsAAAAAEAPQAABu0FsAAVACdAJBMNBQMAAgFKBQQDAwICJUsBAQAAJgBMAAAAFQAVJBEkIQYIGCsBASMjAScHASMjATMTFzcBMzMBFzcTBu3+oIsm/tUWF/7KjST+oMLlHCgBII4VARgoH+AFsPpQBCdtbfvZBbD8Gr6rA/n8B6/DA+UAAQA6AAAEzwWwAAsAIEAdCwgFAgQBAAFKAwEAACVLAgEBASYBTBISEhAECBgrATMBASMBASMBATMBA+Hj/jUB1uX+m/6Z5AHX/jPkAV0FsP0v/SECOv3GAt8C0f3RAAAAAQAPAAAEvAWwAAgAHUAaCAUCAwEAAUoCAQAAJUsAAQEmAUwSEhADCBcrATMBESMRATMBA+Dc/gvD/gveAXkFsPxw/eACIAOQ/SQA//8ADwAABLwHNgAiAc8PAAAiADoAAAEHAbEEoAE2AC9ALAkGAwMBAAFKBQEEAwSDAAMAA4MCAQAAJUsAAQEmAUwKCgoNCg0TEhIRBggjKwAAAQBXAAAEegWwAAsAL0AsCgEBAgQBAAMCSgABAQJdAAICJUsEAQMDAF0AAAAmAEwAAAALAAshEiEFCBcrJRUhIzUBITUhMxUBBHr8JkkDHvzuA6VS/OCcnI8EhJ2L+3cAAQCyAAAFHwWwAAwAJ0AkCgEAAwFKAAMAAAEDAGUEAQICJUsFAQEBJgFMEhEREREQBggaKwEjESMRMxEzATMBASMCJLDCwpUB+/H91AJW7gKP/XEFsP1+AoL9QP0QAAAAAgCyAAAE5AWwABkAIgA1QDIVAQEFAQEAAQJKBgEFAAEABQFlAAQEA10AAwMlSwIBAAAmAEwaGhoiGiEuIREmEgcIGSskFxUjJjU1NCYmIyERIxEhMhYVFAYHFhYVFQA2NTQmIyERIQShQ8c+P3dQ/pvCAg7v/Hp0emr+s5WRmv62AThYPxk4toFPdkH9iwWw1MtwpzEmr4OFAiWBf3uH/f4AAAABALIAAAT8BbAACwAlQCIKBgEDAAEBSgIBAQElSwQDAgAAJgBMAAAACwALExESBQgXKyEBESMRMxEzATMBAQQN/WfCwgsCY/L9bAK8ArT9TAWw/XkCh/07/RUAAAEAfv/rBR8FxQAhADBALQACAwUDAgV+AAUEAwUEfAADAwFfAAEBLUsABAQAXwAAAC4ATBInIhMnIgYIGisABgYjIiQCNTU0EiQzMhYWFyMmJiMiBgYVFRQWFjMyNjczBQ6I76Wx/uGknQEZtaL0kBDDF7mjgb9ob8Z8qKkZwgFB2ny0AUfTPdUBRrR73JGdro3+pT+k/4+moQAAAAIAsgAABRIFsAANABkAJ0AkAAICAV0EAQEBJUsAAwMAXQAAACYATAAAFhQTEQANAAs3BQgVKwAEEhUVFAIEIyEjETMzATQmJiMjETMyNjY1AyYBObOz/sDM/rhZwvAB6nrek+/eneV6BbCu/r/UK9X+wK0FsP0/qviC+4mJ+aQAAQB+/+sFIAXFACQAOUA2JB8CBAUBSgACAwYDAgZ+AAYABQQGBWUAAwMBXwABAS1LAAQEAF8AAAAuAEwREyciEiciBwgbKyQGBiMiJAI1NTQSJDMyBBcjJiYjIgYCFRUUEhYzMjY3ESE1IREFAIXbkbr+1aygARqz/gEXIMMarKx8wmx40oSDqiL+sgIQkGFEswFO3xvgAU2y8c6FnY/++6wdsP75jUIpAUea/ewAAAACAH7/6wVgBcUAEQAjAB9AHAACAgFfAAEBLUsAAwMAXwAAAC4ATCcnJyIECBgrAAIEIyIkAjU1NBIkMzIEEhUVLgIjIgYCFRUUEhYzMjYSNTUFYKL+5LGt/uGnpwEerLEBHaPAcMV8d8ZzdMd3fcRuAfD+tbq6AUzRLNEBS7u6/rXSLNP/jo7/AKQupf7/j44BAKcuAAAAAAIAfv8EBWAFxQAVACcAKkAnAgEAAgFKBAMCAEcAAwMBXwABAS1LAAICAF8AAAAuAEwnJycmBAgYKwACBxcHJQYjIiQCNTU0EiQzMgQSFRUEEhYzMjYSNTU0AiYjIgYCFRUFYKqT+oT+zjw6rf7hp6YBH6yxAR2j+990x3d9xG5wxXx3xnMB6v6vWcR48gu6AUzRKtEBTbu6/rPSKqX+/o6NAQGnLKYBAI6O/v+lLAAAAgBt/+wD6wROACQAMQBEQEEoAQYFBgQCAwAGAkoAAwIBAgMBfgABAAUGAQVlAAICBF8ABAQwSwcBBgYAXwAAAC4ATCUlJTElMCkjEyMmKAgIGiskFhcVIyYnBgYjIiYmNTQ2NjMzNTQmIyIGBhUjNDY2MzIWFhURBDY2NzUjIgYVFBYWMwPFFBLDEQk4nV5sp1t325W2dW5Daju7bMR+eLVm/nFvUBWYoKowXEKzeSoQJ0o8SVWSWXOgUVZjcS9PLk+UXVGjdv4IYC1KK+BlYzVUMf//AG3/7APrBgAAIgHPbQAAIgBFAAABAwGxBHwAAACYQAwpAQYFBwUDAwAGAkpLsCNQWEA0AAcIBAgHBH4AAwIBAgMBfgABAAUGAQVmCgEICCdLAAICBF8ABAQwSwkBBgYAXwAAAC4ATBtAMQoBCAcIgwAHBAeDAAMCAQIDAX4AAQAFBgEFZgACAgRfAAQEMEsJAQYGAF8AAAAuAExZQBczMyYmMzYzNjU0JjImMSkjEyMmKQsIJSv//wBt/+wD6wYAACIBz20AACIARQAAAQIBtW0AAKBAEDUBBwkpAQYFBwUDAwAGA0pLsCNQWEA1CAsCBwkECQcEfgADAgECAwF+AAEABQYBBWUACQknSwACAgRfAAQEMEsKAQYGAF8AAAAuAEwbQDIACQcJgwgLAgcEB4MAAwIBAgMBfgABAAUGAQVlAAICBF8ABAQwSwoBBgYAXwAAAC4ATFlAGTQzJiY6OTc2Mzs0OyYyJjEpIxMjJikMCCUrAAD//wBt/+wD6wXGACIBz20AACIARQAAAQMBtgCfAAAAYkBfKQEGBQcFAwMABgJKAAMCAQIDAX4AAQAFBgEFZQkBBwcIXw0KDAMICC1LAAICBF8ABAQwSwsBBgYAXwAAAC4ATD8/MzMmJj9KP0lFQzM+Mz05NyYyJjEpIxMjJikOCCUrAAD//wBt/+wD6wYAACIBz20AACIARQAAAQMBsARXAAAAk0AMKQEGBQcFAwMABgJKS7AjUFhAMQoBCAcEBwgEfgABAAUGAQVlAAICBF8ABAQwSwADAwddAAcHJ0sJAQYGAF8AAAAuAEwbQC8KAQgHBAcIBH4ABwADAQcDZQABAAUGAQVlAAICBF8ABAQwSwkBBgYAXwAAAC4ATFlAFzMzJiYzNjM2NTQmMiYxKSMTIyYpCwglKwD//wBt/+wD6wZbACIBz20AACIARQAAAQcBuQD1AAsAZkBjKQEGBQcFAwMABgJKAAMCAQIDAX4MAQgACQoICWcNAQoABwQKB2cAAQAFBgEFZQACAgRfAAQEMEsLAQYGAF8AAAAuAExDQzMzJiZDT0NOSUczQjNBOzkmMiYxKSMTIyYpDgglKwAA//8Abf/sA+sF7AAiAc9tAAAiAEUAAAEHAbIEWwAEALNAESkBBgUHBQMDAAYCSk5NAgpIS7AjUFhAOQADAgECAwF+AAsJAQcECwdnAAEABQYBBWUACAgKXwAKCidLAAICBF8ABAQwSwwBBgYAXwAAAC4ATBtAQAAJBwQHCQR+AAMCAQIDAX4ACwAHCQsHZwABAAUGAQVlAAgICl8ACgonSwACAgRfAAQEMEsMAQYGAF8AAAAuAExZQBkmJktJRUNAPz07NzUmMiYxKSMTIyYpDQglKwAAAwBO/+sGfQRPADIAOwBHAE9ATCskIwMEBT4QCQMBAAoBAgEDSgkBBAoBAAEEAGUIAQUFBl8HAQYGMEsMCwIBAQJfAwECAi4CTDw8PEc8RkE/OzomJCcjJSQmIxANCB0rASEeAjMyNjY3FwYGIyImJwYGIyImJjU0NjMzNTQmIyIGBhUnNDY2MzIWFzY2FzIWFhUnNCYjIgYGByEANjc1IyIGBhUUFjMGff0pA0mRbEl0TTFBN7qLg8pBOtubcKBT5tvdbmZIbz26bMR+dLAwP7Fmjsdmu3yETnZJCwIY/FydMdtUdz1oZQHza6ZgICohhy1OXlZMaFCSY5+sVW17NFg0El2TVFRRT1cBdtqWN3ydUI1b/gFMNPA2WjVMXwAAAgCM/+wEIQYAABEAHwCsS7AZUFhADw8BBAMZGAIFBAoBAAUDShtADw8BBAMZGAIFBAoBAQUDSllLsBlQWEAcAAICJ0sABAQDXwYBAwMwSwAFBQBfAQEAAC4ATBtLsCNQWEAgAAICJ0sABAQDXwYBAwMwSwABASZLAAUFAF8AAAAuAEwbQCAABAQDXwYBAwMwSwACAgFdAAEBJksABQUAXwAAAC4ATFlZQBAAAB0bFhQAEQAQERInBwgXKwAWFhUVFAYGIyInByMRMxE2MxM0JiMiBgcRFhYzMjY1AwC+Y2O+g81wCau7b8Xrh5NfgSUmg16NiwROifmmFZ/5jY97BgD9xoj92LLbW07+J01c264AAAAAAQBc/+wD7gROACIANkAzAAIDBQMCBX4GAQUEAwUEfAADAwFfAAEBMEsABAQAXwAAAC4ATAAAACIAIiYjEycjBwgZKwEOAiMiJiY1NTQ2NjMyFhYXIy4CIyIGBhUVFBYzMjY2NwPuBXjDcJ3ZbGzZnXzBbgWxBUJwSGuEOImeQXFIBQFWX6dkkfWWKpb1kWazb0NvQG+vaCqm4DdhPAAAAAEAXP5DA+4ETgAyAE1AShgBAQcXCQIDAQJKAAUGAAYFAH4AAAcGAAd8AAYGBF8ABAQwSwgBBwcBXwABAS5LAAMDAl8AAgIyAkwAAAAyADEjEy8RFhMTCQgbKyQ2NjczDgIHBxYWFRQGIycyNjU0JiYnNy4CNTU0NjYzMhYWFyMuAiMiBgYVFRQWMwJ/cUgFsQVvtWoKQ1aekgdKXCFEPB98qlVs2Z18wW4FsQVCcEhrhDiJnoI3YTxbomYGLAtSUGBxajEyIiYSBYYXmOCEKpb1kWazb0NvQG+vaCqm4AAAAgBf/+wD8QYAABEAIACbQA8QAQQCFBMCBQQDAQAFA0pLsBlQWEAdBgEDAydLAAQEAl8AAgIwSwcBBQUAXwEBAAAmAEwbS7AjUFhAIQYBAwMnSwAEBAJfAAICMEsAAAAmSwcBBQUBXwABAS4BTBtAIQAEBAJfAAICMEsGAQMDAF0AAAAmSwcBBQUBXwABAS4BTFlZQBQSEgAAEiASHxgWABEAESciEQgIFysBESMnBiMiJiY1NTQ2NjMyFxECNxEmJiMiBgYVFRQWFjMD8asJbsZ9wmtqwoC/bFJSJn1aYYE9PYBgBgD6AHOHjvqdFaT6in8CMfqInQH1R1JptHAVcLFoAAACAH7/7AQuBi0AHwAxADJALxIBAgEBSh8eHRwaGRcWFRQKAUgAAQACAwECZwADAwBfAAAALgBMLiwmJCYmBAgWKwASFRUUBgYjIiYmNTQ2NjMyFhcmJwcnNyYnNxYXNxcHEzQnJiYjIgYGFRQWFjMyNjY1A62BeNaGitl5dMyAUZI4MovZSsGFtzrvr71JqD8BIIdhYIlIRoVaVH1EBJf+rctiov2Mgt+Hl+h/PTTHiJNjglwwnzeKgGNy/WQnEjpNV6JuVZthabd0AAACAF3/7AP0BE4AGQAhADBALQkIAgEAAUoABQAAAQUAZQAEBANfAAMDMEsAAQECXwACAi4CTBImJyUjEAYIGisBIR4CMzI2NxcGBiMiJiY1NTQ2NjMyFhYVJyYmIyIGByED9P0kA06PYF6JNnE8yJSU4nuE2XyXx2C7CHmCcJUTAhsB6mWkX0xHWFx1hu6XK6n9hofunFF/q6OVAP//AF3/7AP0BgAAIgHPXQAAIgBSAAABAwGxBGsAAAB5tgoJAgEAAUpLsCNQWEArAAYHAwcGA34ABQAAAQUAZQgBBwcnSwAEBANfAAMDMEsAAQECXwACAi4CTBtAKAgBBwYHgwAGAwaDAAUAAAEFAGUABAQDXwADAzBLAAEBAl8AAgIuAkxZQBAjIyMmIyYSEiYnJSMRCQgmKwAAAP//AF3/7AP0BgAAIgHPXQAAIgBSAAABAgG1XQAAg0ALJQEGCAoJAgEAAkpLsCNQWEAsBwkCBggDCAYDfgAFAAABBQBlAAgIJ0sABAQDXwADAzBLAAEBAl8AAgIuAkwbQCkACAYIgwcJAgYDBoMABQAAAQUAZQAEBANfAAMDMEsAAQECXwACAi4CTFlAEyQjKiknJiMrJCsSJiclIxEKCCUrAAAA//8AXf/sA/QFxgAiAc9dAAAiAFIAAAEDAbYAjgAAAE1ASgoJAgEAAUoABQAAAQUAZQgBBgYHXwsJCgMHBy1LAAQEA18AAwMwSwABAQJfAAICLgJMLy8jIy86Lzk1MyMuIy0lEiYnJSMRDAgmKwAAAP//AF3/7AP0BgAAIgHPXQAAIgBSAAABAwGwBEcAAAB5tgoJAgEAAUpLsCNQWEArCAEHBgMGBwN+AAUAAAEFAGUABgYnSwAEBANfAAMDMEsAAQECXwACAi4CTBtAKAAGBwaDCAEHAweDAAUAAAEFAGUABAQDXwADAzBLAAEBAl8AAgIuAkxZQBAjIyMmIyYSEiYnJSMRCQgmKwAAAAABAD0AAALLBhUAFQA3QDQSAQYFEwEABgJKAAUHAQYABQZnAwEBAQBdBAEAAChLAAICJgJMAAAAFQAUIxERERETCAgaKwAGFRUzFSMRIxEjNTM1NDYzMhcHJiMCBGLm5ruqqr2oPUIKLTcFeGlic438UwOtjXOtuxCVCAAAAgBg/lUD8wROAB4ALQGgS7AZUFhAFB0BBQMhIAIGBRABAgYKCQIBAgRKG0AUHQEFBCEgAgYFEAECBgoJAgECBEpZS7AKUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAqAEwbS7AMUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAyAEwbS7AQUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAqAEwbS7ASUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAyAEwbS7AUUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAqAEwbS7AXUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAyAEwbS7AZUFhAIgAFBQNfBwQCAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAqAEwbQCYHAQQEKEsABQUDXwADAzBLCAEGBgJfAAICLksAAQEAXwAAADIATFlZWVlZWVlAFR8fAAAfLR8sJSMAHgAeJyQlJAkIGCsBERQGBiMiJiYnNxYzMjY1NQYjIiYmNTU0NjYzMhc3AjcRJiYjIgYGFRUUFhYzA/N0z4g/lo8tYXiqhZluwIDBaWjCgsZvCGNSJn5aYYE9PYBgBDr73Y3LaiZUQm2Tl4xdf475nhWk+oqLd/xOnwHxSFNptHAVcLFoAAAAAAEAjAAAA+AGAAASAFVACg8BAQQKAQABAkpLsCNQWEAXAAMDJ0sAAQEEXwUBBAQwSwIBAAAmAEwbQBcAAQEEXwUBBAQwSwADAwBdAgEAACYATFlADQAAABIAERETIxIGCBgrABERIxE0JiMiBgcRIxEzETY2MwPgu2tsVokou7s7omEETv53/TsCx3xwXk/8+gYA/bpHTQAAAgCOAAABagXEAAsADwAsQCkAAAABXwQBAQEtSwACAihLBQEDAyYDTAwMAAAMDwwPDg0ACwAKJAYIFSsAFhUUBiMiJjU0NjMDETMRAS87OzQ0OTk0X7sFxDwuLTs7LS48+jwEOvvGAAABAJwAAAFWBDoAAwAZQBYAAAAoSwIBAQEmAUwAAAADAAMRAwgVKzMRMxGcugQ6+8YAAAD//wCcAAACQwX+ACMBzwCcAAAAIgBbAAABBwGxAzP//gBRS7AkUFhAGgACAwADAgB+BQEDAydLAAAAKEsEAQEBJgFMG0AXBQEDAgODAAIAAoMAAAAoSwQBAQEmAUxZQBIFBQEBBQgFCAcGAQQBBBIGCCArAP///88AAAItBf4AIgHPAAAAIgBbAAABBwG1/yX//gBctQcBAgQBSkuwJFBYQBsDBgICBAAEAgB+AAQEJ0sAAAAoSwUBAQEmAUwbQBgABAIEgwMGAgIAAoMAAAAoSwUBAQEmAUxZQBQGBQEBDAsJCAUNBg0BBAEEEgcIICv///+6AAACRQXEACIBzwAAACIAWwAAAQcBtv9W//4AN0A0BAECAgNfCAUHAwMDLUsAAAAoSwYBAQEmAUwREQUFAQERHBEbFxUFEAUPCwkBBAEEEgkIICsA////swAAAVYF/gAiAc8AAAAiAFsAAAEHAbADDv/+AFFLsCRQWEAaBQEDAgACAwB+AAICJ0sAAAAoSwQBAQEmAUwbQBcAAgMCgwUBAwADgwAAAChLBAEBASYBTFlAEgUFAQEFCAUIBwYBBAEEEgYIICsAAAAAAv+//ksBWwXEAAsAGgA4QDUTAQMEEgECAwJKAAAAAV8FAQEBLUsABAQoSwADAwJgAAICMgJMAAAaGRYUEA4ACwAKJAYIFSsAFhUUBiMiJjU0NjMTFAYjIiYnNxYzMjY1ETMBIDs7NDQ5OTRhjo0aQhcBJTA+P7sFxDwuLTs7LS48+buXnQoHlAlHUQS7AAAAAQCNAAAEDgYAAAwARbcKBgEDAAIBSkuwI1BYQBEAAQEnSwACAihLAwEAACYATBtAFwABAQBdAwEAACZLAAICKEsDAQAAJgBMWbYSExESBAgYKwEHESMRMxE3ATMBASMBvHS7u2MBTuP+WwHX3AH2eP6CBgD8YHUBZf49/YkAAAABAJwAAAFXBgAAAwAwS7AjUFhADAAAACdLAgEBASYBTBtADAAAAAFdAgEBASYBTFlACgAAAAMAAxEDCBUrMxEzEZy7BgD6AAABAIsAAAZ4BE4AIgBcQAwZAQEFHxQMAwABAkpLsBlQWEAWAwEBAQVfCAcGAwUFKEsEAgIAACYATBtAGgAFBShLAwEBAQZfCAcCBgYwSwQCAgAAJgBMWUAQAAAAIgAhIxETIhQjEwkIGysAFhURIxE0JiMiBgYHESMRNCMiBgcRIxEzFzY2MzIWFzY2MwXAuLt1cUhsPQe75116ILuxBjigZ2uhKTivcQROvsn9OQLJgWk9aEH9MwLI61FI/OYEOnVCR1VXUFwAAAEAjAAAA+AETgASAFFACg8BAQMKAQABAkpLsBlQWEATAAEBA18FBAIDAyhLAgEAACYATBtAFwADAyhLAAEBBF8FAQQEMEsCAQAAJgBMWUANAAAAEgARERMjEgYIGCsAEREjETQmIyIGBxEjETMXNjYzA+C7a2xWiSi7sQY7pGMETv53/TsCx3xwXk/8+gQ6hUlQAP//AIwAAAPgBewAIwHPAIwAAAAiAGQAAAEHAbIEUgAEAMBADxABAQMLAQABAkovLgIISEuwGVBYQCYACQcBBQMJBWcABgYIXwAICCdLAAEBA18KBAIDAyhLAgEAACYATBtLsCNQWEAqAAkHAQUECQVnAAYGCF8ACAgnSwADAyhLAAEBBF8KAQQEMEsCAQAAJgBMG0AxAAcFBAUHBH4ACQAFBwkFZwAGBghfAAgIJ0sAAwMoSwABAQRfCgEEBDBLAgEAACYATFlZQBcBASwqJiQhIB4cGBYBEwESERMjEwsIIysAAAACAFz/7AQ1BE4AEQAjAB9AHAADAwBfAAAAMEsAAgIBXwABAS4BTCcnJyIECBgrEjY2MzIWFhUVFAYGIyImJjU1HgIzMjY2NTU0JiYjIgYGFRVceN+UluB4eN+Vld95u0WKY2KKRUaKY2KJRQLF+o+P+p0XnfmPj/mdF4W2a2u2bhdutmxstm4XAAAA//8AXP/sBDUGAAAiAc9cAAAiAGYAAAEDAbEEdQAAAF9LsCNQWEAjAAQFAAUEAH4GAQUFJ0sAAwMAXwAAADBLAAICAWAAAQEuAUwbQCAGAQUEBYMABAAEgwADAwBfAAAAMEsAAgIBYAABAS4BTFlADiUlJSglKBYnJycjBwgkKwD//wBc/+wENQYAACIBz1wAACIAZgAAAQIBtWcAAGu1JwEEBgFKS7AjUFhAJAUHAgQGAAYEAH4ABgYnSwADAwBfAAAAMEsAAgIBXwABAS4BTBtAIQAGBAaDBQcCBAAEgwADAwBfAAAAMEsAAgIBXwABAS4BTFlAESYlLCspKCUtJi0nJycjCAgjKwAAAP//AFz/7AQ1BcYAIgHPXAAAIgBmAAABAwG2AJgAAAA8QDkGAQQEBV8JBwgDBQUtSwADAwBfAAAAMEsAAgIBXwABAS4BTDExJSUxPDE7NzUlMCUvKScnJyMKCCQr//8AXP/sBDUGAAAiAc9cAAAiAGYAAAEDAbAEUQAAAF9LsCNQWEAjBgEFBAAEBQB+AAQEJ0sAAwMAXwAAADBLAAICAWAAAQEuAUwbQCAABAUEgwYBBQAFgwADAwBfAAAAMEsAAgIBYAABAS4BTFlADiUlJSglKBYnJycjBwgkKwAAAwBc/3kENQS5ABkAJAAvAD5AOxkWAgQCKSgdHAQFBAwJAgAFA0oAAwIDgwABAAGEAAQEAl8AAgIwSwAFBQBfAAAALgBMKiYSKBImBggaKwAWFRUUBgYjIicHIzcmJjU1NDY2MzIXNzMHABYXASYjIgYGFRUlNCYnARYzMjY2NQPQZXjflWdXSnxlYmx435RxW0l8Zf2jMS8BVz1JYolGAmQsKv6sN0JiikUDouuPF535jyOWzEnwkxed+o8plM39yZ43ArggbLdtFxdWlzj9Thlrtm4AAAD//wBc/+wENQXsACIBz1wAACIAZgAAAQcBsgRVAAQAd7RAPwIHSEuwI1BYQCgACAYBBAAIBGcABQUHXwAHBydLAAMDAF8AAAAwSwACAgFfAAEBLgFMG0AvAAYEAAQGAH4ACAAEBggEZwAFBQdfAAcHJ0sAAwMAXwAAADBLAAICAV8AAQEuAUxZQAwkIxIkJycnJyMJCCgrAAADAGH/6wb/BE8AJAA2AEAAQUA+HQEJBg4HAgEACAECAQNKAAkAAAEJAGUIAQYGBF8FAQQEMEsHAQEBAl8DAQICLgJMQD8nJyckJyQkIxAKCB0rASEeAjMyNxcGBiMiJicGBiMiJiY1NTQ2NjMyFhc2NhcyFhYVBTQmJiMiBgYVFRQWFjMyNjY1JTQmJiMiBgYHIQb//TQER4ZevndJOb6Hg89APsmDlNx1ddqUhMw+QcVukcZi/G9Bh2JihUFBhmNihUEC1jdzVERxSw0CCwHzaadhbH05TW9lZW+P+p0XnfqPcWZlcwF52ZBEbrZsbLZuF2+1a2u1b5JMfktPiVcAAgCM/mAEHwROABEAIABrQA8PAQQCGhkCBQQKAQAFA0pLsBlQWEAcAAQEAl8GAwICAihLAAUFAF8AAAAuSwABASoBTBtAIAACAihLAAQEA18GAQMDMEsABQUAXwAAAC5LAAEBKgFMWUAQAAAdGxcVABEAEBESJwcIFysAFhYVFRQGBiMiJxEjETMXNjMTNCYmIyIGBxEWMzI2NjUC/75iY72DxHG7qgpxyOs/g2FYfCZTqWCCPwROifmmFZ/5jXz9+AXador92HCzalBF/feTa7RwAAACAJX+YAQpBgAAEQAgAG9ADw8BBAMaGQIFBAoBAAUDSkuwI1BYQCAAAgInSwAEBANfBgEDAzBLAAUFAF8AAAAuSwABASoBTBtAIAACAwKDAAQEA18GAQMDMEsABQUAXwAAAC5LAAEBKgFMWUAQAAAdGxcVABEAEBESJwcIFysAFhYVFRQGBiMiJxEjETMRNjMTNCYmIyIGBxEWMzI2NjUDCL5jY76DxHG7u3HB7D+DYVh8J1OqYII/BE6J+aYVn/mNfP34B6D9zYH92HCzalBF/feTa7RwAAACAF/+YAPwBE4AEQAgAIlLsBlQWEAPEAEEAhQTAgUEAwEBBQNKG0APEAEEAxQTAgUEAwEBBQNKWUuwGVBYQB0ABAQCXwYDAgICMEsHAQUFAV8AAQEuSwAAACoATBtAIQYBAwMoSwAEBAJfAAICMEsHAQUFAV8AAQEuSwAAACoATFlAFBISAAASIBIfGBYAEQARJyIRCAgXKwERIxEGIyImJjU1NDY2MzIXNwI3ESYmIyIGBhUVFBYWMwPwu267g8JoZ8OFv3AIZlYnfFVhgz8/gmAEOvomAgJ2jfqeFaX6iX9r/EiSAhVCTWy2cBVwtGsAAAAAAQCMAAACmAROABAAR0ALDQEAAggDAgEAAkpLsBlQWEARAAAAAl8DAQICKEsAAQEmAUwbQBUAAgIoSwAAAANfAAMDMEsAAQEmAUxZthMREyQECBgrABYXFSYjIgYHESMRMxc2NjMCWDYKKzBcex+7tgQqgFYETgkFqwhSSvz/BDp7REsAAQBf/+wDvQROADAAMEAtAAECBAIBBH4ABAUCBAV8AAICAF8AAAAwSwAFBQNfAAMDLgNMIxMuIxMlBggaKwAmNTQ2NjMyFhYVIzQmJiMiBgYVFBYWFx4CFRQGBiMiJiY1Mx4CMzI2NjU0JiYnAUnFZLd2frxjuzZnRUlgLSlrZYiqU2i+fYrJaLsETHE/SWk2JmxmAgOQf1aQVlqZXi5WNy1JKis5LhYfVHpXXpFRY6JcSFsoKUctLUI4FQABAIv/7ARrBhIAOwCLS7AZUFhACiABAgMfAQECAkobQAogAQIDHwEEAgJKWUuwFlBYQBcAAwMAXwAAACdLAAICAV8FBAIBAS4BTBtLsBlQWEAVAAAAAwIAA2cAAgIBXwUEAgEBLgFMG0AZAAAAAwIAA2cFAQQEJksAAgIBXwABAS4BTFlZQBAAAAA7ADs4NiQiHRskBggVKzMRNDY2MzIWFhUUBgYHBgYVFBYWFx4CFRQGBiMiJic3FhYzMjY1NCYmJy4CNTQ2NzY2NTQmIyIGFRGLYrJ2cKlgFR0YHBooPTNATjdhrnJSsicqLYI+b2soPDNATjcjIyIialVpcQRYj8dkSpdwN1c+KS5BKSdFOSkzTGlBb5dLLB2ZHDBiUClFOSk0TW1ENlU3N1AzYWmQlPuoAAEAoAAAAoMGFQANACFAHgUBAQAGAQIBAkoAAAABAgABZwACAiYCTBMjIgMIFysTNDYzMhcHJiMiBhURI6C0oDtUFykyWF67BK2svBWNDG9j+1MAAAEACf/sAlcFQQAWAClAJgADAgODBQEBAQJdBAECAihLAAYGAGAAAAAuAEwjERERERMhBwgbKyEGIyImNREjNTMRMxEzFSMRFBYzMjY3AldAT3GJxcW7yspANxUyDxSImQKgjQEH/vmN/V9NNQcEAAABAIj/7APdBDoAEQBRQAoQAQMCAwEAAwJKS7AZUFhAEwUEAgICKEsAAwMAXwEBAAAmAEwbQBcFBAICAihLAAAAJksAAwMBXwABAS4BTFlADQAAABEAESMTIhEGCBgrAREjJwYjIiY1ETMRFBYzMjcRA92yA2zRp7y7b1fSRwQ6+8Zrf8TQArr9RIdvnQMVAP//AIj/7APdBgAAIwHPAIgAAAAiAHYAAAEDAbEEbQAAAKBAChEBAwIEAQADAkpLsBlQWEAhAAUGAgYFAn4IAQYGJ0sHBAICAihLAAMDAF8BAQAAJgBMG0uwI1BYQCUABQYCBgUCfggBBgYnSwcEAgICKEsAAAAmSwADAwFfAAEBLgFMG0AiCAEGBQaDAAUCBYMHBAICAihLAAAAJksAAwMBXwABAS4BTFlZQBUTEwEBExYTFhUUARIBEiMTIhIJCCMrAAD//wCI/+wD3QYAACMBzwCIAAAAIgB2AAABAgG1XwAAqUAOFQEFBxEBAwIEAQADA0pLsBlQWEAiBgkCBQcCBwUCfgAHBydLCAQCAgIoSwADAwBgAQEAACYATBtLsCNQWEAmBgkCBQcCBwUCfgAHBydLCAQCAgIoSwAAACZLAAMDAWAAAQEuAUwbQCMABwUHgwYJAgUCBYMIBAICAihLAAAAJksAAwMBYAABAS4BTFlZQBcUEwEBGhkXFhMbFBsBEgESIxMiEgoIIysAAAD//wCI/+wD3QXGACMBzwCIAAAAIgB2AAABAwG2AJEAAAB9QAoRAQMCBAEAAwJKS7AZUFhAIQcBBQUGXwsICgMGBi1LCQQCAgIoSwADAwBfAQEAACYATBtAJQcBBQUGXwsICgMGBi1LCQQCAgIoSwAAACZLAAMDAV8AAQEuAUxZQB0fHxMTAQEfKh8pJSMTHhMdGRcBEgESIxMiEgwIIysA//8AiP/sA90GAAAjAc8AiAAAACIAdgAAAQMBsARJAAAAoEAKEQEDAgQBAAMCSkuwGVBYQCEIAQYFAgUGAn4ABQUnSwcEAgICKEsAAwMAYAEBAAAmAEwbS7AjUFhAJQgBBgUCBQYCfgAFBSdLBwQCAgIoSwAAACZLAAMDAWAAAQEuAUwbQCIABQYFgwgBBgIGgwcEAgICKEsAAAAmSwADAwFgAAEBLgFMWVlAFRMTAQETFhMWFRQBEgESIxMiEgkIIysAAAABACEAAAO7BDoABwAhQB4GAQABAUoDAgIBAShLAAAAJgBMAAAABwAHEhEECBYrAQEjIwEzAQEDu/58fhD+eL8BEgEKBDr7xgQ6/MMDPQAAAAEAKwAABdMEOgAPACdAJA4KBAMAAgFKBQQDAwICKEsBAQAAJgBMAAAADwAPIhEiIQYIGCsBASMjAQEjIwEzExMzMwETBdP+xnwb/vr/AHcg/sa61Pt7HAD/0AQ6+8YDMfzPBDr83QMj/MsDNQAAAAABACkAAAPLBDoACwAgQB0LCAUCBAEAAUoDAQAAKEsCAQEBJgFMEhISEAQIGCsBMwEBIwMDIwEBMxMC5tr+nwFs1/n42gFt/p7Y7AQ6/er93AGX/mkCJAIW/nUAAAEAFv5LA7AEOgATAC1AKhIPCAMBAgcBAAECSgQDAgICKEsAAQEAYAAAADIATAAAABMAExQlIwUIFysBAQYGIyImJzUWFjMyNjc3ATMBEwOw/k4ol4IVQxELHAtfaSEp/n7MAQz7BDr7H22hCwWVAgNPY28ELvzWAyoAAP//ABb+SwOwBgAAIgHPFgAAIgB+AAABAwGxBDIAAABtQAwTEAkDAQIIAQABAkpLsCNQWEAgAAQFAgUEAn4HAQUFJ0sGAwICAihLAAEBAGAAAAAyAEwbQB0HAQUEBYMABAIEgwYDAgICKEsAAQEAYAAAADIATFlAFBUVAQEVGBUYFxYBFAEUFCUkCAgiKwAAAP//ABb+SwOwBcYAIgHPFgAAIgB+AAABAgG2VQAAS0BIExAJAwECCAEAAQJKBgEEBAVfCgcJAwUFLUsIAwICAihLAAEBAGAAAAAyAEwhIRUVAQEhLCErJyUVIBUfGxkBFAEUFCUkCwgiKwAAAAABAFkAAAOzBDoACwAvQCwKAQECBAEAAwJKAAEBAl0AAgIoSwQBAwMAXQAAACYATAAAAAsACyESIQUIFyslFSEjNQEhNSEzFQEDs/ztRwJV/bMC41L9ppaWhwMcl4L83gADAB7+SgQSBE4ALQA/AFABA0ALKBICBAYNAQgFAkpLsBZQWEArAAYABAUGBGcHAQMDAV8CAQEBMEsKAQUFCF0ACAgmSwsBCQkAXwAAADIATBtLsBlQWEApAAYABAUGBGcKAQUACAkFCGUHAQMDAV8CAQEBMEsLAQkJAF8AAAAyAEwbS7AhUFhAMwAGAAQFBgRnCgEFAAgJBQhlBwEDAwFfAAEBMEsHAQMDAl0AAgIoSwsBCQkAXwAAADIATBtAMQAGAAQFBgRnCgEFAAgJBQhlAAcHAV8AAQEwSwADAwJdAAICKEsLAQkJAF8AAAAyAExZWVlAHkBAAABAUEBPSEU8OjMxAC0AKyclHx4dHBsZJQwIFSskFhUUBgYjIiYmNTQ2NyY1NDY3JiY1NTQ2NjMyFyEXIxYVFRQGBiMiJwYVFDMzARQWFjMyNjY1NTQmJiMiBgYVADY2NTQmIyMiJwYGFRQWFjMDMtWC9aKR0m1kTzlAM1JdbcN9UUgBbgKYPGzDfE1KNH+s/qw+b0dGbj0+bkdGbz0BQaBUZXzQKCYzQT18XMSSlFCdZ0t/S1qGKC5LRmMlMplZFmejXBSRVWwWYaRhFSw+UwIONV87O181FjhfOTlfOPv4OlgsSEgGF187LkorAAAAAAEAkwAABBYGAAAMAFa1CgEAAwFKS7AjUFhAGQADAAABAwBlAAICJ0sABAQoSwUBAQEmAUwbQB8AAwAAAQMAZQACAgFdBQEBASZLAAQEKEsFAQEBJgFMWUAJEhEREREQBggaKwEjESMRMxEzATMBASMBz4K6un4BOd3+iAGt3QH2/goGAPyOAaz+FP2yAAAAAAEAkwAAA/MGGAAMAClAJgoGAAMAAgFKAAEBAF0DAQAAJksAAgIoSwMBAAAmAEwSExESBAgYKwEjESMRMxEzATMBASMBUgW6ugEBifL+KwH/5gH0/gwGGPx1Aa3+Dv24AAIAjP/sBDUGAAARACEArEuwGVBYQA8PAQQDGhkCBQQKAQAFA0obQA8PAQQDGhkCBQQKAQEFA0pZS7AZUFhAHAACAidLAAQEA18GAQMDMEsABQUAXwEBAAAuAEwbS7AjUFhAIAACAidLAAQEA18GAQMDMEsAAQEmSwAFBQBfAAAALgBMG0AgAAQEA18GAQMDMEsAAgIBXQABASZLAAUFAF8AAAAuAExZWUAQAAAeHBcVABEAEBESJwcIFysAFhYVFRQGBiMiJwcjETMRNjMBNCYmIyIGBxEWFjMyNjY1AwHIbG3Ig9BwEKG7b8UA/0SIYl+BJSaDXmCIRAROifqkFZ76jpSABgD9xoj92XKzZ1tO/idNXGmzbgABAF3/7APwBE4AIgA2QDMAAgMFAwIFfgYBBQQDBQR8AAMDAV8AAQEwSwAEBABfAAAALgBMAAAAIgAiJiMTJyMHCBkrAQ4CIyImJjU1NDY2MzIWFhcjLgIjIgYGFRUUFjMyNjY3A/AFd8NwndtsbdmdfMFuBbEFQnBIa4Q5ip9BcEgFAVZfp2SR9ZYqlvWRZrNvQ29Ab69oKqbgN2E8AAAAAgBb/+wEAQYAABEAIACbQA8QAQQCFRQCBQQDAQAFA0pLsBlQWEAdBgEDAydLAAQEAl8AAgIwSwcBBQUAXwEBAAAmAEwbS7AjUFhAIQYBAwMnSwAEBAJfAAICMEsAAAAmSwcBBQUBXwABAS4BTBtAIQAEBAJfAAICMEsGAQMDAF0AAAAmSwcBBQUBXwABAS4BTFlZQBQSEgAAEiASHxgWABEAESciEQgIFysBESMnBiMiJiY1NTQ2NjMyFxECNjcRJiMiBgYVFRQWFjMEAaIOb8l9zHV0zIC+bqR8KFOrYopHR4pgBgD6AHeLj/qcFaL7i38CMfqIU0oB9ZlotHEVbrNoAAACAFv+VQQBBE4AHgAsAaZLsBlQWEAXHQEFAyEgAgYFEAECBgkBAQIIAQABBUobQBcdAQUEISACBgUQAQIGCQEBAggBAAEFSllLsApQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAACoATBtLsAxQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAADIATBtLsBBQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAACoATBtLsBJQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAADIATBtLsBRQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAACoATBtLsBdQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAADIATBtLsBlQWEAiAAUFA18HBAIDAzBLCAEGBgJfAAICLksAAQEAXwAAACoATBtAJgcBBAQoSwAFBQNfAAMDMEsIAQYGAl8AAgIuSwABAQBfAAAAMgBMWVlZWVlZWUAVHx8AAB8sHyskIgAeAB4nJCUkCQgYKwERFAYGIyImJzcWFjMyNjU1BiMiJiY1NTQ2NjMyFzcCNxEmIyIGBhUVFBYWMwQBbdOWVsdKOD6hTpKLbsCAy3JxzILLbxBwUlKsYopGRopgBDr8FKHidjYziSoyrqwmf477nBWj+4qRffxOnwHxm2i0cRVus2gAAgBa/+wERQROABEAIwAfQBwAAwMAXwAAADBLAAICAV8AAQEuAUwnJyciBAgYKxI2NjMyFhYVFRQGBiMiJiY1NR4CMzI2NjU1NCYmIyIGBhUVWn3jlJXlfX3jlZXkfbtKjmNijktLkGJhjkoCxPuPj/ucF5z6j4/6nBeFtWxstm0XbLdtbbZtFwAAAAACAIz+YAQzBE4AEQAgAGtADw8BBAIaGQIFBAoBAAUDSkuwGVBYQBwABAQCXwYDAgICKEsABQUAXwAAAC5LAAEBKgFMG0AgAAICKEsABAQDXwYBAwMwSwAFBQBfAAAALksAAQEqAUxZQBAAAB0bFxUAEQAQERInBwgXKwAWFhUVFAYGIyInESMRMxc2MwE0JiYjIgYHERYzMjY2NQL/yGxsyIPEcbugEHLLAP9IjWJYfCZTqWGMSAROivqkFZ75jnz9+AXaeo792HG0aFBF/feTa7ZuAAIAW/5gBAEETgARACAAiUuwGVBYQA8QAQQCFBMCBQQDAQEFA0obQA8QAQQDFBMCBQQDAQEFA0pZS7AZUFhAHQAEBAJfBgMCAgIwSwcBBQUBXwABAS5LAAAAKgBMG0AhBgEDAyhLAAQEAl8AAgIwSwcBBQUBXwABAS5LAAAAKgBMWUAUEhIAABIgEh8YFgARABEnIhEICBcrAREjEQYjIiYmNTU0NjYzMhc3AjcRJiYjIgYGFRUUFhYzBAG7cLmEzXFxzYbCcA5xWCh8VWKNSEiMYQQ6+iYCAnaN+p0VpPqLhHD8SJICFUJNardyFW61awAAAAACAF3/7AP0BE4AGQAjADNAMAgBAQAJAQIBAkoABQAAAQUAZQAEBANfAAMDMEsAAQECXwACAi4CTBMnJyUjEAYIGisBIR4CMzI2NxcGBiMiJiY1NTQ2NjMyFhYVJzQmJiMiBgYHIQP0/SUGWJ1oVY01OTareJ/zhITZfJfHYLs1dFpLeEwLAh0B6majXzIpfjBDivWdLKT1gX3dkTdOgU9JjGMAAAIAYP5VA/METgAeACwBpkuwGVBYQBcdAQUDISACBgUQAQIGCQEBAggBAAEFShtAFx0BBQQhIAIGBRABAgYJAQECCAEAAQVKWUuwClBYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAKgBMG0uwDFBYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAMgBMG0uwEFBYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAKgBMG0uwElBYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAMgBMG0uwFFBYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAKgBMG0uwF1BYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAMgBMG0uwGVBYQCIABQUDXwcEAgMDMEsIAQYGAl8AAgIuSwABAQBfAAAAKgBMG0AmBwEEBChLAAUFA18AAwMwSwgBBgYCXwACAi5LAAEBAF8AAAAyAExZWVlZWVlZQBUfHwAAHywfKyUjAB4AHickJSQJCBgrAREUBgYjIiYnNxYWMzI2NTUGIyImJjU1NDY2MzIXNwI3ESYmIyIGFRUUFhYzA/Nu05ZNs0I4OIxEk4tuwIDBaWjCgspwD29SJn5akI89gGAEOvwCm9pyLCqJIielojl/jfmeFaT7ipF9/E6fAfFIU9uzFXCxZ///AD0AAAWTBhUAIgHPPQAAIgBXAAABAwBXAsgAAABTQFApEwIGBSoUAgAGAkoMAQUPDQ4DBgAFBmcKCAMDAQEAXQsHBAMAAChLCQECAiYCTBcXAQEXLBcrKCYjIiEgHx4dHBsaARYBFSMRERERFBAIJSsAAAIAPQAABjQGFQArAC8AUEBNKBcCCAcpGAIACAJKCgEHDgsCCAAHCGcFAwIBAQBdDw0JBgQAAChLDAQCAgImAkwsLAAALC8sLy4tACsAKiYkIB4jIxERESERERMQCB0rAAYVFTMVIxEjESMjESMRIzUzNTQ2MzIXByYjIgYVFTMzNTQ2NjMyFhcHJiMBESMRBFVq3d27quS7qqq9qD1CCi03WWLvn2Cye0OFVCB9cQFpuwVwaW5fjfxTA638UwOtjXOtuxCVCGlic195q1gfHpoy/sr7xgQ6AAEAPQAABjQGFQAsAEhARR8CAgEJIAUCAgECSg0MAgkKAQECCQFnBwUCAwMCXQsIAgICKEsGBAIAACYATAAAACwAKygmIyEeHBERESERERMiEw4IHSsAFhcRIxEmIyIGFRUzFSMRIxEjIxEjESM1MzU0NjMyFwcmIyIGFRUzMzU0NjME38yJu4BHYWbm5ruq5Luqqr2oPUIKLTdZYvCewrIGFSEc+igFZRNoY3ON/FMDrfxTA62Nc627EJUIaWJzc627AAABAD0AAAPrBhUAGAA5QDYCAQEHBQECAQJKCAEHAAECBwFnBQEDAwJdBgECAihLBAEAACYATAAAABgAFxERERETIhMJCBsrABYXESMRJiMiBhUVMxUjESMRIzUzNTQ2MwKWzIm7gEdhZubmu6qqwrIGFSEc+igFZRNoY3ON/FMDrY1zrroAAAAAAgAfAAADzgYVABcAGwBCQD8UAQYFFQEABgJKAAUJAQYABQZnAwEBAQBdCggEAwAAKEsHAQICJgJMGBgAABgbGBsaGQAXABYkERERERMLCBorAAYVFTMVIxEjESM1MzU0NjYzMhYXByYjAREjEQHvatzcu6urX7N7Q4VTH31xAWm7BXBpbl+N/FMDrY1featYHx6aMv7K+8YEOgAAAgAUAAAEcgSNAAgACwArQCgKAQQCAUoAAgQCgwUBBAAAAQQAZgMBAQEmAUwJCQkLCQsRIREQBggYKwEhAyMBMzMBIwsCA0f9+m++Ad96LQHYvqjHyQEY/ugEjftzAa8B+/4F//8AFAAABHIGHgAiAc8UAAAiAJMAAAEHAbEEfAAeAD5AOwsBBAIBSggBBgUGgwAFAgWDAAIEAoMHAQQAAAEEAGYDAQEBJgFMDQ0KCg0QDRAPDgoMCgwRIRERCQgjKwAA//8AFAAABHIGHgAiAc8UAAAiAJMAAAEGAbVuHgBFQEIPAQUHCwEEAgJKAAcFB4MGCQIFAgWDAAIEAoMIAQQAAAEEAGYDAQEBJgFMDg0KChQTERANFQ4VCgwKDBEhEREKCCMrAP//ABQAAARyBeQAIgHPFAAAIgCTAAABBwG2AJ8AHgBMQEkLAQQCAUoAAgUEBQIEfgkBBAAAAQQAZgcBBQUGXwsICgMGBidLAwEBASYBTBkZDQ0KChkkGSMfHQ0YDRcTEQoMCgwRIRERDAgjK///ABQAAARyBh4AIgHPFAAAIgCTAAABBwGwBFcAHgA+QDsLAQQCAUoABQYFgwgBBgIGgwACBAKDBwEEAAABBABmAwEBASYBTA0NCgoNEA0QDw4KDAoMESEREQkIIysAAP//ABQAAARyBnkAIgHPFAAAIgCTAAABBwG5APUAKQBQQE0LAQQCAUoAAgUEBQIEfgoBBgAHCAYHZwsBCAAFAggFZwkBBAAAAQQAZgMBAQEmAUwdHQ0NCgodKR0oIyENHA0bFRMKDAoMESEREQwIIyv//wAUAAAEcgYKACIBzxQAACIAkwAAAQcBsgRcACIAykALCwEEAgFKKCcCCEhLsCNQWEAtAAIFBAUCBH4KAQQAAAEEAGYABgYIXwAICCdLBwEFBQlfAAkJJUsDAQEBJgFMG0uwLFBYQDMABwUCBQcCfgACBAUCBHwKAQQAAAEEAGYABgYIXwAICCdLAAUFCV8ACQklSwMBAQEmAUwbQC8ABwUCBQcCfgACBAUCBHwACAAGBQgGZwAJAAUHCQVnCgEEAAABBABmAwEBASYBTFlZQBcKCiUjHx0aGRcVEQ8KDAoMESEREQsIIysAAAACAAkAAAXyBI0AEgAVAEVAQhQBBQQBSgADAAQFAwRlAAUABggFBmUKAQgAAQcIAWUJAQcHAF0CAQAAJgBMExMAABMVExUAEgASERERMRERIQsIGyslFSEjAyEDIwEzMyEVIRMhFSETAwMBBfL9xqAL/mejyAKWdpQCH/3mDAHP/jYOxRT+0ZWVAS7+0gSNlf6zlf5/AS8CMv3OAAADAIoAAAPwBI0ADwAXACAAOkA3DwEEAgFKAAEGAQMCAQNlAAIABAUCBGUHAQUFAF0AAAAmAEwYGBAQGCAYHx4cEBcQFichNAgIFysAFhUUBiMhIxEhMhYVFAYHAREzMjY1NCMSNjU0JiMhESEDgHDhxv6IRwGXzONfV/4r6Wx79Jl7cHP+8wEEAkeYYaOrBI2en1F+IQGW/qlXUq78oGJXXGj+gwAAAAABAGH/8AQxBJ0AIAAuQCsAAgMFAwIFfgAFBAMFBHwAAQADAgEDZwAEBABfAAAALgBMEyUiEyciBggaKwAGBiMiJiY1NTQ2NjMyFhYXIyYmIyIGFRUUFjMyNjY3MwQkcs+TleB6e+WajspxDbsRg4edpJubYXk/DbsBBLFjhvilZqb4hmOzeHp9z71nuNU3alIAAAAAAQBh/kkEMQSdAC8AREBBFxYIAwIGAUoABAUABQQAfgAABgUABnwHAQYCBQYCfAADAAUEAwVnAAICAWAAAQEyAUwAAAAvAC4iEy8RGRMICBorJDY2NzMGBgcHFhYVFAYjJzI2NTQmJic3LgI1NTQ2NjMyFhYXIyYmIyIGFRUUFjMCsXk/DbsS07wKQ1aekgdKXCFEPB2CwGd75ZqOynENuxGDh52km5uGN2pSpdEQLAtSUGBxajEyIiYSBX8PjuqYZqb4hmOzeHp9z71nuNUAAAIAigAABCAEjQANABcAJUAiBAEBAAIDAQJlAAMDAF0AAAAmAEwAABUTEhAADQALNwUIFSsAFhYVFRQGBCMhIxEzMwE0JiMjETMyNjUCl/2MjP7+q/74VbuuAXO+ta6ivcIEjYj6pUCm+YcEjf3bwM78oNO9AAAAAAL/vAAABCAEjQARAB8ANUAyCAEDAAQCAwRlBQECBgEBBwIBZQAHBwBdAAAAJgBMAAAdGxoZGBcWFAARAA8RETcJCBcrABYWFRUUBgQjISMRIzUzETMzATQmIyMRMxUjETMyNjUCl/2MjP7+q/74Vc7Ou64Bc761rvX1or3CBI2I+qVApvmHAgKWAfX928DO/qKW/pTTvQAAAQCKAAADrwSNAA0AJ0AkAAEAAgMBAmUAAwAEBQMEZQAFBQBdAAAAJgBMERERESEgBggaKyEhIxEzIRUhESEVIREhA6/9aI27AmT9nAIT/e0CagSNl/6wl/6HAP//AIoAAAOvBh4AIwHPAIoAAAAiAKAAAAEHAbEETwAeADlANggBBwYHgwAGAQaDAAEAAgMBAmYAAwAEBQMEZQAFBQBdAAAAJgBMDw8PEg8SEhEREREhIQkIJisA//8AigAAA68GHgAjAc8AigAAACIAoAAAAQYBtUAeAENAQBEBBggBSgAIBgiDBwkCBgEGgwABAAIDAQJmAAMABAUDBGUABQUAXQAAACYATBAPFhUTEg8XEBcRERERISEKCCUrAP//AIoAAAOvBeQAIwHPAIoAAAAiAKAAAAEGAbZyHgBEQEEAAQACAwECZQADAAQFAwRlCAEGBgdfCwkKAwcHJ0sABQUAXQAAACYATBsbDw8bJhslIR8PGg8ZJREREREhIQwIJiv//wCKAAADrwYeACMBzwCKAAAAIgCgAAABBwGwBCoAHgA5QDYABgcGgwgBBwEHgwABAAIDAQJmAAMABAUDBGUABQUAXQAAACYATA8PDxIPEhIRERERISEJCCYrAAABAIoAAAObBI0ACgAnQCQAAwUBBAADBGUAAAABAgABZQACAiYCTAAAAAoACiEREREGCBgrAREhFSERIxEzIRUBRQIG/fq7uwJWA/b+lpj+DASNlwABAGP/8AQ2BJ0AIQA3QDQhHAIEBQFKAAIDBgMCBn4AAQADAgEDZwAGAAUEBgVlAAQEAF8AAAAuAEwREyYhEichBwgbKyQGIyImJjU1NDY2MzIWFyMmIyIGFRUUFhYzMjY3NSE1IREEA9e5m++GeeSg1egZuSnyoaJUm2ltdyH+6AHSVGSF+6xVrvqExqrZ0cJXf7dgLB/vj/5HAAABAIoAAARZBI0ACwAnQCQABAABAAQBZQYFAgMDAF0CAQAAJgBMAAAACwALEREREREHCBkrAREjESERIxEzESERBFm6/aa7uwJaBI37cwHz/g0Ejf39AgMAAQCXAAABUgSNAAMAGUAWAAAAAV0CAQEBJgFMAAAAAwADEQMIFSszETMRl7sEjftzAAAA//8AlwAAAjsGHgAjAc8AlwAAACIAqAAAAQcBsQMrAB4ALEApBQEDAgODAAIAAoMAAAABXQQBAQEmAUwFBQEBBQgFCAcGAQQBBBIGCCArAAD////GAAACJAYeACIBzwAAACIAqAAAAQcBtf8cAB4ANUAyBwECBAFKAAQCBIMDBgICAAKDAAAAAV4FAQEBJgFMBgUBAQwLCQgFDQYNAQQBBBIHCCArAAAA////sgAAAj0F5AAiAc8AAAAiAKgAAAEHAbb/TgAeADdANAQBAgIDXwgFBwMDAydLAAAAAV0GAQEBJgFMEREFBQEBERwRGxcVBRAFDwsJAQQBBBIJCCArAP///6sAAAFSBh4AIgHPAAAAIgCoAAABBwGwAwYAHgAsQCkAAgMCgwUBAwADgwAAAAFdBAEBASYBTAUFAQEFCAUIBwYBBAEEEgYIICsAAQAr//ADTgSNABEAJUAiBAEDAQODAAECAYMAAgIAXwAAAC4ATAAAABEAESITJAUIFysBERQGBiMiJiY1MxQWMzI2NREDTmSxcHm7arx3a1tuBI38xm2gVk2ccmVgbl8DOgAAAAEAigAABFgEjQAMAB9AHAoGAQMAAQFKAgEBAQBdAwEAACYATBITERIECBgrAQcRIxEzETcBMwEBIwHYk7u7gQGL5f4iAgDjAgiN/oUEjf3WjgGc/fr9eQABAIoAAAOMBI0ABgAfQBwAAQIBgwMBAgIAXgAAACYATAAAAAYABhEhBAgWKyUVISMRMxEDjP2MjruWlgSN/AkAAQCKAAAFeASNAA8AIUAeDQYDAwADAUoEAQMDAF0CAQIAACYATBIhExMQBQgZKyEjERMBIwETESMRMzMBATMFeLoT/nSJ/nUTup9TAYUBhfIBkwIP/F4DoP3z/m0EjfxvA5EAAAEAigAABFkEjQAJACRAIQYBAgABAUoCAQEBAF0EAwIAACYATAAAAAkACRIREgUIFyshAREjETMBETMRA5/9pru7Alq6A278kgSN/JIDbvtzAAAA//8AigAABFkGCgAjAc8AigAAACIAsQAAAQcBsgSGACIArkAMBwICAAEBSiYlAgdIS7AjUFhAIwAFBQdfAAcHJ0sGAQQECF8ACAglSwIBAQEAXQkDAgAAJgBMG0uwLFBYQCoABgQBBAYBfgAFBQdfAAcHJ0sABAQIXwAICCVLAgEBAQBdCQMCAAAmAEwbQCYABgQBBAYBfgAHAAUEBwVnAAgABAYIBGcCAQEBAF0JAwIAACYATFlZQBYBASMhHRsYFxUTDw0BCgEKEhETCggiKwACAGD/8ARcBJ0AEQAhAB1AGgABAAIDAQJnAAMDAF8AAAAuAEwnJiciBAgYKwAGBiMiJiY1NTQ2NjMyFhIVFQImIyIGBhUVFBYWMzI2NTUEXH7mmZbogYDnlpnnf7mrm2OST1CTY5upAXf/iIj/rUSt/4mI/wCtRAEK1V+4gkaCuWDVxkYAAAD//wBg//AEXAYeACIBz2AAACIAswAAAQcBsQSVAB4AL0AsBgEFBAWDAAQBBIMAAQACAwECaAADAwBfAAAALgBMIyMjJiMmFScmJyMHCCQrAP//AGD/8ARcBh4AIgHPYAAAIgCzAAABBwG1AIcAHgA5QDYlAQQGAUoABgQGgwUHAgQBBIMAAQACAwECaAADAwBfAAAALgBMJCMqKScmIyskKycmJyMICCMrAAAA//8AYP/wBFwF5AAiAc9gAAAiALMAAAEHAbYAuAAeADpANwABAAIDAQJnBgEEBAVfCQcIAwUFJ0sAAwMAXwAAAC4ATC8vIyMvOi85NTMjLiMtKCcmJyMKCCQrAAD//wBg//AEXAYeACIBz2AAACIAswAAAQcBsARwAB4AL0AsAAQFBIMGAQUBBYMAAQACAwECZwADAwBgAAAALgBMIyMjJiMmFScmJyMHCCQrAAADAGD/xgRcBLcAGQAjACwAYkATFgEEAicmHBsZBQUEDAkCAAUDSkuwFFBYQBkAAQABhAMBAgAEBQIEZwAFBQBfAAAALgBMG0AdAAMCA4MAAQABhAACAAQFAgRnAAUFAF8AAAAuAExZQAkpJRIoEiYGCBorABYVFRQGBiMiJwcjNyYmNTU0NjYzMhc3MwcAFwEmIyIGBhUVJTQnARYzMjY1BBhEfuaZmXVKl4BFS4DnlqN4RZZ8/UI8AchNc2OSTwKKM/48TGebqQO0zn5Erf+IRnC/S9KCRK3/iU1nuf2GZwKqPF+4gkZGlWT9WzXVxv//AGD/8ARcBgoAIgHPYAAAIgCzAAABBwGyBHUAIgCrtD49AgdIS7AjUFhAKAABAAIDAQJnAAUFB18ABwcnSwYBBAQIXwAICCVLAAMDAF8AAAAuAEwbS7AsUFhALwAGBAEEBgF+AAEAAgMBAmcABQUHXwAHBydLAAQECF8ACAglSwADAwBfAAAALgBMG0ArAAYEAQQGAX4ABwAFBAcFZwAIAAQGCARnAAEAAgMBAmcAAwMAXwAAAC4ATFlZQAwkIxIkJicmJyMJCCgrAAACAIoAAAQcBI0ADAAWAC5AKwUBAgADBAIDZQYBBAAAAQQAZQABASYBTA0NAAANFg0VFBIADAALESYHCBYrABYWFRQGBiMhESMRIRI2NTQmJiMhESEC6slpaMiO/ue7AdSEfzd0WP7nARkEjV2mbHChVv5JBI39wW9fPGI8/lgAAAIAigAAA7gEjQAOABgANEAxBgEDAAQFAwRlBwEFAAABBQBlAAICAV0AAQEmAUwPDwAADxgPFxYUAA4ADRERJggIFysAFhYVFAYGIyMVIxEzFTMSNjU0JiYjIxEzAobJaWjIjrW7u7WEfzd0WLW1A8NepmtwolbsBI3K/cFvXzxiO/5ZAAIAWf81BFkEnQAVACUAJ0AkFAEAAgFKFQEARwABAAMCAQNnAAICAF8AAAAuAEwlKyciBAgYKwUlBiMiJgI1NTQ2NjMyFhIVFRQGBxcAFhYzMjY1NTQmIyIGBhUVA9v++jhEluiCgeeWmed/cGfa/LpQk2ObqaubY5JPy8gNiAEArESt/4mI/wCtRKP1R6IB/7lg1cZGxNVfuIJGAAIAigAABCYEjQAOABgAKUAmDgEBBAFKAAMABQQDBWUABAABAAQBZQIBAAAmAEwlJiERESAGCBorJRUjASERIxEhMhYVFAYHJTMyNjY1NCYjIwQmyP78/uu7AarV6Hpw/j70UnI5gILvCgoBwv4+BI24q3KhKms0XDticQABAEP/8APeBJ0AKwAuQCsAAQIEAgEEfgAEBQIEBXwAAAACAQACZwAFBQNfAAMDLgNMIhMsIhMlBggaKwAmNTQ2NjMyFhYVIzQmIyIGFRQWFhcWFhUUBgYjIiYmNTMUFjMyNjU0JiYnAUTZbcWBiMpru4SAeH46f2zJym7IhHvdibyjgnqEL3VrAiynil2RUl+iYl1wXUwuRTgbM66LYI9NU6V1bmpaSzRIORoAAQCK/+sD+gSdACMAgEuwF1BYQBEjIhQTBAIDCQEBAggBAAEDShtAESMiFBMEAgMJAQECCAEEAQNKWUuwF1BYQBwAAgMBAwIBfgAFAAMCBQNnAAEBAF8EAQAALgBMG0AgAAIDAQMCAX4ABQADAgUDZwAEBCZLAAEBAF8AAAAuAExZQAkjEiQkIyUGCBorABYVFAYGIyInNxYzMjY1NCYjIzUTJiYjIhERIxE2NjMyFhcBA0G5ZLN1eGc1UlhhcIaIVOwkUzjWugHGyXnBWf7vAo+iiXaqWTOXM3tkYFeIASgcI/7o/REC8dbWYVb+twAAAQApAAAD/QSNAAgAGUAWAAMCAQABAwBlAAEBJgFMIREREAQIGCsBIREjESE1ISED/f5yuv50AkYBjgP2/AoD9pcAAAABAHT/8AQLBI0AEwAbQBgDAQECAYMAAgIAXwAAAC4ATBMjFCMECBgrARQGBiMiJiY1ETMRFBYzMjY1ETMEC3vRgIXQdrmSgICRuwGBgbRcW7WBAwz89H59fX4DDAAAAP//AHT/8AQLBh4AIgHPdAAAIgDBAAABBwGxBHUAHgAtQCoGAQUEBYMABAEEgwMBAQIBgwACAgBfAAAALgBMFRUVGBUYEhMjFCQHCCQrAAAA//8AdP/wBAsGHgAiAc90AAAiAMEAAAEGAbVnHgA3QDQXAQQGAUoABgQGgwUHAgQBBIMDAQECAYMAAgIAYAAAAC4ATBYVHBsZGBUdFh0TIxQkCAgjKwAAAP//AHT/8AQLBeQAIgHPdAAAIgDBAAABBwG2AJkAHgA7QDgDAQEEAgQBAn4GAQQEBV8JBwgDBQUnSwACAgBfAAAALgBMISEVFSEsISsnJRUgFR8lEyMUJAoIJCsA//8AdP/wBAsGHgAiAc90AAAiAMEAAAEHAbAEUQAeAC1AKgAEBQSDBgEFAQWDAwEBAgGDAAICAF8AAAAuAEwVFRUYFRgSEyMUJAcIJCsAAAAAAQAUAAAEVQSNAAkAIUAeBwEAAQFKAwICAQABgwAAACYATAAAAAkACREhBAgWKwEBIyMBMwEXNwEEVf43jyD+N8kBPRsZAT8EjftzBI38ll5cA2wAAQAxAAAF8QSNABUAJ0AkEw0EAwACAUoFBAMDAgACgwEBAAAmAEwAAAAVABUkESQhBggYKwEBIyMBJxUBIyMBMxMXNxMzMxMXNxMF8f7WgC7+/AH+9X8w/te5xgsO94Il8w0MxQSN+3MDlgMD/GoEjfyaTTwDd/yGOlADZAAAAAABACcAAAQzBI0ACwAgQB0LCAUCBAEAAUoDAQAAAV0CAQEBJgFMEhISEAQIGCsBMwEBIwEBIwEBMwEDRt7+dgGZ3/7X/tndAZX+c90BHASN/b/9tAG8/kQCTAJB/kwAAAABAA4AAAQdBI0ACAAdQBoGAwADAAEBSgIBAQABgwAAACYATBISEQMIFysBESMRATMBATMCb7r+WdEBNgE20gGY/mgBowLq/cECPwD//wAOAAAEHQYeACIBzw4AACIAyQAAAQcBsQRLAB4AL0AsBwQBAwABAUoFAQQDBIMAAwEDgwIBAQABgwAAACYATAoKCg0KDRISEhIGCCMrAAABAEgAAAPhBI0ACwAtQCoKAQECBAEAAwJKAAIAAQMCAWUEAQMDAF0AAAAmAEwAAAALAAshEiEFCBcrJRUhIzUBITUhMxUBA+H8pj8Cmf1vAy5L/WeWlnsDe5d3/IAAAAACAJMCtAMQBcUAHgApAMBLsCRQWEAPFBMCAQIhAQYFAgEABgNKG0APFBMCAQIhAQYFAgEEBgNKWUuwJFBYQCAAAQAFBgEFZwACAgNfAAMDSUsIAQYGAF8HBAIAAEYATBtLsClQWEAnBwEEBgAGBAB+AAEABQYBBWcAAgIDXwADA0lLCAEGBgBfAAAARgBMG0AkBwEEBgAGBAB+AAEABQYBBWcIAQYAAAYAYwACAgNfAAMDSQJMWVlAFR8fAAAfKR8oJCIAHgAeJiMkJAkKGCsBJicGBiMiJjU0NjMzNTQmIyIGFSc0NjYzMhYVERQXJDY3NSMiBhUUFjMCagwFI2dDdoOrqGw9PkZNok2MXISbGv7NWRxrV1k+PgLCJiImMHpocHc1QkU3Mw1DajyOhv7GYVR6Jx2OQTMtMQAAAgB7ArMDKAXFABEAHwA7S7AqUFhAFQADAwBfAAAASUsAAgIBXwABAUYBTBtAEgACAAECAWMAAwMAXwAAAEkDTFm2JSYnIgQKGCsSNjYzMhYWFRUUBgYjIiYmNTUWFjMyNjU1NCYjIgYVFXtUm2dom1RTm2donFSkW1lWW1xXV1sEy6BaWqBnUWegWVmgZ1GxdnZgUV92d15RAAAAAgBk/+sEeAROABoAJwCjS7AhUFhADB4dEgMEBQUBAAQCShtADB4dEgMEBQUBBgQCSllLsBlQWEAZAAUFAl8DAQICMEsHBgIEBABfAQEAAC4ATBtLsCFQWEAdAAMDKEsABQUCXwACAjBLBwYCBAQAXwEBAAAuAEwbQCQABAUGBQQGfgADAyhLAAUFAl8AAgIwSwcBBgYAXwEBAAAuAExZWUAPGxsbJxsmJxMSJyIiCAgaKyUGBiMiJwYjIiYmNTU0EjYzMhc3MxEUFjMyNwQ2NxEmIyIGFRUUFjMEeB06JZImbM+DvmRjv4XLahGeKyUMFv4xeydUp4+Lio4NFA6kpITtmRWsAQaSn4v87VVCBRRiVwHCvPG9FabOAAACAGT/6wRaBE4AEwAhAIdACiEUEg8BBQUEAUpLsBdQWEAYAAQEAV8CAQEBMEsABQUAXwYDAgAALgBMG0uwGVBYQBwABAQBXwIBAQEwSwYBAwMmSwAFBQBfAAAALgBMG0AgAAICKEsABAQBXwABATBLBgEDAyZLAAUFAF8AAAAuAExZWUAQAAAfHRgWABMAExInIwcIFyshJwYGIyImJjU1NBI2MyAXNzMDEwEmJiMiBhUVFBYzMjY3A6gmNLuKg75kY7+FAQhsIrJqcf73GYl2j4uKjnWEIe9+hoTtmRWsAQaS/en94v3kAi6p4fG9FabO17H//wBkAAAEAgSaACIBz2QAACMA2QJbAAABAgDZAAAAH0AcAwEAAAFdBAEBARdLBQECAhgCTBEREREREQYHJSsA//8AZAAABCUEmgAiAc9kAAAjAN0CXAAAAQIA2QAAACZAIwACAAUAAgV+AwEAAAFdBAEBARdLAAUFGAVMERERERERBgclKwAA//8ANwIJBDQEmgAjAc8ANwIJACMA3QJrAAABAgDdAAAAH0AcBQECAAKEAwEAAAFdBAEBARcATBEREREREQYHJSsAAAAAAQBTAAAESwXdAAkAIUAeBwYDAwACAUoAAQIBgwACAhdLAAAAGABMExIRAwcXKwEBIwERMxEBATMCywFozfztuQFNASHRAfn+BwRMAZH+j/4tAgEAAQCYAAAEwgSaABAAH0AcDgsDAwACAUoDAQICF0sBAQAAGABMEhUUEQQHGCsBASMBBhURIxE0NjcBMwEBMwNIAXXP/fNrtGht/wHQAW0BHNECA/39AtRDo/4SAauZwzIBYf4IAfgAAAABAD0AAAQFBJoADwApQCYPAQECAAEAAQJKAAICA10AAwMXSwABAQBdAAAAGABMISMREQQHGCslFSE1IRE0JiMhNSEyFhURBAX8OAKAiZr+4wEn5+uOjpYCTZmJleDc/b8AAAAAAQAU//gCxwSaABYAL0AsEQcCAwABAUoAAQECXQACAhdLAAAAA18FBAIDAxgDTAAAABYAFRQREyMGBxgrFic1FjM2NjcRITUhERQXEyMDIw4CIyMPGx+QtBn+9gHBE1KvRQcMbqtkCAOrBASngwI1lf2gXVf+egFKWZxdAAEAPAAAA+sEmgAHACFAHgIBAAADXQQBAwMXSwABARgBTAAAAAcABxEREQUHFysBFSMRIxEhNQPrxbr90ASalfv7BAWVAAAAAAIAlAAABBwEmgALABUAIUAeAAAAAV0AAQEXSwADAwJdBAECAhgCTBQUEyEiBQcZKwE0JiMhNSEyFhURIwE0NjczBgYVESMDYoqZ/lUBtOftuv00GxmYDAy0AuOZiZXg3P0iAZJbaTUucE/+YgAAAAABAGQAAAGnBJoABQAZQBYAAAABXQABARdLAAICGAJMEREQAwcXKxMjNSERI+2JAUO6BAWV+2YAAAEATAAAAogEmgAHAB9AHAEBAQABSgABAQBdAAAAF0sAAgIYAkwRERIDBxcrEyc1IQchEyOfUwI8Af7Qp78D/Q6Plfv7AAAAAAEAtQAABEIEmgANABtAGAACAgBdAAAAF0sDAQEBGAFMESMTIAQHGCsTITIWFxEjETQmIyEDI7YBu+rmAbmEkv78AbkEmt/k/SkC4pmK+/sAAQC1/+oEYQSlACEAWUAKFQECABQBAQICSkuwLVBYQBcAAgIAXwMBAAAXSwABAQRfBQEEBB0ETBtAGwAAABdLAAICA18AAwMXSwABAQRfBQEEBB0ETFlADQAAACEAICQmJBQGBxgrBCYmNREzERQWFjMyNjY1ETQmIyIHJzY2MzIWFhURFAYGIwIA1Xa5R4FVVIFHVU5BTx0yeThbhUd11IwWdtaNAtf9HF2NTk6NXQGLYGkmfCAlUppp/nON1nYAAQA3AgkByQSaAAUAGUAWAAIAAoQAAAABXQABARcATBEREAMHFysBIzUhESMBD9gBkroEBZX9bwABAEH+YANCBJoABQAZQBYAAAABXQABARdLAAICGQJMEREQAwcXKwEhNSERIwKL/bYDAbcEBZX5xgAAAAABAFH/7wOYBJoADwAvQCwCAQABAQEDAAJKAAEBAl0AAgIXSwAAAANfBAEDAx0DTAAAAA8ADhETIwUHFysEJzcWMzI2NREhNSERFAQjAQq5Hp97p6/9mwMe/vbtETWLK6OYAkaV/U36/gAAAAABAFMAAAPSBgAACAAlQCIAAQEDAUoAAgMCgwABAQNdAAMDF0sAAAAYAEwRERERBAcYKwEBIwEhETMRIQPS/lDHAav9TbkCxgQl+9sEBQH7/poAAAACALQAAARgBJoAAwAHACVAIgACAgBdAAAAF0sEAQMDAV0AAQEYAUwEBAQHBAcSERAFBxcrEyERISURIRG0A6z8VALz/cYEmvtmlgNv/JEAAAEAhAAABHsEsAAdACxAKQoBAwAJBQICAwJKAAMDAF8AAAAcSwACAgFdBAEBARgBTBMjERMsBQcZKwE2Njc2NycGBgcnNiQzMhYXESE1IRE0JiMiBgcDIwEFCkkxBBACLYc4K3QBIHnu+wH+GwEsjodhgg9ZtwLtUpEnAwsDCCMTjCgy/fX9QpUCPp2klYD9AQAAAAABAFb+YAGYBJoABQAZQBYAAAABXQABARdLAAICGQJMEREQAwcXKxMjNSERI+GLAUK3BAWV+cYAAAEAPP/sAjEEmgAPAC9ALAIBAAEBAQMAAkoAAQECXQACAhdLAAAAA18EAQMDHQNMAAAADwAOERMjBQcXKxYnNxYzMjY1ESM1IREUBiOfYwVBUVJS5wGhoJYUFpENXmICv5X8naKpAAAAAgBn/+oEjgSwABoALAB3S7AiUFhACg4BAAENAQQAAkobQAoOAQMBDQEEAAJKWUuwIlBYQBgDAQAAAV8AAQEcSwYBBAQCXwUBAgIdAkwbQB8AAAMEAwAEfgADAwFfAAEBHEsGAQQEAl8FAQICHQJMWUATGxsAABssGyskIgAaABkkKgcHFisEJiYnNTQ2Njc2NycGByc2JDMyFhYVFRQGBiM+AjU1NCYmIyIGBgcVFBYWMwIK4X4BK0wwCAYBNZ4kdQEmfKTtf3zilF2PTk+TY1mHSwJOjV0Wh/WheE2YeSAGAgMGK4wjKoHzqI2h9YedX65yjHavXV+zeYNyrl8AAAEAPf/nA+wEmgAWACxAKRIIAgECBwEAAQJKBAMCAgIXSwABAQBgAAAAHQBMAAAAFgAWExMkBQcXKwERFAYGIyInNxYXJicDMxMWFhc2NjURA+yD7ZzJ2i2ui0oZw7q/DRwVeIcEmv0+n99zU4pDCVZxA1v8mz1UIxusiQLJAAAAAQCP/mAECgSaABUAL0AsBwECAQ4BAwICSgACAAMAAgNnBAEBAQVdAAUFF0sAAAAZAEwRFhEWERAGBxorASMRIQYGFRUWFjMVIiYnJyY2NyM1IQQKt/5sICgXSjx8jkgDAjQtjwN7/mAFpTeuU4gKCKAdJsVkxkCVAAAAAAEAjv/sBE8EmgAiAENAQA8BAgEWAQMCAwEAAwIBBgAESgACAAMAAgNnBAEBAQVdAAUFF0sAAAAGXwcBBgYdBkwAAAAiACERFhEWEyUIBxorBCYnNxYWMzY2NREhBgYVFRYWMxUiJicnNDY3IzUhERQGBiMB9M5fJFi1SKSy/iggJxZKPHyORwQzLI8DwYLnmRQiIYsbGwKilwJGN6lUWgoIoB0mmGXBP5X9SKbhbwAAAQA7/mAEHASaAA4AG0AYCwEAAQFKAgEBARdLAAAAGQBMEhQUAwcXKwEWFhURIxE0JwEzAQEzAQLJHhuvJv4O0QE4AQfR/pUB+itlQf03AtpPOALZ/iMB3f2DAAABADsAAAQcBJoACgAlQCIIAQECAQEAAQJKAwECAhdLAAEBAF4AAAAYAEwSERESBAcYKwEBFSE1IQEzAQEzArMBN/yYAnn9QNEBOgEF0QIf/j9elgQE/iYB2gAAAAIAs/5gBEQEmgANABcAK0AoAAEBAl0AAgIXSwAAAANfAAMDGEsABAQFXQAFBRkFTBQUFBEUEAYHGislPgI1ESE1IREUBgYHATQ2NzMGBhURIwJBY5RS/SkDkX/pm/6JGxmYDQu0kgNSlWYCI5X9TJPZdwMBulpoNS1uUfybAAAAAQA8AAADVASaAAwAGUAWAAAAAV0AAQEXSwACAhgCTBQhIgMHFysBNCYjITUhMhYWFREjAptxYv50AZJ5sF25AzZgb5VYpnT82AAAAAIAtf/kBaUEmgATAB0ALUAqAAEABREBAgACSgAFBQFdBAMCAQEXSwAAAAJfAAICHQJMFBQTJBQiBgcaKyUWFjMyNjY1ETMRFAYEIyIkJxEzADY1ETMRFAYHJwFpYNRgkOKCtLP+0LyS/syLtAFAJrUyNJrRKS5TqXwCqP1HpuVyRUMELvzSiVsCSv3XZpc1BgAAAAABAAn/9gRnBJoAHwBvS7AxUFhAChMBAwEBShIBAEcbQAoTAQMBEgECAAJKWUuwMVBYQBgEAQEBBV0GAQUFF0sAAwMAXwIBAAAYAEwbQBwEAQEBBV0GAQUFF0sAAAAYSwADAwJfAAICGAJMWUAOAAAAHwAeFiMmIxMHBxkrABYVESMDNCYjIwYGFREUBiMiJzUWMzI2NRE0NjcjNSEDgOe5AYOSpyEnmpMrSCAlV08wKt0CJQSa3+T9KQLimYo2qVn+c6KoCZUEWmEBdFu1NpX//wA3/t8ENASaACIBzzcAACMA3QJrAAAAIgDdAAABAwHC/xwAAAArQCgFAQIABgACBn4ABgAHBgdhAwEAAAFdBAEBARcATBERERERERERCAcnKwAAAQA9/9kEDQSaABYALEApEggCAQIHAQABAkoEAwICAhdLAAEBAGAAAAAdAEwAAAAWABYTEyQFBxcrAQMOAiMiJzcWFyYnAzMTFhYXNjY3EwQNIwiC5Zu66S26l1Ebw7q+DR4WbXoHIgSa/TWg4nRXi0kFWXkDW/ybPlckHqiFAtMAAAAAAQCYAAAGtASaABAAH0AcDgsDAwACAUoDAQICF0sBAQAAGABMEhQVEQQHGCsBASEBBgYVESMRNDcnMwEBMwTzAaH/APxJJSW0pu34At0Bd9ABf/6BA247lGP9xAJC/YDb/WACoAAAAAABADwAAAUDBJoABwApQCYBAQECAgEAAQJKAAEBAl0DAQICF0sAAAAYAEwAAAAHAAcREwQHFisBFQcRIxEhNQUD7rn84ASaixb8BwQFlQAAAAIAlAAABccEmgAMABcAIUAeEQECAAFKAAAAAV0AAQEXSwMBAgIYAkwaFCEiBAcYKwE0JiMhNSEyFhYVESMBNDY3NxcGBhURIwUNcGH8WQOrerBduvuHFhGmCA0RtwM2YG+VWKZ0/NgBjE5tMBgJMoZJ/nsAAQBR/94FZgSaABEAL0AsAwEAAQIBAwACSgABAQJdAAICF0sAAAADXwQBAwMdA0wAAAARABAREyUFBxcrBCQnNxYEMyAkNxEhNSERBgQhAiz+yaQcogEmcQEGAQAB+8YE8wH+mf6vIiQjlSAhraoCNZX9Nff6AAAAAQBTAAAFPwXdAAgAJUAiAAEBAwFKAAIDAoMAAQEDXQADAxdLAAAAGABMEREREQQHGCsBASMBIREzESEFP/2YxwJL+/i5BDMEJfvbBAUB2P69AAAAAgByAAAGTgSaAAMABwAlQCIAAgIAXQAAABdLBAEDAwFdAAEBGAFMBAQEBwQHEhEQBQcXKxMhESElESERcgXc+iQFI/uWBJr7ZpYDb/yRAAABADwAAAUCBJoADAAZQBYAAAABXQABARdLAAICGAJMFCEiAwcXKwE0JiMhNSEyFhYVESMESXBi/MUDP3qwXbkDNmBvlVimdPzYAAAAAQAJ//YGQwSaAB8Ab0uwMVBYQAoTAQMBAUoSAQBHG0AKEwEDARIBAgACSllLsDFQWEAYBAEBAQVdBgEFBRdLAAMDAF8CAQAAGABMG0AcBAEBAQVdBgEFBRdLAAAAGEsAAwMCXwACAhgCTFlADgAAAB8AHhYjJiMTBwcZKwAWFREjAzQmIyEGBhURFAYjIic1FjMyNjURNDY3IzUhBV3muQGDkv19ISeakytIICVXTzAq3QQCBJrf5P0pAuKZijapWf5zoqgJlQRaYQF0W7U2lQAAAP//ALX/5AWlBcQAIwHPALUAAAAiAO0AAAECAcoVAAA9QDoBAQAFEgECAAJKAAYIAQcBBgdnAAUFAV0EAwIBARdLAAAAAl8AAgIdAkwfHx8qHyklFBQTJBQjCQcmKwAAAP//ALX/5AWlBcQAIwHPALUAAAAiAO0AAAECAcsaAAA9QDoBAQAFEgECAAJKAAYIAQcBBgdnAAUFAV0EAwIBARdLAAAAAl8AAgIdAkwfHx8qHyklFBQTJBQjCQcmKwAAAP//ALX/5AWlBcQAIwHPALUAAAAiAO0AAAAnAcgC/gAEAQIByhUAAE5ASwEBAAUSAQIAAkoACAsBCQEICWcABgoBBwUGB2cABQUBXQQDAgEBF0sAAAACXwACAh0CTCsrHx8rNis1MS8fKh8pJRQUEyQUIwwHJisAAP//ALX/5AWlBcQAIwHPALUAAAAiAO0AAAAnAcgC/gAEAQIByxoAAE5ASwEBAAUSAQIAAkoACAsBCQEICWcABgoBBwUGB2cABQUBXQQDAgEBF0sAAAACXwACAh0CTCsrHx8rNis1MS8fKh8pJRQUEyQUIwwHJisAAP//AJj+3wTCBJoAIwHPAJgAAAAiANQAAAECAcJSAAAoQCUPDAQDAAIBSgAEAAUEBWEDAQICF0sBAQAAGABMERESFRQSBgclK///AJj96QTCBJoAIwHPAJgAAAAiANQAAAECAcRSAABlQAwPDAQDAAIUAQcEAkpLsAtQWEAdCAEHBAQHbwAFBgEEBwUEZQMBAgIXSwEBAAAYAEwbQBwIAQcEB4QABQYBBAcFBGUDAQICF0sBAQAAGABMWUAQEhISKBInEREZEhUUEgkHJisAAAD//wCYAAAEwgSaACMBzwCYAAAAIgDUAAABBwHIAQv+hwAvQCwPDAQDBAIBSgAEBgEFAAQFZwMBAgIXSwEBAAAYAEwSEhIdEhwlEhUUEgcHJCsAAAD//wA9AAAEBQSaACIBzz0AACIA1QAAAQYByEEEADlANhABAQUBAQABAkoABAYBBQEEBWcAAgIDXQADAxdLAAEBAF0AAAAYAEwREREcERsoISMREgcHJCsA//8AFP/4AscEmgAiAc8UAAAiANYAAAEGAciNBABAQD0SCAMDAAYBSgAFCAEGAAUGZwABAQJdAAICF0sAAAADXwcEAgMDGANMGBgBARgjGCIeHAEXARYUERMkCQcjKwAA//8APAAAA+sEmgAiAc88AAAiANcAAAEGAcgGBAAyQC8ABAcBBQEEBWcCAQAAA10GAQMDF0sAAQEYAUwJCQEBCRQJEw8NAQgBCBEREggHIiv//wCUAAAEHASaACMBzwCUAAAAIgDYAAABBwHIARoABABaS7AVUFhAHgcBBgIDBlcAAAABXQABARdLBQEDAwJdBAECAhgCTBtAHwAFBwEGAgUGZwAAAAFdAAEBF0sAAwMCXQQBAgIYAkxZQA8XFxciFyElFBQTISMIByUr////+QAAAacEmgAiAc8AAAAiANkAAAEHAcj/CwAEAClAJgADBQEEAgMEZwAAAAFdAAEBF0sAAgIYAkwHBwcSBxElERERBgcjKwAAAP//AEwAAAKIBJoAIgHPTAAAIgDaAAABBwHIAPkABAAvQCwCAQEAAUoAAwUBBAIDBGcAAQEAXQAAABdLAAICGAJMCQkJFAkTJREREwYHIysA//8Atf/qBGEEpQAjAc8AtQAAACIA3AAAAQcByAFPAAQAc0AKFgECABUBBQICSkuwLVBYQCAABQgBBgEFBmcAAgIAXwMBAAAXSwABAQRfBwEEBB0ETBtAJAAFCAEGAQUGZwAAABdLAAICA18AAwMXSwABAQRfBwEEBB0ETFlAFSMjAQEjLiMtKScBIgEhJCYkFQkHIysAAAD//wANAgkByQSaACMBzwANAgkAIgDdAAABBwHI/x8AwAApQCYAAgQChAADBQEEAgMEZwAAAAFdAAEBFwBMBwcHEgcRJREREQYHIysA//8AQf5gA0IEmgAiAc9BAAAiAN4AAAEGAcgNBAApQCYAAwUBBAIDBGcAAAABXQABARdLAAICGQJMBwcHEgcRJREREQYHIysA//8AUf/vA5gEmgAiAc9RAAAiAN8AAAEGAchcBABAQD0DAQAFAgEDAAJKAAQHAQUABAVnAAEBAl0AAgIXSwAAAANfBgEDAx0DTBERAQERHBEbFxUBEAEPERMkCAciKwAA//8AUwAAA9IGAAAiAc9TAAAiAOAAAAEGAcj6UAA1QDIBAQEDAUoAAgMCgwAEBgEFAAQFZwABAQNdAAMDF0sAAAAYAEwKCgoVChQlEREREgcHJCsA//8AhAAABHsEsAAjAc8AhAAAACIA4gAAAQcByAF0AAQAPEA5CwEDAAoGAgUDAkoABQcBBgIFBmcAAwMAXwAAABxLAAICAV0EAQEBGAFMHx8fKh8pJRMjERMtCAclKwAA//8APP/sAjEEmgAiAc88AAAiAOQAAAEHAcj/cAAEAEBAPQMBAAUCAQMAAkoABAcBBQAEBWcAAQECXQACAhdLAAAAA18GAQMDHQNMEREBAREcERsXFQEQAQ8REyQIByIr//8AZ//qBI4EsAAiAc9nAAAiAOUAAAEHAcgBYAAEAJFLsCJQWEAKDwEAAQ4BBQACShtACg8BAwEOAQUAAkpZS7AiUFhAIQAFCQEGBAUGZwMBAAABXwABARxLCAEEBAJfBwECAh0CTBtAKAAAAwUDAAV+AAUJAQYEBQZnAAMDAV8AAQEcSwgBBAQCXwcBAgIdAkxZQBsuLhwcAQEuOS44NDIcLRwsJSMBGwEaJCsKByErAAAA//8Aj/5gBAoEmgAjAc8AjwAAACIA5wAAAQcByAEsALYAP0A8CAECBw8BAwICSgAGCAEHAgYHZwACAAMAAgNnBAEBAQVdAAUFF0sAAAAZAEwXFxciFyElERYRFhERCQcmKwAAAP//AI7/7ARPBJoAIwHPAI4AAAAiAOgAAAEHAcgBTQDCAFRAURABAggXAQMCBAEAAwMBBgAESgAHCgEIAgcIZwACAAMAAgNnBAEBAQVdAAUFF0sAAAAGXwkBBgYdBkwkJAEBJC8kLiooASMBIhEWERYTJgsHJSsAAAACADsAAAQcBJoACgAWAC9ALAgBBQIBAQABAkoABQAEAQUEZwMBAgIXSwABAQBeAAAAGABMJCISERESBgcaKwEBFSE1IQEzAQEzAAYjIiY1NDYzMhYVArMBN/yYAnn9QNEBOgEF0f11LSIhLS0hIi0CH/4/XpYEBP4mAdr8ryosHh8qKh8AAAD//wCz/mAERASaACMBzwCzAAAAIgDrAAABBwHIAUIABABtS7AmUFhAJwgBBwAEB1cAAQECXQACAhdLAAAAA18AAwMYSwYBBAQFXQAFBRkFTBtAKAAGCAEHAAYHZwABAQJdAAICF0sAAAADXwADAxhLAAQEBV0ABQUZBUxZQBAZGRkkGSMlFBQUERQRCQcmKwD//wA8AAADVASaACIBzzwAACIA7AAAAQYByBkEAClAJgADBQEEAgMEZwAAAAFdAAEBF0sAAgIYAkwODg4ZDhglFCEjBgcjKwD//wC1/+QFpQSaACMBzwC1AAAAIgDtAAABBwHIAv4ABAA9QDoBAQAFEgECAAJKAAYIAQcFBgdnAAUFAV0EAwIBARdLAAAAAl8AAgIdAkwfHx8qHyklFBQTJBQjCQcmKwD//wAJ//YEZwSaACIBzwkAACIA7gAAAQcByAFvAAQAiUuwMVBYQAoUAQMHAUoTAQBHG0AKFAEDBxMBAgACSllLsDFQWEAhAAYJAQcDBgdnBAEBAQVdCAEFBRdLAAMDAF8CAQAAGABMG0AlAAYJAQcDBgdnBAEBAQVdCAEFBRdLAAAAGEsAAwMCXwACAhgCTFlAFiEhAQEhLCErJyUBIAEfFiMmIxQKByQrAAAA//8AZAAAAacFxAAiAc9kAAAiANkAAAECAcUDAAApQCYAAwUBBAEDBGcAAAABXQABARdLAAICGAJMBwcHEgcRJREREQYHIysA//8APQAABAUFuAAiAc89AAAiANUAAAECAckAAAAzQDAQAQECAQEAAQJKAAQABQMEBWUAAgIDXQADAxdLAAEBAF0AAAAYAEwRFCEjERIGByUrAAAA//8AUf/vA5gFuAAiAc9RAAAiAN8AAAECAcn6AAA7QDgDAQABAgEDAAJKAAQABQIEBWUAAQECXQACAhdLAAAAA18GAQMDHQNMAQEUExIRARABDxETJAcHIisAAAD//wCO/+wETwW4ACMBzwCOAAAAIgDoAAABAwHJAKEAAABPQEwQAQIBFwEDAgQBAAMDAQYABEoABwAIBQcIZQACAAMAAgNnBAEBAQVdAAUFF0sAAAAGXwkBBgYdBkwBAScmJSQBIwEiERYRFhMmCgclKwAAAAABABT/+ALHBJoAFgAtQCoRBwIDAAEBSgACAAEAAgFlAAAAA18FBAIDAyYDTAAAABYAFRQREyMGCBgrFic1FjM2NjcRITUhERQXEyMDIw4CIyMPGx+QtBn+9gHBE1KvRQcMbqtkCAOrBASngwI1lf2gXVf+egFKWZxdAAAAAQBKAAADYgSaAAwAF0AUAAEAAAIBAGUAAgImAkwUISIDCBcrATQmIyE1ITIWFhURIwKocGL+dAGRerBdugM2YG+VWKZ0/NgAAQBKAAADYgSaAAwAF0AUAAEAAAIBAGUAAgImAkwUISIDCBcrATQmIyE1ITIWFhURIwKocGL+dAGRerBdugM2YG+VWKZ0/NgAAQBKAAADYgSaAAwAF0AUAAEAAAIBAGUAAgImAkwUISIDCBcrATQmIyE1ITIWFhURIwKocGL+dAGRerBdugM2YG+VWKZ0/NgAAQBKAAADYgSaAAwAF0AUAAEAAAIBAGUAAgImAkwUISIDCBcrATQmIyE1ITIWFhURIwKocGL+dAGRerBdugM2YG+VWKZ0/NgAAgBz/+wECwXEAA0AGwAfQBwAAgIBXwABAS1LAAMDAF8AAAAuAEwlJSUhBAgYKwACIyICETUQEjMyEhEVAiYjIgYVERQWMzI2NREEC+vf3vDt3+Dsu4eKiImMh4mGARf+1QEsAVXfAVQBJP7c/qzfAe7T1O/+5e/f3fEBGwAAAAABAKsAAALaBbgABgAhQB4DAgEDAQABSgAAACVLAgEBASYBTAAAAAYABhQDCBUrIREFNSUzEQIf/owCEh0E04mnx/pIAAEAXQAABDQFxAAcADRAMQEBBAMBSgABAAMAAQN+AAAAAl8AAgItSwADAwRdBQEEBCYETAAAABwAHBcjEikGCBgrMzUBPgI1NCYmIyIGFSM0NjYzMhYWFRQGBwEhFXkB3VlgJzxyTo6Uu27Yl4fGapN+/noC2oQCE2OHbTtJd0WiiH3Md2GwdHj/if5XlgAAAAEAX//sA/oFxAAxAEFAPjEBAwQBSgAGBQQFBgR+AAEDAgMBAn4ABAADAQQDZwAFBQdfAAcHLUsAAgIAXwAAAC4ATCMTJSIkIxMlCAgcKwAWFRQGBiMiJiY1MxQWFjMyNjU0JiMjNTUzMjY2NTQmIyIGBhUjNDY2MzIWFhUUBgYHA4V1eNGEgdJ7u0N8VIGRo5GEhF2BQH9+TnZCu3HMhIXGbTJnTALBuXKFwGVgt35MdD+KhoWFbihAckl+gz5xTG+4al65hDl6aiAAAAIANQAABFEFsAALAA8AMUAuDQEEAwYBAAQCSgYFAgQCAQABBABmAAMDJUsAAQEmAUwMDAwPDA8RIhEREAcIGSsBIxEjESE1ATMzETMhEQcBBFHKu/1pAouYL8r+exb+UgFT/q0BU2wD8fw5Assn/VwAAAAAAQCa/+wELgWwACQAPUA6BAEGAiQjAgQGAkoABAYFBgQFfgACAAYEAgZnAAEBAF0AAAAlSwAFBQNfAAMDLgNMJiITJiMREAcIGysBIRUhAzY2MzIWFhUUBgYjIiYmJzMWFjMyNjY1NCYmIyIGBgcnARgC6/2zKy17SoXDaWfPl3nFfQyxEY92VntBRINbO1E6J5YFsKn+cRskd9yUjtt7WrWEfIFRlWZdk1QVJiImAAACAIX/7AQdBbIAGAApAEBAPRUBBAMkAQUEAkoGAQMABAUDBGcAAgIBXwABASVLBwEFBQBfAAAALgBMGRkAABkpGSghHwAYABciFiYICBcrABYWFRQGBiMiJiY1NRAAITMVIyICBzY2MxI2NjU0JiYjIgYGBxUUFhYzAwa8W2jMkJbUagE3AYMQEPfyEjqfXzF5Pzt3Vj90VRVOgUoDu4fbfozhgqH/jlgBbAHUnP7z0UJB/MlVl2Jaml01WzhGfbZeAAAAAQBOAAAEJwWwAAYAH0AcBgEBAgFKAAEBAl0AAgIlSwAAACYATBEREAMIFyshIwEhNSEVAczEAln87QPZBRqWZwAAAAADAHH/7AQPBcQAGwArADkAPUA6Gw0CBAIBSgACAAQFAgRnBgEDAwFfAAEBLUsHAQUFAF8AAAAuAEwsLBwcLDksODMxHCscKi0sJQgIFysAFhUUBgYjIiYmNTQ2NyYmNTQ2NjMyFhYVFAYHAAYGFRQWFjMyNjY1NCYmIxI2NTQmJiMiBgYVFBYzA4qFedKEhNN4gnJjcG7DfX3CbnJj/uBuPTxvSEhuPD1uR4GTRn9RU3xDk4ECvbx3gbxhYbuCeL0wMKtrfLVeX7R8a6wwAkA+ckxLcD4+cEtIc0H7VI5+UH5GRn1Rfo4AAgBk//4D+QXEABkAKQA9QDocDgIFBAFKBwEFAAIBBQJnAAQEA18GAQMDLUsAAQEAXwAAACYATBoaAAAaKRooIiAAGQAYJCIXCAgXKwAWEhUVFAIEISM1MyASNwYGIyImJjU0NjYzEjY3NTQmJiMiBgYVFBYWMwLH0WFo/s7+5RMTARTdCTyiXIm8W2jMkFabJkyAS1N6QDt3VQXEpf72nUXi/prtnAEJ3UdSi959jeWF/LtxV0mAu2FYm2JanWH//wBQAo0CngW4ACMBzwBQAo0BBwEvAAACmAAcQBkAAwAAAwBjAAICAV8AAQElAkwjJCUiBAgjK///ADYCmAK8Ba0AIwHPADYCmAEHATMAAAKYADFALg4BBAMHAQAEAkoGBQIEAgEAAQQAZQABAQNdAAMDJQFMDQ0NEA0QESIREREHCCQrAAAA//8AWwKNAqgFrQAjAc8AWwKNAQcBNAAAApgAOkA3BQEGAiEgAgQGAkoABAYFBgQFfgACAAYEAgZnAAUAAwUDYwABAQBdAAAAJQFMJCITJSMREQcIJisAAP//AFYCjQKsBbcAIwHPAFYCjQEHATUAAAKYAD1AOhUBBAMhAQUEAkoGAQMABAUDBGcHAQUAAAUAYwACAgFfAAEBJQJMGRkBARklGSQfHQEYARchJicICCIrAAAA//8AOwKYAqYFrQAjAc8AOwKYAQcBNgAAApgAH0AcBwEBAgFKAAABAIQAAQECXQACAiUBTBEREQMIIisA//8ATwKNAqAFuAAjAc8ATwKNAQcBNwAAApgAZLYbDgIEAgFKS7AjUFhAHgcBBQAABQBjBgEDAwFfAAEBJUsABAQCXwACAjAETBtAHAACAAQFAgRnBwEFAAAFAGMGAQMDAV8AAQElA0xZQBQoKBwcKDMoMi4sHCccJisrJggIIiv//wBJApEClgW4ACMBzwBJApEBBwE4AAACmABoQAobAQUEDQECBQJKS7AXUFhAHgABAAABAGMABAQDXwYBAwMlSwACAgVfBwEFBSgCTBtAHAcBBQACAQUCZwABAAABAGMABAQDXwYBAwMlBExZQBQZGQEBGSUZJCAeARgBFyQiFggIIisAAgBQ//UCngMgAA0AFwByS7AKUFhAEwABAAIDAQJnAAMDAF8AAAAmAEwbS7AMUFhAEwABAAIDAQJnAAMDAF8AAAAuAEwbS7AOUFhAEwABAAIDAQJnAAMDAF8AAAAmAEwbQBMAAQACAwECZwADAwBfAAAALgBMWVlZtiMkJSEECBgrJAYjIiY1NTQ2MzIWFRUCIyIVFRQzMjU1Ap6diIqfnomJnp6JiIqHnqmpp4umqqqmiwFbwqbExKYAAAABAHsAAAHwAxUABgAaQBcEAwIBBABIAQEAACYATAAAAAYABgIIFCshEQc1JTMRAVLXAWMSAls6gHT86wABAEIAAAKsAyAAGwAyQC8BAQQDAUoAAQADAAEDfgACAAABAgBnAAMDBF0FAQQEJgRMAAAAGwAbGCMSJwYIGCszNQE2NjU0JiMiBhUjNDY2MzIWFhUUBgYHByEVVAEhQDhBOkhJn0mIW1mBRC1PQ68BjmwBDzxYIzI9SjxId0Y5a0gzW1c9k38AAQA///UCmwMgACsA3rUrAQMEAUpLsApQWEArAAYFBAUGBH4AAQMCAwECfgAHAAUGBwVnAAQAAwEEA2cAAgIAXwAAACYATBtLsAxQWEArAAYFBAUGBH4AAQMCAwECfgAHAAUGBwVnAAQAAwEEA2cAAgIAXwAAAC4ATBtLsA5QWEArAAYFBAUGBH4AAQMCAwECfgAHAAUGBwVnAAQAAwEEA2cAAgIAXwAAACYATBtAKwAGBQQFBgR+AAEDAgMBAn4ABwAFBgcFZwAEAAMBBANnAAICAF8AAAAuAExZWVlACyMSJCIkIhMkCAgcKwAVFAYGIyImJjUzFBYzMjY1NCYjIzU1MzI2NTQmIyIGFSM0NjYzMhYVFAYHAptPi1hPiFOeUUBESlFNVVVHSkJCOUqfTINQhp9GQQFrj0hoNzNpTS48PTM9N00mPDIvOTMrQmU2dWs4WxgAAAACADYAAAK8AxUACwAPADFALg0BBAMGAQAEAkoGBQIEAgEAAQQAZQADAwFdAAEBJgFMDAwMDwwPESIRERAHCBkrJSMVIzUhJwEzMxEzIREHAwK8a5/+iwcBeX0la/72D86qqqplAgb+FgEgGv76AAAAAQBb//UCqAMVACAAw0ALBAEGAiAfAgQGAkpLsApQWEAjAAQGBQYEBX4AAAABAgABZQACAAYEAgZnAAUFA18AAwMmA0wbS7AMUFhAIwAEBgUGBAV+AAAAAQIAAWUAAgAGBAIGZwAFBQNfAAMDLgNMG0uwDlBYQCMABAYFBgQFfgAAAAECAAFlAAIABgQCBmcABQUDXwADAyYDTBtAIwAEBgUGBAV+AAAAAQIAAWUAAgAGBAIGZwAFBQNfAAMDLgNMWVlZQAokIhMlIxEQBwgbKxMhFSEHNjYzMhYVFAYGIyImJiczFhYzMjY1NCYjIgYHJ6IB3/6kFxlJKYCPQYRhTIRTBJwGSTxFQk9HMTYdfwMVgqwLEol5SnpJNmdGMjJPQUBMFhUfAAACAFb/9QKsAx8AFwAkALRAChQBBAMgAQUEAkpLsApQWEAdAAEAAgMBAmcGAQMABAUDBGcHAQUFAF8AAAAmAEwbS7AMUFhAHQABAAIDAQJnBgEDAAQFAwRnBwEFBQBfAAAALgBMG0uwDlBYQB0AAQACAwECZwYBAwAEBQMEZwcBBQUAXwAAACYATBtAHQABAAIDAQJnBgEDAAQFAwRnBwEFBQBfAAAALgBMWVlZQBQYGAAAGCQYIx4cABcAFiEmJggIFysAFhYVFAYGIyImJjU1NDYzMxUjIAc2NjMSNjU0JiMiBgcVFBYzAgFyOUeFWF2KS+XYFgv+8BgiXjkUS0hCME4SUEECDEV3TE17R0yOXzjS54LeIyr+aFE+P0srICNSWQAAAQA7AAACpgMVAAYAHUAaBgEBAgFKAAIAAQACAWUAAAAmAEwRERADCBcrISMBITUhFQFJpwFd/jwCawKVgFkAAAMAT//1AqADIAAaACYAMgCwthoNAgQCAUpLsApQWEAdAAEGAQMCAQNnAAIABAUCBGcHAQUFAF8AAAAmAEwbS7AMUFhAHQABBgEDAgEDZwACAAQFAgRnBwEFBQBfAAAALgBMG0uwDlBYQB0AAQYBAwIBA2cAAgAEBQIEZwcBBQUAXwAAACYATBtAHQABBgEDAgEDZwACAAQFAgRnBwEFBQBfAAAALgBMWVlZQBQnJxsbJzInMS0rGyYbJSsrJQgIFysAFhUUBgYjIiYmNTQ2NyYmNTQ2MzIWFhUUBgcCBhUUFjMyNjU0JiMSNjU0JiMiBhUUFjMCVkpNhlVVh01KQTg/mHxRfUc/ONU/QDc2QEE2P0xNPj9KSz8Be2JBSWY0NGZJQWIcGlk6aHQzZEU6WRsBCzcvLzg4Ly44/dM8NDM8PDM0PAAAAAACAEn/+QKWAyAAFwAkAD5AOxoBBQQMAQIFAkoGAQMABAUDBGcHAQUAAgEFAmcAAQEAXwAAACYATBgYAAAYJBgjHx0AFwAWJCIVCAgXKwAWFRUUBiMjNTMyNjcGBiMiJiY1NDY2MxI2NzU0JiMiBhUUFjMB+Z3Y3g8Oh4YKIFgyU3Q8SIZXMkcRTD49SkdCAyCqmDTU3X5iZx4jQ3dOTYBM/lgpHztQVVlAP1D//wB+/+wEFgWyACIBz34AAQIBJPkAAEBAPRYBBAMlAQUEAkoGAQMABAUDBGcAAgIBXwABASVLBwEFBQBfAAAALgBMGhoBARoqGikiIAEZARgiFicICCIr//8Acf/sBA8FxAAiAc9xAAECASYAAAA9QDocDgIEAgFKAAIABAUCBGcGAQMDAV8AAQEtSwcBBQUAXwAAAC4ATC0tHR0tOi05NDIdLB0rLSwmCAgiKwAAAAABADwAbwNsBSMAAwAGswMBATArNwEXATwCx2n9ObEEckL7jv//AFD+hQKeAbAAIgHPUAABBwEvAAD+kAAfQBwAAgIBXwABAT1LAAMDAF8AAAA+AEwjJCUiBAkjKwAAAP//AHv+kQHwAaYAIgHPewABBwEwAAD+kQAaQBcFBAMCBABIAQEAADoATAEBAQcBBwIJHyv//wBC/pECrAGxACIBz0IAAQcBMQAA/pEANEAxAgEEAwFKAAEAAwABA34AAAACXwACAj1LAAMDBF0FAQQEOgRMAQEBHAEcGCMSKAYJIysAAP//AD/+hgKbAbEAIgHPPwABBwEyAAD+kQBBQD4sAQMEAUoABgUEBQYEfgABAwIDAQJ+AAQAAwEEA2cABQUHXwAHBz1LAAICAF8AAAA+AEwjEiQiJCITJQgJJysA//8ANv6RArwBpgAiAc82AAEHATMAAP6RADFALg4BBAMHAQAEAkoGBQIEAgEAAQQAZQADAzlLAAEBOgFMDQ0NEA0QESIREREHCSQrAP//AFv+hgKoAaYAIgHPWwABBwE0AAD+kQA9QDoFAQYCISACBAYCSgAEBgUGBAV+AAIABgQCBmcAAQEAXQAAADlLAAUFA18AAwM+A0wkIhMlIxERBwkmKwD//wBW/oYCrAGwACIBz1YAAQcBNQAA/pEAQEA9FQEEAyEBBQQCSgYBAwAEBQMEZwACAgFfAAEBPUsHAQUFAF8AAAA+AEwZGQEBGSUZJB8dARgBFyEmJwgJIisAAP//ADv+kQKmAaYAIgHPOwABBwE2AAD+kQAfQBwHAQECAUoAAQECXQACAjlLAAAAOgBMERERAwkiKwAAAP//AE/+hgKgAbEAIgHPTwABBwE3AAD+kQA9QDobDgIEAgFKAAIABAUCBGcGAQMDAV8AAQE9SwcBBQUAXwAAAD4ATCgoHBwoMygyLiwcJxwmKysmCAkiKwD//wBJ/ooClgGxACIBz0kAAQcBOAAA/pEAQEA9GwEFBA0BAgUCSgcBBQACAQUCZwAEBANfBgEDAz1LAAEBAF8AAAA+AEwZGQEBGSUZJCAeARgBFyQiFggJIisAAP//AFACkAKeBbsAIwHPAFACkAEHAS8AAAKbAB9AHAACAgFfAAEBSUsAAwMAXwAAAEoATCMkJSIECiMrAP//AHsCmwHwBbAAIwHPAHsCmwEHATAAAAKbABpAFwUEAwIEAEgBAQAARgBMAQEBBwEHAgofKwAA//8AQgKbAqwFuwAjAc8AQgKbAQcBMQAAApsANEAxAgEEAwFKAAEAAwABA34AAAACXwACAklLAAMDBF0FAQQERgRMAQEBHAEcGCMSKAYKIyv//wA/ApACmwW7ACMBzwA/ApABBwEyAAACmwBBQD4sAQMEAUoABgUEBQYEfgABAwIDAQJ+AAQAAwEEA2cABQUHXwAHB0lLAAICAF8AAABKAEwjEiQiJCITJQgKJysAAAD//wA2ApsCvAWwACMBzwA2ApsBBwEzAAACmwAxQC4OAQQDBwEABAJKBgUCBAIBAAEEAGUAAwNFSwABAUYBTA0NDRANEBEiERERBwokKwAAAP//AFsCkAKoBbAAIwHPAFsCkAEHATQAAAKbAD1AOgUBBgIhIAIEBgJKAAQGBQYEBX4AAgAGBAIGZwABAQBdAAAARUsABQUDXwADA0oDTCQiEyUjEREHCiYrAAAA//8AVgKQAqwFugAjAc8AVgKQAQcBNQAAApsAQEA9FQEEAyEBBQQCSgYBAwAEBQMEZwACAgFfAAEBSUsHAQUFAF8AAABKAEwZGQEBGSUZJB8dARgBFyEmJwgKIiv//wA7ApsCpgWwACMBzwA7ApsBBwE2AAACmwAfQBwHAQECAUoAAQECXQACAkVLAAAARgBMERERAwoiKwD//wBPApACoAW7ACMBzwBPApABBwE3AAACmwA9QDobDgIEAgFKAAIABAUCBGcGAQMDAV8AAQFJSwcBBQUAXwAAAEoATCgoHBwoMygyLiwcJxwmKysmCAoiKwAAAP//AEkClAKWBbsAIwHPAEkClAEHATgAAAKbAEBAPRsBBQQNAQIFAkoHAQUAAgEFAmcABAQDXwYBAwNJSwABAQBfAAAASgBMGRkBARklGSQgHgEYARckIhYICiIrAAIAY//wA6wEnQAOABwAHUAaAAEAAgMBAmcAAwMAXwAAAC4ATCUlJiEECBgrJAIjIiYmNTU0EjMyFhUVAiYjIgYVFRQWMzI2NTUDrN7Fgb1o38XG37x2c3J3enFydfL+/nHlq67+AQD//64Bb6anpOSlrKum5AAAAAEAmAAAAp8EkAAGACFAHgMCAQMBAAFKAAAAAV0CAQEBJgFMAAAABgAGFAMIFSshEQU1JTMRAeP+tQHsGwOxY56k+3AAAQBOAAADywSdABwAMkAvAQEEAwFKAAEAAwABA34AAgAAAQIAZwADAwRdBQEEBCYETAAAABwAHBgjEigGCBgrMzUBNjY1NCYmIyIGFSM0NjYzMhYWFRQGBgcBIRVpAatuVjJgQH99vGjIiHm0YEBmWP65An2CAZ5mhj42VTF1Z2epY1SXY0WEe1z+55YAAAAAAQBO//ADoASdAC8AP0A8LwEDBAFKAAYFBAUGBH4AAQMCAwECfgAHAAUGBwVnAAQAAwEEA2cAAgIAXwAAAC4ATCMTJCIkIxMlCAgcKwAWFRQGBiMiJiY1MxQWFjMyNjU0JiMjNTUzMjY1NCYjIgYGFSM0NjYzMhYWFRQGBwM8ZG/Be3TBcrs7bEZvf4t+e3t4fW5sQWY5u2i7d3q3ZWFfAjWPYWqaUUuVajVRLmRaZGBoK2VTVV4pTDNckVJMlGpNiiYAAAIAMAAAA+UEjQALAA8AMUAuDQEEAwYBAAQCSgADBAODBgUCBAIBAAEEAGYAAQEmAUwMDAwPDA8RIhEREAcIGSsBIxEjESEnATMzETMhEQcBA+Wvu/24AwJCkTOv/pYa/pcBB/75AQdyAxT9EAH8Lv4yAAAAAQCA//ADxgSNACIAO0A4BAEGAiIhAgQGAkoABAYFBgQFfgAAAAECAAFlAAIABgQCBmcABQUDXwADAy4DTCUiEyYiERAHCBsrEyEVIQM2MzIWFhUUBgYjIiYmJzMWFjMyNjU0JiYjIgYGByfpAqj99SVnb3m0Yly9imy4dgm0DH9kcXc9c081STAklgSNof7eMF6wd2+wZkuVaFhahHBEazwSGxklAAAAAgBy//ADvASUABgAKAA+QDsVAQQDIwEFBAJKAAEAAgMBAmcGAQMABAUDBGcHAQUFAF8AAAAuAEwZGQAAGSgZJyEfABgAFyEmJggIFysAFhYVFAYGIyImJjU1EAAhMxUjIgYHNjYzEjY2NTQmJiMiBgcVFBYWMwLBqVJhu4GIwmMBJwFUFA/e2Q0yj1YfaDg0aEpPhR1BbkEDA2qwa3G1aHnPflQBKAFim8SpNkH9hD5uRkVuQU8+Kl+KRgABAEIAAAPBBI0ABgAdQBoGAQECAUoAAgABAAIBZQAAACYATBEREAMIFyshIwEhNSEVAarFAhb9RwN/A/aXaAAAAwBh//ADrgSdABsAKAA2ADtAOBsNAgQCAUoAAQYBAwIBA2cAAgAEBQIEZwcBBQUAXwAAAC4ATCkpHBwpNik1MC4cKBwnKywlCAgXKwAWFRQGBiMiJiY1NDY3JiY1NDY2MzIWFhUUBgcABhUUFjMyNjU0JiYjEjY1NCYmIyIGBhUUFjMDPHJvv3h5wG5xYlVgZLFycrNlYVb+0G5vXl1xNV49bYA8a0ZHajl/bQIwk2Bpl01NlmpglCcnh1VlkUxMkWVVhycBrmBSUmFiUTNSLfyAZ1o6WTExWTpaZwAAAAACAFb/+QObBJ0AFwAnAD5AOxoBBQQMAQIFAkoGAQMABAUDBGcHAQUAAgEFAmcAAQEAXwAAACYATBgYAAAYJxgmIB4AFwAWIyIWCAgXKwAWFhUVEAAhIzczIBMGBiMiJiY1NDY2MxI2NzU0JiYjIgYGFRQWFjMCgr1c/uP+phMBEgGjGC+JV3yqVGK8gU2BHkBrQUdoNzNmSQSdfNWJRf7b/qCZAVszPGqxbHK5bf1zUj5CXIRERHRFQ3FFAAEAHAJiA1YFsQAOABxAGQ4NDAsKCQgHBgUCAQwARwAAACUATBMBCBUrEzcFAzMDJRcFEwcDAycTHC8BLQmaCgEoL/7OxX64tH7IBDOVcAFZ/qJvmFv+8F0BIf7mWwEUAAEAKf+DAzoFsAADABlAFgAAAQCEAgEBASUBTAAAAAMAAxEDCBUrEwEjAdoCYLH9oAWw+dMGLQAAAQCTAmwBegNIAAsAH0AcAgEBAAABVwIBAQEAXwAAAQBPAAAACwAKJAMIFSsAFhUUBiMiJjU0NjMBPT09NzY9PDcDSD4xMD09MDE+AAEAiwIYAiMDygAQABhAFQAAAQEAVwAAAAFfAAEAAU8mIgIIFisSNjYzMhYWFRUUBiMiJiY1NYsyXD0/XTFuXj5cMgM9WjMzWTooWGwyWTkoAAAA//8Ahf/1AW0ERQAjAc8AhQAAACIBbPUAAQcBbP/2A3QAjkuwClBYQBcAAgIDXwUBAwMoSwQBAQEAXwAAACYATBtLsAxQWEAXAAICA18FAQMDMEsEAQEBAF8AAAAuAEwbS7AOUFhAFwACAgNfBQEDAyhLBAEBAQBfAAAAJgBMG0AXAAICA18FAQMDMEsEAQEBAF8AAAAuAExZWVlAEg0NAQENGA0XExEBDAELJQYIICsAAQAc/t4BNgDbAAkAEEANAwICAEcAAAB0FwEIFSsEBgcnNjY1NTMVATZeUmoxMrcYyEJIRYpPl5QAAAD//wCT//UE0ADRACMBzwCTAAAAIgFsAwAAIwFsAbkAAAEDAWwDWQAAAIJLsApQWEASCAUHAwYFAQEAXwQCAgAAJgBMG0uwDFBYQBIIBQcDBgUBAQBfBAICAAAuAEwbS7AOUFhAEggFBwMGBQEBAF8EAgIAACYATBtAEggFBwMGBQEBAF8EAgIAAC4ATFlZWUAaGRkNDQEBGSQZIx8dDRgNFxMRAQwBCyUJCCArAAIAof/1AX0FsAADAA8AjkuwClBYQBcEAQEBAF0AAAAlSwUBAwMCXwACAiYCTBtLsAxQWEAXBAEBAQBdAAAAJUsFAQMDAl8AAgIuAkwbS7AOUFhAFwQBAQEAXQAAACVLBQEDAwJfAAICJgJMG0AXBAEBAQBdAAAAJUsFAQMDAl8AAgIuAkxZWVlAEgQEAAAEDwQOCggAAwADEQYIFSsTAzMDBhYVFAYjIiY1NDYztQ7EDhs7OzQ0OTk0AZsEFfvr1DwuLjo6Li48AAAAAAIAi/6XAWcETQALAA8AKUAmBQEDAAIDAmEEAQEBAF8AAAAwAUwMDAAADA8MDw4NAAsACiQGCBUrEiY1NDYzMhYVFAYjFxMjE8Y7OzQ0OTk0WQ7EDgN7PC4tOzstLjzP++sEFQACAHgAAATUBbAAGwAfAHpLsBdQWEAoDwYCAAUDAgECAAFlCwEJCSVLDhANAwcHCF0MCgIICChLBAECAiYCTBtAJgwKAggOEA0DBwAIB2YPBgIABQMCAQIAAWULAQkJJUsEAQICJgJMWUAeAAAfHh0cABsAGxoZGBcWFRQTEREREREREREREQgdKwEDMxUjAyMTIQMjEyM1IRMhNSETMwMhEzMDMxUhIQMhA+5F4PtQkFD++VCQUO8BCkX+/QEeUZBRAQdRkVLL/or++UUBBwOG/p2J/mYBmv5mAZqJAWOKAaD+YAGg/mCK/p0AAAEAkP/1AXcA0QALAFpLsApQWEAMAgEBAQBfAAAAJgBMG0uwDFBYQAwCAQEBAF8AAAAuAEwbS7AOUFhADAIBAQEAXwAAACYATBtADAIBAQEAXwAAAC4ATFlZWUAKAAAACwAKJAMIFSskFhUUBiMiJjU0NjMBOj09NzY9PTbRPzEwPD0vMT8AAAACAEv/9QN3BcQAIQAtAMJLsApQWEAlAAEAAwABA34AAwUAAwV8AAAAAl8AAgItSwYBBQUEXwAEBCYETBtLsAxQWEAlAAEAAwABA34AAwUAAwV8AAAAAl8AAgItSwYBBQUEXwAEBC4ETBtLsA5QWEAlAAEAAwABA34AAwUAAwV8AAAAAl8AAgItSwYBBQUEXwAEBCYETBtAJQABAAMAAQN+AAMFAAMFfAAAAAJfAAICLUsGAQUFBF8ABAQuBExZWVlADiIiIi0iLCUaIxMrBwgZKwA2NjcwNz4CNTQmIyIGBhUjPgIzMhYWFRQGBgcGBhUjFhYVFAYjIiY1NDYzAWYgRUYfMzYkb2c9Zj67AW26dH60XkRqTDQpu5o6OjQ0Ojo0Afl6YkcfNENYOWp3LFtCbqRXXKt1VJZ/SC99UdM8Li46Oi4uPAAAAgBE/n4DegROAAsALABnS7AjUFhAJQAFAQMBBQN+AAMCAQMCfAYBAQEAXwAAADBLAAICBGAABAQqBEwbQCIABQEDAQUDfgADAgEDAnwAAgAEAgRkBgEBAQBfAAAAMAFMWUASAAAsKyEfHBsYFgALAAokBwgVKwAmNTQ2MzIWFRQGIxIGBgcHDgIVFBYzMjY2NTMOAiMiJiY1NDY2NzY2NTMBtDs7NDQ5OTRlIUFHGDI2JXNsPGY/uwFsu3SBuGFEaEkyKLsDfDwuLTs7LS48/s51XU0ZNUZdO21zLlxDb6ZZWqt3VZl/SDF4UAACAIgEFAIlBgAABQALADy2CwUCAAEBSkuwI1BYQA0CAQAAAV0DAQEBJwBMG0ATAwEBAAABVQMBAQEAXQIBAAEATVm2EhISEAQIGCsTIxM1MxUTIxM1MxX4cAGN8XABjQQUAVuRiP6cAWKKiAABAGcEIgD/BgAABQAtS7AjUFhACwAAAAFdAAEBJwBMG0AQAAEAAAFVAAEBAF0AAAEATVm0EhACCBYrEyMTNTMV6YIBlwQiAV9/bwD//wAp/t4BVQRFACIBzykAACcBbP/eA3QBAgFnDQAAdrQQDwICR0uwClBYQBEAAgAChAAAAAFfAwEBASgATBtLsAxQWEARAAIAAoQAAAABXwMBAQEwAEwbS7AOUFhAEQACAAKEAAAAAV8DAQEBKABMG0ARAAIAAoQAAAABXwMBAQEwAExZWVlADAEBFRQBDAELJQQIICsAAAABABL/gwMRBbAAAwAZQBYCAQEAAYQAAAAlAEwAAAADAAMRAwgVKxcBMwESAmCf/aF9Bi350wAAAAEABP9qA5kAAAADACaxBmREQBsAAAEBAFUAAAABXQIBAQABTQAAAAMAAxEDCBUrsQYARBc1IRUEA5WWlpYAAAABAED+kQKfBj0AHwAoQCUbAQABAUoUEwIBSAMCAgBHAAEAAAFXAAEBAF8AAAEATxIZAggWKwQWFwcmJjU1NCYjNTUyNjU1NDY3FwYGFRUUBgcWFhUVAc9gcCfApGdtbWekwCdwYFNVVVMktiNyNvCrz3F9eRd9cc+r8DZxJLaGz2ihLS2haM8AAQAU/pECcwY9AB8AKEAlEwEBAAFKGxoCAEgMCwIBRwAAAQEAVwAAAAFfAAEAAU8SEQIIFisAFjMVFSIGFRUUBgcnNjY1NTQ2NyYmNTU0Jic3FhYVFQGfaGxsaKPBJ3BgW11dW2BwJ8GjAyx9cCB+cM+s7zZyI7aGz2yhKSmhbM+GtiRxNu+szwAAAAABAJL+yAIMBoAABwAoQCUAAgQBAwACA2UAAAEBAFUAAAABXQABAAFNAAAABwAHERERBQgXKwERMxUhESEVAU2//oYBegXq+XSWB7iWAAAAAAEACv7IAYUGgAAHAChAJQQBAwACAQMCZQABAAABVQABAQBdAAABAE0AAAAHAAcREREFCBcrAREhNTMRIzUBhf6FwMAGgPhIlgaMlgABAIX+KgKXBmsAEwAGsw4DATArGgI3FwYGAhUVFBIWFwcmAgIRNYWd42snVpxlZZtXJ2vjnQNdAbYBHTt5Qvn+iO0P7f6I/UhvPAEcAbUBDgoAAAABACf+KgI4BmsAEwAGsw4DATArAAICByc2NhI1NTQCJic3FhISERUCOJ3iaydWnGVqnFEna+KdATf+S/7kPG9B/wF97Q/rAX3/QnA7/uP+Sv7yCgABAJECjAXJAyIAAwAeQBsAAAEBAFUAAAABXQIBAQABTQAAAAMAAxEDCBUrEzUhFZEFOAKMlpYAAAEAogKMBIwDIgADAB5AGwAAAQEAVQAAAAFdAgEBAAFNAAAAAwADEQMIFSsTNSEVogPqAoyWlgAAAQAlAiACDgK3AAMAHkAbAAABAQBVAAAAAV0CAQEAAU0AAAADAAMRAwgVKxM1IRUlAekCIJeXAP//ACUCIAIOArcAIwHPACUCIAECAXwAAAAeQBsAAAEBAFUAAAABXQIBAQABTQEBAQQBBBIDCCAr//8AZACXA2YDsgAjAc8AZACXACYBgPj9AQcBgAFE//0AJEAhDAQCAAEBSgMBAQAAAVUDAQEBAF0CAQABAE0UEhQSBAgjKwAA//8AZwCZA3oDtQAjAc8AZwCZACIBgQ0AAQMBgQFrAAAAJEAhDgYCAAEBSgMBAQAAAVUDAQEBAF0CAQABAE0SFBIUBAgjKwAAAAEAbACaAiIDtQAHAB5AGwMBAAEBSgABAAABVQABAQBdAAABAE0UEQIIFisBASMBNTUBMwEgAQKP/tkBJ48CJ/5zAYQNBgGEAAEAWgCZAg8DtQAHAB5AGwUBAAEBSgABAAABVQABAQBdAAABAE0SEwIIFisBFRUBIwEBMwIP/tmOAQH+/44CMA0G/nwBjgGOAAIAJP7UAmcA9gAJABMAFEARDQwDAgQARwEBAAB0GRcCCBYrBAYHJzY2NTUzFQQGByc2NjU1MxUBPV5RajAvugEqXlFqLy+7FNNFSEeUVaqnY9NFSEeUVaqnAAD//wBpBDICvwYUACMBzwBpBDIAIgGFCQABAwGFAUUAAAAUQBEODQQDBABIAQEAAHQZGAIIISsAAP//ADwEFwKJBgAAIwHPADwEFwAiAYYMAAEDAYYBQAAAACS2Dg0EAwQAR0uwI1BYtgEBAAAnAEwbtAEBAAB0WbQZGAIIISsAAAABAGAEMgF6BhQACQAQQA0DAgIASAAAAHQXAQgVKxI2NxcGBhUVIzVgXlJqMTC5BQnJQkhEi098eQAAAAABADAEFwFJBgAACQAftAMCAgBHS7AjUFi1AAAAJwBMG7MAAAB0WbMXAQgVKwAGByc2NjU1MxUBSV5RajAwuQUiyUJIQ4xQgoAAAAAAAQAk/uYBPQC3AAkAEEANAwICAEcAAAB0FwEIFSsEBgcnNjY1NTMVAT1eUWowL7oPyUJIQ4xPa2gAAAAAAQAPA4MBFgWRAAgAD0AMCAEARwAAAHQTAQcVKxM2NTUzBxQGBw9eqQFSRAO4fpjDtWO4PgAAAAACABsDgwJQBZEACAARABJADxEIAgBHAQEAAHQYEwIHFisTNjU1MxUUBgc3NjU1MwcUBgcbXqlTQ71eqQFSQwO4fJrDtWK4PzV8msO1Y7c/AAEAPAQFAckEmgADACaxBmREQBsAAAEBAFUAAAABXQIBAQABTQAAAAMAAxEDBxUrsQYARBM1IRU8AY0EBZWVAAAB/JT/OP70BbAACgBPtQYBAQQBSkuwEFBYQBkAAgEAAQJwAAAAggAEAAECBAFmAAMDJQNMG0AaAAIBAAECAH4AAACCAAQAAQIEAWYAAwMlA0xZtxESEREQBQgZKwUjESEXIyc3Mwch/vR1/vO/pfjupsABjMgFTqTe8LUAAfzR/zj/MQWwAAoASEuwEFBYQBkAAAECAQBwAAICggADAAEAAwFmAAQEJQRMG0AaAAABAgEAAn4AAgKCAAMAAQADAWYABAQlBExZtxERERERBQgZKwMHIzchESMRISczz/ilvv70dQGMwKYEwN6k+rIFw7UAAAAAAfys/zj+xAXcAA4AGkAXDg0MCwoJCAcGAwIBDABIAAAAdBQBCBUrARcHJxEjEQcnNyc3FzcX/g62V390dli2tle1tlYEy7lXgfr8BPx5V7m6V7m5VwAAAAH9fP84/fEE+wADAB9AHAIBAQAAAVUCAQEBAF0AAAEATQAAAAMAAxEDCBUrAREjEf3xdQT7+j0FwwAAAAEAYP/sBB0FxAAlAFVAUhUBBgUWAQQGAQELAQIBAAsESgcBBAgBAwIEA2UJAQIKAQELAgFlAAYGBV8ABQUtSwwBCwsAXwAAAC4ATAAAACUAJCIhIB8REiMjEREREyMNCB0rJDcXBiMiJiYnIzUzNSM1Mz4CMzIXByYjIgYHIRUhFSEVIRYWMwOhaBR1e5/wiQOysbGyBonunWuGFG5tnrUHAX/+gQF//oEEt6CHIp8ef/y3e4p8s/Z8H6AjxcR8invKzQABAGn/CwP6BSYAJwBGQEMXFAIEAgsIAgEFAkoAAwQABAMAfgAABQQABXwAAgAEAwIEZwYBBQEBBVcGAQUFAV0AAQUBTQAAACcAJiMVGxUTBwgZKyQ2NjczDgIHFSM1LgI1NTQ2Njc1MxUeAhcjLgIjIgYVFRQWMwKMcUcFsQRenF67fKpUVKp8u2WbWASxBUFwSJ2KiZ6CN2E8U5ZoEenqF5jghCqE4JgX4d4QbKRiRG5A4aUqpuAAAgBp/+UFWwTxACMAMwBGQEMfHRcVBAIBIBQOAgQDAg0LBQMEAAMDSh4WAgFIDAQCAEcAAQACAwECZwQBAwMAXwAAAC4ATCQkJDMkMiwqGxknBQgVKwAGBxcHJwYGIyImJwcnNyYmNTQ2Nyc3FzY2MzIWFzcXBxYWFQA2NjU0JiYjIgYGFRQWFjMFMjQyj4WITbxmZbtNhYSLMjY7NpSEkkuzYmK2S5SFlzU5/iDBcXHBc3PBcnLBcwH+tE2QiIo/REQ+iIeNTbZjaLxPloeVOT4+OpeImk67Zv42fNJ8fNF7e9F8fNJ8AAEAbv8wBBIGnAAwAD9APB0aAgUDBQICAAICSgAEBQEFBAF+AAECBQECfAADAAUEAwVnAAIAAAJXAAICAF0AAAIATSMUHSIVEwYIGiskBgcVIzUuAjUzFBYzMjY1NCYnJiY1NDY2NzUzFRYWFSM0JiYjIgYVFBYWFx4CFQQSzrSVbrJtu6J0hJSBmMnPV6Ful6m5ujlvTnl7NHxuh7BczswTv74MYsCRm4d/bVyAMz3MqW6oZg3b3BbvylqLT35sQl9MJSxvonMAAQBbAAAEaQXEACQAPkA7AAUGAwYFA34HAQMIAQIBAwJlAAYGBF8ABAQtSwoJAgEBAF0AAAAmAEwAAAAkACQRFCITJBEUERELCB0rJQchNTM2NjUnIzUzAzQ2NjMyFhYVIzQmIyIGBhUTIRUhFxYGBwRpAfv3TS8tCKWgCXPKgIC7Y8KHZUJrPgkBPv7IBwEgH5ycnAyXUt6cAQSHxmhhsHR2c0J/WP78nN1IgC4AAAABAA8AAAQlBbAAFgA5QDYUAQAJAUoIAQAHAQECAAFmBgECBQEDBAIDZQoBCQklSwAEBCYETBYVExIRERERERERERALCB0rASEVIRUhFSERIxEhNSE1ITUhATMBATMCtwEE/rwBRP68wv7DAT3+wwEG/pHaATIBM9cC4HymfP6+AUJ8pnwC0P1sApQAAAEAgwGTBO8DIgAcAGixBmRES7AUUFhAIQACBAAEAgB+BQEDAAEEAwFnAAQCAARXAAQEAF8AAAQATxtAKAAFAwEDBQF+AAIEAAQCAH4AAwABBAMBZwAEAgAEVwAEBABfAAAEAE9ZQAkTJCMSJCIGCBorsQYARAAGBiMiJicmJiMiBhUHNDY2MzIWFxYWMzI2NjUzBO9TlF1ThFM3VS9MVaJSlF1TiFA5Uy4yTiuZAqOrZUFKMzBsXwFnp19DSDUuNmE+AAADAEcArQQtBLoACwAPABsAQUA+BgEBAAACAQBnAAIHAQMFAgNlCAEFBAQFVwgBBQUEXwAEBQRPEBAMDAAAEBsQGhYUDA8MDw4NAAsACiQJCBUrABYVFAYjIiY1NDYzATUhFQQWFRQGIyImNTQ2MwJ3PT03Nzw8N/4HA+b+Sj09Nzc8PDcEuj8xLz49MDE//Z+3t9A+MS8+PTAxPgACAJgBkAPaA84AAwAHAC9ALAAABAEBAgABZQACAwMCVQACAgNdBQEDAgNNBAQAAAQHBAcGBQADAAMRBggVKxM1IRUBNSEVmANC/L4DQgMvn5/+YZ+fAAAAAQCHAMYD3QRLAAcABrMHAwEwKwEVFQE1AQE1A938qgKV/WsC0XMd/oW+AQcBA70AAQBIAMQDegRKAAcABrMHAwEwKwEBBRUBNTUBA3r9kAJw/M4DMgOI/v3+wwF7cx4BegAAAAEAfwF4A78DIAAGAEhLsApQWEAYAAABAQBvAwECAQECVQMBAgIBXQABAgFNG0AXAAABAIQDAQIBAQJVAwECAgFdAAECAU1ZQAsAAAAGAAYREgQIFisBFREjESE1A7+7/XsDIKD++AEIoAAAAAEAqAKMA+sDIgADAB5AGwAAAQEAVQAAAAFdAgEBAAFNAAAAAwADEQMIFSsTNSEVqANDAoyWlgAAAQBZAMwD3gRgAAsABrMGAAEwKyUBAScBATcBARcBAQNm/rb+tXgBS/63eAFJAUh4/rgBSswBUf6vegFRAU96/rIBTnr+sf6vAAAAAAUAaf/rBYMFxQARACAAJAA2AEUAPkA7IyICAgMkAQYHAkoAAgABBAIBZwAEAAcGBAdnAAMDAF8AAAAtSwAGBgVfAAUFLgVMJScnKyUmJyIICBwrEjY2MzIWFhUVFAYGIyImJjU1FhYzMjY1NTQmIyIGBhUVEwEXAQA2NjMyFhYVFRQGBiMiJiY1NR4CMzI2NTU0JiMiBhUVaUmHWluISUiIWluISYxTTUxSU00zRyRhAsdp/TkBb0mHWluISEiHWluISYskSTRMUlNNTFME64lRUIpTTVKJUFCJUk2YaGhLTUxpMlIxTfxmBHJC+44BSYlQUIlTTlOJUFCJU05+UjJoTE5MaGhMTgAAAAABAE4AkgQ0BLYACwAmQCMABAMBBFUFAQMCAQABAwBlAAQEAV0AAQQBTREREREREAYIGisBIREjESE1IREzESEENP5ru/5qAZa7AZUCX/4zAc2tAar+VgAAAgBhAAED9QTzAAsADwAzQDAIBQIDAgEAAQMAZQAEAAEGBAFlAAYGB10ABwcmB0wAAA8ODQwACwALEREREREJCBkrARUhESMRITUhETMRASEVIQP1/pap/n8Bgan9/QND/L0DVpb+YQGflgGd/mP9QZYAAAABALD+8gFGBbAAAwAZQBYCAQEAAYQAAAAlAEwAAAADAAMRAwgVKxMRMxGwlv7yBr75QgAAAgCT/vIBTgWwAAMABwAkQCEAAgADAgNhBAEBAQBdAAAAJQFMAAAHBgUEAAMAAxEFCBUrExEzEQczESOTu7u7uwK6Avb9CrH86QAAAAIAcv47BsgFlwBGAFYA80uwFlBYQBMcAQgCSwwCAwg4AQUAOQEGBQRKG0ATHAEIAksMAgkIOAEFADkBBgUESllLsBZQWEArAAIACAMCCGcABAQHXwoBBwclSwsJAgMDAF8BAQAALksABQUGXwAGBjIGTBtLsChQWEA1AAIACAkCCGcABAQHXwoBBwclSwsBCQkAXwEBAAAuSwADAwBfAQEAAC5LAAUFBl8ABgYyBkwbQDMKAQcABAIHBGcAAgAICQIIZwsBCQkAXwEBAAAuSwADAwBfAQEAAC5LAAUFBl8ABgYyBkxZWUAYR0cAAEdWR1VOTABGAEUlKCcoJyQoDAgbKwAEEhEUBw4CIyImJwYGIyImNTQ3NhI2MzIWFhcDBhUUFjMyNjY3NjUQACEiBAIHBhUUEgQzMjY3FwYGIyIkAhE0NxISJDMCNjc3EyYjIgYGBwYVFBYzBLsBWrMBBVe0iVx7GTSMS4OOBA9/u2dEZUkvMwJKN05xPgUC/sD+vc7+ybAKApYBHslZtD4mQs9k8v6iugEM2QF69i5wJQEuOEBJelANA1FNBZfR/nv+9ysWhu6YWFFSV8KoFzaqAQWNHCsk/dYSIWZOarhzNBcBXAF2zv6M9C4U5v6zsiokcSsu0gGHAQwpFQESAbH2+uNjZAgB+B1lyI8iIG93AAAAAwBl/+wE9AXEACIAMQA9AJ5LsBlQWEATKRsMAwIENTQhHAQFAgEBAAUDShtAEykbDAMCBDU0IRwEBQIBAQMFA0pZS7AZUFhAJQcBBAQBXwABAS1LAAICAF8GAwIAAC5LCAEFBQBfBgMCAAAuAEwbQCIHAQQEAV8AAQEtSwACAgNdBgEDAyZLCAEFBQBfAAAALgBMWUAYMjIjIwAAMj0yPCMxIzAAIgAiGi0jCQgXKyEnBgYjIiYmNTQ2NjcmJjU0NjYzMhYWFRQGBwcBNjUzFAcXAAYGFRQWFzc2NjU0JiYjEjY3AQcGBhUUFhYzBBRgSchmj9ZzRXheU1FapW5onlVkYm0BQ0Ooe9D9HFApPDpnRT4qTzY9ijv+nCleP0CAXXNCRWm6eFOHckJlmVBwn1JTjVNikklR/n2Bn/+n+gUuNVo2NHNJSi9ZRihJLPtUOzYBqh9GfzJJdkYAAQBDAAADQgWwAAwAGUAWAAAAAV0AAQElSwACAiYCTBEmIAMIFysBIyImJjU0NjYzIREjAoZXm95zc96bARO8Agh41IiH1Hn6UAAAAAMAW//rBeYFxAAPAB8APQBjsQZkREBYAAYHCQcGCX4ACQgHCQh8CgEBAAIFAQJnAAUABwYFB2cACAAEAwgEZwsBAwAAA1cLAQMDAGAAAAMAUBAQAAA9PDo4MzEvLiwqIyEQHxAeGBYADwAOJgwIFSuxBgBEAAQSFRQCBCMiJAI1NBIkMxIkEjU0AiQjIgQCFRQSBDMABiMiJiY1NTQ2NjMyFhUjNCYjIgYVFRQWMzI2NTMD4gFFv7/+u8HC/ru/vwFFwqEBEZ+f/vCiov7vn6ABEKIBPq+caZ1VVZ1pnLCSXV1gamlhXVySBcTF/qrQ0P6px8cBV9DQAVbF+p6nASGvrwEfpqb+4a+v/t+nAVmfYbByc3KwYZ+aYVmOdXR2jVdjAAAEAFr/6wXmBcQADwAfADcAQQBusQZkREBjNAEECCIBBQQCSiQBBQFJAAUEAwQFA34JAQEAAgYBAmcABgAHCAYHZwsBCAAEBQgEZQoBAwAAA1cKAQMDAF8AAAMATzg4EBAAADhBOEA/PTAuLSwrKRAfEB4YFgAPAA4mDAgVK7EGAEQABBIVFAIEIyIkAjU0EiQzEiQSNTQCJCMiBAIVFBIEMwEUFxUjJjU1NCYjIxEjESEyFhUUBxYWFQI2NTQmJiMjETMD4gFFv7/+usHC/ru/vwFFwqEBEZ+f/vCiov7vn6ABEKIBMRGSDURPo44BFZiqgUI65V0iT0SHnAXExf6q0ND+qcfHAVfQ0AFWxfqepwEhr68BH6am/uGvr/7fpwFqVCsQIm40SkT+rgNQgX57QRpqTAEOQDgzOhr+/wAAAAIAWv4RBHoFxQA9AE8AOUA2TEM9HgQBBAFKAAQFAQUEAX4AAQIFAQJ8AAIAAAIAYwAFBQNfAAMDLQVMLy0qKSYkIxMlBggXKyQWFRQGBiMiJiY1NxQWFjMyNjY1NCYmJy4CNTQ2NyYmNTQ2NjMyFhYVIzQmJiMiBgYVFBYWFx4CFRQGBwAWFhcWFzY2NTQmJicmJwYGFQQHRnjdk4vslLtimFZeiEc/lIyeyWxgV0VFed6Tmd12u0eKYGWIQjqRkqPHa2Fa/Vw7lJNnOk1UQZaNY0JNTmyKY3KlV1XBmQJkfzg4YT0/U0YqKmageFyNKTOKYW6lW2TCiU6ASjlgPEVUQSktZJ15XY0nAUZWRCkeFRRlRT9XSCoaGhJlSAAAAAIAgwPBAn0FxQAPABsAOLEGZERALQQBAQACAwECZwUBAwAAA1cFAQMDAF8AAAMATxAQAAAQGxAaFhQADwAOJgYIFSuxBgBEABYWFRQGBiMiJiY1NDY2MxI2NTQmIyIGFRQWMwHGc0REc0RFdUVFdkQ2SEg2NkxMNgXFR3dGRnZERHZGRndH/ntKNzlMTDk3SgAAAAABAEAC2QMVBbAABwAnsQZkREAcAQEAAQFKAAEAAYMDAgIAAHQAAAAHAAcREgQIFiuxBgBEAQMDIwEzMwECaL2+rQErcBABKgLZAeT+HALX/SkAAAADAGf/8ASSBJ0AIwAxADsAlEuwH1BYQBMmGwsDAgQ0MyIcBAUCAQEABQNKG0ATJhsLAwIENDMiHAQFAgEBAwUDSllLsB9QWEAiAAEABAIBBGcAAgIAXwYDAgAALksHAQUFAF8GAwIAAC4ATBtAHwABAAQCAQRnAAICA10GAQMDJksHAQUFAF8AAAAuAExZQBQyMgAAMjsyOi4sACMAIxouIggIFyshJwYjIiYmNTQ2NzcuAjU0NjYzMhYWFRQGBwcBNjUzFAYHFwAWFzc2NjU0JiMiBgYVADcBBwYGFRQWMwO+XZrfdK9eWl9IPkEpT5BeXY5OT0BWAQxDqkE+x/zkNz5AKStJQCg6HgEVaf7bVDUmb2pick2GU2CFRTNBTl04TXdCR3hGSH8vPf7jcp1txVHUA15NQC4dSSU1RSM4H/z/SwEzPCZOKExaAAEAQ/8TA94FcwAxAD9APB8cAgUDBgMCAAICSgAEBQEFBAF+AAECBQECfAADAAUEAwVnAAIAAAJXAAICAF0AAAIATSIVHiIVFAYIGiskBgYHFSM1LgI1MxQWMzI2NTQmJicmJjU0NjY3NTMVHgIVIzQmIyIGFRQWFhcWFhUD3lmkb5ZuunG8o4J6hC91a8/ZW6dvlnCjVruEgHh+On9sycrWhlIK4eELWZtqbmpaSzRIORozp4pViFYK2dsOZJVXXXBdTC5FOBszrosAAAEAQ//wA58EnQAkAFNAUBUBBgUWAQQGAgELAQMBAAsESgAFAAYEBQZnBwEECAEDAgQDZQkBAgoBAQsCAWUMAQsLAF8AAAAuAEwAAAAkACMiISAfERElIhERERIkDQgdKyQ2NxcGIyImJyM1MzUjNTM2NjMyFhcHJiYjIAMhFSEVIRUhEiEC8ls3G3F00fcVmpeXmxX2zztoRBUyYz3+/x4BmP5jAZ3+aB4BA4YOD5Qf2M55bXnN2w8Qkw8N/u55bXn+8AABADEAAAPwBJ0AJAA/QDwDAQgBSQAEBQIFBAJ+AAMABQQDBWcGAQIHAQEIAgFlCQEICABdAAAAJgBMAAAAJAAkERMiEyQRFxEKCBwrJQchNTM2NjUnJyM1MycmNjYzMhYWFSM0JiMiBhcXIRUhFxcUBwPwAfyDCi8rAQGjnwQEW651fq5Xu29ZXWYEBAGT/nABATOXl5cOp2YgI3l6iMVoYK51eXCTiHp5IyeuZgAAAAEADgAAA5MEjQAXADlANhUBAAkBSgoBCQAJgwgBAAcBAQIAAWYGAQIFAQMEAgNlAAQEJgRMFxYUExERERERERIREAsIHSsBMxUhBxUhFSEVIzUhNSE1ITUzATMTEzMCZ9T+8gMBEf7vuv7uARL+7tv+1cj7/MYCGXgIQ3je3nhLeAJ0/cICPgABAD8CWQQlBLYABwBBS7AkUFhADwIBAAABAAFiBAEDAxcDTBtAGAQBAwADgwIBAAEBAFUCAQAAAV4AAQABTllADAAAAAcABxEREQUHFysBESEVITUhEQKBAaT8GgGaBLb+P5ycAcEAAAH8pQTb/kgGAAADAB+xBmREQBQAAAEAgwIBAQF0AAAAAwADEQMIFSuxBgBEAQEzE/2o/v3iwQTbASX+2wAAAAAB/W0E2/8QBgAAAwAfsQZkREAUAgEBAAGDAAAAdAAAAAMAAxEDCBUrsQYARAMBIxPw/vOWwgYA/tsBJQAB/IkE2v9NBegAGwBZsQZkRLQbGgIDSEuwI1BYQBkABAEABFcAAwABAAMBZwAEBABfAgEABABPG0AdAAIAAoQABAEABFcAAwABAAMBZwAEBABfAAAEAE9ZtyQjEiQiBQgZK7EGAEQCBgYjIiYnJiYjIgYVJzQ2NjMyFhcWFjMyNjUXszljPC9AJSA0Iys5fTliPCY8KiY4Iio6fQWVbjwaGRYWQS8HR3A/GBkYF0EwDAAAAAABAHsE2wIeBgAAAwAfsQZkREAUAgEBAAGDAAAAdAAAAAMAAxEDCBUrsQYARAEBIxMCHv7zlsIGAP7bASUAAAAAAQB0/k0BqwAAABAALLEGZERAIQ4BAQIBSgACAQKDAAEAAAFXAAEBAGAAAAEAUBcRFQMIFyuxBgBEBRYWFRQGIycyNjU0JiYnNzMBEkNWnpIHSlwhRDwfhjULUlBgcWoxMiImEgWHAAABAKoE5QMIBgAACAApsQZkREAeAgEAAgFKAAIAAoMBAwIAAHQBAAcGBAMACAEIBAgUK7EGAEQBIycHIzUTMxMDCJqWlZn2cPgE5aqqCwEQ/u8AAAACAGQE8gLvBcYACwAXADSxBmREQCkFAwQDAQAAAVcFAwQDAQEAXwIBAAEATwwMAAAMFwwWEhAACwAKJAYIFSuxBgBEABYVFAYjIiY1NDYzBBYVFAYjIiY1NDYzAQY6OjQ0Ojo0AeI7OzQ0OTk0BcY8Li07Oy0uPAI8Li07Oy0uPAAAAAABADgE2wHbBgAAAwAfsQZkREAUAAABAIMCAQEBdAAAAAMAAxEDCBUrsQYARAEBMxMBO/794cIE2wEl/tsAAAAAAQCPBRgDLwWlAAMAJrEGZERAGwAAAQEAVQAAAAFdAgEBAAFNAAAAAwADEQMIFSuxBgBEEzUhFY8CoAUYjY0AAAIAeQS1AigGUAAPABwAOLEGZERALQQBAQACAwECZwUBAwAAA1cFAQMDAF8AAAMATxAQAAAQHBAbFhQADwAOJgYIFSuxBgBEABYWFRQGBiMiJiY1NDY2MxI2NTQmIyIGFRQWFjMBjWI5OWI8PWI5OWM8MkJCMjJCHjUhBlA3YDk5XTU1XTk5YDf+vUUuMEdHMB41IAABAHwE2gNABegAGwBZsQZkRLQbGgIDSEuwI1BYQBkABAEABFcAAwABAAMBZwAEBABfAgEABABPG0AdAAIAAoQABAEABFcAAwABAAMBZwAEBABfAAAEAE9ZtyQjEiQiBQgZK7EGAEQABgYjIiYnJiYjIgYVJzQ2NjMyFhcWFjMyNjUXA0A5YzwvQCYhMiQqOX04Yj0mOyokOiMqOn0FlW48GhkWFkEvB0dwPxgZFxhBMAwAAAACAgf93QKj/1YACwAXADexBmREQCwAAAQBAQIAAWcAAgMDAlcAAgIDXwUBAwIDTwwMAAAMFwwWEhAACwAKJAYHFSuxBgBEACY1NDYzMhYVFAYjBiY1NDYzMhYVFAYjAjQtLSEiLCwiIS0tISIsLCL+xCofHyopIB8q5yofHyopIB8qAAAFASP93QOJ/1YACwAXACMALwA7AF6xBmREQFMEAgIADAULAwoFAQYAAWcACAcJCFcABg0BBwkGB2cACAgJXw4BCQgJTzAwJCQYGAwMAAAwOzA6NjQkLyQuKigYIxgiHhwMFwwWEhAACwAKJA8HFSuxBgBEACY1NDYzMhYVFAYjMiY1NDYzMhYVFAYjMiY1NDYzMhYVFAYjBCY1NDYzMhYVFAYjBCY1NDYzMhYVFAYjAVAtLSEiLCwiwy0tISIsLCLFLS0iISwsIf6GLS0iIS0tIQE2LS0iISwsIf7EKh8fKikgHyoqHx8qKSAfKiofHyoqHx8qxCkfICorHx8pIyofHyoqHx8qAAADAS393QOA/1YACwAPABsAlLEGZERLsBNQWEAcAgEAAwYCAQQAAWcABAUFBFcABAQFXwcBBQQFTxtLsBVQWEAhAAMBAANVAgEABgEBBAABZwAEBQUEVwAEBAVfBwEFBAVPG0AiAAIAAwECA2UAAAYBAQQAAWcABAUFBFcABAQFXwcBBQQFT1lZQBYQEAAAEBsQGhYUDw4NDAALAAokCAcVK7EGAEQAJjU0NjMyFhUUBiMlIRUhACY1NDYzMhYVFAYjAxAtLSIhLS0h/fsBb/6RAeMtLSIhLS0h/sQqHx8qKh8fKnle/v4qHx8qKh8fKgADASz93QOA/1YACwAiAC4A4bEGZES1DgEFBgFKS7ATUFhAHwMBAAQCCAMBBgABZQAGBQUGVwAGBgVfCgcJAwUGBU8bS7AVUFhAJAQBAgEAAlUDAQAIAQEGAAFnAAYFBQZXAAYGBV8KBwkDBQYFTxtLsB5QWEAlAAMEAQIBAwJlAAAIAQEGAAFnAAYFBQZXAAYGBV8KBwkDBQYFTxtALAkBBQYHBgUHfgADBAECAQMCZQAACAEBBgABZwAGBQcGVwAGBgdfCgEHBgdPWVlZQB4jIwwMAAAjLiMtKScMIgwhGRgXFhUUAAsACiQLBxUrsQYARAAmNTQ2MzIWFRQGIwQmNTQ2NzY2NyM1IRUjFhYXFhYVFAYjBCY1NDYzMhYVFAYjAxAtLSIhLS0h/pUtBwgNDwOcAXOYAw8NCAcsIQEoLS0iIS0tIf7EKh8fKiofHyrWKx4NFBAYMyxeXiwzGBAUDR8qESofHyoqHx8qAAAAAAECB/7EAqP/VgALACaxBmREQBsAAAEBAFcAAAABXwIBAQABTwAAAAsACiQDBxUrsQYARAAmNTQ2MzIWFRQGIwI1Li0hIiwsIv7ELB0fKikgHyoAAAIBlf7EAxX/VgALABcAMrEGZERAJwIBAAEBAFcCAQAAAV8FAwQDAQABTwwMAAAMFwwWEhAACwAKJAYHFSuxBgBEACY1NDYzMhYVFAYjMiY1NDYzMhYVFAYjAcMuLSIhLS0hwi4uISIsLCL+xCwdHyoqHx8qLB0fKikgHyoAAAAAAwGV/gADFf9WAAsAFwAjAEKxBmREQDcCAQAHAwYDAQQAAWcABAUFBFcABAQFXwgBBQQFTxgYDAwAABgjGCIeHAwXDBYSEAALAAokCQcVK7EGAEQAJjU0NjMyFhUUBiMyJjU0NjMyFhUUBiMGJjU0NjMyFhUUBiMBwy4tIiEtLSHCLi4hIiwsIpMtLSEiLCwi/sQsHR8qKh8fKiwdHyopIB8qxCkfHysqIB8pAAEBi/7fAx//PQADACCxBmREQBUAAAEBAFUAAAABXQABAAFNERACBxYrsQYARAUhFSEBiwGU/mzDXgAAAAEBpP3NAwr/PgAWAFixBmREtQIBAwABSkuwClBYQBgEAQMAAANvAAEAAAFVAAEBAF0CAQABAE0bQBcEAQMAA4QAAQAAAVUAAQEAXQIBAAEATVlADAAAABYAFRERGAUHFyuxBgBEACY1NDY3NjY3IzUhFSMWFhcWFhUUBiMCOC4GBw0RA5QBZpEDEQ4HBi0i/c0sHQ0TEBpCPGBgO0IbEBMNHyoAAQGZ/ekDEv89ABYAWLEGZES1AgEDAAFKS7ALUFhAGAQBAwAAA28AAQAAAVUAAQEAXQIBAAEATRtAFwQBAwADhAABAAABVQABAQBdAgEAAQBNWUAMAAAAFgAVEREYBQcXK7EGAEQAJjU0Njc2NjcjNSEVIxYWFxYWFRQGIwI3LgcIDRADnwF5mwMQDQgHLSH96SwdDRQQGTUuXl4uNRkREg4fKgABALYFMwFUBcQACwAmsQZkREAbAAABAQBXAAAAAV8CAQEAAU8AAAALAAokAwcVK7EGAEQSJjU0NjMyFhUUBiPkLi0iIi0tIgUzKx0fKiofHykAAAABALYFMwFUBcQACwAmsQZkREAbAAABAQBXAAAAAV8CAQEAAU8AAAALAAokAwcVK7EGAEQSJjU0NjMyFhUUBiPkLi0iIi0tIgUzKx0fKiofHykAAAADAgj9UQOf/1YACwAXACMASLEGZERAPQAABgEBAgABZwACBwEDBAIDZwAEBQUEVwAEBAVfCAEFBAVPGBgMDAAAGCMYIh4cDBcMFhIQAAsACiQJBxUrsQYARAAmNTQ2MzIWFRQGIxYmNTQ2MzIWFRQGIxYmNTQ2MzIWFRQGIwI2Li0hIiwsIl0uLSIhLS0hXS4tISIsLCL+xCwdHyopIB8quSsdICorHx8puiwdHysqIB8qAAABAO4CDQGLAqAACwAmsQZkREAbAAABAQBXAAAAAV8CAQEAAU8AAAALAAokAwcVK7EGAEQAJjU0NjMyFhUUBiMBGy0tISItLSICDSweHyoqHyAqAAABAPAFTQLXBbgAAwAgsQZkREAVAAABAQBVAAAAAV0AAQABTREQAgcWK7EGAEQTIRUh8AHn/hkFuGsAAAABBOEFMwV+BcQACwAmsQZkREAbAAABAQBXAAAAAV8CAQEAAU8AAAALAAokAwcVK7EGAEQAJjU0NjMyFhUUBiMFDy4tIiEtLSEFMysdHyoqHx8pAAABAKAFMwE9BcQACwAmsQZkREAbAAABAQBXAAAAAV8CAQEAAU8AAAALAAokAwcVK7EGAEQSJjU0NjMyFhUUBiPNLS0hIi0tIgUzKx0fKiofHykAAAABAD3/7AScBhUAKQCWS7AZUFhAEh4BAwcLAQIDKQEKAQABAAoEShtAEx4BAwcLAQIDKQEKAQNKAAEEAUlZS7AZUFhAIgAHAAMCBwNnCQUCAQECXQgGAgICKEsACgoAXwQBAAAuAEwbQCYABwADAgcDZwkFAgEBAl0IBgICAihLAAQEJksACgoAXwAAAC4ATFlAECgmIyIUIxEREyQREyELCB0rIQYjIiY1ESM1MxE1JiYjIgYVESMRIzUzNTQ2MzIWFxURMxUjERQWMzI3BJxAT3GJxcUdcC5YXruqqrSgWd5cyck/NygvFIiZAqCNAQcZDxZvY/tTA62Nc6y8Ny1w/vmN/V9MNgsAAAAAAQBf/+wGVQYRAFgBOkuwLFBYQA5GAQECWAEIBwABAAgDShtADkYBBQJYAQgHAAEACANKWUuwFlBYQDQABAEHAQQHfgAHCAEHCHwAAwMKXwAKCidLDAUCAQECXQsJAgICKEsNAQgIAF8GAQAALgBMG0uwGVBYQDIABAEHAQQHfgAHCAEHCHwACgADAgoDZwwFAgEBAl0LCQICAihLDQEICABfBgEAAC4ATBtLsCxQWEA9AAQBBwEEB34ABwgBBwh8AAoAAwkKA2cMBQIBAQlfAAkJMEsMBQIBAQJdCwECAihLDQEICABfBgEAAC4ATBtAOgAEAQcBBAd+AAcIAQcIfAAKAAMJCgNnAAUFCV8ACQkwSwwBAQECXQsBAgIoSw0BCAgAXwYBAAAuAExZWVlAFldVU1JRUE1LRUMjEy4jKCMREyEOCB0rIQYjIiY1ESM1MzU0JiMiBhUUFhcWFhUjIzQmJiMiBgYVFBYWFx4CFRQGBiMiJiY1Mx4CMzI2NjU0JiYnJiY1NDY2MzIXJjU0NjYzMhYXFTMVIxEUMzI3BlVAT2+LvLxhZ1ZdFhYbG2hUNmdFSWAtKWtliKpTaL59islouwRMcT9JaTYmbGbBxWS3dmZPLFujadarAcnJdigvFJujAoONVmx7VkgyWkBPdksuVjctSSorOS4WH1R6V16RUWOiXEhbKClHLS1COBUokH9WkFYdblFRhEzinleN/XyfCwABAI8CjAMNAyIAAwAeQBsAAAEBAFUAAAABXQIBAQABTQAAAAMAAxEDCBUrEzUhFY8CfgKMlpYAAAEAAAAAAAAAAAAAAAeyAmQCRWBEMQAAAAEAAAACAEFMdo5CXw889QADCAAAAAAA0yFnYgAAAADTIWyq/In9UQdZCGIAAAAHAAIAAAAAAAAAAQAACGL8ogAAB6L8ifqCB1kAAQAAAAAAAAAAAAAAAAAAAc8FRwD7AAAAAAH8AAAFOgAcBToAHAU6ABwFOgAcBToAHAU6ABwFOgAcB3v/8AT8AKkFNQB3BTUAdwVAAKkFQAAGBIwAqQSMAKkEjACpBIwAqQSMAKkEbACpBXMAegW1AKkCLgC3Ai4AtwIu/+kCLv/UAi7/zQRqADUFBQCpBE8AqQb9AKkFtQCpBbUAqQWCAHYFggB2BYIAdgWCAHYFggB2BYIAdgWCAHYHogBpBQ0AqQS6AKYFggBtBO0AqQTAAFEFKwCPBMYAMgUwAIwFMACMBTAAjAUwAIwFMACMBRkAHQcZAD0FBQA6BM4ADwTOAA8EywBXBSQAsgVMALIFPgCyBYMAfgWUALIFrwB+BeEAfgXkAH4EWwBtBFsAbQRbAG0EWwBtBFsAbQRbAG0EWwBtBsIATgR+AIwEMQBcBDEAXASEAF8EsQB+BD8AXQQ/AF0EPwBdBD8AXQQ/AF0CyAA9BH4AYARoAIwB8gCOAfwAnAH8AJwB/P/PAfz/ugH8/7MB6/+/BA8AjQHyAJwHAwCLBGsAjARrAIwEkQBcBJEAXASRAFwEkQBcBJEAXASJAFwEkQBcB0MAYQR+AIwEnQCVBI0AXwK2AIwEIQBfBMIAiwH9AKACngAJBGkAiARpAIgEaQCIBGkAiARpAIgD4QAhBgMAKwP4ACkDyQAWA8kAFgPJABYD+ABZBDoAHgQVAJMEEQCTBH4AjAQzAF0ElABbBI0AWwSfAFoEjgCMBJ0AWwQ/AF0EfgBgBZEAPQbVAD0G1QA9BIwAPQRvAB8EhwAUBIcAFASHABQEhwAUBIcAFASHABQEhwAUBjoACQRRAIoEfABhBHwAYQSAAIoEgP+8A+YAigPmAIoD5gCKA+YAigPmAIoDywCKBKwAYwTkAIoB6QCXAekAlwHp/8YB6f+yAen/qwPPACsEVQCKA7UAigYCAIoE4wCKBOMAigS8AGAEvABgBLwAYAS8AGAEvABgBLwAYAS8AGAEXQCKA/kAigS8AFkESgCKBCAAQwRRAIoEJwApBHwAdAR8AHQEfAB0BHwAdAR8AHQEaQAUBhUAMQRVACcEKwAOBCsADgQjAEgDlACTA6QAewSGAGQExABkBLcAZATGAGQE1QA3BKsAUwUiAJgENwA9AxgAFAQrADwEtACUAlwAZALUAEwE2wC1BO8AtQJqADcD9gBBBE0AUQQcAFMFFQC0BRMAhAJNAFYC5gA8BRkAZwShAD0EvwCPBQQAjgRIADsEXwA7BPkAswPtADwGWgC1BP8ACQTVADcEwgA9BxQAmAVDADwGXwCUBhsAUQWJAFMG5AByBZsAPAbbAAkGWgC1BloAtQZaALUGWgC1BSIAmAUiAJgFIgCYBDcAPQMYABQEKwA8BLQAlAJc//kC1ABMBO8AtQJqAA0D9gBBBE0AUQQcAFMFEwCEAuYAPAUZAGcEvwCPBQQAjgRfADsE+QCzA+0APAZaALUE/wAJAlwAZAQ3AD0ETQBRBQQAjgMYABQD6gBKA+oASgPqAEoD6gBKBH8AcwR/AKsEfwBdBH8AXwR/ADUEfwCaBH8AhQR/AE4EfwBxBH8AZALwAFAC8AA2AvAAWwLwAFYC8AA7AvAATwLwAEkC8ABQAvAAewLwAEIC8AA/AvAANgLwAFsC8ABWAvAAOwLwAE8C8ABJBQAAAAPVAAAEggAABFgAAATPAAAEawAABH8AfgQuAAAEfwBxBLoAAAOjADwC8ABQAvAAewLwAEIC8AA/AvAANgLwAFsC8ABWAvAAOwLwAE8C8ABJAvAAUALwAHsC8ABCAvAAPwLwADYC8ABbAvAAVgLwADsC8ABPAvAASQQQAGMEEACYBBAATgQQAE4EEAAwBBAAgAQPAHIEEABCBBAAYQQQAFYDcgAcA0kAKQIYAJMCswCLAfIAhQGTABwFWwCTAhEAoQH1AIsE7wB4Ah0AkAPJAEsDygBEApIAiAFmAGcBsgApA04AEgOdAAQCtgBAArYAFAIhAJICIQAKAr4AhQLKACcGPwCRBUAAogI1ACUCWAAlA8EAZAPBAGcCZwBsAmcAWgLFACQC1wBpAt4APAGbAGABmwAwAZkAJAFXAA8CoAAbAgUAPAAA/JQAAPzRAAD8rAAA/XwEfwBgBGEAaQW0AGkEfwBuBKcAWwQ0AA8FcQCDBJIARwRkAJgELwCHBBEASARvAH8EkwCoBEUAWQXcAGkEigBOBEcAYQH1ALAB7QCTBy4AcgT6AGUD6gBDBkkAWwZJAFoE6QBaAv0AgwNYAEAE1gBnBCAAQwPlAEMENQAxA6EADgRoAD8AAPylAAD9bQAA/IkChAB7AfwAdAPGAKoDWABkAnoAOAOsAI8CrwB5A8gAfAAAAgcAAAEjAAABLQAAASwAAAIHAAABlQAAAZUAAAGLAAABpAAAAZkAAAC2AAAAtgAAAggAAADuAAAA8AAABOEAAACgBO8APQacAF8DmACPAAAAAAAAAYIBggGCAbQB5AIYAkwCfAK0AwgDWAOyBAIEcASwBQIFNAVmBZ4F1gYIBjQGiAa0Bs4G9gciB04HdAeoB9QH9AgmCFAIngjoCRIJQAluCZgKBApOCvgLOguCC9gMHAx2DPgNGg1KDXINoA3MDfQOHA5cDowOsg7aDwoPOg+MD7oQCBBIEKAQ7BFEEa4SChJqEqwTBhNKE7QURBTOFR4VjhYQFnYWxBcSF2QXnBfqGCgZPhmKGb4Z2BoSGlAafBq2Gv4bPhtiG8YcEByCHMgdCB1OHXwdvB4oHnQe9B9eH8ogRCCGIOQhfiGoIeAiKCKKIvAjQCOiI8okAiQwJG4ktiTsJRwmECZYJognFCdkJ+Yo/ClCKawqJip4K44ryCw0LJYs2i0oLVotii28LfIuIi5aLtAvHi9yL7wwJDBgMKww2jEIMToxbDGaMcQyEjI+MlgygDKsMtgy/jMwM1wzfDOuM9g0QDSENKw02jUINTA1qDYONk42kDbeNxw3cjfqOAw4PDhkOJA4vjjmORA5UDmAOaY5zjn+Opw66jt4O/I8Ejw2PFg8gjy2POg9Jj1KPYI9nj3CPeo+Sj5mPoQ+uj7kPww/Vj9yP6ZAJkBmQKRA/EEqQVhBmEG+QghCcEKaQtxDEEM4Q3JDrkPYRABEJkSQRMBE8EUsRWhFjEXQRfpGJkZWRn5GvEbiRwpHVkd8R6BH0Ef6SCpIWki0SOZJIklmSa5J0koCSlhKfEqmStRLDktMS3BLlEu4S9xMHEw+TIZM7E0mTX5N4E4CTnZO1k7yTxpPRk90T5JP0lAUUHJQkFDUUYBRuFJMUt5S/lOiU/hT+FP4U/hT+FP4U/hUJFQkVFBUUFRiVIBUmlTCVPBVFlVCVXBVjlW6VehWBlYiVkpWelaiVtBW/lccV0pXeFe0V9ZYHliAWLpZDllsWYxZ/FpaWopaplrMWvZbTltsW8JcKFxaXNBdFF24Xi5eZF6KXtZe8l8SX1ZfnF/EX+pgFGA+YFpgdmCSYK5g0mD2YRphPmFsYYhhrGHKYfBiDmIqYlJicmKwYuxjGGM2Y5hj9GRoZMxlJGVqZcxmGmZGZl5meGauZspm8Gd2Z6Jn3Gf2aBxpHGnKafBqgmsia7Jr/GwmbMxtMm2UbexuMG5kboRuom78bxxvUG96b7xv3G/8cEZwoHDicWZx3nKWcsBzAHNWc3RzxnQYdEJ0bHTGdPB1DnU4dWJ16Hb6dxZ3IQAAAAEAAAHQAFkACgBQAAUAAgBWAGgAiwAAAQgNXgAEAAEAAAAaAT4AAQAAAAAAAAApAAAAAQAAAAAAAQAFACkAAQAAAAAAAgAHAC4AAQAAAAAAAwAYADUAAQAAAAAABAAFAE0AAQAAAAAABQBiAFIAAQAAAAAABgANALQAAQAAAAAACAAKAMEAAQAAAAAACQAJAMsAAQAAAAAACwANANQAAQAAAAAADAAMAOEAAQAAAAAADQCqAO0AAQAAAAAADgAaAZcAAwABBAkAAABSAbEAAwABBAkAAQAKAgMAAwABBAkAAgAOAg0AAwABBAkAAwAwAhsAAwABBAkABAAaAksAAwABBAkABQDEAmUAAwABBAkABgAaAykAAwABBAkACAAUA0MAAwABBAkACQASA1cAAwABBAkACwAaA2kAAwABBAkADAAYA4MAAwABBAkADQFUA5sAAwABBAkADgA0BO9Db3B5cmlnaHQgMjAxNCBUaGUgSGVlYm8gUHJvamVjdCBBdXRob3JzLkhlZWJvUmVndWxhcjIuMDAxO1VLV047SGVlYm8tUmVndWxhckhlZWJvVmVyc2lvbiAyLjAwMTsgdHRmYXV0b2hpbnQgKHYxLjUuMTQtY2UwMikgLWwgOCAtciA1MCAtRyAyMDAgLXggMTQgLUQgaGViciAtZiBsYXRuIC13IEcgLVcgLWMgLVggIiJIZWViby1SZWd1bGFyTWVpciBTYWRhbk9kZWQgRXplcm1laXJzYWRhbi5jb21vZGVkZXplci5jb21UaGlzIEZvbnQgU29mdHdhcmUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIFNJTCBPcGVuIEZvbnQgTGljZW5zZSwgVmVyc2lvbiAxLjEuIFRoaXMgbGljZW5zZSBpcyBjb3BpZWQgYmVsb3csIGFuZCBpcyBhbHNvIGF2YWlsYWJsZSB3aXRoIGEgRkFRIGF0OiBodHRwOi8vc2NyaXB0cy5zaWwub3JnL09GTGh0dHA6Ly9zY3JpcHRzLnNpbC5vcmcvT0ZMAEMAbwBwAHkAcgBpAGcAaAB0ACAAMgAwADEANAAgAFQAaABlACAASABlAGUAYgBvACAAUAByAG8AagBlAGMAdAAgAEEAdQB0AGgAbwByAHMALgBIAGUAZQBiAG8AUgBlAGcAdQBsAGEAcgAyAC4AMAAwADEAOwBVAEsAVwBOADsASABlAGUAYgBvAC0AUgBlAGcAdQBsAGEAcgBIAGUAZQBiAG8ALQBSAGUAZwB1AGwAYQByAFYAZQByAHMAaQBvAG4AIAAyAC4AMAAwADEAOwAgAHQAdABmAGEAdQB0AG8AaABpAG4AdAAgACgAdgAxAC4ANQAuADEANAAtAGMAZQAwADIAKQAgAC0AbAAgADgAIAAtAHIAIAA1ADAAIAAtAEcAIAAyADAAMAAgAC0AeAAgADEANAAgAC0ARAAgAGgAZQBiAHIAIAAtAGYAIABsAGEAdABuACAALQB3ACAARwAgAC0AVwAgAC0AYwAgAC0AWAAgACIAIgBIAGUAZQBiAG8ALQBSAGUAZwB1AGwAYQByAE0AZQBpAHIAIABTAGEAZABhAG4ATwBkAGUAZAAgAEUAegBlAHIAbQBlAGkAcgBzAGEAZABhAG4ALgBjAG8AbQBvAGQAZQBkAGUAegBlAHIALgBjAG8AbQBUAGgAaQBzACAARgBvAG4AdAAgAFMAbwBmAHQAdwBhAHIAZQAgAGkAcwAgAGwAaQBjAGUAbgBzAGUAZAAgAHUAbgBkAGUAcgAgAHQAaABlACAAUwBJAEwAIABPAHAAZQBuACAARgBvAG4AdAAgAEwAaQBjAGUAbgBzAGUALAAgAFYAZQByAHMAaQBvAG4AIAAxAC4AMQAuACAAVABoAGkAcwAgAGwAaQBjAGUAbgBzAGUAIABpAHMAIABjAG8AcABpAGUAZAAgAGIAZQBsAG8AdwAsACAAYQBuAGQAIABpAHMAIABhAGwAcwBvACAAYQB2AGEAaQBsAGEAYgBsAGUAIAB3AGkAdABoACAAYQAgAEYAQQBRACAAYQB0ADoAIABoAHQAdABwADoALwAvAHMAYwByAGkAcAB0AHMALgBzAGkAbAAuAG8AcgBnAC8ATwBGAEwAaAB0AHQAcAA6AC8ALwBzAGMAcgBpAHAAdABzAC4AcwBpAGwALgBvAHIAZwAvAE8ARgBMAAAAAAIAAAAAAAD/nABkAAAAAAAAAAAAAAAAAAAAAAAAAAAB0AAAAAEAAwAkAMkAxwBiAK0AYwCuAJAAJQAmAGQAJwDpACgAZQDIAMoAywApACoAKwAsAMwAzQDOAM8ALQAuAC8AMAAxAGYAMgDQANEAZwDTAJEArwCwADMA7QA0ADUANgECADcAOADUANUAaADWADkAOgA7ADwA6wA9AQMBBAEFAQYBBwEIAQkBCgBEAGkAawBsAGoAbgBtAKAARQBGAG8ARwDqAEgAcAByAHMAcQBJAEoASwBMANcAdAB2AHcAdQBNAE4ATwBQAFEAeABSAHkAewB8AHoAoQB9ALEAUwDuAFQAVQBWAIkBCwBXAFgAfgCAAIEAfwBZAFoAWwBcAOwAugBdAQwBDQEOAQ8BEAERARIBEwEUARUBFgEXARgBGQEaARsAwAEcAR0BHgEfASABIQEiASMBJAElASYBJwEoASkBKgErASwBLQEuAS8BMAExATIBMwE0ATUBNgE3ATgBOQE6ATsBPAE9AT4BPwFAAUEBQgFDAUQBRQFGAUcBSAFJAUoBSwFMAU0BTgFPAVABUQFSAVMBVACdAJ4BVQFWAVcBWAFZAVoBWwFcAV0BXgFfAWABYQFiAWMBZAFlAWYBZwFoAWkBagFrAWwBbQFuAW8BcAFxAXIBcwF0AXUBdgF3AXgBeQF6AXsBfAF9AX4BfwGAAYEBggGDAYQBhQGGAYcBiAGJAYoBiwGMAY0BjgGPAZABkQGSAZMBlAGVAZYBlwGYAZkBmgGbAZwBnQGeAZ8BoAGhAaIBowGkABMAFAAVABYAFwAYABkAGgAbABwBpQGmAacBqAGpAaoBqwGsAa0BrgGvAbABsQGyAbMBtAG1AbYBtwG4AbkBugG7AbwBvQG+Ab8AvAHAAcEBwgHDAcQBxQHGAccByAHJAcoBywHMAc0BzgHPAdAB0QHSAdMB1AHVAdYB1wHYAdkB2gHbAdwB3QANAD8AwwCHAB0ADwCrAAQAowAGABEAIgCiAAUACgAeABIAQgBeAGAAPgBAAAsADACzALIAEAHeAKkAqgC+AL8AxQC0ALUAtgC3AMQB3wHgAeEB4gHjAeQB5QHmAIQAvQAHAIUAlgBhALgAIAAhAB8ApADvAPAACAAOAJMAXwDoACMACQCIAIsAigCGAIMAQQHnAegB6QHqAesB7AHtAe4B7wCNAN4A2ACOAEMA2gDdANkB8AHxAfIB8wH0AfUB9gH3AfgB+QH6AfsB/AH9Af4B/wIAAgECAgIDAgQHdW5pMUU5RQVLLmFsdAVSLmFsdAZLLmFsdDIGQy5zczA2BkQuc3MwNgZHLnNzMDYGTy5zczA2BlEuc3MwNgVsb25ncwVnLmFsdAVrLmFsdAZrLmFsdDIGYi5zczA2BmMuc3MwNgZkLnNzMDYGZy5zczA2Bm8uc3MwNgZwLnNzMDYGcS5zczA2BmUuc3MwNwZnLnNzMDcDZl9mBWZfZl9pBWZfZl9sA2ZfbAZBLnNtY3ALQWFjdXRlLnNtY3AQQWNpcmN1bWZsZXguc21jcA5BZGllcmVzaXMuc21jcAtBZ3JhdmUuc21jcApBcmluZy5zbWNwC0F0aWxkZS5zbWNwB0FFLnNtY3AGQi5zbWNwBkMuc21jcA1DY2VkaWxsYS5zbWNwBkQuc21jcAhFdGguc21jcAZFLnNtY3ALRWFjdXRlLnNtY3AQRWNpcmN1bWZsZXguc21jcA5FZGllcmVzaXMuc21jcAtFZ3JhdmUuc21jcAZGLnNtY3AGRy5zbWNwBkguc21jcAZJLnNtY3ALSWFjdXRlLnNtY3AQSWNpcmN1bWZsZXguc21jcA5JZGllcmVzaXMuc21jcAtJZ3JhdmUuc21jcAZKLnNtY3AGSy5zbWNwBkwuc21jcAZNLnNtY3AGTi5zbWNwC050aWxkZS5zbWNwBk8uc21jcAtPYWN1dGUuc21jcBBPY2lyY3VtZmxleC5zbWNwDk9kaWVyZXNpcy5zbWNwC09ncmF2ZS5zbWNwC09zbGFzaC5zbWNwC090aWxkZS5zbWNwBlAuc21jcApUaG9ybi5zbWNwBlEuc21jcAZSLnNtY3AGUy5zbWNwDHVuaTFFOUUuc21jcAZULnNtY3AGVS5zbWNwC1VhY3V0ZS5zbWNwEFVjaXJjdW1mbGV4LnNtY3AOVWRpZXJlc2lzLnNtY3ALVWdyYXZlLnNtY3AGVi5zbWNwBlcuc21jcAZYLnNtY3AGWS5zbWNwC1lhY3V0ZS5zbWNwBlouc21jcAVhbHBoYQlhbHBoYS5hbHQHdW5pMDVGMAd1bmkwNUYxB3VuaTA1RjIHdW5pRkI0Rgd1bmkwNUQwB3VuaTA1RDEHdW5pMDVEMgd1bmkwNUQzB3VuaTA1RDQHdW5pMDVENQd1bmkwNUQ2B3VuaTA1RDcHdW5pMDVEOAd1bmkwNUQ5B3VuaTA1REEHdW5pMDVEQgd1bmkwNURDB3VuaTA1REQHdW5pMDVERQd1bmkwNURGB3VuaTA1RTAHdW5pMDVFMQd1bmkwNUUyB3VuaTA1RTMHdW5pMDVFNAd1bmkwNUU1B3VuaTA1RTYHdW5pMDVFNwd1bmkwNUU4B3VuaTA1RTkHdW5pMDVFQQd1bmlGQjFGB3VuaUZCMjAHdW5pRkIyMQd1bmlGQjIyB3VuaUZCMjMHdW5pRkIyNAd1bmlGQjI1B3VuaUZCMjYHdW5pRkIyNwd1bmlGQjI4B3VuaUZCMkEHdW5pRkIyQgd1bmlGQjJDB3VuaUZCMkQHdW5pRkIyRQd1bmlGQjJGB3VuaUZCMzAHdW5pRkIzMQd1bmlGQjMyB3VuaUZCMzMHdW5pRkIzNAd1bmlGQjM1B3VuaUZCMzYHdW5pRkIzOAd1bmlGQjM5B3VuaUZCM0EHdW5pRkIzQgd1bmlGQjNDB3VuaUZCM0UHdW5pRkI0MAd1bmlGQjQxB3VuaUZCNDMHdW5pRkI0NAd1bmlGQjQ2B3VuaUZCNDcHdW5pRkI0OAd1bmlGQjQ5B3VuaUZCNEEHdW5pRkI0Qgd1bmlGQjRDB3VuaUZCNEQHdW5pRkI0RQt1bmkwNUQyLjAwMQt1bmkwNUU4LjAwMQt1bmkwNUU4LjAwMgt1bmkwNUU4LjAwMwt1bmkwNUU4LjAwNAl6ZXJvLmZyYWMJZm91ci5mcmFjCWZpdmUuZnJhYwhzaXguZnJhYwpzZXZlbi5mcmFjCmVpZ2h0LmZyYWMJbmluZS5mcmFjCHplcm8uc3VwB29uZS5zdXAHdHdvLnN1cAl0aHJlZS5zdXAIZm91ci5zdXAIZml2ZS5zdXAHc2l4LnN1cAlzZXZlbi5zdXAJZWlnaHQuc3VwCG5pbmUuc3VwCXplcm8udG51bQhvbmUudG51bQh0d28udG51bQp0aHJlZS50bnVtCWZvdXIudG51bQlmaXZlLnRudW0Ic2l4LnRudW0Kc2V2ZW4udG51bQplaWdodC50bnVtCW5pbmUudG51bQd1bmkyMDgwB3VuaTIwODEHdW5pMjA4Mgd1bmkyMDgzB3VuaTIwODQHdW5pMjA4NQd1bmkyMDg2B3VuaTIwODcHdW5pMjA4OAd1bmkyMDg5B3VuaTIwNzAHdW5pMDBCOQd1bmkwMEIyB3VuaTAwQjMHdW5pMjA3NAd1bmkyMDc1B3VuaTIwNzYHdW5pMjA3Nwd1bmkyMDc4B3VuaTIwNzkJemVyby5zbWNwCG9uZS5zbWNwCHR3by5zbWNwCnRocmVlLnNtY3AJZm91ci5zbWNwCWZpdmUuc21jcAhzaXguc21jcApzZXZlbi5zbWNwCmVpZ2h0LnNtY3AJbmluZS5zbWNwB3VuaTAwQUQHdW5pMDVGMwd1bmkwNUY0B3VuaTA1QkUHdW5pMjAwRgd1bmkyMDBFB3VuaTIwMEQHdW5pMjAwQwRFdXJvDmFtcGVyc2FuZC5zbWNwC2RvbGxhci5zbWNwCUV1cm8uc21jcA1zdGVybGluZy5zbWNwCHllbi5zbWNwB3VuaUZCMjkJZ3JhdmVjb21iCWFjdXRlY29tYgl0aWxkZWNvbWIHdW5pMDVCMAd1bmkwNUIxB3VuaTA1QjIHdW5pMDVCMwd1bmkwNUI0B3VuaTA1QjUHdW5pMDVCNgd1bmkwNUI3B3VuaTA1QzcHdW5pMDVCOAd1bmkwNUI5B3VuaTA1QkEHdW5pMDVCQgd1bmkwNUJDB3VuaTA1QkYHdW5pMDVDMQd1bmkwNUMyBmxvbmdzdAJzdAhjcm9zc2JhcgwudHRmYXV0b2hpbnQAAQAB//8ADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALkAuQCVAJUEmgAA/mAIYvyiBLD/6v5gCGL8ogC7ALsAlgCWBbAAAAXiBDoAAP5gCGL8ogXE/+wF4gRO/+z+Swhi/KIAngCeAH8AfwGm/pEIYvyiAbH+hghi/KIAngCeAH8AfwWwApsIYvyiBbsCkAhi/KIAALAALCCwAFVYRVkgIEu4AA5RS7AGU1pYsDQbsChZYGYgilVYsAIlYbkIAAgAY2MjYhshIbAAWbAAQyNEsgABAENgQi2wASywIGBmLbACLCBkILDAULAEJlqyKAELQ0VjRbAGRVghsAMlWVJbWCEjIRuKWCCwUFBYIbBAWRsgsDhQWCGwOFlZILEBC0NFY0VhZLAoUFghsQELQ0VjRSCwMFBYIbAwWRsgsMBQWCBmIIqKYSCwClBYYBsgsCBQWCGwCmAbILA2UFghsDZgG2BZWVkbsAIlsApDY7AAUliwAEuwClBYIbAKQxtLsB5QWCGwHkthuBAAY7AKQ2O4BQBiWVlkYVmwAStZWSOwAFBYZVlZLbADLCBFILAEJWFkILAFQ1BYsAUjQrAGI0IbISFZsAFgLbAELCMhIyEgZLEFYkIgsAYjQrAGRVgbsQELQ0VjsQELQ7AEYEVjsAMqISCwBkMgiiCKsAErsTAFJbAEJlFYYFAbYVJZWCNZIVkgsEBTWLABKxshsEBZI7AAUFhlWS2wBSywB0MrsgACAENgQi2wBiywByNCIyCwACNCYbACYmawAWOwAWCwBSotsAcsICBFILAMQ2O4BABiILAAUFiwQGBZZrABY2BEsAFgLbAILLIHDABDRUIqIbIAAQBDYEItsAkssABDI0SyAAEAQ2BCLbAKLCAgRSCwASsjsABDsAQlYCBFiiNhIGQgsCBQWCGwABuwMFBYsCAbsEBZWSOwAFBYZVmwAyUjYUREsAFgLbALLCAgRSCwASsjsABDsAQlYCBFiiNhIGSwJFBYsAAbsEBZI7AAUFhlWbADJSNhRESwAWAtsAwsILAAI0KyCwoDRVghGyMhWSohLbANLLECAkWwZGFELbAOLLABYCAgsA1DSrAAUFggsA0jQlmwDkNKsABSWCCwDiNCWS2wDywgsBBiZrABYyC4BABjiiNhsA9DYCCKYCCwDyNCIy2wECxLVFixBGREWSSwDWUjeC2wESxLUVhLU1ixBGREWRshWSSwE2UjeC2wEiyxABBDVVixEBBDsAFhQrAPK1mwAEOwAiVCsQ0CJUKxDgIlQrABFiMgsAMlUFixAQBDYLAEJUKKiiCKI2GwDiohI7ABYSCKI2GwDiohG7EBAENgsAIlQrACJWGwDiohWbANQ0ewDkNHYLACYiCwAFBYsEBgWWawAWMgsAxDY7gEAGIgsABQWLBAYFlmsAFjYLEAABMjRLABQ7AAPrIBAQFDYEItsBMsALEAAkVUWLAQI0IgRbAMI0KwCyOwBGBCIGCwAWG1EhIBAA8AQkKKYLESBiuwiSsbIlktsBQssQATKy2wFSyxARMrLbAWLLECEystsBcssQMTKy2wGCyxBBMrLbAZLLEFEystsBossQYTKy2wGyyxBxMrLbAcLLEIEystsB0ssQkTKy2wKSwjILAQYmawAWOwBmBLVFgjIC6wAV0bISFZLbAqLCMgsBBiZrABY7AWYEtUWCMgLrABcRshIVktsCssIyCwEGJmsAFjsCZgS1RYIyAusAFyGyEhWS2wHiwAsA0rsQACRVRYsBAjQiBFsAwjQrALI7AEYEIgYLABYbUSEgEADwBCQopgsRIGK7CJKxsiWS2wHyyxAB4rLbAgLLEBHistsCEssQIeKy2wIiyxAx4rLbAjLLEEHistsCQssQUeKy2wJSyxBh4rLbAmLLEHHistsCcssQgeKy2wKCyxCR4rLbAsLCA8sAFgLbAtLCBgsBJgIEMjsAFgQ7ACJWGwAWCwLCohLbAuLLAtK7AtKi2wLywgIEcgILAMQ2O4BABiILAAUFiwQGBZZrABY2AjYTgjIIpVWCBHICCwDENjuAQAYiCwAFBYsEBgWWawAWNgI2E4GyFZLbAwLACxAAJFVFiwARawLyqxBQEVRVgwWRsiWS2wMSwAsA0rsQACRVRYsAEWsC8qsQUBFUVYMFkbIlktsDIsIDWwAWAtsDMsALABRWO4BABiILAAUFiwQGBZZrABY7ABK7AMQ2O4BABiILAAUFiwQGBZZrABY7ABK7AAFrQAAAAAAEQ+IzixMgEVKiEtsDQsIDwgRyCwDENjuAQAYiCwAFBYsEBgWWawAWNgsABDYTgtsDUsLhc8LbA2LCA8IEcgsAxDY7gEAGIgsABQWLBAYFlmsAFjYLAAQ2GwAUNjOC2wNyyxAgAWJSAuIEewACNCsAIlSYqKRyNHI2EgWGIbIVmwASNCsjYBARUUKi2wOCywABawESNCsAQlsAQlRyNHI2GxCgBCsAlDK2WKLiMgIDyKOC2wOSywABawESNCsAQlsAQlIC5HI0cjYSCwBCNCsQoAQrAJQysgsGBQWCCwQFFYswIgAyAbswImAxpZQkIjILAIQyCKI0cjRyNhI0ZgsARDsAJiILAAUFiwQGBZZrABY2AgsAErIIqKYSCwAkNgZCOwA0NhZFBYsAJDYRuwA0NgWbADJbACYiCwAFBYsEBgWWawAWNhIyAgsAQmI0ZhOBsjsAhDRrACJbAIQ0cjRyNhYCCwBEOwAmIgsABQWLBAYFlmsAFjYCMgsAErI7AEQ2CwASuwBSVhsAUlsAJiILAAUFiwQGBZZrABY7AEJmEgsAQlYGQjsAMlYGRQWCEbIyFZIyAgsAQmI0ZhOFktsDossAAWsBEjQiAgILAFJiAuRyNHI2EjPDgtsDsssAAWsBEjQiCwCCNCICAgRiNHsAErI2E4LbA8LLAAFrARI0KwAyWwAiVHI0cjYbAAVFguIDwjIRuwAiWwAiVHI0cjYSCwBSWwBCVHI0cjYbAGJbAFJUmwAiVhuQgACABjYyMgWGIbIVljuAQAYiCwAFBYsEBgWWawAWNgIy4jICA8ijgjIVktsD0ssAAWsBEjQiCwCEMgLkcjRyNhIGCwIGBmsAJiILAAUFiwQGBZZrABYyMgIDyKOC2wPiwjIC5GsAIlRrARQ1hQG1JZWCA8WS6xLgEUKy2wPywjIC5GsAIlRrARQ1hSG1BZWCA8WS6xLgEUKy2wQCwjIC5GsAIlRrARQ1hQG1JZWCA8WSMgLkawAiVGsBFDWFIbUFlYIDxZLrEuARQrLbBBLLA4KyMgLkawAiVGsBFDWFAbUllYIDxZLrEuARQrLbBCLLA5K4ogIDywBCNCijgjIC5GsAIlRrARQ1hQG1JZWCA8WS6xLgEUK7AEQy6wListsEMssAAWsAQlsAQmICAgRiNHYbAKI0IuRyNHI2GwCUMrIyA8IC4jOLEuARQrLbBELLEIBCVCsAAWsAQlsAQlIC5HI0cjYSCwBCNCsQoAQrAJQysgsGBQWCCwQFFYswIgAyAbswImAxpZQkIjIEewBEOwAmIgsABQWLBAYFlmsAFjYCCwASsgiophILACQ2BkI7ADQ2FkUFiwAkNhG7ADQ2BZsAMlsAJiILAAUFiwQGBZZrABY2GwAiVGYTgjIDwjOBshICBGI0ewASsjYTghWbEuARQrLbBFLLEAOCsusS4BFCstsEYssQA5KyEjICA8sAQjQiM4sS4BFCuwBEMusC4rLbBHLLAAFSBHsAAjQrIAAQEVFBMusDQqLbBILLAAFSBHsAAjQrIAAQEVFBMusDQqLbBJLLEAARQTsDUqLbBKLLA3Ki2wSyywABZFIyAuIEaKI2E4sS4BFCstsEwssAgjQrBLKy2wTSyyAABEKy2wTiyyAAFEKy2wTyyyAQBEKy2wUCyyAQFEKy2wUSyyAABFKy2wUiyyAAFFKy2wUyyyAQBFKy2wVCyyAQFFKy2wVSyzAAAAQSstsFYsswABAEErLbBXLLMBAABBKy2wWCyzAQEAQSstsFksswAAAUErLbBaLLMAAQFBKy2wWyyzAQABQSstsFwsswEBAUErLbBdLLIAAEMrLbBeLLIAAUMrLbBfLLIBAEMrLbBgLLIBAUMrLbBhLLIAAEYrLbBiLLIAAUYrLbBjLLIBAEYrLbBkLLIBAUYrLbBlLLMAAABCKy2wZiyzAAEAQistsGcsswEAAEIrLbBoLLMBAQBCKy2waSyzAAABQistsGosswABAUIrLbBrLLMBAAFCKy2wbCyzAQEBQistsG0ssQA6Ky6xLgEUKy2wbiyxADorsD4rLbBvLLEAOiuwPystsHAssAAWsQA6K7BAKy2wcSyxATorsD4rLbByLLEBOiuwPystsHMssAAWsQE6K7BAKy2wdCyxADsrLrEuARQrLbB1LLEAOyuwPistsHYssQA7K7A/Ky2wdyyxADsrsEArLbB4LLEBOyuwPistsHkssQE7K7A/Ky2weiyxATsrsEArLbB7LLEAPCsusS4BFCstsHwssQA8K7A+Ky2wfSyxADwrsD8rLbB+LLEAPCuwQCstsH8ssQE8K7A+Ky2wgCyxATwrsD8rLbCBLLEBPCuwQCstsIIssQA9Ky6xLgEUKy2wgyyxAD0rsD4rLbCELLEAPSuwPystsIUssQA9K7BAKy2whiyxAT0rsD4rLbCHLLEBPSuwPystsIgssQE9K7BAKy2wiSyzCQQCA0VYIRsjIVlCK7AIZbADJFB4sQUBFUVYMFktAAAAS7gAyFJYsQEBjlmwAbkIAAgAY3CxAAdCtQAAMAAEACqxAAdCQApDBDcEIwgVBQQIKrEAB0JACkkCPQItBhwDBAgqsQALQr0RAA4ACQAFgAAEAAkqsQAPQr0AQABAAEAAQAAEAAkqsQMARLEkAYhRWLBAiFixA2REsSYBiFFYugiAAAEEQIhjVFixAwBEWVlZWUAKRQQ5BCUIFwUEDCq4Af+FsASNsQIARLMFZAYAREQAAAAAAAABAAAAAA==");

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(5);
            var content = __webpack_require__(48);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".response-card-button-stack {\r\n    display: flex;\r\n    flex-direction: column;\r\n    background-color: #FFF;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n\r\n.response-card-button-stack.yesno {\r\n    flex-direction: row;\r\n}\r\n\r\n.chat-button {\r\n    flex: 1;\r\n    background: #393c3f;\r\n    color: #FFF;\r\n    border: solid 1px #FFF;\r\n    font-family: Heebo, Arial, Helvetica, sans-serif;\r\n    padding: 5px;\r\n    width: 100%;\r\n}\r\n\r\n.chat-button:hover {\r\n    background: #D14210;\r\n    color: #FFF;\r\n    cursor: pointer;\r\n    cursor: hand;\r\n}\r\n\r\n.chat-button:focus {\r\n    background: #D14210;\r\n    color: #FFF;\r\n}\r\n\r\n.chat-bot-container .chat-button:hover, .chat-bot-container .chat-button:focus, .chat-bot-container .chat-button:active {\r\n  outline: 0;\r\n}", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(5);
            var content = __webpack_require__(50);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".response-carousel {\r\n    display: flex;\r\n    flex-direction: column;\r\n    margin-left: 10px;\r\n    grid-column: 2;\r\n    -ms-grid-column: 2;\r\n    grid-row: 2;\r\n    -ms-grid-row: 2;\r\n    margin-top: 10px;\r\n}\r\n\r\n.response-carousel .carousel-top {\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex: 10;\r\n}\r\n\r\n.response-carousel .carousel-indicators {\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex: 1;\r\n    justify-content: center;\r\n    margin-top: 5px;\r\n}\r\n\r\n\r\n\r\n.response-carousel .carousel-indicators .page-indicator {\r\n    height: 10px;\r\n    width: 10px;\r\n    background-color: #bbb;\r\n    border-radius: 50%;\r\n    margin: 5px;\r\n}\r\n\r\n.response-carousel .carousel-indicators .page-indicator:hover {\r\n    cursor: pointer;\r\n    cursor: hand;\r\n}\r\n\r\n.response-carousel .carousel-indicators .page-indicator.active {\r\n    height: 10px;\r\n    width: 10px;\r\n    background-color: black;\r\n    border-radius: 50%;\r\n}\r\n\r\n.response-carousel .carousel-top .previous-button {\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    flex: 1;\r\n}\r\n\r\n.response-carousel .carousel-top .next-button:hover, .response-carousel .carousel-top .previous-button:hover {\r\n    cursor: pointer;\r\n    cursor: hand;\r\n}\r\n\r\n.response-carousel .carousel-top .next-button {\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    flex: 1;\r\n}\r\n\r\n.response-carousel .carousel-top .page-container {\r\n    flex: 5;\r\n    display: flex;\r\n    flex-direction: row;\r\n    overflow-x: hidden;\r\n    display: grid;\r\n    grid-template-columns: 100%;\r\n    grid-template-rows: 100%;\r\n}\r\n\r\n.response-carousel .carousel-top .page-container .response-carousel-page {\r\n    visibility: hidden;\r\n    width: 100%;\r\n    -ms-grid-row: 1;\r\n    grid-row: 1;\r\n    -ms-grid-column: 1;\r\n    grid-column: 1;\r\n}\r\n\r\n.response-carousel .carousel-top .page-container .response-carousel-page.active {\r\n    visibility: visible;\r\n}\r\n\r\n.response-carousel .carousel-control-prev-icon {\r\n    background-image: url(\"data:image/svg+xml;utf-8,<svg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 24 24' fill='%23fff' stroke='%23000' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-left'><polyline points='15 18 9 12 15 6'></polyline></svg>\");\r\n}\r\n\r\n.response-carousel .carousel-control-next-icon {\r\n    background-image: url(\"data:image/svg+xml;utf-8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='%23fff' stroke='%23000' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-right'><polyline points='9 18 15 12 9 6'></polyline></svg>\");\r\n}", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(5);
            var content = __webpack_require__(52);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(24);
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "@font-face {\r\n    font-family: 'IcoMoon';\r\n    src: local('IcoMoon'), url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") format('truetype');\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-header {\r\n    margin-left: 10px;\r\n    align-self: center;\r\n    flex: 2;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-name {\r\n    font-weight: bold;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .icon-item {\r\n    font-family: IcoMoon;\r\n    justify-self: end;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-summary {\r\n    display: flex;\r\n    flex-direction: row;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-photo-container .minister-photo {\r\n    width: 100%;\r\n    height: auto;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-photo-container {\r\n    flex: 1;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-details-row {\r\n    display: flex;\r\n    flex-direction: row;\r\n}\r\n\r\n.chat-bot-container .minister-contact-card-container .minister-details-row .minister-details-item {\r\n    margin-left: 10px;\r\n}\r\n\r\n", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(5);
            var content = __webpack_require__(54);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(24);
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "@font-face {\r\n    font-family: 'IcoMoon';\r\n    src: local('IcoMoon'), url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") format('truetype');\r\n}\r\n\r\n.chat-bot-container .agency-contact-card-container {\r\n    display: grid;\r\n    grid-template-columns: auto auto;\r\n    grid-template-rows: auto auto auto auto auto auto auto auto auto;\r\n    grid-column-gap: 10px;\r\n}\r\n\r\n.chat-bot-container .agency-contact-card-container .agency-header {\r\n    font-weight: bold;\r\n}\r\n\r\n.chat-bot-container .agency-contact-card-container .icon-item {\r\n    font-family: IcoMoon;\r\n    justify-self: end;\r\n}", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(56);
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(8)))

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(8), __webpack_require__(7)))

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);

function QueryParamSerializer() {
}

QueryParamSerializer.prototype.serialize = function(params, shape, fn) {
  serializeStructure('', params, shape, fn);
};

function ucfirst(shape) {
  if (shape.isQueryName || shape.api.protocol !== 'ec2') {
    return shape.name;
  } else {
    return shape.name[0].toUpperCase() + shape.name.substr(1);
  }
}

function serializeStructure(prefix, struct, rules, fn) {
  util.each(rules.members, function(name, member) {
    var value = struct[name];
    if (value === null || value === undefined) return;

    var memberName = ucfirst(member);
    memberName = prefix ? prefix + '.' + memberName : memberName;
    serializeMember(memberName, value, member, fn);
  });
}

function serializeMap(name, map, rules, fn) {
  var i = 1;
  util.each(map, function (key, value) {
    var prefix = rules.flattened ? '.' : '.entry.';
    var position = prefix + (i++) + '.';
    var keyName = position + (rules.key.name || 'key');
    var valueName = position + (rules.value.name || 'value');
    serializeMember(name + keyName, key, rules.key, fn);
    serializeMember(name + valueName, value, rules.value, fn);
  });
}

function serializeList(name, list, rules, fn) {
  var memberRules = rules.member || {};

  if (list.length === 0) {
    fn.call(this, name, null);
    return;
  }

  util.arrayEach(list, function (v, n) {
    var suffix = '.' + (n + 1);
    if (rules.api.protocol === 'ec2') {
      // Do nothing for EC2
      suffix = suffix + ''; // make linter happy
    } else if (rules.flattened) {
      if (memberRules.name) {
        var parts = name.split('.');
        parts.pop();
        parts.push(ucfirst(memberRules));
        name = parts.join('.');
      }
    } else {
      suffix = '.' + (memberRules.name ? memberRules.name : 'member') + suffix;
    }
    serializeMember(name + suffix, v, memberRules, fn);
  });
}

function serializeMember(name, value, rules, fn) {
  if (value === null || value === undefined) return;
  if (rules.type === 'structure') {
    serializeStructure(name, value, rules, fn);
  } else if (rules.type === 'list') {
    serializeList(name, value, rules, fn);
  } else if (rules.type === 'map') {
    serializeMap(name, value, rules, fn);
  } else {
    fn(name, rules.toWireFormat(value).toString());
  }
}

/**
 * @api private
 */
module.exports = QueryParamSerializer;


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var XmlNode = __webpack_require__(59).XmlNode;
var XmlText = __webpack_require__(61).XmlText;

function XmlBuilder() { }

XmlBuilder.prototype.toXML = function(params, shape, rootElement, noEmpty) {
  var xml = new XmlNode(rootElement);
  applyNamespaces(xml, shape, true);
  serialize(xml, params, shape);
  return xml.children.length > 0 || noEmpty ? xml.toString() : '';
};

function serialize(xml, value, shape) {
  switch (shape.type) {
    case 'structure': return serializeStructure(xml, value, shape);
    case 'map': return serializeMap(xml, value, shape);
    case 'list': return serializeList(xml, value, shape);
    default: return serializeScalar(xml, value, shape);
  }
}

function serializeStructure(xml, params, shape) {
  util.arrayEach(shape.memberNames, function(memberName) {
    var memberShape = shape.members[memberName];
    if (memberShape.location !== 'body') return;

    var value = params[memberName];
    var name = memberShape.name;
    if (value !== undefined && value !== null) {
      if (memberShape.isXmlAttribute) {
        xml.addAttribute(name, value);
      } else if (memberShape.flattened) {
        serialize(xml, value, memberShape);
      } else {
        var element = new XmlNode(name);
        xml.addChildNode(element);
        applyNamespaces(element, memberShape);
        serialize(element, value, memberShape);
      }
    }
  });
}

function serializeMap(xml, map, shape) {
  var xmlKey = shape.key.name || 'key';
  var xmlValue = shape.value.name || 'value';

  util.each(map, function(key, value) {
    var entry = new XmlNode(shape.flattened ? shape.name : 'entry');
    xml.addChildNode(entry);

    var entryKey = new XmlNode(xmlKey);
    var entryValue = new XmlNode(xmlValue);
    entry.addChildNode(entryKey);
    entry.addChildNode(entryValue);

    serialize(entryKey, key, shape.key);
    serialize(entryValue, value, shape.value);
  });
}

function serializeList(xml, list, shape) {
  if (shape.flattened) {
    util.arrayEach(list, function(value) {
      var name = shape.member.name || shape.name;
      var element = new XmlNode(name);
      xml.addChildNode(element);
      serialize(element, value, shape.member);
    });
  } else {
    util.arrayEach(list, function(value) {
      var name = shape.member.name || 'member';
      var element = new XmlNode(name);
      xml.addChildNode(element);
      serialize(element, value, shape.member);
    });
  }
}

function serializeScalar(xml, value, shape) {
  xml.addChildNode(
    new XmlText(shape.toWireFormat(value))
  );
}

function applyNamespaces(xml, shape, isRoot) {
  var uri, prefix = 'xmlns';
  if (shape.xmlNamespaceUri) {
    uri = shape.xmlNamespaceUri;
    if (shape.xmlNamespacePrefix) prefix += ':' + shape.xmlNamespacePrefix;
  } else if (isRoot && shape.api.xmlNamespaceUri) {
    uri = shape.api.xmlNamespaceUri;
  }

  if (uri) xml.addAttribute(prefix, uri);
}

/**
 * @api private
 */
module.exports = XmlBuilder;


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

var escapeAttribute = __webpack_require__(60).escapeAttribute;

/**
 * Represents an XML node.
 * @api private
 */
function XmlNode(name, children) {
    if (children === void 0) { children = []; }
    this.name = name;
    this.children = children;
    this.attributes = {};
}
XmlNode.prototype.addAttribute = function (name, value) {
    this.attributes[name] = value;
    return this;
};
XmlNode.prototype.addChildNode = function (child) {
    this.children.push(child);
    return this;
};
XmlNode.prototype.removeAttribute = function (name) {
    delete this.attributes[name];
    return this;
};
XmlNode.prototype.toString = function () {
    var hasChildren = Boolean(this.children.length);
    var xmlText = '<' + this.name;
    // add attributes
    var attributes = this.attributes;
    for (var i = 0, attributeNames = Object.keys(attributes); i < attributeNames.length; i++) {
        var attributeName = attributeNames[i];
        var attribute = attributes[attributeName];
        if (typeof attribute !== 'undefined' && attribute !== null) {
            xmlText += ' ' + attributeName + '=\"' + escapeAttribute('' + attribute) + '\"';
        }
    }
    return xmlText += !hasChildren ? '/>' : '>' + this.children.map(function (c) { return c.toString(); }).join('') + '</' + this.name + '>';
};

/**
 * @api private
 */
module.exports = {
    XmlNode: XmlNode
};


/***/ }),
/* 60 */
/***/ (function(module, exports) {

/**
 * Escapes characters that can not be in an XML attribute.
 */
function escapeAttribute(value) {
    return value.replace(/&/g, '&amp;').replace(/'/g, '&apos;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/**
 * @api private
 */
module.exports = {
    escapeAttribute: escapeAttribute
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

var escapeElement = __webpack_require__(62).escapeElement;

/**
 * Represents an XML text value.
 * @api private
 */
function XmlText(value) {
    this.value = value;
}

XmlText.prototype.toString = function () {
    return escapeElement('' + this.value);
};

/**
 * @api private
 */
module.exports = {
    XmlText: XmlText
};


/***/ }),
/* 62 */
/***/ (function(module, exports) {

/**
 * Escapes characters that can not be in an XML element.
 */
function escapeElement(value) {
    return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

/**
 * @api private
 */
module.exports = {
    escapeElement: escapeElement
};


/***/ }),
/* 63 */
/***/ (function(module, exports) {

function apiLoader(svc, version) {
  if (!apiLoader.services.hasOwnProperty(svc)) {
    throw new Error('InvalidService: Failed to load api for ' + svc);
  }
  return apiLoader.services[svc][version];
}

/**
 * @api private
 *
 * This member of AWS.apiLoader is private, but changing it will necessitate a
 * change to ../scripts/services-table-generator.ts
 */
apiLoader.services = {};

/**
 * @api private
 */
module.exports = apiLoader;


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var LRU_1 = __webpack_require__(65);
var CACHE_SIZE = 1000;
/**
 * Inspired node-lru-cache[https://github.com/isaacs/node-lru-cache]
 */
var EndpointCache = /** @class */ (function () {
    function EndpointCache(maxSize) {
        if (maxSize === void 0) { maxSize = CACHE_SIZE; }
        this.maxSize = maxSize;
        this.cache = new LRU_1.LRUCache(maxSize);
    }
    ;
    Object.defineProperty(EndpointCache.prototype, "size", {
        get: function () {
            return this.cache.length;
        },
        enumerable: true,
        configurable: true
    });
    EndpointCache.prototype.put = function (key, value) {
      var keyString = typeof key !== 'string' ? EndpointCache.getKeyString(key) : key;
        var endpointRecord = this.populateValue(value);
        this.cache.put(keyString, endpointRecord);
    };
    EndpointCache.prototype.get = function (key) {
      var keyString = typeof key !== 'string' ? EndpointCache.getKeyString(key) : key;
        var now = Date.now();
        var records = this.cache.get(keyString);
        if (records) {
            for (var i = 0; i < records.length; i++) {
                var record = records[i];
                if (record.Expire < now) {
                    this.cache.remove(keyString);
                    return undefined;
                }
            }
        }
        return records;
    };
    EndpointCache.getKeyString = function (key) {
        var identifiers = [];
        var identifierNames = Object.keys(key).sort();
        for (var i = 0; i < identifierNames.length; i++) {
            var identifierName = identifierNames[i];
            if (key[identifierName] === undefined)
                continue;
            identifiers.push(key[identifierName]);
        }
        return identifiers.join(' ');
    };
    EndpointCache.prototype.populateValue = function (endpoints) {
        var now = Date.now();
        return endpoints.map(function (endpoint) { return ({
            Address: endpoint.Address || '',
            Expire: now + (endpoint.CachePeriodInMinutes || 1) * 60 * 1000
        }); });
    };
    EndpointCache.prototype.empty = function () {
        this.cache.empty();
    };
    EndpointCache.prototype.remove = function (key) {
      var keyString = typeof key !== 'string' ? EndpointCache.getKeyString(key) : key;
        this.cache.remove(keyString);
    };
    return EndpointCache;
}());
exports.EndpointCache = EndpointCache;

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var LinkedListNode = /** @class */ (function () {
    function LinkedListNode(key, value) {
        this.key = key;
        this.value = value;
    }
    return LinkedListNode;
}());
var LRUCache = /** @class */ (function () {
    function LRUCache(size) {
        this.nodeMap = {};
        this.size = 0;
        if (typeof size !== 'number' || size < 1) {
            throw new Error('Cache size can only be positive number');
        }
        this.sizeLimit = size;
    }
    Object.defineProperty(LRUCache.prototype, "length", {
        get: function () {
            return this.size;
        },
        enumerable: true,
        configurable: true
    });
    LRUCache.prototype.prependToList = function (node) {
        if (!this.headerNode) {
            this.tailNode = node;
        }
        else {
            this.headerNode.prev = node;
            node.next = this.headerNode;
        }
        this.headerNode = node;
        this.size++;
    };
    LRUCache.prototype.removeFromTail = function () {
        if (!this.tailNode) {
            return undefined;
        }
        var node = this.tailNode;
        var prevNode = node.prev;
        if (prevNode) {
            prevNode.next = undefined;
        }
        node.prev = undefined;
        this.tailNode = prevNode;
        this.size--;
        return node;
    };
    LRUCache.prototype.detachFromList = function (node) {
        if (this.headerNode === node) {
            this.headerNode = node.next;
        }
        if (this.tailNode === node) {
            this.tailNode = node.prev;
        }
        if (node.prev) {
            node.prev.next = node.next;
        }
        if (node.next) {
            node.next.prev = node.prev;
        }
        node.next = undefined;
        node.prev = undefined;
        this.size--;
    };
    LRUCache.prototype.get = function (key) {
        if (this.nodeMap[key]) {
            var node = this.nodeMap[key];
            this.detachFromList(node);
            this.prependToList(node);
            return node.value;
        }
    };
    LRUCache.prototype.remove = function (key) {
        if (this.nodeMap[key]) {
            var node = this.nodeMap[key];
            this.detachFromList(node);
            delete this.nodeMap[key];
        }
    };
    LRUCache.prototype.put = function (key, value) {
        if (this.nodeMap[key]) {
            this.remove(key);
        }
        else if (this.size === this.sizeLimit) {
            var tailNode = this.removeFromTail();
            var key_1 = tailNode.key;
            delete this.nodeMap[key_1];
        }
        var newNode = new LinkedListNode(key, value);
        this.nodeMap[key] = newNode;
        this.prependToList(newNode);
    };
    LRUCache.prototype.empty = function () {
        var keys = Object.keys(this.nodeMap);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var node = this.nodeMap[key];
            this.detachFromList(node);
            delete this.nodeMap[key];
        }
    };
    return LRUCache;
}());
exports.LRUCache = LRUCache;

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {var AWS = __webpack_require__(1);
var Api = __webpack_require__(29);
var regionConfig = __webpack_require__(67);

var inherit = AWS.util.inherit;
var clientCount = 0;

/**
 * The service class representing an AWS service.
 *
 * @class_abstract This class is an abstract class.
 *
 * @!attribute apiVersions
 *   @return [Array<String>] the list of API versions supported by this service.
 *   @readonly
 */
AWS.Service = inherit({
  /**
   * Create a new service object with a configuration object
   *
   * @param config [map] a map of configuration options
   */
  constructor: function Service(config) {
    if (!this.loadServiceClass) {
      throw AWS.util.error(new Error(),
        'Service must be constructed with `new\' operator');
    }
    var ServiceClass = this.loadServiceClass(config || {});
    if (ServiceClass) {
      var originalConfig = AWS.util.copy(config);
      var svc = new ServiceClass(config);
      Object.defineProperty(svc, '_originalConfig', {
        get: function() { return originalConfig; },
        enumerable: false,
        configurable: true
      });
      svc._clientId = ++clientCount;
      return svc;
    }
    this.initialize(config);
  },

  /**
   * @api private
   */
  initialize: function initialize(config) {
    var svcConfig = AWS.config[this.serviceIdentifier];
    this.config = new AWS.Config(AWS.config);
    if (svcConfig) this.config.update(svcConfig, true);
    if (config) this.config.update(config, true);

    this.validateService();
    if (!this.config.endpoint) regionConfig.configureEndpoint(this);

    this.config.endpoint = this.endpointFromTemplate(this.config.endpoint);
    this.setEndpoint(this.config.endpoint);
    //enable attaching listeners to service client
    AWS.SequentialExecutor.call(this);
    AWS.Service.addDefaultMonitoringListeners(this);
    if ((this.config.clientSideMonitoring || AWS.Service._clientSideMonitoring) && this.publisher) {
      var publisher = this.publisher;
      this.addNamedListener('PUBLISH_API_CALL', 'apiCall', function PUBLISH_API_CALL(event) {
        process.nextTick(function() {publisher.eventHandler(event);});
      });
      this.addNamedListener('PUBLISH_API_ATTEMPT', 'apiCallAttempt', function PUBLISH_API_ATTEMPT(event) {
        process.nextTick(function() {publisher.eventHandler(event);});
      });
    }
  },

  /**
   * @api private
   */
  validateService: function validateService() {
  },

  /**
   * @api private
   */
  loadServiceClass: function loadServiceClass(serviceConfig) {
    var config = serviceConfig;
    if (!AWS.util.isEmpty(this.api)) {
      return null;
    } else if (config.apiConfig) {
      return AWS.Service.defineServiceApi(this.constructor, config.apiConfig);
    } else if (!this.constructor.services) {
      return null;
    } else {
      config = new AWS.Config(AWS.config);
      config.update(serviceConfig, true);
      var version = config.apiVersions[this.constructor.serviceIdentifier];
      version = version || config.apiVersion;
      return this.getLatestServiceClass(version);
    }
  },

  /**
   * @api private
   */
  getLatestServiceClass: function getLatestServiceClass(version) {
    version = this.getLatestServiceVersion(version);
    if (this.constructor.services[version] === null) {
      AWS.Service.defineServiceApi(this.constructor, version);
    }

    return this.constructor.services[version];
  },

  /**
   * @api private
   */
  getLatestServiceVersion: function getLatestServiceVersion(version) {
    if (!this.constructor.services || this.constructor.services.length === 0) {
      throw new Error('No services defined on ' +
                      this.constructor.serviceIdentifier);
    }

    if (!version) {
      version = 'latest';
    } else if (AWS.util.isType(version, Date)) {
      version = AWS.util.date.iso8601(version).split('T')[0];
    }

    if (Object.hasOwnProperty(this.constructor.services, version)) {
      return version;
    }

    var keys = Object.keys(this.constructor.services).sort();
    var selectedVersion = null;
    for (var i = keys.length - 1; i >= 0; i--) {
      // versions that end in "*" are not available on disk and can be
      // skipped, so do not choose these as selectedVersions
      if (keys[i][keys[i].length - 1] !== '*') {
        selectedVersion = keys[i];
      }
      if (keys[i].substr(0, 10) <= version) {
        return selectedVersion;
      }
    }

    throw new Error('Could not find ' + this.constructor.serviceIdentifier +
                    ' API to satisfy version constraint `' + version + '\'');
  },

  /**
   * @api private
   */
  api: {},

  /**
   * @api private
   */
  defaultRetryCount: 3,

  /**
   * @api private
   */
  customizeRequests: function customizeRequests(callback) {
    if (!callback) {
      this.customRequestHandler = null;
    } else if (typeof callback === 'function') {
      this.customRequestHandler = callback;
    } else {
      throw new Error('Invalid callback type \'' + typeof callback + '\' provided in customizeRequests');
    }
  },

  /**
   * Calls an operation on a service with the given input parameters.
   *
   * @param operation [String] the name of the operation to call on the service.
   * @param params [map] a map of input options for the operation
   * @callback callback function(err, data)
   *   If a callback is supplied, it is called when a response is returned
   *   from the service.
   *   @param err [Error] the error object returned from the request.
   *     Set to `null` if the request is successful.
   *   @param data [Object] the de-serialized data returned from
   *     the request. Set to `null` if a request error occurs.
   */
  makeRequest: function makeRequest(operation, params, callback) {
    if (typeof params === 'function') {
      callback = params;
      params = null;
    }

    params = params || {};
    if (this.config.params) { // copy only toplevel bound params
      var rules = this.api.operations[operation];
      if (rules) {
        params = AWS.util.copy(params);
        AWS.util.each(this.config.params, function(key, value) {
          if (rules.input.members[key]) {
            if (params[key] === undefined || params[key] === null) {
              params[key] = value;
            }
          }
        });
      }
    }

    var request = new AWS.Request(this, operation, params);
    this.addAllRequestListeners(request);
    this.attachMonitoringEmitter(request);
    if (callback) request.send(callback);
    return request;
  },

  /**
   * Calls an operation on a service with the given input parameters, without
   * any authentication data. This method is useful for "public" API operations.
   *
   * @param operation [String] the name of the operation to call on the service.
   * @param params [map] a map of input options for the operation
   * @callback callback function(err, data)
   *   If a callback is supplied, it is called when a response is returned
   *   from the service.
   *   @param err [Error] the error object returned from the request.
   *     Set to `null` if the request is successful.
   *   @param data [Object] the de-serialized data returned from
   *     the request. Set to `null` if a request error occurs.
   */
  makeUnauthenticatedRequest: function makeUnauthenticatedRequest(operation, params, callback) {
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }

    var request = this.makeRequest(operation, params).toUnauthenticated();
    return callback ? request.send(callback) : request;
  },

  /**
   * Waits for a given state
   *
   * @param state [String] the state on the service to wait for
   * @param params [map] a map of parameters to pass with each request
   * @option params $waiter [map] a map of configuration options for the waiter
   * @option params $waiter.delay [Number] The number of seconds to wait between
   *                                       requests
   * @option params $waiter.maxAttempts [Number] The maximum number of requests
   *                                             to send while waiting
   * @callback callback function(err, data)
   *   If a callback is supplied, it is called when a response is returned
   *   from the service.
   *   @param err [Error] the error object returned from the request.
   *     Set to `null` if the request is successful.
   *   @param data [Object] the de-serialized data returned from
   *     the request. Set to `null` if a request error occurs.
   */
  waitFor: function waitFor(state, params, callback) {
    var waiter = new AWS.ResourceWaiter(this, state);
    return waiter.wait(params, callback);
  },

  /**
   * @api private
   */
  addAllRequestListeners: function addAllRequestListeners(request) {
    var list = [AWS.events, AWS.EventListeners.Core, this.serviceInterface(),
                AWS.EventListeners.CorePost];
    for (var i = 0; i < list.length; i++) {
      if (list[i]) request.addListeners(list[i]);
    }

    // disable parameter validation
    if (!this.config.paramValidation) {
      request.removeListener('validate',
        AWS.EventListeners.Core.VALIDATE_PARAMETERS);
    }

    if (this.config.logger) { // add logging events
      request.addListeners(AWS.EventListeners.Logger);
    }

    this.setupRequestListeners(request);
    // call prototype's customRequestHandler
    if (typeof this.constructor.prototype.customRequestHandler === 'function') {
      this.constructor.prototype.customRequestHandler(request);
    }
    // call instance's customRequestHandler
    if (Object.prototype.hasOwnProperty.call(this, 'customRequestHandler') && typeof this.customRequestHandler === 'function') {
      this.customRequestHandler(request);
    }
  },

  /**
   * Event recording metrics for a whole API call.
   * @returns {object} a subset of api call metrics
   * @api private
   */
  apiCallEvent: function apiCallEvent(request) {
    var api = request.service.api.operations[request.operation];
    var monitoringEvent = {
      Type: 'ApiCall',
      Api: api ? api.name : request.operation,
      Version: 1,
      Service: request.service.api.serviceId || request.service.api.endpointPrefix,
      Region: request.httpRequest.region,
      MaxRetriesExceeded: 0,
      UserAgent: request.httpRequest.getUserAgent(),
    };
    var response = request.response;
    if (response.httpResponse.statusCode) {
      monitoringEvent.FinalHttpStatusCode = response.httpResponse.statusCode;
    }
    if (response.error) {
      var error = response.error;
      var statusCode = response.httpResponse.statusCode;
      if (statusCode > 299) {
        if (error.code) monitoringEvent.FinalAwsException = error.code;
        if (error.message) monitoringEvent.FinalAwsExceptionMessage = error.message;
      } else {
        if (error.code || error.name) monitoringEvent.FinalSdkException = error.code || error.name;
        if (error.message) monitoringEvent.FinalSdkExceptionMessage = error.message;
      }
    }
    return monitoringEvent;
  },

  /**
   * Event recording metrics for an API call attempt.
   * @returns {object} a subset of api call attempt metrics
   * @api private
   */
  apiAttemptEvent: function apiAttemptEvent(request) {
    var api = request.service.api.operations[request.operation];
    var monitoringEvent = {
      Type: 'ApiCallAttempt',
      Api: api ? api.name : request.operation,
      Version: 1,
      Service: request.service.api.serviceId || request.service.api.endpointPrefix,
      Fqdn: request.httpRequest.endpoint.hostname,
      UserAgent: request.httpRequest.getUserAgent(),
    };
    var response = request.response;
    if (response.httpResponse.statusCode) {
      monitoringEvent.HttpStatusCode = response.httpResponse.statusCode;
    }
    if (
      !request._unAuthenticated &&
      request.service.config.credentials &&
      request.service.config.credentials.accessKeyId
    ) {
      monitoringEvent.AccessKey = request.service.config.credentials.accessKeyId;
    }
    if (!response.httpResponse.headers) return monitoringEvent;
    if (request.httpRequest.headers['x-amz-security-token']) {
      monitoringEvent.SessionToken = request.httpRequest.headers['x-amz-security-token'];
    }
    if (response.httpResponse.headers['x-amzn-requestid']) {
      monitoringEvent.XAmznRequestId = response.httpResponse.headers['x-amzn-requestid'];
    }
    if (response.httpResponse.headers['x-amz-request-id']) {
      monitoringEvent.XAmzRequestId = response.httpResponse.headers['x-amz-request-id'];
    }
    if (response.httpResponse.headers['x-amz-id-2']) {
      monitoringEvent.XAmzId2 = response.httpResponse.headers['x-amz-id-2'];
    }
    return monitoringEvent;
  },

  /**
   * Add metrics of failed request.
   * @api private
   */
  attemptFailEvent: function attemptFailEvent(request) {
    var monitoringEvent = this.apiAttemptEvent(request);
    var response = request.response;
    var error = response.error;
    if (response.httpResponse.statusCode > 299 ) {
      if (error.code) monitoringEvent.AwsException = error.code;
      if (error.message) monitoringEvent.AwsExceptionMessage = error.message;
    } else {
      if (error.code || error.name) monitoringEvent.SdkException = error.code || error.name;
      if (error.message) monitoringEvent.SdkExceptionMessage = error.message;
    }
    return monitoringEvent;
  },

  /**
   * Attach listeners to request object to fetch metrics of each request
   * and emit data object through \'ApiCall\' and \'ApiCallAttempt\' events.
   * @api private
   */
  attachMonitoringEmitter: function attachMonitoringEmitter(request) {
    var attemptTimestamp; //timestamp marking the beginning of a request attempt
    var attemptStartRealTime; //Start time of request attempt. Used to calculating attemptLatency
    var attemptLatency; //latency from request sent out to http response reaching SDK
    var callStartRealTime; //Start time of API call. Used to calculating API call latency
    var attemptCount = 0; //request.retryCount is not reliable here
    var region; //region cache region for each attempt since it can be updated in plase (e.g. s3)
    var callTimestamp; //timestamp when the request is created
    var self = this;
    var addToHead = true;

    request.on('validate', function () {
      callStartRealTime = AWS.util.realClock.now();
      callTimestamp = Date.now();
    }, addToHead);
    request.on('sign', function () {
      attemptStartRealTime = AWS.util.realClock.now();
      attemptTimestamp = Date.now();
      region = request.httpRequest.region;
      attemptCount++;
    }, addToHead);
    request.on('validateResponse', function() {
      attemptLatency = Math.round(AWS.util.realClock.now() - attemptStartRealTime);
    });
    request.addNamedListener('API_CALL_ATTEMPT', 'success', function API_CALL_ATTEMPT() {
      var apiAttemptEvent = self.apiAttemptEvent(request);
      apiAttemptEvent.Timestamp = attemptTimestamp;
      apiAttemptEvent.AttemptLatency = attemptLatency >= 0 ? attemptLatency : 0;
      apiAttemptEvent.Region = region;
      self.emit('apiCallAttempt', [apiAttemptEvent]);
    });
    request.addNamedListener('API_CALL_ATTEMPT_RETRY', 'retry', function API_CALL_ATTEMPT_RETRY() {
      var apiAttemptEvent = self.attemptFailEvent(request);
      apiAttemptEvent.Timestamp = attemptTimestamp;
      //attemptLatency may not be available if fail before response
      attemptLatency = attemptLatency ||
        Math.round(AWS.util.realClock.now() - attemptStartRealTime);
      apiAttemptEvent.AttemptLatency = attemptLatency >= 0 ? attemptLatency : 0;
      apiAttemptEvent.Region = region;
      self.emit('apiCallAttempt', [apiAttemptEvent]);
    });
    request.addNamedListener('API_CALL', 'complete', function API_CALL() {
      var apiCallEvent = self.apiCallEvent(request);
      apiCallEvent.AttemptCount = attemptCount;
      if (apiCallEvent.AttemptCount <= 0) return;
      apiCallEvent.Timestamp = callTimestamp;
      var latency = Math.round(AWS.util.realClock.now() - callStartRealTime);
      apiCallEvent.Latency = latency >= 0 ? latency : 0;
      var response = request.response;
      if (
        typeof response.retryCount === 'number' &&
        typeof response.maxRetries === 'number' &&
        (response.retryCount >= response.maxRetries)
      ) {
        apiCallEvent.MaxRetriesExceeded = 1;
      }
      self.emit('apiCall', [apiCallEvent]);
    });
  },

  /**
   * Override this method to setup any custom request listeners for each
   * new request to the service.
   *
   * @method_abstract This is an abstract method.
   */
  setupRequestListeners: function setupRequestListeners(request) {
  },

  /**
   * Gets the signer class for a given request
   * @api private
   */
  getSignerClass: function getSignerClass(request) {
    var version;
    // get operation authtype if present
    var operation = null;
    var authtype = '';
    if (request) {
      var operations = request.service.api.operations || {};
      operation = operations[request.operation] || null;
      authtype = operation ? operation.authtype : '';
    }
    if (this.config.signatureVersion) {
      version = this.config.signatureVersion;
    } else if (authtype === 'v4' || authtype === 'v4-unsigned-body') {
      version = 'v4';
    } else {
      version = this.api.signatureVersion;
    }
    return AWS.Signers.RequestSigner.getVersion(version);
  },

  /**
   * @api private
   */
  serviceInterface: function serviceInterface() {
    switch (this.api.protocol) {
      case 'ec2': return AWS.EventListeners.Query;
      case 'query': return AWS.EventListeners.Query;
      case 'json': return AWS.EventListeners.Json;
      case 'rest-json': return AWS.EventListeners.RestJson;
      case 'rest-xml': return AWS.EventListeners.RestXml;
    }
    if (this.api.protocol) {
      throw new Error('Invalid service `protocol\' ' +
        this.api.protocol + ' in API config');
    }
  },

  /**
   * @api private
   */
  successfulResponse: function successfulResponse(resp) {
    return resp.httpResponse.statusCode < 300;
  },

  /**
   * How many times a failed request should be retried before giving up.
   * the defaultRetryCount can be overriden by service classes.
   *
   * @api private
   */
  numRetries: function numRetries() {
    if (this.config.maxRetries !== undefined) {
      return this.config.maxRetries;
    } else {
      return this.defaultRetryCount;
    }
  },

  /**
   * @api private
   */
  retryDelays: function retryDelays(retryCount, err) {
    return AWS.util.calculateRetryDelay(retryCount, this.config.retryDelayOptions, err);
  },

  /**
   * @api private
   */
  retryableError: function retryableError(error) {
    if (this.timeoutError(error)) return true;
    if (this.networkingError(error)) return true;
    if (this.expiredCredentialsError(error)) return true;
    if (this.throttledError(error)) return true;
    if (error.statusCode >= 500) return true;
    return false;
  },

  /**
   * @api private
   */
  networkingError: function networkingError(error) {
    return error.code === 'NetworkingError';
  },

  /**
   * @api private
   */
  timeoutError: function timeoutError(error) {
    return error.code === 'TimeoutError';
  },

  /**
   * @api private
   */
  expiredCredentialsError: function expiredCredentialsError(error) {
    // TODO : this only handles *one* of the expired credential codes
    return (error.code === 'ExpiredTokenException');
  },

  /**
   * @api private
   */
  clockSkewError: function clockSkewError(error) {
    switch (error.code) {
      case 'RequestTimeTooSkewed':
      case 'RequestExpired':
      case 'InvalidSignatureException':
      case 'SignatureDoesNotMatch':
      case 'AuthFailure':
      case 'RequestInTheFuture':
        return true;
      default: return false;
    }
  },

  /**
   * @api private
   */
  getSkewCorrectedDate: function getSkewCorrectedDate() {
    return new Date(Date.now() + this.config.systemClockOffset);
  },

  /**
   * @api private
   */
  applyClockOffset: function applyClockOffset(newServerTime) {
    if (newServerTime) {
      this.config.systemClockOffset = newServerTime - Date.now();
    }
  },

  /**
   * @api private
   */
  isClockSkewed: function isClockSkewed(newServerTime) {
    if (newServerTime) {
      return Math.abs(this.getSkewCorrectedDate().getTime() - newServerTime) >= 300000;
    }
  },

  /**
   * @api private
   */
  throttledError: function throttledError(error) {
    // this logic varies between services
    if (error.statusCode === 429) return true;
    switch (error.code) {
      case 'ProvisionedThroughputExceededException':
      case 'Throttling':
      case 'ThrottlingException':
      case 'RequestLimitExceeded':
      case 'RequestThrottled':
      case 'RequestThrottledException':
      case 'TooManyRequestsException':
      case 'TransactionInProgressException': //dynamodb
        return true;
      default:
        return false;
    }
  },

  /**
   * @api private
   */
  endpointFromTemplate: function endpointFromTemplate(endpoint) {
    if (typeof endpoint !== 'string') return endpoint;

    var e = endpoint;
    e = e.replace(/\{service\}/g, this.api.endpointPrefix);
    e = e.replace(/\{region\}/g, this.config.region);
    e = e.replace(/\{scheme\}/g, this.config.sslEnabled ? 'https' : 'http');
    return e;
  },

  /**
   * @api private
   */
  setEndpoint: function setEndpoint(endpoint) {
    this.endpoint = new AWS.Endpoint(endpoint, this.config);
  },

  /**
   * @api private
   */
  paginationConfig: function paginationConfig(operation, throwException) {
    var paginator = this.api.operations[operation].paginator;
    if (!paginator) {
      if (throwException) {
        var e = new Error();
        throw AWS.util.error(e, 'No pagination configuration for ' + operation);
      }
      return null;
    }

    return paginator;
  }
});

AWS.util.update(AWS.Service, {

  /**
   * Adds one method for each operation described in the api configuration
   *
   * @api private
   */
  defineMethods: function defineMethods(svc) {
    AWS.util.each(svc.prototype.api.operations, function iterator(method) {
      if (svc.prototype[method]) return;
      var operation = svc.prototype.api.operations[method];
      if (operation.authtype === 'none') {
        svc.prototype[method] = function (params, callback) {
          return this.makeUnauthenticatedRequest(method, params, callback);
        };
      } else {
        svc.prototype[method] = function (params, callback) {
          return this.makeRequest(method, params, callback);
        };
      }
    });
  },

  /**
   * Defines a new Service class using a service identifier and list of versions
   * including an optional set of features (functions) to apply to the class
   * prototype.
   *
   * @param serviceIdentifier [String] the identifier for the service
   * @param versions [Array<String>] a list of versions that work with this
   *   service
   * @param features [Object] an object to attach to the prototype
   * @return [Class<Service>] the service class defined by this function.
   */
  defineService: function defineService(serviceIdentifier, versions, features) {
    AWS.Service._serviceMap[serviceIdentifier] = true;
    if (!Array.isArray(versions)) {
      features = versions;
      versions = [];
    }

    var svc = inherit(AWS.Service, features || {});

    if (typeof serviceIdentifier === 'string') {
      AWS.Service.addVersions(svc, versions);

      var identifier = svc.serviceIdentifier || serviceIdentifier;
      svc.serviceIdentifier = identifier;
    } else { // defineService called with an API
      svc.prototype.api = serviceIdentifier;
      AWS.Service.defineMethods(svc);
    }
    AWS.SequentialExecutor.call(this.prototype);
    //util.clientSideMonitoring is only available in node
    if (!this.prototype.publisher && AWS.util.clientSideMonitoring) {
      var Publisher = AWS.util.clientSideMonitoring.Publisher;
      var configProvider = AWS.util.clientSideMonitoring.configProvider;
      var publisherConfig = configProvider();
      this.prototype.publisher = new Publisher(publisherConfig);
      if (publisherConfig.enabled) {
        //if csm is enabled in environment, SDK should send all metrics
        AWS.Service._clientSideMonitoring = true;
      }
    }
    AWS.SequentialExecutor.call(svc.prototype);
    AWS.Service.addDefaultMonitoringListeners(svc.prototype);
    return svc;
  },

  /**
   * @api private
   */
  addVersions: function addVersions(svc, versions) {
    if (!Array.isArray(versions)) versions = [versions];

    svc.services = svc.services || {};
    for (var i = 0; i < versions.length; i++) {
      if (svc.services[versions[i]] === undefined) {
        svc.services[versions[i]] = null;
      }
    }

    svc.apiVersions = Object.keys(svc.services).sort();
  },

  /**
   * @api private
   */
  defineServiceApi: function defineServiceApi(superclass, version, apiConfig) {
    var svc = inherit(superclass, {
      serviceIdentifier: superclass.serviceIdentifier
    });

    function setApi(api) {
      if (api.isApi) {
        svc.prototype.api = api;
      } else {
        svc.prototype.api = new Api(api, {
          serviceIdentifier: superclass.serviceIdentifier
        });
      }
    }

    if (typeof version === 'string') {
      if (apiConfig) {
        setApi(apiConfig);
      } else {
        try {
          setApi(AWS.apiLoader(superclass.serviceIdentifier, version));
        } catch (err) {
          throw AWS.util.error(err, {
            message: 'Could not find API configuration ' +
              superclass.serviceIdentifier + '-' + version
          });
        }
      }
      if (!Object.prototype.hasOwnProperty.call(superclass.services, version)) {
        superclass.apiVersions = superclass.apiVersions.concat(version).sort();
      }
      superclass.services[version] = svc;
    } else {
      setApi(version);
    }

    AWS.Service.defineMethods(svc);
    return svc;
  },

  /**
   * @api private
   */
  hasService: function(identifier) {
    return Object.prototype.hasOwnProperty.call(AWS.Service._serviceMap, identifier);
  },

  /**
   * @param attachOn attach default monitoring listeners to object
   *
   * Each monitoring event should be emitted from service client to service constructor prototype and then
   * to global service prototype like bubbling up. These default monitoring events listener will transfer
   * the monitoring events to the upper layer.
   * @api private
   */
  addDefaultMonitoringListeners: function addDefaultMonitoringListeners(attachOn) {
    attachOn.addNamedListener('MONITOR_EVENTS_BUBBLE', 'apiCallAttempt', function EVENTS_BUBBLE(event) {
      var baseClass = Object.getPrototypeOf(attachOn);
      if (baseClass._events) baseClass.emit('apiCallAttempt', [event]);
    });
    attachOn.addNamedListener('CALL_EVENTS_BUBBLE', 'apiCall', function CALL_EVENTS_BUBBLE(event) {
      var baseClass = Object.getPrototypeOf(attachOn);
      if (baseClass._events) baseClass.emit('apiCall', [event]);
    });
  },

  /**
   * @api private
   */
  _serviceMap: {}
});

AWS.util.mixin(AWS.Service, AWS.SequentialExecutor);

/**
 * @api private
 */
module.exports = AWS.Service;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(7)))

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var regionConfig = __webpack_require__(68);

function generateRegionPrefix(region) {
  if (!region) return null;

  var parts = region.split('-');
  if (parts.length < 3) return null;
  return parts.slice(0, parts.length - 2).join('-') + '-*';
}

function derivedKeys(service) {
  var region = service.config.region;
  var regionPrefix = generateRegionPrefix(region);
  var endpointPrefix = service.api.endpointPrefix;

  return [
    [region, endpointPrefix],
    [regionPrefix, endpointPrefix],
    [region, '*'],
    [regionPrefix, '*'],
    ['*', endpointPrefix],
    ['*', '*']
  ].map(function(item) {
    return item[0] && item[1] ? item.join('/') : null;
  });
}

function applyConfig(service, config) {
  util.each(config, function(key, value) {
    if (key === 'globalEndpoint') return;
    if (service.config[key] === undefined || service.config[key] === null) {
      service.config[key] = value;
    }
  });
}

function configureEndpoint(service) {
  var keys = derivedKeys(service);
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    if (!key) continue;

    if (Object.prototype.hasOwnProperty.call(regionConfig.rules, key)) {
      var config = regionConfig.rules[key];
      if (typeof config === 'string') {
        config = regionConfig.patterns[config];
      }

      // set dualstack endpoint
      if (service.config.useDualstack && util.isDualstackAvailable(service)) {
        config = util.copy(config);
        config.endpoint = '{service}.dualstack.{region}.amazonaws.com';
      }

      // set global endpoint
      service.isGlobalEndpoint = !!config.globalEndpoint;

      // signature version
      if (!config.signatureVersion) config.signatureVersion = 'v4';

      // merge config
      applyConfig(service, config);
      return;
    }
  }
}

function getEndpointSuffix(region) {
  var regionRegexes = {
    '^(us|eu|ap|sa|ca|me)\\-\\w+\\-\\d+$': 'amazonaws.com',
    '^cn\\-\\w+\\-\\d+$': 'amazonaws.com.cn',
    '^us\\-gov\\-\\w+\\-\\d+$': 'amazonaws.com',
    '^us\\-iso\\-\\w+\\-\\d+$': 'c2s.ic.gov',
    '^us\\-isob\\-\\w+\\-\\d+$': 'sc2s.sgov.gov'
  };
  var defaultSuffix = 'amazonaws.com';
  var regexes = Object.keys(regionRegexes);
  for (var i = 0; i < regexes.length; i++) {
    var regionPattern = RegExp(regexes[i]);
    var dnsSuffix = regionRegexes[regexes[i]];
    if (regionPattern.test(region)) return dnsSuffix;
  }
  return defaultSuffix;
}

/**
 * @api private
 */
module.exports = {
  configureEndpoint: configureEndpoint,
  getEndpointSuffix: getEndpointSuffix
};


/***/ }),
/* 68 */
/***/ (function(module) {

module.exports = JSON.parse("{\"rules\":{\"*/*\":{\"endpoint\":\"{service}.{region}.amazonaws.com\"},\"cn-*/*\":{\"endpoint\":\"{service}.{region}.amazonaws.com.cn\"},\"us-iso-*/*\":{\"endpoint\":\"{service}.{region}.c2s.ic.gov\"},\"us-isob-*/*\":{\"endpoint\":\"{service}.{region}.sc2s.sgov.gov\"},\"*/budgets\":\"globalSSL\",\"*/cloudfront\":\"globalSSL\",\"*/iam\":\"globalSSL\",\"*/sts\":\"globalSSL\",\"*/importexport\":{\"endpoint\":\"{service}.amazonaws.com\",\"signatureVersion\":\"v2\",\"globalEndpoint\":true},\"*/route53\":{\"endpoint\":\"https://{service}.amazonaws.com\",\"signatureVersion\":\"v3https\",\"globalEndpoint\":true},\"*/waf\":\"globalSSL\",\"us-gov-*/iam\":\"globalGovCloud\",\"us-gov-*/sts\":{\"endpoint\":\"{service}.{region}.amazonaws.com\"},\"us-gov-west-1/s3\":\"s3signature\",\"us-west-1/s3\":\"s3signature\",\"us-west-2/s3\":\"s3signature\",\"eu-west-1/s3\":\"s3signature\",\"ap-southeast-1/s3\":\"s3signature\",\"ap-southeast-2/s3\":\"s3signature\",\"ap-northeast-1/s3\":\"s3signature\",\"sa-east-1/s3\":\"s3signature\",\"us-east-1/s3\":{\"endpoint\":\"{service}.amazonaws.com\",\"signatureVersion\":\"s3\"},\"us-east-1/sdb\":{\"endpoint\":\"{service}.amazonaws.com\",\"signatureVersion\":\"v2\"},\"*/sdb\":{\"endpoint\":\"{service}.{region}.amazonaws.com\",\"signatureVersion\":\"v2\"}},\"patterns\":{\"globalSSL\":{\"endpoint\":\"https://{service}.amazonaws.com\",\"globalEndpoint\":true},\"globalGovCloud\":{\"endpoint\":\"{service}.us-gov.amazonaws.com\"},\"s3signature\":{\"endpoint\":\"{service}.{region}.amazonaws.com\",\"signatureVersion\":\"s3\"}}}");

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
__webpack_require__(35);
__webpack_require__(36);
var PromisesDependency;

/**
 * The main configuration class used by all service objects to set
 * the region, credentials, and other options for requests.
 *
 * By default, credentials and region settings are left unconfigured.
 * This should be configured by the application before using any
 * AWS service APIs.
 *
 * In order to set global configuration options, properties should
 * be assigned to the global {AWS.config} object.
 *
 * @see AWS.config
 *
 * @!group General Configuration Options
 *
 * @!attribute credentials
 *   @return [AWS.Credentials] the AWS credentials to sign requests with.
 *
 * @!attribute region
 *   @example Set the global region setting to us-west-2
 *     AWS.config.update({region: 'us-west-2'});
 *   @return [AWS.Credentials] The region to send service requests to.
 *   @see http://docs.amazonwebservices.com/general/latest/gr/rande.html
 *     A list of available endpoints for each AWS service
 *
 * @!attribute maxRetries
 *   @return [Integer] the maximum amount of retries to perform for a
 *     service request. By default this value is calculated by the specific
 *     service object that the request is being made to.
 *
 * @!attribute maxRedirects
 *   @return [Integer] the maximum amount of redirects to follow for a
 *     service request. Defaults to 10.
 *
 * @!attribute paramValidation
 *   @return [Boolean|map] whether input parameters should be validated against
 *     the operation description before sending the request. Defaults to true.
 *     Pass a map to enable any of the following specific validation features:
 *
 *     * **min** [Boolean] &mdash; Validates that a value meets the min
 *       constraint. This is enabled by default when paramValidation is set
 *       to `true`.
 *     * **max** [Boolean] &mdash; Validates that a value meets the max
 *       constraint.
 *     * **pattern** [Boolean] &mdash; Validates that a string value matches a
 *       regular expression.
 *     * **enum** [Boolean] &mdash; Validates that a string value matches one
 *       of the allowable enum values.
 *
 * @!attribute computeChecksums
 *   @return [Boolean] whether to compute checksums for payload bodies when
 *     the service accepts it (currently supported in S3 only).
 *
 * @!attribute convertResponseTypes
 *   @return [Boolean] whether types are converted when parsing response data.
 *     Currently only supported for JSON based services. Turning this off may
 *     improve performance on large response payloads. Defaults to `true`.
 *
 * @!attribute correctClockSkew
 *   @return [Boolean] whether to apply a clock skew correction and retry
 *     requests that fail because of an skewed client clock. Defaults to
 *     `false`.
 *
 * @!attribute sslEnabled
 *   @return [Boolean] whether SSL is enabled for requests
 *
 * @!attribute s3ForcePathStyle
 *   @return [Boolean] whether to force path style URLs for S3 objects
 *
 * @!attribute s3BucketEndpoint
 *   @note Setting this configuration option requires an `endpoint` to be
 *     provided explicitly to the service constructor.
 *   @return [Boolean] whether the provided endpoint addresses an individual
 *     bucket (false if it addresses the root API endpoint).
 *
 * @!attribute s3DisableBodySigning
 *   @return [Boolean] whether to disable S3 body signing when using signature version `v4`.
 *     Body signing can only be disabled when using https. Defaults to `true`.
 *
 * @!attribute s3UsEast1RegionalEndpoint
 *   @return ['legacy'|'regional'] when region is set to 'us-east-1', whether to send s3
 *     request to global endpoints or 'us-east-1' regional endpoints. This config is only
 *     applicable to S3 client;
 *     Defaults to 'legacy'
 * @!attribute s3UseArnRegion
 *   @return [Boolean] whether to override the request region with the region inferred
 *     from requested resource's ARN. Only available for S3 buckets
 *     Defaults to `true`
 *
 * @!attribute useAccelerateEndpoint
 *   @note This configuration option is only compatible with S3 while accessing
 *     dns-compatible buckets.
 *   @return [Boolean] Whether to use the Accelerate endpoint with the S3 service.
 *     Defaults to `false`.
 *
 * @!attribute retryDelayOptions
 *   @example Set the base retry delay for all services to 300 ms
 *     AWS.config.update({retryDelayOptions: {base: 300}});
 *     // Delays with maxRetries = 3: 300, 600, 1200
 *   @example Set a custom backoff function to provide delay values on retries
 *     AWS.config.update({retryDelayOptions: {customBackoff: function(retryCount, err) {
 *       // returns delay in ms
 *     }}});
 *   @return [map] A set of options to configure the retry delay on retryable errors.
 *     Currently supported options are:
 *
 *     * **base** [Integer] &mdash; The base number of milliseconds to use in the
 *       exponential backoff for operation retries. Defaults to 100 ms for all services except
 *       DynamoDB, where it defaults to 50ms.
 *
 *     * **customBackoff ** [function] &mdash; A custom function that accepts a
 *       retry count and error and returns the amount of time to delay in
 *       milliseconds. If the result is a non-zero negative value, no further
 *       retry attempts will be made. The `base` option will be ignored if this
 *       option is supplied.
 *
 * @!attribute httpOptions
 *   @return [map] A set of options to pass to the low-level HTTP request.
 *     Currently supported options are:
 *
 *     * **proxy** [String] &mdash; the URL to proxy requests through
 *     * **agent** [http.Agent, https.Agent] &mdash; the Agent object to perform
 *       HTTP requests with. Used for connection pooling. Note that for
 *       SSL connections, a special Agent object is used in order to enable
 *       peer certificate verification. This feature is only supported in the
 *       Node.js environment.
 *     * **connectTimeout** [Integer] &mdash; Sets the socket to timeout after
 *       failing to establish a connection with the server after
 *       `connectTimeout` milliseconds. This timeout has no effect once a socket
 *       connection has been established.
 *     * **timeout** [Integer] &mdash; Sets the socket to timeout after timeout
 *       milliseconds of inactivity on the socket. Defaults to two minutes
 *       (120000)
 *     * **xhrAsync** [Boolean] &mdash; Whether the SDK will send asynchronous
 *       HTTP requests. Used in the browser environment only. Set to false to
 *       send requests synchronously. Defaults to true (async on).
 *     * **xhrWithCredentials** [Boolean] &mdash; Sets the "withCredentials"
 *       property of an XMLHttpRequest object. Used in the browser environment
 *       only. Defaults to false.
 * @!attribute logger
 *   @return [#write,#log] an object that responds to .write() (like a stream)
 *     or .log() (like the console object) in order to log information about
 *     requests
 *
 * @!attribute systemClockOffset
 *   @return [Number] an offset value in milliseconds to apply to all signing
 *     times. Use this to compensate for clock skew when your system may be
 *     out of sync with the service time. Note that this configuration option
 *     can only be applied to the global `AWS.config` object and cannot be
 *     overridden in service-specific configuration. Defaults to 0 milliseconds.
 *
 * @!attribute signatureVersion
 *   @return [String] the signature version to sign requests with (overriding
 *     the API configuration). Possible values are: 'v2', 'v3', 'v4'.
 *
 * @!attribute signatureCache
 *   @return [Boolean] whether the signature to sign requests with (overriding
 *     the API configuration) is cached. Only applies to the signature version 'v4'.
 *     Defaults to `true`.
 *
 * @!attribute endpointDiscoveryEnabled
 *   @return [Boolean] whether to enable endpoint discovery for operations that
 *     allow optionally using an endpoint returned by the service.
 *     Defaults to 'false'
 *
 * @!attribute endpointCacheSize
 *   @return [Number] the size of the global cache storing endpoints from endpoint
 *     discovery operations. Once endpoint cache is created, updating this setting
 *     cannot change existing cache size.
 *     Defaults to 1000
 *
 * @!attribute hostPrefixEnabled
 *   @return [Boolean] whether to marshal request parameters to the prefix of
 *     hostname. Defaults to `true`.
 *
 * @!attribute stsRegionalEndpoints
 *   @return ['legacy'|'regional'] whether to send sts request to global endpoints or
 *     regional endpoints.
 *     Defaults to 'legacy'
 */
AWS.Config = AWS.util.inherit({
  /**
   * @!endgroup
   */

  /**
   * Creates a new configuration object. This is the object that passes
   * option data along to service requests, including credentials, security,
   * region information, and some service specific settings.
   *
   * @example Creating a new configuration object with credentials and region
   *   var config = new AWS.Config({
   *     accessKeyId: 'AKID', secretAccessKey: 'SECRET', region: 'us-west-2'
   *   });
   * @option options accessKeyId [String] your AWS access key ID.
   * @option options secretAccessKey [String] your AWS secret access key.
   * @option options sessionToken [AWS.Credentials] the optional AWS
   *   session token to sign requests with.
   * @option options credentials [AWS.Credentials] the AWS credentials
   *   to sign requests with. You can either specify this object, or
   *   specify the accessKeyId and secretAccessKey options directly.
   * @option options credentialProvider [AWS.CredentialProviderChain] the
   *   provider chain used to resolve credentials if no static `credentials`
   *   property is set.
   * @option options region [String] the region to send service requests to.
   *   See {region} for more information.
   * @option options maxRetries [Integer] the maximum amount of retries to
   *   attempt with a request. See {maxRetries} for more information.
   * @option options maxRedirects [Integer] the maximum amount of redirects to
   *   follow with a request. See {maxRedirects} for more information.
   * @option options sslEnabled [Boolean] whether to enable SSL for
   *   requests.
   * @option options paramValidation [Boolean|map] whether input parameters
   *   should be validated against the operation description before sending
   *   the request. Defaults to true. Pass a map to enable any of the
   *   following specific validation features:
   *
   *   * **min** [Boolean] &mdash; Validates that a value meets the min
   *     constraint. This is enabled by default when paramValidation is set
   *     to `true`.
   *   * **max** [Boolean] &mdash; Validates that a value meets the max
   *     constraint.
   *   * **pattern** [Boolean] &mdash; Validates that a string value matches a
   *     regular expression.
   *   * **enum** [Boolean] &mdash; Validates that a string value matches one
   *     of the allowable enum values.
   * @option options computeChecksums [Boolean] whether to compute checksums
   *   for payload bodies when the service accepts it (currently supported
   *   in S3 only)
   * @option options convertResponseTypes [Boolean] whether types are converted
   *     when parsing response data. Currently only supported for JSON based
   *     services. Turning this off may improve performance on large response
   *     payloads. Defaults to `true`.
   * @option options correctClockSkew [Boolean] whether to apply a clock skew
   *     correction and retry requests that fail because of an skewed client
   *     clock. Defaults to `false`.
   * @option options s3ForcePathStyle [Boolean] whether to force path
   *   style URLs for S3 objects.
   * @option options s3BucketEndpoint [Boolean] whether the provided endpoint
   *   addresses an individual bucket (false if it addresses the root API
   *   endpoint). Note that setting this configuration option requires an
   *   `endpoint` to be provided explicitly to the service constructor.
   * @option options s3DisableBodySigning [Boolean] whether S3 body signing
   *   should be disabled when using signature version `v4`. Body signing
   *   can only be disabled when using https. Defaults to `true`.
   * @option options s3UsEast1RegionalEndpoint ['legacy'|'regional'] when region
   *   is set to 'us-east-1', whether to send s3 request to global endpoints or
   *   'us-east-1' regional endpoints. This config is only applicable to S3 client.
   *   Defaults to `legacy`
   * @option options s3UseArnRegion [Boolean] whether to override the request region
   *   with the region inferred from requested resource's ARN. Only available for S3 buckets
   *   Defaults to `true`
   *
   * @option options retryDelayOptions [map] A set of options to configure
   *   the retry delay on retryable errors. Currently supported options are:
   *
   *   * **base** [Integer] &mdash; The base number of milliseconds to use in the
   *     exponential backoff for operation retries. Defaults to 100 ms for all
   *     services except DynamoDB, where it defaults to 50ms.
   *   * **customBackoff ** [function] &mdash; A custom function that accepts a
   *     retry count and error and returns the amount of time to delay in
   *     milliseconds. If the result is a non-zero negative value, no further
   *     retry attempts will be made. The `base` option will be ignored if this
   *     option is supplied.
   * @option options httpOptions [map] A set of options to pass to the low-level
   *   HTTP request. Currently supported options are:
   *
   *   * **proxy** [String] &mdash; the URL to proxy requests through
   *   * **agent** [http.Agent, https.Agent] &mdash; the Agent object to perform
   *     HTTP requests with. Used for connection pooling. Defaults to the global
   *     agent (`http.globalAgent`) for non-SSL connections. Note that for
   *     SSL connections, a special Agent object is used in order to enable
   *     peer certificate verification. This feature is only available in the
   *     Node.js environment.
   *   * **connectTimeout** [Integer] &mdash; Sets the socket to timeout after
   *     failing to establish a connection with the server after
   *     `connectTimeout` milliseconds. This timeout has no effect once a socket
   *     connection has been established.
   *   * **timeout** [Integer] &mdash; Sets the socket to timeout after timeout
   *     milliseconds of inactivity on the socket. Defaults to two minutes
   *     (120000).
   *   * **xhrAsync** [Boolean] &mdash; Whether the SDK will send asynchronous
   *     HTTP requests. Used in the browser environment only. Set to false to
   *     send requests synchronously. Defaults to true (async on).
   *   * **xhrWithCredentials** [Boolean] &mdash; Sets the "withCredentials"
   *     property of an XMLHttpRequest object. Used in the browser environment
   *     only. Defaults to false.
   * @option options apiVersion [String, Date] a String in YYYY-MM-DD format
   *   (or a date) that represents the latest possible API version that can be
   *   used in all services (unless overridden by `apiVersions`). Specify
   *   'latest' to use the latest possible version.
   * @option options apiVersions [map<String, String|Date>] a map of service
   *   identifiers (the lowercase service class name) with the API version to
   *   use when instantiating a service. Specify 'latest' for each individual
   *   that can use the latest available version.
   * @option options logger [#write,#log] an object that responds to .write()
   *   (like a stream) or .log() (like the console object) in order to log
   *   information about requests
   * @option options systemClockOffset [Number] an offset value in milliseconds
   *   to apply to all signing times. Use this to compensate for clock skew
   *   when your system may be out of sync with the service time. Note that
   *   this configuration option can only be applied to the global `AWS.config`
   *   object and cannot be overridden in service-specific configuration.
   *   Defaults to 0 milliseconds.
   * @option options signatureVersion [String] the signature version to sign
   *   requests with (overriding the API configuration). Possible values are:
   *   'v2', 'v3', 'v4'.
   * @option options signatureCache [Boolean] whether the signature to sign
   *   requests with (overriding the API configuration) is cached. Only applies
   *   to the signature version 'v4'. Defaults to `true`.
   * @option options dynamoDbCrc32 [Boolean] whether to validate the CRC32
   *   checksum of HTTP response bodies returned by DynamoDB. Default: `true`.
   * @option options useAccelerateEndpoint [Boolean] Whether to use the
   *   S3 Transfer Acceleration endpoint with the S3 service. Default: `false`.
   * @option options clientSideMonitoring [Boolean] whether to collect and
   *   publish this client's performance metrics of all its API requests.
   * @option options endpointDiscoveryEnabled [Boolean] whether to enable endpoint
   *   discovery for operations that allow optionally using an endpoint returned by
   *   the service.
   *   Defaults to 'false'
   * @option options endpointCacheSize [Number] the size of the global cache storing
   *   endpoints from endpoint discovery operations. Once endpoint cache is created,
   *   updating this setting cannot change existing cache size.
   *   Defaults to 1000
   * @option options hostPrefixEnabled [Boolean] whether to marshal request
   *   parameters to the prefix of hostname.
   *   Defaults to `true`.
   * @option options stsRegionalEndpoints ['legacy'|'regional'] whether to send sts request
   *   to global endpoints or regional endpoints.
   *   Defaults to 'legacy'.
   */
  constructor: function Config(options) {
    if (options === undefined) options = {};
    options = this.extractCredentials(options);

    AWS.util.each.call(this, this.keys, function (key, value) {
      this.set(key, options[key], value);
    });
  },

  /**
   * @!group Managing Credentials
   */

  /**
   * Loads credentials from the configuration object. This is used internally
   * by the SDK to ensure that refreshable {Credentials} objects are properly
   * refreshed and loaded when sending a request. If you want to ensure that
   * your credentials are loaded prior to a request, you can use this method
   * directly to provide accurate credential data stored in the object.
   *
   * @note If you configure the SDK with static or environment credentials,
   *   the credential data should already be present in {credentials} attribute.
   *   This method is primarily necessary to load credentials from asynchronous
   *   sources, or sources that can refresh credentials periodically.
   * @example Getting your access key
   *   AWS.config.getCredentials(function(err) {
   *     if (err) console.log(err.stack); // credentials not loaded
   *     else console.log("Access Key:", AWS.config.credentials.accessKeyId);
   *   })
   * @callback callback function(err)
   *   Called when the {credentials} have been properly set on the configuration
   *   object.
   *
   *   @param err [Error] if this is set, credentials were not successfully
   *     loaded and this error provides information why.
   * @see credentials
   * @see Credentials
   */
  getCredentials: function getCredentials(callback) {
    var self = this;

    function finish(err) {
      callback(err, err ? null : self.credentials);
    }

    function credError(msg, err) {
      return new AWS.util.error(err || new Error(), {
        code: 'CredentialsError',
        message: msg,
        name: 'CredentialsError'
      });
    }

    function getAsyncCredentials() {
      self.credentials.get(function(err) {
        if (err) {
          var msg = 'Could not load credentials from ' +
            self.credentials.constructor.name;
          err = credError(msg, err);
        }
        finish(err);
      });
    }

    function getStaticCredentials() {
      var err = null;
      if (!self.credentials.accessKeyId || !self.credentials.secretAccessKey) {
        err = credError('Missing credentials');
      }
      finish(err);
    }

    if (self.credentials) {
      if (typeof self.credentials.get === 'function') {
        getAsyncCredentials();
      } else { // static credentials
        getStaticCredentials();
      }
    } else if (self.credentialProvider) {
      self.credentialProvider.resolve(function(err, creds) {
        if (err) {
          err = credError('Could not load credentials from any providers', err);
        }
        self.credentials = creds;
        finish(err);
      });
    } else {
      finish(credError('No credentials to load'));
    }
  },

  /**
   * @!group Loading and Setting Configuration Options
   */

  /**
   * @overload update(options, allowUnknownKeys = false)
   *   Updates the current configuration object with new options.
   *
   *   @example Update maxRetries property of a configuration object
   *     config.update({maxRetries: 10});
   *   @param [Object] options a map of option keys and values.
   *   @param [Boolean] allowUnknownKeys whether unknown keys can be set on
   *     the configuration object. Defaults to `false`.
   *   @see constructor
   */
  update: function update(options, allowUnknownKeys) {
    allowUnknownKeys = allowUnknownKeys || false;
    options = this.extractCredentials(options);
    AWS.util.each.call(this, options, function (key, value) {
      if (allowUnknownKeys || Object.prototype.hasOwnProperty.call(this.keys, key) ||
          AWS.Service.hasService(key)) {
        this.set(key, value);
      }
    });
  },

  /**
   * Loads configuration data from a JSON file into this config object.
   * @note Loading configuration will reset all existing configuration
   *   on the object.
   * @!macro nobrowser
   * @param path [String] the path relative to your process's current
   *    working directory to load configuration from.
   * @return [AWS.Config] the same configuration object
   */
  loadFromPath: function loadFromPath(path) {
    this.clear();

    var options = JSON.parse(AWS.util.readFileSync(path));
    var fileSystemCreds = new AWS.FileSystemCredentials(path);
    var chain = new AWS.CredentialProviderChain();
    chain.providers.unshift(fileSystemCreds);
    chain.resolve(function (err, creds) {
      if (err) throw err;
      else options.credentials = creds;
    });

    this.constructor(options);

    return this;
  },

  /**
   * Clears configuration data on this object
   *
   * @api private
   */
  clear: function clear() {
    /*jshint forin:false */
    AWS.util.each.call(this, this.keys, function (key) {
      delete this[key];
    });

    // reset credential provider
    this.set('credentials', undefined);
    this.set('credentialProvider', undefined);
  },

  /**
   * Sets a property on the configuration object, allowing for a
   * default value
   * @api private
   */
  set: function set(property, value, defaultValue) {
    if (value === undefined) {
      if (defaultValue === undefined) {
        defaultValue = this.keys[property];
      }
      if (typeof defaultValue === 'function') {
        this[property] = defaultValue.call(this);
      } else {
        this[property] = defaultValue;
      }
    } else if (property === 'httpOptions' && this[property]) {
      // deep merge httpOptions
      this[property] = AWS.util.merge(this[property], value);
    } else {
      this[property] = value;
    }
  },

  /**
   * All of the keys with their default values.
   *
   * @constant
   * @api private
   */
  keys: {
    credentials: null,
    credentialProvider: null,
    region: null,
    logger: null,
    apiVersions: {},
    apiVersion: null,
    endpoint: undefined,
    httpOptions: {
      timeout: 120000
    },
    maxRetries: undefined,
    maxRedirects: 10,
    paramValidation: true,
    sslEnabled: true,
    s3ForcePathStyle: false,
    s3BucketEndpoint: false,
    s3DisableBodySigning: true,
    s3UsEast1RegionalEndpoint: 'legacy',
    s3UseArnRegion: undefined,
    computeChecksums: true,
    convertResponseTypes: true,
    correctClockSkew: false,
    customUserAgent: null,
    dynamoDbCrc32: true,
    systemClockOffset: 0,
    signatureVersion: null,
    signatureCache: true,
    retryDelayOptions: {},
    useAccelerateEndpoint: false,
    clientSideMonitoring: false,
    endpointDiscoveryEnabled: false,
    endpointCacheSize: 1000,
    hostPrefixEnabled: true,
    stsRegionalEndpoints: 'legacy'
  },

  /**
   * Extracts accessKeyId, secretAccessKey and sessionToken
   * from a configuration hash.
   *
   * @api private
   */
  extractCredentials: function extractCredentials(options) {
    if (options.accessKeyId && options.secretAccessKey) {
      options = AWS.util.copy(options);
      options.credentials = new AWS.Credentials(options);
    }
    return options;
  },

  /**
   * Sets the promise dependency the SDK will use wherever Promises are returned.
   * Passing `null` will force the SDK to use native Promises if they are available.
   * If native Promises are not available, passing `null` will have no effect.
   * @param [Constructor] dep A reference to a Promise constructor
   */
  setPromisesDependency: function setPromisesDependency(dep) {
    PromisesDependency = dep;
    // if null was passed in, we should try to use native promises
    if (dep === null && typeof Promise === 'function') {
      PromisesDependency = Promise;
    }
    var constructors = [AWS.Request, AWS.Credentials, AWS.CredentialProviderChain];
    if (AWS.S3) {
      constructors.push(AWS.S3);
      if (AWS.S3.ManagedUpload) {
        constructors.push(AWS.S3.ManagedUpload);
      }
    }
    AWS.util.addPromises(constructors, PromisesDependency);
  },

  /**
   * Gets the promise dependency set by `AWS.config.setPromisesDependency`.
   */
  getPromisesDependency: function getPromisesDependency() {
    return PromisesDependency;
  }
});

/**
 * @return [AWS.Config] The global configuration object singleton instance
 * @readonly
 * @see AWS.Config
 */
AWS.config = new AWS.Config();


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var SequentialExecutor = __webpack_require__(34);
var DISCOVER_ENDPOINT = __webpack_require__(71).discoverEndpoint;
/**
 * The namespace used to register global event listeners for request building
 * and sending.
 */
AWS.EventListeners = {
  /**
   * @!attribute VALIDATE_CREDENTIALS
   *   A request listener that validates whether the request is being
   *   sent with credentials.
   *   Handles the {AWS.Request~validate 'validate' Request event}
   *   @example Sending a request without validating credentials
   *     var listener = AWS.EventListeners.Core.VALIDATE_CREDENTIALS;
   *     request.removeListener('validate', listener);
   *   @readonly
   *   @return [Function]
   * @!attribute VALIDATE_REGION
   *   A request listener that validates whether the region is set
   *   for a request.
   *   Handles the {AWS.Request~validate 'validate' Request event}
   *   @example Sending a request without validating region configuration
   *     var listener = AWS.EventListeners.Core.VALIDATE_REGION;
   *     request.removeListener('validate', listener);
   *   @readonly
   *   @return [Function]
   * @!attribute VALIDATE_PARAMETERS
   *   A request listener that validates input parameters in a request.
   *   Handles the {AWS.Request~validate 'validate' Request event}
   *   @example Sending a request without validating parameters
   *     var listener = AWS.EventListeners.Core.VALIDATE_PARAMETERS;
   *     request.removeListener('validate', listener);
   *   @example Disable parameter validation globally
   *     AWS.EventListeners.Core.removeListener('validate',
   *       AWS.EventListeners.Core.VALIDATE_REGION);
   *   @readonly
   *   @return [Function]
   * @!attribute SEND
   *   A request listener that initiates the HTTP connection for a
   *   request being sent. Handles the {AWS.Request~send 'send' Request event}
   *   @example Replacing the HTTP handler
   *     var listener = AWS.EventListeners.Core.SEND;
   *     request.removeListener('send', listener);
   *     request.on('send', function(response) {
   *       customHandler.send(response);
   *     });
   *   @return [Function]
   *   @readonly
   * @!attribute HTTP_DATA
   *   A request listener that reads data from the HTTP connection in order
   *   to build the response data.
   *   Handles the {AWS.Request~httpData 'httpData' Request event}.
   *   Remove this handler if you are overriding the 'httpData' event and
   *   do not want extra data processing and buffering overhead.
   *   @example Disabling default data processing
   *     var listener = AWS.EventListeners.Core.HTTP_DATA;
   *     request.removeListener('httpData', listener);
   *   @return [Function]
   *   @readonly
   */
  Core: {} /* doc hack */
};

/**
 * @api private
 */
function getOperationAuthtype(req) {
  if (!req.service.api.operations) {
    return '';
  }
  var operation = req.service.api.operations[req.operation];
  return operation ? operation.authtype : '';
}

AWS.EventListeners = {
  Core: new SequentialExecutor().addNamedListeners(function(add, addAsync) {
    addAsync('VALIDATE_CREDENTIALS', 'validate',
        function VALIDATE_CREDENTIALS(req, done) {
      if (!req.service.api.signatureVersion && !req.service.config.signatureVersion) return done(); // none
      req.service.config.getCredentials(function(err) {
        if (err) {
          req.response.error = AWS.util.error(err,
            {code: 'CredentialsError', message: 'Missing credentials in config'});
        }
        done();
      });
    });

    add('VALIDATE_REGION', 'validate', function VALIDATE_REGION(req) {
      if (!req.service.config.region && !req.service.isGlobalEndpoint) {
        req.response.error = AWS.util.error(new Error(),
          {code: 'ConfigError', message: 'Missing region in config'});
      }
    });

    add('BUILD_IDEMPOTENCY_TOKENS', 'validate', function BUILD_IDEMPOTENCY_TOKENS(req) {
      if (!req.service.api.operations) {
        return;
      }
      var operation = req.service.api.operations[req.operation];
      if (!operation) {
        return;
      }
      var idempotentMembers = operation.idempotentMembers;
      if (!idempotentMembers.length) {
        return;
      }
      // creates a copy of params so user's param object isn't mutated
      var params = AWS.util.copy(req.params);
      for (var i = 0, iLen = idempotentMembers.length; i < iLen; i++) {
        if (!params[idempotentMembers[i]]) {
          // add the member
          params[idempotentMembers[i]] = AWS.util.uuid.v4();
        }
      }
      req.params = params;
    });

    add('VALIDATE_PARAMETERS', 'validate', function VALIDATE_PARAMETERS(req) {
      if (!req.service.api.operations) {
        return;
      }
      var rules = req.service.api.operations[req.operation].input;
      var validation = req.service.config.paramValidation;
      new AWS.ParamValidator(validation).validate(rules, req.params);
    });

    addAsync('COMPUTE_SHA256', 'afterBuild', function COMPUTE_SHA256(req, done) {
      req.haltHandlersOnError();
      if (!req.service.api.operations) {
        return;
      }
      var operation = req.service.api.operations[req.operation];
      var authtype = operation ? operation.authtype : '';
      if (!req.service.api.signatureVersion && !authtype && !req.service.config.signatureVersion) return done(); // none
      if (req.service.getSignerClass(req) === AWS.Signers.V4) {
        var body = req.httpRequest.body || '';
        if (authtype.indexOf('unsigned-body') >= 0) {
          req.httpRequest.headers['X-Amz-Content-Sha256'] = 'UNSIGNED-PAYLOAD';
          return done();
        }
        AWS.util.computeSha256(body, function(err, sha) {
          if (err) {
            done(err);
          }
          else {
            req.httpRequest.headers['X-Amz-Content-Sha256'] = sha;
            done();
          }
        });
      } else {
        done();
      }
    });

    add('SET_CONTENT_LENGTH', 'afterBuild', function SET_CONTENT_LENGTH(req) {
      var authtype = getOperationAuthtype(req);
      var payloadMember = AWS.util.getRequestPayloadShape(req);
      if (req.httpRequest.headers['Content-Length'] === undefined) {
        try {
          var length = AWS.util.string.byteLength(req.httpRequest.body);
          req.httpRequest.headers['Content-Length'] = length;
        } catch (err) {
          if (payloadMember && payloadMember.isStreaming) {
            if (payloadMember.requiresLength) {
              //streaming payload requires length(s3, glacier)
              throw err;
            } else if (authtype.indexOf('unsigned-body') >= 0) {
              //unbounded streaming payload(lex, mediastore)
              req.httpRequest.headers['Transfer-Encoding'] = 'chunked';
              return;
            } else {
              throw err;
            }
          }
          throw err;
        }
      }
    });

    add('SET_HTTP_HOST', 'afterBuild', function SET_HTTP_HOST(req) {
      req.httpRequest.headers['Host'] = req.httpRequest.endpoint.host;
    });

    add('RESTART', 'restart', function RESTART() {
      var err = this.response.error;
      if (!err || !err.retryable) return;

      this.httpRequest = new AWS.HttpRequest(
        this.service.endpoint,
        this.service.region
      );

      if (this.response.retryCount < this.service.config.maxRetries) {
        this.response.retryCount++;
      } else {
        this.response.error = null;
      }
    });

    var addToHead = true;
    addAsync('DISCOVER_ENDPOINT', 'sign', DISCOVER_ENDPOINT, addToHead);

    addAsync('SIGN', 'sign', function SIGN(req, done) {
      var service = req.service;
      var operations = req.service.api.operations || {};
      var operation = operations[req.operation];
      var authtype = operation ? operation.authtype : '';
      if (!service.api.signatureVersion && !authtype && !service.config.signatureVersion) return done(); // none

      service.config.getCredentials(function (err, credentials) {
        if (err) {
          req.response.error = err;
          return done();
        }

        try {
          var date = service.getSkewCorrectedDate();
          var SignerClass = service.getSignerClass(req);
          var signer = new SignerClass(req.httpRequest,
            service.api.signingName || service.api.endpointPrefix,
            {
              signatureCache: service.config.signatureCache,
              operation: operation,
              signatureVersion: service.api.signatureVersion
            });
          signer.setServiceClientId(service._clientId);

          // clear old authorization headers
          delete req.httpRequest.headers['Authorization'];
          delete req.httpRequest.headers['Date'];
          delete req.httpRequest.headers['X-Amz-Date'];

          // add new authorization
          signer.addAuthorization(credentials, date);
          req.signedAt = date;
        } catch (e) {
          req.response.error = e;
        }
        done();
      });
    });

    add('VALIDATE_RESPONSE', 'validateResponse', function VALIDATE_RESPONSE(resp) {
      if (this.service.successfulResponse(resp, this)) {
        resp.data = {};
        resp.error = null;
      } else {
        resp.data = null;
        resp.error = AWS.util.error(new Error(),
          {code: 'UnknownError', message: 'An unknown error occurred.'});
      }
    });

    addAsync('SEND', 'send', function SEND(resp, done) {
      resp.httpResponse._abortCallback = done;
      resp.error = null;
      resp.data = null;

      function callback(httpResp) {
        resp.httpResponse.stream = httpResp;
        var stream = resp.request.httpRequest.stream;
        var service = resp.request.service;
        var api = service.api;
        var operationName = resp.request.operation;
        var operation = api.operations[operationName] || {};

        httpResp.on('headers', function onHeaders(statusCode, headers, statusMessage) {
          resp.request.emit(
            'httpHeaders',
            [statusCode, headers, resp, statusMessage]
          );

          if (!resp.httpResponse.streaming) {
            if (AWS.HttpClient.streamsApiVersion === 2) { // streams2 API check
              // if we detect event streams, we're going to have to
              // return the stream immediately
              if (operation.hasEventOutput && service.successfulResponse(resp)) {
                // skip reading the IncomingStream
                resp.request.emit('httpDone');
                done();
                return;
              }

              httpResp.on('readable', function onReadable() {
                var data = httpResp.read();
                if (data !== null) {
                  resp.request.emit('httpData', [data, resp]);
                }
              });
            } else { // legacy streams API
              httpResp.on('data', function onData(data) {
                resp.request.emit('httpData', [data, resp]);
              });
            }
          }
        });

        httpResp.on('end', function onEnd() {
          if (!stream || !stream.didCallback) {
            if (AWS.HttpClient.streamsApiVersion === 2 && (operation.hasEventOutput && service.successfulResponse(resp))) {
              // don't concatenate response chunks when streaming event stream data when response is successful
              return;
            }
            resp.request.emit('httpDone');
            done();
          }
        });
      }

      function progress(httpResp) {
        httpResp.on('sendProgress', function onSendProgress(value) {
          resp.request.emit('httpUploadProgress', [value, resp]);
        });

        httpResp.on('receiveProgress', function onReceiveProgress(value) {
          resp.request.emit('httpDownloadProgress', [value, resp]);
        });
      }

      function error(err) {
        if (err.code !== 'RequestAbortedError') {
          var errCode = err.code === 'TimeoutError' ? err.code : 'NetworkingError';
          err = AWS.util.error(err, {
            code: errCode,
            region: resp.request.httpRequest.region,
            hostname: resp.request.httpRequest.endpoint.hostname,
            retryable: true
          });
        }
        resp.error = err;
        resp.request.emit('httpError', [resp.error, resp], function() {
          done();
        });
      }

      function executeSend() {
        var http = AWS.HttpClient.getInstance();
        var httpOptions = resp.request.service.config.httpOptions || {};
        try {
          var stream = http.handleRequest(resp.request.httpRequest, httpOptions,
                                          callback, error);
          progress(stream);
        } catch (err) {
          error(err);
        }
      }
      var timeDiff = (resp.request.service.getSkewCorrectedDate() - this.signedAt) / 1000;
      if (timeDiff >= 60 * 10) { // if we signed 10min ago, re-sign
        this.emit('sign', [this], function(err) {
          if (err) done(err);
          else executeSend();
        });
      } else {
        executeSend();
      }
    });

    add('HTTP_HEADERS', 'httpHeaders',
        function HTTP_HEADERS(statusCode, headers, resp, statusMessage) {
      resp.httpResponse.statusCode = statusCode;
      resp.httpResponse.statusMessage = statusMessage;
      resp.httpResponse.headers = headers;
      resp.httpResponse.body = AWS.util.buffer.toBuffer('');
      resp.httpResponse.buffers = [];
      resp.httpResponse.numBytes = 0;
      var dateHeader = headers.date || headers.Date;
      var service = resp.request.service;
      if (dateHeader) {
        var serverTime = Date.parse(dateHeader);
        if (service.config.correctClockSkew
            && service.isClockSkewed(serverTime)) {
          service.applyClockOffset(serverTime);
        }
      }
    });

    add('HTTP_DATA', 'httpData', function HTTP_DATA(chunk, resp) {
      if (chunk) {
        if (AWS.util.isNode()) {
          resp.httpResponse.numBytes += chunk.length;

          var total = resp.httpResponse.headers['content-length'];
          var progress = { loaded: resp.httpResponse.numBytes, total: total };
          resp.request.emit('httpDownloadProgress', [progress, resp]);
        }

        resp.httpResponse.buffers.push(AWS.util.buffer.toBuffer(chunk));
      }
    });

    add('HTTP_DONE', 'httpDone', function HTTP_DONE(resp) {
      // convert buffers array into single buffer
      if (resp.httpResponse.buffers && resp.httpResponse.buffers.length > 0) {
        var body = AWS.util.buffer.concat(resp.httpResponse.buffers);
        resp.httpResponse.body = body;
      }
      delete resp.httpResponse.numBytes;
      delete resp.httpResponse.buffers;
    });

    add('FINALIZE_ERROR', 'retry', function FINALIZE_ERROR(resp) {
      if (resp.httpResponse.statusCode) {
        resp.error.statusCode = resp.httpResponse.statusCode;
        if (resp.error.retryable === undefined) {
          resp.error.retryable = this.service.retryableError(resp.error, this);
        }
      }
    });

    add('INVALIDATE_CREDENTIALS', 'retry', function INVALIDATE_CREDENTIALS(resp) {
      if (!resp.error) return;
      switch (resp.error.code) {
        case 'RequestExpired': // EC2 only
        case 'ExpiredTokenException':
        case 'ExpiredToken':
          resp.error.retryable = true;
          resp.request.service.config.credentials.expired = true;
      }
    });

    add('EXPIRED_SIGNATURE', 'retry', function EXPIRED_SIGNATURE(resp) {
      var err = resp.error;
      if (!err) return;
      if (typeof err.code === 'string' && typeof err.message === 'string') {
        if (err.code.match(/Signature/) && err.message.match(/expired/)) {
          resp.error.retryable = true;
        }
      }
    });

    add('CLOCK_SKEWED', 'retry', function CLOCK_SKEWED(resp) {
      if (!resp.error) return;
      if (this.service.clockSkewError(resp.error)
          && this.service.config.correctClockSkew) {
        resp.error.retryable = true;
      }
    });

    add('REDIRECT', 'retry', function REDIRECT(resp) {
      if (resp.error && resp.error.statusCode >= 300 &&
          resp.error.statusCode < 400 && resp.httpResponse.headers['location']) {
        this.httpRequest.endpoint =
          new AWS.Endpoint(resp.httpResponse.headers['location']);
        this.httpRequest.headers['Host'] = this.httpRequest.endpoint.host;
        resp.error.redirect = true;
        resp.error.retryable = true;
      }
    });

    add('RETRY_CHECK', 'retry', function RETRY_CHECK(resp) {
      if (resp.error) {
        if (resp.error.redirect && resp.redirectCount < resp.maxRedirects) {
          resp.error.retryDelay = 0;
        } else if (resp.retryCount < resp.maxRetries) {
          resp.error.retryDelay = this.service.retryDelays(resp.retryCount, resp.error) || 0;
        }
      }
    });

    addAsync('RESET_RETRY_STATE', 'afterRetry', function RESET_RETRY_STATE(resp, done) {
      var delay, willRetry = false;

      if (resp.error) {
        delay = resp.error.retryDelay || 0;
        if (resp.error.retryable && resp.retryCount < resp.maxRetries) {
          resp.retryCount++;
          willRetry = true;
        } else if (resp.error.redirect && resp.redirectCount < resp.maxRedirects) {
          resp.redirectCount++;
          willRetry = true;
        }
      }

      // delay < 0 is a signal from customBackoff to skip retries
      if (willRetry && delay >= 0) {
        resp.error = null;
        setTimeout(done, delay);
      } else {
        done();
      }
    });
  }),

  CorePost: new SequentialExecutor().addNamedListeners(function(add) {
    add('EXTRACT_REQUEST_ID', 'extractData', AWS.util.extractRequestId);
    add('EXTRACT_REQUEST_ID', 'extractError', AWS.util.extractRequestId);

    add('ENOTFOUND_ERROR', 'httpError', function ENOTFOUND_ERROR(err) {
      if (err.code === 'NetworkingError' && err.errno === 'ENOTFOUND') {
        var message = 'Inaccessible host: `' + err.hostname +
          '\'. This service may not be available in the `' + err.region +
          '\' region.';
        this.response.error = AWS.util.error(new Error(message), {
          code: 'UnknownEndpoint',
          region: err.region,
          hostname: err.hostname,
          retryable: true,
          originalError: err
        });
      }
    });
  }),

  Logger: new SequentialExecutor().addNamedListeners(function(add) {
    add('LOG_REQUEST', 'complete', function LOG_REQUEST(resp) {
      var req = resp.request;
      var logger = req.service.config.logger;
      if (!logger) return;
      function filterSensitiveLog(inputShape, shape) {
        if (!shape) {
          return shape;
        }
        switch (inputShape.type) {
          case 'structure':
            var struct = {};
            AWS.util.each(shape, function(subShapeName, subShape) {
              if (Object.prototype.hasOwnProperty.call(inputShape.members, subShapeName)) {
                struct[subShapeName] = filterSensitiveLog(inputShape.members[subShapeName], subShape);
              } else {
                struct[subShapeName] = subShape;
              }
            });
            return struct;
          case 'list':
            var list = [];
            AWS.util.arrayEach(shape, function(subShape, index) {
              list.push(filterSensitiveLog(inputShape.member, subShape));
            });
            return list;
          case 'map':
            var map = {};
            AWS.util.each(shape, function(key, value) {
              map[key] = filterSensitiveLog(inputShape.value, value);
            });
            return map;
          default:
            if (inputShape.isSensitive) {
              return '***SensitiveInformation***';
            } else {
              return shape;
            }
        }
      }

      function buildMessage() {
        var time = resp.request.service.getSkewCorrectedDate().getTime();
        var delta = (time - req.startTime.getTime()) / 1000;
        var ansi = logger.isTTY ? true : false;
        var status = resp.httpResponse.statusCode;
        var censoredParams = req.params;
        if (
          req.service.api.operations &&
              req.service.api.operations[req.operation] &&
              req.service.api.operations[req.operation].input
        ) {
          var inputShape = req.service.api.operations[req.operation].input;
          censoredParams = filterSensitiveLog(inputShape, req.params);
        }
        var params = __webpack_require__(72).inspect(censoredParams, true, null);
        var message = '';
        if (ansi) message += '\x1B[33m';
        message += '[AWS ' + req.service.serviceIdentifier + ' ' + status;
        message += ' ' + delta.toString() + 's ' + resp.retryCount + ' retries]';
        if (ansi) message += '\x1B[0;1m';
        message += ' ' + AWS.util.string.lowerFirst(req.operation);
        message += '(' + params + ')';
        if (ansi) message += '\x1B[0m';
        return message;
      }

      var line = buildMessage();
      if (typeof logger.log === 'function') {
        logger.log(line);
      } else if (typeof logger.write === 'function') {
        logger.write(line + '\n');
      }
    });
  }),

  Json: new SequentialExecutor().addNamedListeners(function(add) {
    var svc = __webpack_require__(16);
    add('BUILD', 'build', svc.buildRequest);
    add('EXTRACT_DATA', 'extractData', svc.extractData);
    add('EXTRACT_ERROR', 'extractError', svc.extractError);
  }),

  Rest: new SequentialExecutor().addNamedListeners(function(add) {
    var svc = __webpack_require__(13);
    add('BUILD', 'build', svc.buildRequest);
    add('EXTRACT_DATA', 'extractData', svc.extractData);
    add('EXTRACT_ERROR', 'extractError', svc.extractError);
  }),

  RestJson: new SequentialExecutor().addNamedListeners(function(add) {
    var svc = __webpack_require__(27);
    add('BUILD', 'build', svc.buildRequest);
    add('EXTRACT_DATA', 'extractData', svc.extractData);
    add('EXTRACT_ERROR', 'extractError', svc.extractError);
  }),

  RestXml: new SequentialExecutor().addNamedListeners(function(add) {
    var svc = __webpack_require__(28);
    add('BUILD', 'build', svc.buildRequest);
    add('EXTRACT_DATA', 'extractData', svc.extractData);
    add('EXTRACT_ERROR', 'extractError', svc.extractError);
  }),

  Query: new SequentialExecutor().addNamedListeners(function(add) {
    var svc = __webpack_require__(25);
    add('BUILD', 'build', svc.buildRequest);
    add('EXTRACT_DATA', 'extractData', svc.extractData);
    add('EXTRACT_ERROR', 'extractError', svc.extractError);
  })
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var util = __webpack_require__(2);
var endpointDiscoveryEnabledEnvs = ['AWS_ENABLE_ENDPOINT_DISCOVERY', 'AWS_ENDPOINT_DISCOVERY_ENABLED'];

/**
 * Generate key (except resources and operation part) to index the endpoints in the cache
 * If input shape has endpointdiscoveryid trait then use
 *   accessKey + operation + resources + region + service as cache key
 * If input shape doesn't have endpointdiscoveryid trait then use
 *   accessKey + region + service as cache key
 * @return [map<String,String>] object with keys to index endpoints.
 * @api private
 */
function getCacheKey(request) {
  var service = request.service;
  var api = service.api || {};
  var operations = api.operations;
  var identifiers = {};
  if (service.config.region) {
    identifiers.region = service.config.region;
  }
  if (api.serviceId) {
    identifiers.serviceId = api.serviceId;
  }
  if (service.config.credentials.accessKeyId) {
    identifiers.accessKeyId = service.config.credentials.accessKeyId;
  }
  return identifiers;
}

/**
 * Recursive helper for marshallCustomIdentifiers().
 * Looks for required string input members that have 'endpointdiscoveryid' trait.
 * @api private
 */
function marshallCustomIdentifiersHelper(result, params, shape) {
  if (!shape || params === undefined || params === null) return;
  if (shape.type === 'structure' && shape.required && shape.required.length > 0) {
    util.arrayEach(shape.required, function(name) {
      var memberShape = shape.members[name];
      if (memberShape.endpointDiscoveryId === true) {
        var locationName = memberShape.isLocationName ? memberShape.name : name;
        result[locationName] = String(params[name]);
      } else {
        marshallCustomIdentifiersHelper(result, params[name], memberShape);
      }
    });
  }
}

/**
 * Get custom identifiers for cache key.
 * Identifies custom identifiers by checking each shape's `endpointDiscoveryId` trait.
 * @param [object] request object
 * @param [object] input shape of the given operation's api
 * @api private
 */
function marshallCustomIdentifiers(request, shape) {
  var identifiers = {};
  marshallCustomIdentifiersHelper(identifiers, request.params, shape);
  return identifiers;
}

/**
 * Call endpoint discovery operation when it's optional.
 * When endpoint is available in cache then use the cached endpoints. If endpoints
 * are unavailable then use regional endpoints and call endpoint discovery operation
 * asynchronously. This is turned off by default.
 * @param [object] request object
 * @api private
 */
function optionalDiscoverEndpoint(request) {
  var service = request.service;
  var api = service.api;
  var operationModel = api.operations ? api.operations[request.operation] : undefined;
  var inputShape = operationModel ? operationModel.input : undefined;

  var identifiers = marshallCustomIdentifiers(request, inputShape);
  var cacheKey = getCacheKey(request);
  if (Object.keys(identifiers).length > 0) {
    cacheKey = util.update(cacheKey, identifiers);
    if (operationModel) cacheKey.operation = operationModel.name;
  }
  var endpoints = AWS.endpointCache.get(cacheKey);
  if (endpoints && endpoints.length === 1 && endpoints[0].Address === '') {
    //endpoint operation is being made but response not yet received
    //or endpoint operation just failed in 1 minute
    return;
  } else if (endpoints && endpoints.length > 0) {
    //found endpoint record from cache
    request.httpRequest.updateEndpoint(endpoints[0].Address);
  } else {
    //endpoint record not in cache or outdated. make discovery operation
    var endpointRequest = service.makeRequest(api.endpointOperation, {
      Operation: operationModel.name,
      Identifiers: identifiers,
    });
    addApiVersionHeader(endpointRequest);
    endpointRequest.removeListener('validate', AWS.EventListeners.Core.VALIDATE_PARAMETERS);
    endpointRequest.removeListener('retry', AWS.EventListeners.Core.RETRY_CHECK);
    //put in a placeholder for endpoints already requested, prevent
    //too much in-flight calls
    AWS.endpointCache.put(cacheKey, [{
      Address: '',
      CachePeriodInMinutes: 1
    }]);
    endpointRequest.send(function(err, data) {
      if (data && data.Endpoints) {
        AWS.endpointCache.put(cacheKey, data.Endpoints);
      } else if (err) {
        AWS.endpointCache.put(cacheKey, [{
          Address: '',
          CachePeriodInMinutes: 1 //not to make more endpoint operation in next 1 minute
        }]);
      }
    });
  }
}

var requestQueue = {};

/**
 * Call endpoint discovery operation when it's required.
 * When endpoint is available in cache then use cached ones. If endpoints are
 * unavailable then SDK should call endpoint operation then use returned new
 * endpoint for the api call. SDK will automatically attempt to do endpoint
 * discovery. This is turned off by default
 * @param [object] request object
 * @api private
 */
function requiredDiscoverEndpoint(request, done) {
  var service = request.service;
  var api = service.api;
  var operationModel = api.operations ? api.operations[request.operation] : undefined;
  var inputShape = operationModel ? operationModel.input : undefined;

  var identifiers = marshallCustomIdentifiers(request, inputShape);
  var cacheKey = getCacheKey(request);
  if (Object.keys(identifiers).length > 0) {
    cacheKey = util.update(cacheKey, identifiers);
    if (operationModel) cacheKey.operation = operationModel.name;
  }
  var cacheKeyStr = AWS.EndpointCache.getKeyString(cacheKey);
  var endpoints = AWS.endpointCache.get(cacheKeyStr); //endpoint cache also accepts string keys
  if (endpoints && endpoints.length === 1 && endpoints[0].Address === '') {
    //endpoint operation is being made but response not yet received
    //push request object to a pending queue
    if (!requestQueue[cacheKeyStr]) requestQueue[cacheKeyStr] = [];
    requestQueue[cacheKeyStr].push({request: request, callback: done});
    return;
  } else if (endpoints && endpoints.length > 0) {
    request.httpRequest.updateEndpoint(endpoints[0].Address);
    done();
  } else {
    var endpointRequest = service.makeRequest(api.endpointOperation, {
      Operation: operationModel.name,
      Identifiers: identifiers,
    });
    endpointRequest.removeListener('validate', AWS.EventListeners.Core.VALIDATE_PARAMETERS);
    addApiVersionHeader(endpointRequest);

    //put in a placeholder for endpoints already requested, prevent
    //too much in-flight calls
    AWS.endpointCache.put(cacheKeyStr, [{
      Address: '',
      CachePeriodInMinutes: 60 //long-live cache
    }]);
    endpointRequest.send(function(err, data) {
      if (err) {
        var errorParams = {
          code: 'EndpointDiscoveryException',
          message: 'Request cannot be fulfilled without specifying an endpoint',
          retryable: false
        };
        request.response.error = util.error(err, errorParams);
        AWS.endpointCache.remove(cacheKey);

        //fail all the pending requests in batch
        if (requestQueue[cacheKeyStr]) {
          var pendingRequests = requestQueue[cacheKeyStr];
          util.arrayEach(pendingRequests, function(requestContext) {
            requestContext.request.response.error = util.error(err, errorParams);
            requestContext.callback();
          });
          delete requestQueue[cacheKeyStr];
        }
      } else if (data) {
        AWS.endpointCache.put(cacheKeyStr, data.Endpoints);
        request.httpRequest.updateEndpoint(data.Endpoints[0].Address);

        //update the endpoint for all the pending requests in batch
        if (requestQueue[cacheKeyStr]) {
          var pendingRequests = requestQueue[cacheKeyStr];
          util.arrayEach(pendingRequests, function(requestContext) {
            requestContext.request.httpRequest.updateEndpoint(data.Endpoints[0].Address);
            requestContext.callback();
          });
          delete requestQueue[cacheKeyStr];
        }
      }
      done();
    });
  }
}

/**
 * add api version header to endpoint operation
 * @api private
 */
function addApiVersionHeader(endpointRequest) {
  var api = endpointRequest.service.api;
  var apiVersion = api.apiVersion;
  if (apiVersion && !endpointRequest.httpRequest.headers['x-amz-api-version']) {
    endpointRequest.httpRequest.headers['x-amz-api-version'] = apiVersion;
  }
}

/**
 * If api call gets invalid endpoint exception, SDK should attempt to remove the invalid
 * endpoint from cache.
 * @api private
 */
function invalidateCachedEndpoints(response) {
  var error = response.error;
  var httpResponse = response.httpResponse;
  if (error &&
    (error.code === 'InvalidEndpointException' || httpResponse.statusCode === 421)
  ) {
    var request = response.request;
    var operations = request.service.api.operations || {};
    var inputShape = operations[request.operation] ? operations[request.operation].input : undefined;
    var identifiers = marshallCustomIdentifiers(request, inputShape);
    var cacheKey = getCacheKey(request);
    if (Object.keys(identifiers).length > 0) {
      cacheKey = util.update(cacheKey, identifiers);
      if (operations[request.operation]) cacheKey.operation = operations[request.operation].name;
    }
    AWS.endpointCache.remove(cacheKey);
  }
}

/**
 * If endpoint is explicitly configured, SDK should not do endpoint discovery in anytime.
 * @param [object] client Service client object.
 * @api private
 */
function hasCustomEndpoint(client) {
  //if set endpoint is set for specific client, enable endpoint discovery will raise an error.
  if (client._originalConfig && client._originalConfig.endpoint && client._originalConfig.endpointDiscoveryEnabled === true) {
    throw util.error(new Error(), {
      code: 'ConfigurationException',
      message: 'Custom endpoint is supplied; endpointDiscoveryEnabled must not be true.'
    });
  };
  var svcConfig = AWS.config[client.serviceIdentifier] || {};
  return Boolean(AWS.config.endpoint || svcConfig.endpoint || (client._originalConfig && client._originalConfig.endpoint));
}

/**
 * @api private
 */
function isFalsy(value) {
  return ['false', '0'].indexOf(value) >= 0;
}

/**
 * If endpoint discovery should perform for this request when endpoint discovery is optional.
 * SDK performs config resolution in order like below:
 * 1. If turned on client configuration(default to off) then turn on endpoint discovery.
 * 2. If turned on in env AWS_ENABLE_ENDPOINT_DISCOVERY then turn on endpoint discovery.
 * 3. If turned on in shared ini config file with key 'endpoint_discovery_enabled', then
 *   turn on endpoint discovery.
 * @param [object] request request object.
 * @api private
 */
function isEndpointDiscoveryApplicable(request) {
  var service = request.service || {};
  if (service.config.endpointDiscoveryEnabled === true) return true;

  //shared ini file is only available in Node
  //not to check env in browser
  if (util.isBrowser()) return false;

  for (var i = 0; i < endpointDiscoveryEnabledEnvs.length; i++) {
    var env = endpointDiscoveryEnabledEnvs[i];
    if (Object.prototype.hasOwnProperty.call(Object({"NODE_ENV":undefined}), env)) {
      if (Object({"NODE_ENV":undefined})[env] === '' || Object({"NODE_ENV":undefined})[env] === undefined) {
        throw util.error(new Error(), {
          code: 'ConfigurationException',
          message: 'environmental variable ' + env + ' cannot be set to nothing'
        });
      }
      if (!isFalsy(Object({"NODE_ENV":undefined})[env])) return true;
    }
  }

  var configFile = {};
  try {
    configFile = AWS.util.iniLoader ? AWS.util.iniLoader.loadFrom({
      isConfig: true,
      filename: Object({"NODE_ENV":undefined})[AWS.util.sharedConfigFileEnv]
    }) : {};
  } catch (e) {}
  var sharedFileConfig = configFile[
    Object({"NODE_ENV":undefined}).AWS_PROFILE || AWS.util.defaultProfile
  ] || {};
  if (Object.prototype.hasOwnProperty.call(sharedFileConfig, 'endpoint_discovery_enabled')) {
    if (sharedFileConfig.endpoint_discovery_enabled === undefined) {
      throw util.error(new Error(), {
        code: 'ConfigurationException',
        message: 'config file entry \'endpoint_discovery_enabled\' cannot be set to nothing'
      });
    }
    if (!isFalsy(sharedFileConfig.endpoint_discovery_enabled)) return true;
  }
  return false;
}

/**
 * attach endpoint discovery logic to request object
 * @param [object] request
 * @api private
 */
function discoverEndpoint(request, done) {
  var service = request.service || {};
  if (hasCustomEndpoint(service) || request.isPresigned()) return done();

  if (!isEndpointDiscoveryApplicable(request)) return done();

  request.httpRequest.appendToUserAgent('endpoint-discovery');

  var operations = service.api.operations || {};
  var operationModel = operations[request.operation];
  var isEndpointDiscoveryRequired = operationModel ? operationModel.endpointDiscoveryRequired : 'NULL';
  switch (isEndpointDiscoveryRequired) {
    case 'OPTIONAL':
      optionalDiscoverEndpoint(request);
      request.addNamedListener('INVALIDATE_CACHED_ENDPOINTS', 'extractError', invalidateCachedEndpoints);
      done();
      break;
    case 'REQUIRED':
      request.addNamedListener('INVALIDATE_CACHED_ENDPOINTS', 'extractError', invalidateCachedEndpoints);
      requiredDiscoverEndpoint(request, done);
      break;
    case 'NULL':
    default:
      done();
      break;
  }
}

module.exports = {
  discoverEndpoint: discoverEndpoint,
  requiredDiscoverEndpoint: requiredDiscoverEndpoint,
  optionalDiscoverEndpoint: optionalDiscoverEndpoint,
  marshallCustomIdentifiers: marshallCustomIdentifiers,
  getCacheKey: getCacheKey,
  invalidateCachedEndpoint: invalidateCachedEndpoints,
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var getOwnPropertyDescriptors = Object.getOwnPropertyDescriptors ||
  function getOwnPropertyDescriptors(obj) {
    var keys = Object.keys(obj);
    var descriptors = {};
    for (var i = 0; i < keys.length; i++) {
      descriptors[keys[i]] = Object.getOwnPropertyDescriptor(obj, keys[i]);
    }
    return descriptors;
  };

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  if (typeof process !== 'undefined' && process.noDeprecation === true) {
    return fn;
  }

  // Allow for deprecating things in the process of starting up.
  if (typeof process === 'undefined') {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = Object({"NODE_ENV":undefined}).NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = __webpack_require__(73);

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = __webpack_require__(74);

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

var kCustomPromisifiedSymbol = typeof Symbol !== 'undefined' ? Symbol('util.promisify.custom') : undefined;

exports.promisify = function promisify(original) {
  if (typeof original !== 'function')
    throw new TypeError('The "original" argument must be of type Function');

  if (kCustomPromisifiedSymbol && original[kCustomPromisifiedSymbol]) {
    var fn = original[kCustomPromisifiedSymbol];
    if (typeof fn !== 'function') {
      throw new TypeError('The "util.promisify.custom" argument must be of type Function');
    }
    Object.defineProperty(fn, kCustomPromisifiedSymbol, {
      value: fn, enumerable: false, writable: false, configurable: true
    });
    return fn;
  }

  function fn() {
    var promiseResolve, promiseReject;
    var promise = new Promise(function (resolve, reject) {
      promiseResolve = resolve;
      promiseReject = reject;
    });

    var args = [];
    for (var i = 0; i < arguments.length; i++) {
      args.push(arguments[i]);
    }
    args.push(function (err, value) {
      if (err) {
        promiseReject(err);
      } else {
        promiseResolve(value);
      }
    });

    try {
      original.apply(this, args);
    } catch (err) {
      promiseReject(err);
    }

    return promise;
  }

  Object.setPrototypeOf(fn, Object.getPrototypeOf(original));

  if (kCustomPromisifiedSymbol) Object.defineProperty(fn, kCustomPromisifiedSymbol, {
    value: fn, enumerable: false, writable: false, configurable: true
  });
  return Object.defineProperties(
    fn,
    getOwnPropertyDescriptors(original)
  );
}

exports.promisify.custom = kCustomPromisifiedSymbol

function callbackifyOnRejected(reason, cb) {
  // `!reason` guard inspired by bluebird (Ref: https://goo.gl/t5IS6M).
  // Because `null` is a special error value in callbacks which means "no error
  // occurred", we error-wrap so the callback consumer can distinguish between
  // "the promise rejected with null" or "the promise fulfilled with undefined".
  if (!reason) {
    var newReason = new Error('Promise was rejected with a falsy value');
    newReason.reason = reason;
    reason = newReason;
  }
  return cb(reason);
}

function callbackify(original) {
  if (typeof original !== 'function') {
    throw new TypeError('The "original" argument must be of type Function');
  }

  // We DO NOT return the promise as it gives the user a false sense that
  // the promise is actually somehow related to the callback's execution
  // and that the callback throwing will reject the promise.
  function callbackified() {
    var args = [];
    for (var i = 0; i < arguments.length; i++) {
      args.push(arguments[i]);
    }

    var maybeCb = args.pop();
    if (typeof maybeCb !== 'function') {
      throw new TypeError('The last argument must be of type Function');
    }
    var self = this;
    var cb = function() {
      return maybeCb.apply(self, arguments);
    };
    // In true node style we process the callback on `nextTick` with all the
    // implications (stack, `uncaughtException`, `async_hooks`)
    original.apply(this, args)
      .then(function(ret) { process.nextTick(cb, null, ret) },
            function(rej) { process.nextTick(callbackifyOnRejected, rej, cb) });
  }

  Object.setPrototypeOf(callbackified, Object.getPrototypeOf(original));
  Object.defineProperties(callbackified,
                          getOwnPropertyDescriptors(original));
  return callbackified;
}
exports.callbackify = callbackify;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(7)))

/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}

/***/ }),
/* 74 */
/***/ (function(module, exports) {

if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {var AWS = __webpack_require__(1);
var AcceptorStateMachine = __webpack_require__(76);
var inherit = AWS.util.inherit;
var domain = AWS.util.domain;
var jmespath = __webpack_require__(20);

/**
 * @api private
 */
var hardErrorStates = {success: 1, error: 1, complete: 1};

function isTerminalState(machine) {
  return Object.prototype.hasOwnProperty.call(hardErrorStates, machine._asm.currentState);
}

var fsm = new AcceptorStateMachine();
fsm.setupStates = function() {
  var transition = function(_, done) {
    var self = this;
    self._haltHandlersOnError = false;

    self.emit(self._asm.currentState, function(err) {
      if (err) {
        if (isTerminalState(self)) {
          if (domain && self.domain instanceof domain.Domain) {
            err.domainEmitter = self;
            err.domain = self.domain;
            err.domainThrown = false;
            self.domain.emit('error', err);
          } else {
            throw err;
          }
        } else {
          self.response.error = err;
          done(err);
        }
      } else {
        done(self.response.error);
      }
    });

  };

  this.addState('validate', 'build', 'error', transition);
  this.addState('build', 'afterBuild', 'restart', transition);
  this.addState('afterBuild', 'sign', 'restart', transition);
  this.addState('sign', 'send', 'retry', transition);
  this.addState('retry', 'afterRetry', 'afterRetry', transition);
  this.addState('afterRetry', 'sign', 'error', transition);
  this.addState('send', 'validateResponse', 'retry', transition);
  this.addState('validateResponse', 'extractData', 'extractError', transition);
  this.addState('extractError', 'extractData', 'retry', transition);
  this.addState('extractData', 'success', 'retry', transition);
  this.addState('restart', 'build', 'error', transition);
  this.addState('success', 'complete', 'complete', transition);
  this.addState('error', 'complete', 'complete', transition);
  this.addState('complete', null, null, transition);
};
fsm.setupStates();

/**
 * ## Asynchronous Requests
 *
 * All requests made through the SDK are asynchronous and use a
 * callback interface. Each service method that kicks off a request
 * returns an `AWS.Request` object that you can use to register
 * callbacks.
 *
 * For example, the following service method returns the request
 * object as "request", which can be used to register callbacks:
 *
 * ```javascript
 * // request is an AWS.Request object
 * var request = ec2.describeInstances();
 *
 * // register callbacks on request to retrieve response data
 * request.on('success', function(response) {
 *   console.log(response.data);
 * });
 * ```
 *
 * When a request is ready to be sent, the {send} method should
 * be called:
 *
 * ```javascript
 * request.send();
 * ```
 *
 * Since registered callbacks may or may not be idempotent, requests should only
 * be sent once. To perform the same operation multiple times, you will need to
 * create multiple request objects, each with its own registered callbacks.
 *
 * ## Removing Default Listeners for Events
 *
 * Request objects are built with default listeners for the various events,
 * depending on the service type. In some cases, you may want to remove
 * some built-in listeners to customize behaviour. Doing this requires
 * access to the built-in listener functions, which are exposed through
 * the {AWS.EventListeners.Core} namespace. For instance, you may
 * want to customize the HTTP handler used when sending a request. In this
 * case, you can remove the built-in listener associated with the 'send'
 * event, the {AWS.EventListeners.Core.SEND} listener and add your own.
 *
 * ## Multiple Callbacks and Chaining
 *
 * You can register multiple callbacks on any request object. The
 * callbacks can be registered for different events, or all for the
 * same event. In addition, you can chain callback registration, for
 * example:
 *
 * ```javascript
 * request.
 *   on('success', function(response) {
 *     console.log("Success!");
 *   }).
 *   on('error', function(error, response) {
 *     console.log("Error!");
 *   }).
 *   on('complete', function(response) {
 *     console.log("Always!");
 *   }).
 *   send();
 * ```
 *
 * The above example will print either "Success! Always!", or "Error! Always!",
 * depending on whether the request succeeded or not.
 *
 * @!attribute httpRequest
 *   @readonly
 *   @!group HTTP Properties
 *   @return [AWS.HttpRequest] the raw HTTP request object
 *     containing request headers and body information
 *     sent by the service.
 *
 * @!attribute startTime
 *   @readonly
 *   @!group Operation Properties
 *   @return [Date] the time that the request started
 *
 * @!group Request Building Events
 *
 * @!event validate(request)
 *   Triggered when a request is being validated. Listeners
 *   should throw an error if the request should not be sent.
 *   @param request [Request] the request object being sent
 *   @see AWS.EventListeners.Core.VALIDATE_CREDENTIALS
 *   @see AWS.EventListeners.Core.VALIDATE_REGION
 *   @example Ensuring that a certain parameter is set before sending a request
 *     var req = s3.putObject(params);
 *     req.on('validate', function() {
 *       if (!req.params.Body.match(/^Hello\s/)) {
 *         throw new Error('Body must start with "Hello "');
 *       }
 *     });
 *     req.send(function(err, data) { ... });
 *
 * @!event build(request)
 *   Triggered when the request payload is being built. Listeners
 *   should fill the necessary information to send the request
 *   over HTTP.
 *   @param (see AWS.Request~validate)
 *   @example Add a custom HTTP header to a request
 *     var req = s3.putObject(params);
 *     req.on('build', function() {
 *       req.httpRequest.headers['Custom-Header'] = 'value';
 *     });
 *     req.send(function(err, data) { ... });
 *
 * @!event sign(request)
 *   Triggered when the request is being signed. Listeners should
 *   add the correct authentication headers and/or adjust the body,
 *   depending on the authentication mechanism being used.
 *   @param (see AWS.Request~validate)
 *
 * @!group Request Sending Events
 *
 * @!event send(response)
 *   Triggered when the request is ready to be sent. Listeners
 *   should call the underlying transport layer to initiate
 *   the sending of the request.
 *   @param response [Response] the response object
 *   @context [Request] the request object that was sent
 *   @see AWS.EventListeners.Core.SEND
 *
 * @!event retry(response)
 *   Triggered when a request failed and might need to be retried or redirected.
 *   If the response is retryable, the listener should set the
 *   `response.error.retryable` property to `true`, and optionally set
 *   `response.error.retryDelay` to the millisecond delay for the next attempt.
 *   In the case of a redirect, `response.error.redirect` should be set to
 *   `true` with `retryDelay` set to an optional delay on the next request.
 *
 *   If a listener decides that a request should not be retried,
 *   it should set both `retryable` and `redirect` to false.
 *
 *   Note that a retryable error will be retried at most
 *   {AWS.Config.maxRetries} times (based on the service object's config).
 *   Similarly, a request that is redirected will only redirect at most
 *   {AWS.Config.maxRedirects} times.
 *
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *   @example Adding a custom retry for a 404 response
 *     request.on('retry', function(response) {
 *       // this resource is not yet available, wait 10 seconds to get it again
 *       if (response.httpResponse.statusCode === 404 && response.error) {
 *         response.error.retryable = true;   // retry this error
 *         response.error.retryDelay = 10000; // wait 10 seconds
 *       }
 *     });
 *
 * @!group Data Parsing Events
 *
 * @!event extractError(response)
 *   Triggered on all non-2xx requests so that listeners can extract
 *   error details from the response body. Listeners to this event
 *   should set the `response.error` property.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @!event extractData(response)
 *   Triggered in successful requests to allow listeners to
 *   de-serialize the response body into `response.data`.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @!group Completion Events
 *
 * @!event success(response)
 *   Triggered when the request completed successfully.
 *   `response.data` will contain the response data and
 *   `response.error` will be null.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @!event error(error, response)
 *   Triggered when an error occurs at any point during the
 *   request. `response.error` will contain details about the error
 *   that occurred. `response.data` will be null.
 *   @param error [Error] the error object containing details about
 *     the error that occurred.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @!event complete(response)
 *   Triggered whenever a request cycle completes. `response.error`
 *   should be checked, since the request may have failed.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @!group HTTP Events
 *
 * @!event httpHeaders(statusCode, headers, response, statusMessage)
 *   Triggered when headers are sent by the remote server
 *   @param statusCode [Integer] the HTTP response code
 *   @param headers [map<String,String>] the response headers
 *   @param (see AWS.Request~send)
 *   @param statusMessage [String] A status message corresponding to the HTTP
 *                                 response code
 *   @context (see AWS.Request~send)
 *
 * @!event httpData(chunk, response)
 *   Triggered when data is sent by the remote server
 *   @param chunk [Buffer] the buffer data containing the next data chunk
 *     from the server
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *   @see AWS.EventListeners.Core.HTTP_DATA
 *
 * @!event httpUploadProgress(progress, response)
 *   Triggered when the HTTP request has uploaded more data
 *   @param progress [map] An object containing the `loaded` and `total` bytes
 *     of the request.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *   @note This event will not be emitted in Node.js 0.8.x.
 *
 * @!event httpDownloadProgress(progress, response)
 *   Triggered when the HTTP request has downloaded more data
 *   @param progress [map] An object containing the `loaded` and `total` bytes
 *     of the request.
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *   @note This event will not be emitted in Node.js 0.8.x.
 *
 * @!event httpError(error, response)
 *   Triggered when the HTTP request failed
 *   @param error [Error] the error object that was thrown
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @!event httpDone(response)
 *   Triggered when the server is finished sending data
 *   @param (see AWS.Request~send)
 *   @context (see AWS.Request~send)
 *
 * @see AWS.Response
 */
AWS.Request = inherit({

  /**
   * Creates a request for an operation on a given service with
   * a set of input parameters.
   *
   * @param service [AWS.Service] the service to perform the operation on
   * @param operation [String] the operation to perform on the service
   * @param params [Object] parameters to send to the operation.
   *   See the operation's documentation for the format of the
   *   parameters.
   */
  constructor: function Request(service, operation, params) {
    var endpoint = service.endpoint;
    var region = service.config.region;
    var customUserAgent = service.config.customUserAgent;

    // global endpoints sign as us-east-1
    if (service.isGlobalEndpoint) region = 'us-east-1';

    this.domain = domain && domain.active;
    this.service = service;
    this.operation = operation;
    this.params = params || {};
    this.httpRequest = new AWS.HttpRequest(endpoint, region);
    this.httpRequest.appendToUserAgent(customUserAgent);
    this.startTime = service.getSkewCorrectedDate();

    this.response = new AWS.Response(this);
    this._asm = new AcceptorStateMachine(fsm.states, 'validate');
    this._haltHandlersOnError = false;

    AWS.SequentialExecutor.call(this);
    this.emit = this.emitEvent;
  },

  /**
   * @!group Sending a Request
   */

  /**
   * @overload send(callback = null)
   *   Sends the request object.
   *
   *   @callback callback function(err, data)
   *     If a callback is supplied, it is called when a response is returned
   *     from the service.
   *     @context [AWS.Request] the request object being sent.
   *     @param err [Error] the error object returned from the request.
   *       Set to `null` if the request is successful.
   *     @param data [Object] the de-serialized data returned from
   *       the request. Set to `null` if a request error occurs.
   *   @example Sending a request with a callback
   *     request = s3.putObject({Bucket: 'bucket', Key: 'key'});
   *     request.send(function(err, data) { console.log(err, data); });
   *   @example Sending a request with no callback (using event handlers)
   *     request = s3.putObject({Bucket: 'bucket', Key: 'key'});
   *     request.on('complete', function(response) { ... }); // register a callback
   *     request.send();
   */
  send: function send(callback) {
    if (callback) {
      // append to user agent
      this.httpRequest.appendToUserAgent('callback');
      this.on('complete', function (resp) {
        callback.call(resp, resp.error, resp.data);
      });
    }
    this.runTo();

    return this.response;
  },

  /**
   * @!method  promise()
   *   Sends the request and returns a 'thenable' promise.
   *
   *   Two callbacks can be provided to the `then` method on the returned promise.
   *   The first callback will be called if the promise is fulfilled, and the second
   *   callback will be called if the promise is rejected.
   *   @callback fulfilledCallback function(data)
   *     Called if the promise is fulfilled.
   *     @param data [Object] the de-serialized data returned from the request.
   *   @callback rejectedCallback function(error)
   *     Called if the promise is rejected.
   *     @param error [Error] the error object returned from the request.
   *   @return [Promise] A promise that represents the state of the request.
   *   @example Sending a request using promises.
   *     var request = s3.putObject({Bucket: 'bucket', Key: 'key'});
   *     var result = request.promise();
   *     result.then(function(data) { ... }, function(error) { ... });
   */

  /**
   * @api private
   */
  build: function build(callback) {
    return this.runTo('send', callback);
  },

  /**
   * @api private
   */
  runTo: function runTo(state, done) {
    this._asm.runTo(state, done, this);
    return this;
  },

  /**
   * Aborts a request, emitting the error and complete events.
   *
   * @!macro nobrowser
   * @example Aborting a request after sending
   *   var params = {
   *     Bucket: 'bucket', Key: 'key',
   *     Body: Buffer.alloc(1024 * 1024 * 5) // 5MB payload
   *   };
   *   var request = s3.putObject(params);
   *   request.send(function (err, data) {
   *     if (err) console.log("Error:", err.code, err.message);
   *     else console.log(data);
   *   });
   *
   *   // abort request in 1 second
   *   setTimeout(request.abort.bind(request), 1000);
   *
   *   // prints "Error: RequestAbortedError Request aborted by user"
   * @return [AWS.Request] the same request object, for chaining.
   * @since v1.4.0
   */
  abort: function abort() {
    this.removeAllListeners('validateResponse');
    this.removeAllListeners('extractError');
    this.on('validateResponse', function addAbortedError(resp) {
      resp.error = AWS.util.error(new Error('Request aborted by user'), {
         code: 'RequestAbortedError', retryable: false
      });
    });

    if (this.httpRequest.stream && !this.httpRequest.stream.didCallback) { // abort HTTP stream
      this.httpRequest.stream.abort();
      if (this.httpRequest._abortCallback) {
         this.httpRequest._abortCallback();
      } else {
        this.removeAllListeners('send'); // haven't sent yet, so let's not
      }
    }

    return this;
  },

  /**
   * Iterates over each page of results given a pageable request, calling
   * the provided callback with each page of data. After all pages have been
   * retrieved, the callback is called with `null` data.
   *
   * @note This operation can generate multiple requests to a service.
   * @example Iterating over multiple pages of objects in an S3 bucket
   *   var pages = 1;
   *   s3.listObjects().eachPage(function(err, data) {
   *     if (err) return;
   *     console.log("Page", pages++);
   *     console.log(data);
   *   });
   * @example Iterating over multiple pages with an asynchronous callback
   *   s3.listObjects(params).eachPage(function(err, data, done) {
   *     doSomethingAsyncAndOrExpensive(function() {
   *       // The next page of results isn't fetched until done is called
   *       done();
   *     });
   *   });
   * @callback callback function(err, data, [doneCallback])
   *   Called with each page of resulting data from the request. If the
   *   optional `doneCallback` is provided in the function, it must be called
   *   when the callback is complete.
   *
   *   @param err [Error] an error object, if an error occurred.
   *   @param data [Object] a single page of response data. If there is no
   *     more data, this object will be `null`.
   *   @param doneCallback [Function] an optional done callback. If this
   *     argument is defined in the function declaration, it should be called
   *     when the next page is ready to be retrieved. This is useful for
   *     controlling serial pagination across asynchronous operations.
   *   @return [Boolean] if the callback returns `false`, pagination will
   *     stop.
   *
   * @see AWS.Request.eachItem
   * @see AWS.Response.nextPage
   * @since v1.4.0
   */
  eachPage: function eachPage(callback) {
    // Make all callbacks async-ish
    callback = AWS.util.fn.makeAsync(callback, 3);

    function wrappedCallback(response) {
      callback.call(response, response.error, response.data, function (result) {
        if (result === false) return;

        if (response.hasNextPage()) {
          response.nextPage().on('complete', wrappedCallback).send();
        } else {
          callback.call(response, null, null, AWS.util.fn.noop);
        }
      });
    }

    this.on('complete', wrappedCallback).send();
  },

  /**
   * Enumerates over individual items of a request, paging the responses if
   * necessary.
   *
   * @api experimental
   * @since v1.4.0
   */
  eachItem: function eachItem(callback) {
    var self = this;
    function wrappedCallback(err, data) {
      if (err) return callback(err, null);
      if (data === null) return callback(null, null);

      var config = self.service.paginationConfig(self.operation);
      var resultKey = config.resultKey;
      if (Array.isArray(resultKey)) resultKey = resultKey[0];
      var items = jmespath.search(data, resultKey);
      var continueIteration = true;
      AWS.util.arrayEach(items, function(item) {
        continueIteration = callback(null, item);
        if (continueIteration === false) {
          return AWS.util.abort;
        }
      });
      return continueIteration;
    }

    this.eachPage(wrappedCallback);
  },

  /**
   * @return [Boolean] whether the operation can return multiple pages of
   *   response data.
   * @see AWS.Response.eachPage
   * @since v1.4.0
   */
  isPageable: function isPageable() {
    return this.service.paginationConfig(this.operation) ? true : false;
  },

  /**
   * Sends the request and converts the request object into a readable stream
   * that can be read from or piped into a writable stream.
   *
   * @note The data read from a readable stream contains only
   *   the raw HTTP body contents.
   * @example Manually reading from a stream
   *   request.createReadStream().on('data', function(data) {
   *     console.log("Got data:", data.toString());
   *   });
   * @example Piping a request body into a file
   *   var out = fs.createWriteStream('/path/to/outfile.jpg');
   *   s3.service.getObject(params).createReadStream().pipe(out);
   * @return [Stream] the readable stream object that can be piped
   *   or read from (by registering 'data' event listeners).
   * @!macro nobrowser
   */
  createReadStream: function createReadStream() {
    var streams = AWS.util.stream;
    var req = this;
    var stream = null;

    if (AWS.HttpClient.streamsApiVersion === 2) {
      stream = new streams.PassThrough();
      process.nextTick(function() { req.send(); });
    } else {
      stream = new streams.Stream();
      stream.readable = true;

      stream.sent = false;
      stream.on('newListener', function(event) {
        if (!stream.sent && event === 'data') {
          stream.sent = true;
          process.nextTick(function() { req.send(); });
        }
      });
    }

    this.on('error', function(err) {
      stream.emit('error', err);
    });

    this.on('httpHeaders', function streamHeaders(statusCode, headers, resp) {
      if (statusCode < 300) {
        req.removeListener('httpData', AWS.EventListeners.Core.HTTP_DATA);
        req.removeListener('httpError', AWS.EventListeners.Core.HTTP_ERROR);
        req.on('httpError', function streamHttpError(error) {
          resp.error = error;
          resp.error.retryable = false;
        });

        var shouldCheckContentLength = false;
        var expectedLen;
        if (req.httpRequest.method !== 'HEAD') {
          expectedLen = parseInt(headers['content-length'], 10);
        }
        if (expectedLen !== undefined && !isNaN(expectedLen) && expectedLen >= 0) {
          shouldCheckContentLength = true;
          var receivedLen = 0;
        }

        var checkContentLengthAndEmit = function checkContentLengthAndEmit() {
          if (shouldCheckContentLength && receivedLen !== expectedLen) {
            stream.emit('error', AWS.util.error(
              new Error('Stream content length mismatch. Received ' +
                receivedLen + ' of ' + expectedLen + ' bytes.'),
              { code: 'StreamContentLengthMismatch' }
            ));
          } else if (AWS.HttpClient.streamsApiVersion === 2) {
            stream.end();
          } else {
            stream.emit('end');
          }
        };

        var httpStream = resp.httpResponse.createUnbufferedStream();

        if (AWS.HttpClient.streamsApiVersion === 2) {
          if (shouldCheckContentLength) {
            var lengthAccumulator = new streams.PassThrough();
            lengthAccumulator._write = function(chunk) {
              if (chunk && chunk.length) {
                receivedLen += chunk.length;
              }
              return streams.PassThrough.prototype._write.apply(this, arguments);
            };

            lengthAccumulator.on('end', checkContentLengthAndEmit);
            stream.on('error', function(err) {
              shouldCheckContentLength = false;
              httpStream.unpipe(lengthAccumulator);
              lengthAccumulator.emit('end');
              lengthAccumulator.end();
            });
            httpStream.pipe(lengthAccumulator).pipe(stream, { end: false });
          } else {
            httpStream.pipe(stream);
          }
        } else {

          if (shouldCheckContentLength) {
            httpStream.on('data', function(arg) {
              if (arg && arg.length) {
                receivedLen += arg.length;
              }
            });
          }

          httpStream.on('data', function(arg) {
            stream.emit('data', arg);
          });
          httpStream.on('end', checkContentLengthAndEmit);
        }

        httpStream.on('error', function(err) {
          shouldCheckContentLength = false;
          stream.emit('error', err);
        });
      }
    });

    return stream;
  },

  /**
   * @param [Array,Response] args This should be the response object,
   *   or an array of args to send to the event.
   * @api private
   */
  emitEvent: function emit(eventName, args, done) {
    if (typeof args === 'function') { done = args; args = null; }
    if (!done) done = function() { };
    if (!args) args = this.eventParameters(eventName, this.response);

    var origEmit = AWS.SequentialExecutor.prototype.emit;
    origEmit.call(this, eventName, args, function (err) {
      if (err) this.response.error = err;
      done.call(this, err);
    });
  },

  /**
   * @api private
   */
  eventParameters: function eventParameters(eventName) {
    switch (eventName) {
      case 'restart':
      case 'validate':
      case 'sign':
      case 'build':
      case 'afterValidate':
      case 'afterBuild':
        return [this];
      case 'error':
        return [this.response.error, this.response];
      default:
        return [this.response];
    }
  },

  /**
   * @api private
   */
  presign: function presign(expires, callback) {
    if (!callback && typeof expires === 'function') {
      callback = expires;
      expires = null;
    }
    return new AWS.Signers.Presign().sign(this.toGet(), expires, callback);
  },

  /**
   * @api private
   */
  isPresigned: function isPresigned() {
    return Object.prototype.hasOwnProperty.call(this.httpRequest.headers, 'presigned-expires');
  },

  /**
   * @api private
   */
  toUnauthenticated: function toUnauthenticated() {
    this._unAuthenticated = true;
    this.removeListener('validate', AWS.EventListeners.Core.VALIDATE_CREDENTIALS);
    this.removeListener('sign', AWS.EventListeners.Core.SIGN);
    return this;
  },

  /**
   * @api private
   */
  toGet: function toGet() {
    if (this.service.api.protocol === 'query' ||
        this.service.api.protocol === 'ec2') {
      this.removeListener('build', this.buildAsGet);
      this.addListener('build', this.buildAsGet);
    }
    return this;
  },

  /**
   * @api private
   */
  buildAsGet: function buildAsGet(request) {
    request.httpRequest.method = 'GET';
    request.httpRequest.path = request.service.endpoint.path +
                               '?' + request.httpRequest.body;
    request.httpRequest.body = '';

    // don't need these headers on a GET request
    delete request.httpRequest.headers['Content-Length'];
    delete request.httpRequest.headers['Content-Type'];
  },

  /**
   * @api private
   */
  haltHandlersOnError: function haltHandlersOnError() {
    this._haltHandlersOnError = true;
  }
});

/**
 * @api private
 */
AWS.Request.addPromisesToClass = function addPromisesToClass(PromiseDependency) {
  this.prototype.promise = function promise() {
    var self = this;
    // append to user agent
    this.httpRequest.appendToUserAgent('promise');
    return new PromiseDependency(function(resolve, reject) {
      self.on('complete', function(resp) {
        if (resp.error) {
          reject(resp.error);
        } else {
          // define $response property so that it is not enumberable
          // this prevents circular reference errors when stringifying the JSON object
          resolve(Object.defineProperty(
            resp.data || {},
            '$response',
            {value: resp}
          ));
        }
      });
      self.runTo();
    });
  };
};

/**
 * @api private
 */
AWS.Request.deletePromisesFromClass = function deletePromisesFromClass() {
  delete this.prototype.promise;
};

AWS.util.addPromises(AWS.Request);

AWS.util.mixin(AWS.Request, AWS.SequentialExecutor);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(7)))

/***/ }),
/* 76 */
/***/ (function(module, exports) {

function AcceptorStateMachine(states, state) {
  this.currentState = state || null;
  this.states = states || {};
}

AcceptorStateMachine.prototype.runTo = function runTo(finalState, done, bindObject, inputError) {
  if (typeof finalState === 'function') {
    inputError = bindObject; bindObject = done;
    done = finalState; finalState = null;
  }

  var self = this;
  var state = self.states[self.currentState];
  state.fn.call(bindObject || self, inputError, function(err) {
    if (err) {
      if (state.fail) self.currentState = state.fail;
      else return done ? done.call(bindObject, err) : null;
    } else {
      if (state.accept) self.currentState = state.accept;
      else return done ? done.call(bindObject) : null;
    }
    if (self.currentState === finalState) {
      return done ? done.call(bindObject, err) : null;
    }

    self.runTo(finalState, done, bindObject, err);
  });
};

AcceptorStateMachine.prototype.addState = function addState(name, acceptState, failState, fn) {
  if (typeof acceptState === 'function') {
    fn = acceptState; acceptState = null; failState = null;
  } else if (typeof failState === 'function') {
    fn = failState; failState = null;
  }

  if (!this.currentState) this.currentState = name;
  this.states[name] = { accept: acceptState, fail: failState, fn: fn };
  return this;
};

/**
 * @api private
 */
module.exports = AcceptorStateMachine;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;
var jmespath = __webpack_require__(20);

/**
 * This class encapsulates the response information
 * from a service request operation sent through {AWS.Request}.
 * The response object has two main properties for getting information
 * back from a request:
 *
 * ## The `data` property
 *
 * The `response.data` property contains the serialized object data
 * retrieved from the service request. For instance, for an
 * Amazon DynamoDB `listTables` method call, the response data might
 * look like:
 *
 * ```
 * > resp.data
 * { TableNames:
 *    [ 'table1', 'table2', ... ] }
 * ```
 *
 * The `data` property can be null if an error occurs (see below).
 *
 * ## The `error` property
 *
 * In the event of a service error (or transfer error), the
 * `response.error` property will be filled with the given
 * error data in the form:
 *
 * ```
 * { code: 'SHORT_UNIQUE_ERROR_CODE',
 *   message: 'Some human readable error message' }
 * ```
 *
 * In the case of an error, the `data` property will be `null`.
 * Note that if you handle events that can be in a failure state,
 * you should always check whether `response.error` is set
 * before attempting to access the `response.data` property.
 *
 * @!attribute data
 *   @readonly
 *   @!group Data Properties
 *   @note Inside of a {AWS.Request~httpData} event, this
 *     property contains a single raw packet instead of the
 *     full de-serialized service response.
 *   @return [Object] the de-serialized response data
 *     from the service.
 *
 * @!attribute error
 *   An structure containing information about a service
 *   or networking error.
 *   @readonly
 *   @!group Data Properties
 *   @note This attribute is only filled if a service or
 *     networking error occurs.
 *   @return [Error]
 *     * code [String] a unique short code representing the
 *       error that was emitted.
 *     * message [String] a longer human readable error message
 *     * retryable [Boolean] whether the error message is
 *       retryable.
 *     * statusCode [Numeric] in the case of a request that reached the service,
 *       this value contains the response status code.
 *     * time [Date] the date time object when the error occurred.
 *     * hostname [String] set when a networking error occurs to easily
 *       identify the endpoint of the request.
 *     * region [String] set when a networking error occurs to easily
 *       identify the region of the request.
 *
 * @!attribute requestId
 *   @readonly
 *   @!group Data Properties
 *   @return [String] the unique request ID associated with the response.
 *     Log this value when debugging requests for AWS support.
 *
 * @!attribute retryCount
 *   @readonly
 *   @!group Operation Properties
 *   @return [Integer] the number of retries that were
 *     attempted before the request was completed.
 *
 * @!attribute redirectCount
 *   @readonly
 *   @!group Operation Properties
 *   @return [Integer] the number of redirects that were
 *     followed before the request was completed.
 *
 * @!attribute httpResponse
 *   @readonly
 *   @!group HTTP Properties
 *   @return [AWS.HttpResponse] the raw HTTP response object
 *     containing the response headers and body information
 *     from the server.
 *
 * @see AWS.Request
 */
AWS.Response = inherit({

  /**
   * @api private
   */
  constructor: function Response(request) {
    this.request = request;
    this.data = null;
    this.error = null;
    this.retryCount = 0;
    this.redirectCount = 0;
    this.httpResponse = new AWS.HttpResponse();
    if (request) {
      this.maxRetries = request.service.numRetries();
      this.maxRedirects = request.service.config.maxRedirects;
    }
  },

  /**
   * Creates a new request for the next page of response data, calling the
   * callback with the page data if a callback is provided.
   *
   * @callback callback function(err, data)
   *   Called when a page of data is returned from the next request.
   *
   *   @param err [Error] an error object, if an error occurred in the request
   *   @param data [Object] the next page of data, or null, if there are no
   *     more pages left.
   * @return [AWS.Request] the request object for the next page of data
   * @return [null] if no callback is provided and there are no pages left
   *   to retrieve.
   * @since v1.4.0
   */
  nextPage: function nextPage(callback) {
    var config;
    var service = this.request.service;
    var operation = this.request.operation;
    try {
      config = service.paginationConfig(operation, true);
    } catch (e) { this.error = e; }

    if (!this.hasNextPage()) {
      if (callback) callback(this.error, null);
      else if (this.error) throw this.error;
      return null;
    }

    var params = AWS.util.copy(this.request.params);
    if (!this.nextPageTokens) {
      return callback ? callback(null, null) : null;
    } else {
      var inputTokens = config.inputToken;
      if (typeof inputTokens === 'string') inputTokens = [inputTokens];
      for (var i = 0; i < inputTokens.length; i++) {
        params[inputTokens[i]] = this.nextPageTokens[i];
      }
      return service.makeRequest(this.request.operation, params, callback);
    }
  },

  /**
   * @return [Boolean] whether more pages of data can be returned by further
   *   requests
   * @since v1.4.0
   */
  hasNextPage: function hasNextPage() {
    this.cacheNextPageTokens();
    if (this.nextPageTokens) return true;
    if (this.nextPageTokens === undefined) return undefined;
    else return false;
  },

  /**
   * @api private
   */
  cacheNextPageTokens: function cacheNextPageTokens() {
    if (Object.prototype.hasOwnProperty.call(this, 'nextPageTokens')) return this.nextPageTokens;
    this.nextPageTokens = undefined;

    var config = this.request.service.paginationConfig(this.request.operation);
    if (!config) return this.nextPageTokens;

    this.nextPageTokens = null;
    if (config.moreResults) {
      if (!jmespath.search(this.data, config.moreResults)) {
        return this.nextPageTokens;
      }
    }

    var exprs = config.outputToken;
    if (typeof exprs === 'string') exprs = [exprs];
    AWS.util.arrayEach.call(this, exprs, function (expr) {
      var output = jmespath.search(this.data, expr);
      if (output) {
        this.nextPageTokens = this.nextPageTokens || [];
        this.nextPageTokens.push(output);
      }
    });

    return this.nextPageTokens;
  }

});


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright 2012-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"). You
 * may not use this file except in compliance with the License. A copy of
 * the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;
var jmespath = __webpack_require__(20);

/**
 * @api private
 */
function CHECK_ACCEPTORS(resp) {
  var waiter = resp.request._waiter;
  var acceptors = waiter.config.acceptors;
  var acceptorMatched = false;
  var state = 'retry';

  acceptors.forEach(function(acceptor) {
    if (!acceptorMatched) {
      var matcher = waiter.matchers[acceptor.matcher];
      if (matcher && matcher(resp, acceptor.expected, acceptor.argument)) {
        acceptorMatched = true;
        state = acceptor.state;
      }
    }
  });

  if (!acceptorMatched && resp.error) state = 'failure';

  if (state === 'success') {
    waiter.setSuccess(resp);
  } else {
    waiter.setError(resp, state === 'retry');
  }
}

/**
 * @api private
 */
AWS.ResourceWaiter = inherit({
  /**
   * Waits for a given state on a service object
   * @param service [Service] the service object to wait on
   * @param state [String] the state (defined in waiter configuration) to wait
   *   for.
   * @example Create a waiter for running EC2 instances
   *   var ec2 = new AWS.EC2;
   *   var waiter = new AWS.ResourceWaiter(ec2, 'instanceRunning');
   */
  constructor: function constructor(service, state) {
    this.service = service;
    this.state = state;
    this.loadWaiterConfig(this.state);
  },

  service: null,

  state: null,

  config: null,

  matchers: {
    path: function(resp, expected, argument) {
      try {
        var result = jmespath.search(resp.data, argument);
      } catch (err) {
        return false;
      }

      return jmespath.strictDeepEqual(result,expected);
    },

    pathAll: function(resp, expected, argument) {
      try {
        var results = jmespath.search(resp.data, argument);
      } catch (err) {
        return false;
      }

      if (!Array.isArray(results)) results = [results];
      var numResults = results.length;
      if (!numResults) return false;
      for (var ind = 0 ; ind < numResults; ind++) {
        if (!jmespath.strictDeepEqual(results[ind], expected)) {
          return false;
        }
      }
      return true;
    },

    pathAny: function(resp, expected, argument) {
      try {
        var results = jmespath.search(resp.data, argument);
      } catch (err) {
        return false;
      }

      if (!Array.isArray(results)) results = [results];
      var numResults = results.length;
      for (var ind = 0 ; ind < numResults; ind++) {
        if (jmespath.strictDeepEqual(results[ind], expected)) {
          return true;
        }
      }
      return false;
    },

    status: function(resp, expected) {
      var statusCode = resp.httpResponse.statusCode;
      return (typeof statusCode === 'number') && (statusCode === expected);
    },

    error: function(resp, expected) {
      if (typeof expected === 'string' && resp.error) {
        return expected === resp.error.code;
      }
      // if expected is not string, can be boolean indicating presence of error
      return expected === !!resp.error;
    }
  },

  listeners: new AWS.SequentialExecutor().addNamedListeners(function(add) {
    add('RETRY_CHECK', 'retry', function(resp) {
      var waiter = resp.request._waiter;
      if (resp.error && resp.error.code === 'ResourceNotReady') {
        resp.error.retryDelay = (waiter.config.delay || 0) * 1000;
      }
    });

    add('CHECK_OUTPUT', 'extractData', CHECK_ACCEPTORS);

    add('CHECK_ERROR', 'extractError', CHECK_ACCEPTORS);
  }),

  /**
   * @return [AWS.Request]
   */
  wait: function wait(params, callback) {
    if (typeof params === 'function') {
      callback = params; params = undefined;
    }

    if (params && params.$waiter) {
      params = AWS.util.copy(params);
      if (typeof params.$waiter.delay === 'number') {
        this.config.delay = params.$waiter.delay;
      }
      if (typeof params.$waiter.maxAttempts === 'number') {
        this.config.maxAttempts = params.$waiter.maxAttempts;
      }
      delete params.$waiter;
    }

    var request = this.service.makeRequest(this.config.operation, params);
    request._waiter = this;
    request.response.maxRetries = this.config.maxAttempts;
    request.addListeners(this.listeners);

    if (callback) request.send(callback);
    return request;
  },

  setSuccess: function setSuccess(resp) {
    resp.error = null;
    resp.data = resp.data || {};
    resp.request.removeAllListeners('extractData');
  },

  setError: function setError(resp, retryable) {
    resp.data = null;
    resp.error = AWS.util.error(resp.error || new Error(), {
      code: 'ResourceNotReady',
      message: 'Resource is not in the state ' + this.state,
      retryable: retryable
    });
  },

  /**
   * Loads waiter configuration from API configuration
   *
   * @api private
   */
  loadWaiterConfig: function loadWaiterConfig(state) {
    if (!this.service.api.waiters[state]) {
      throw new AWS.util.error(new Error(), {
        code: 'StateNotFoundError',
        message: 'State ' + state + ' not found.'
      });
    }

    this.config = AWS.util.copy(this.service.api.waiters[state]);
  }
});


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

var inherit = AWS.util.inherit;

/**
 * @api private
 */
AWS.Signers.RequestSigner = inherit({
  constructor: function RequestSigner(request) {
    this.request = request;
  },

  setServiceClientId: function setServiceClientId(id) {
    this.serviceClientId = id;
  },

  getServiceClientId: function getServiceClientId() {
    return this.serviceClientId;
  }
});

AWS.Signers.RequestSigner.getVersion = function getVersion(version) {
  switch (version) {
    case 'v2': return AWS.Signers.V2;
    case 'v3': return AWS.Signers.V3;
    case 's3v4': return AWS.Signers.V4;
    case 'v4': return AWS.Signers.V4;
    case 's3': return AWS.Signers.S3;
    case 'v3https': return AWS.Signers.V3Https;
  }
  throw new Error('Unknown signing version ' + version);
};

__webpack_require__(80);
__webpack_require__(38);
__webpack_require__(81);
__webpack_require__(82);
__webpack_require__(84);
__webpack_require__(85);


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;

/**
 * @api private
 */
AWS.Signers.V2 = inherit(AWS.Signers.RequestSigner, {
  addAuthorization: function addAuthorization(credentials, date) {

    if (!date) date = AWS.util.date.getDate();

    var r = this.request;

    r.params.Timestamp = AWS.util.date.iso8601(date);
    r.params.SignatureVersion = '2';
    r.params.SignatureMethod = 'HmacSHA256';
    r.params.AWSAccessKeyId = credentials.accessKeyId;

    if (credentials.sessionToken) {
      r.params.SecurityToken = credentials.sessionToken;
    }

    delete r.params.Signature; // delete old Signature for re-signing
    r.params.Signature = this.signature(credentials);

    r.body = AWS.util.queryParamsToString(r.params);
    r.headers['Content-Length'] = r.body.length;
  },

  signature: function signature(credentials) {
    return AWS.util.crypto.hmac(credentials.secretAccessKey, this.stringToSign(), 'base64');
  },

  stringToSign: function stringToSign() {
    var parts = [];
    parts.push(this.request.method);
    parts.push(this.request.endpoint.host.toLowerCase());
    parts.push(this.request.pathname());
    parts.push(AWS.util.queryParamsToString(this.request.params));
    return parts.join('\n');
  }

});

/**
 * @api private
 */
module.exports = AWS.Signers.V2;


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;

__webpack_require__(38);

/**
 * @api private
 */
AWS.Signers.V3Https = inherit(AWS.Signers.V3, {
  authorization: function authorization(credentials) {
    return 'AWS3-HTTPS ' +
      'AWSAccessKeyId=' + credentials.accessKeyId + ',' +
      'Algorithm=HmacSHA256,' +
      'Signature=' + this.signature(credentials);
  },

  stringToSign: function stringToSign() {
    return this.request.headers['X-Amz-Date'];
  }
});

/**
 * @api private
 */
module.exports = AWS.Signers.V3Https;


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var v4Credentials = __webpack_require__(83);
var inherit = AWS.util.inherit;

/**
 * @api private
 */
var expiresHeader = 'presigned-expires';

/**
 * @api private
 */
AWS.Signers.V4 = inherit(AWS.Signers.RequestSigner, {
  constructor: function V4(request, serviceName, options) {
    AWS.Signers.RequestSigner.call(this, request);
    this.serviceName = serviceName;
    options = options || {};
    this.signatureCache = typeof options.signatureCache === 'boolean' ? options.signatureCache : true;
    this.operation = options.operation;
    this.signatureVersion = options.signatureVersion;
  },

  algorithm: 'AWS4-HMAC-SHA256',

  addAuthorization: function addAuthorization(credentials, date) {
    var datetime = AWS.util.date.iso8601(date).replace(/[:\-]|\.\d{3}/g, '');

    if (this.isPresigned()) {
      this.updateForPresigned(credentials, datetime);
    } else {
      this.addHeaders(credentials, datetime);
    }

    this.request.headers['Authorization'] =
      this.authorization(credentials, datetime);
  },

  addHeaders: function addHeaders(credentials, datetime) {
    this.request.headers['X-Amz-Date'] = datetime;
    if (credentials.sessionToken) {
      this.request.headers['x-amz-security-token'] = credentials.sessionToken;
    }
  },

  updateForPresigned: function updateForPresigned(credentials, datetime) {
    var credString = this.credentialString(datetime);
    var qs = {
      'X-Amz-Date': datetime,
      'X-Amz-Algorithm': this.algorithm,
      'X-Amz-Credential': credentials.accessKeyId + '/' + credString,
      'X-Amz-Expires': this.request.headers[expiresHeader],
      'X-Amz-SignedHeaders': this.signedHeaders()
    };

    if (credentials.sessionToken) {
      qs['X-Amz-Security-Token'] = credentials.sessionToken;
    }

    if (this.request.headers['Content-Type']) {
      qs['Content-Type'] = this.request.headers['Content-Type'];
    }
    if (this.request.headers['Content-MD5']) {
      qs['Content-MD5'] = this.request.headers['Content-MD5'];
    }
    if (this.request.headers['Cache-Control']) {
      qs['Cache-Control'] = this.request.headers['Cache-Control'];
    }

    // need to pull in any other X-Amz-* headers
    AWS.util.each.call(this, this.request.headers, function(key, value) {
      if (key === expiresHeader) return;
      if (this.isSignableHeader(key)) {
        var lowerKey = key.toLowerCase();
        // Metadata should be normalized
        if (lowerKey.indexOf('x-amz-meta-') === 0) {
          qs[lowerKey] = value;
        } else if (lowerKey.indexOf('x-amz-') === 0) {
          qs[key] = value;
        }
      }
    });

    var sep = this.request.path.indexOf('?') >= 0 ? '&' : '?';
    this.request.path += sep + AWS.util.queryParamsToString(qs);
  },

  authorization: function authorization(credentials, datetime) {
    var parts = [];
    var credString = this.credentialString(datetime);
    parts.push(this.algorithm + ' Credential=' +
      credentials.accessKeyId + '/' + credString);
    parts.push('SignedHeaders=' + this.signedHeaders());
    parts.push('Signature=' + this.signature(credentials, datetime));
    return parts.join(', ');
  },

  signature: function signature(credentials, datetime) {
    var signingKey = v4Credentials.getSigningKey(
      credentials,
      datetime.substr(0, 8),
      this.request.region,
      this.serviceName,
      this.signatureCache
    );
    return AWS.util.crypto.hmac(signingKey, this.stringToSign(datetime), 'hex');
  },

  stringToSign: function stringToSign(datetime) {
    var parts = [];
    parts.push('AWS4-HMAC-SHA256');
    parts.push(datetime);
    parts.push(this.credentialString(datetime));
    parts.push(this.hexEncodedHash(this.canonicalString()));
    return parts.join('\n');
  },

  canonicalString: function canonicalString() {
    var parts = [], pathname = this.request.pathname();
    if (this.serviceName !== 's3' && this.signatureVersion !== 's3v4') pathname = AWS.util.uriEscapePath(pathname);

    parts.push(this.request.method);
    parts.push(pathname);
    parts.push(this.request.search());
    parts.push(this.canonicalHeaders() + '\n');
    parts.push(this.signedHeaders());
    parts.push(this.hexEncodedBodyHash());
    return parts.join('\n');
  },

  canonicalHeaders: function canonicalHeaders() {
    var headers = [];
    AWS.util.each.call(this, this.request.headers, function (key, item) {
      headers.push([key, item]);
    });
    headers.sort(function (a, b) {
      return a[0].toLowerCase() < b[0].toLowerCase() ? -1 : 1;
    });
    var parts = [];
    AWS.util.arrayEach.call(this, headers, function (item) {
      var key = item[0].toLowerCase();
      if (this.isSignableHeader(key)) {
        var value = item[1];
        if (typeof value === 'undefined' || value === null || typeof value.toString !== 'function') {
          throw AWS.util.error(new Error('Header ' + key + ' contains invalid value'), {
            code: 'InvalidHeader'
          });
        }
        parts.push(key + ':' +
          this.canonicalHeaderValues(value.toString()));
      }
    });
    return parts.join('\n');
  },

  canonicalHeaderValues: function canonicalHeaderValues(values) {
    return values.replace(/\s+/g, ' ').replace(/^\s+|\s+$/g, '');
  },

  signedHeaders: function signedHeaders() {
    var keys = [];
    AWS.util.each.call(this, this.request.headers, function (key) {
      key = key.toLowerCase();
      if (this.isSignableHeader(key)) keys.push(key);
    });
    return keys.sort().join(';');
  },

  credentialString: function credentialString(datetime) {
    return v4Credentials.createScope(
      datetime.substr(0, 8),
      this.request.region,
      this.serviceName
    );
  },

  hexEncodedHash: function hash(string) {
    return AWS.util.crypto.sha256(string, 'hex');
  },

  hexEncodedBodyHash: function hexEncodedBodyHash() {
    var request = this.request;
    if (this.isPresigned() && this.serviceName === 's3' && !request.body) {
      return 'UNSIGNED-PAYLOAD';
    } else if (request.headers['X-Amz-Content-Sha256']) {
      return request.headers['X-Amz-Content-Sha256'];
    } else {
      return this.hexEncodedHash(this.request.body || '');
    }
  },

  unsignableHeaders: [
    'authorization',
    'content-type',
    'content-length',
    'user-agent',
    expiresHeader,
    'expect',
    'x-amzn-trace-id'
  ],

  isSignableHeader: function isSignableHeader(key) {
    if (key.toLowerCase().indexOf('x-amz-') === 0) return true;
    return this.unsignableHeaders.indexOf(key) < 0;
  },

  isPresigned: function isPresigned() {
    return this.request.headers[expiresHeader] ? true : false;
  }

});

/**
 * @api private
 */
module.exports = AWS.Signers.V4;


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

/**
 * @api private
 */
var cachedSecret = {};

/**
 * @api private
 */
var cacheQueue = [];

/**
 * @api private
 */
var maxCacheEntries = 50;

/**
 * @api private
 */
var v4Identifier = 'aws4_request';

/**
 * @api private
 */
module.exports = {
  /**
   * @api private
   *
   * @param date [String]
   * @param region [String]
   * @param serviceName [String]
   * @return [String]
   */
  createScope: function createScope(date, region, serviceName) {
    return [
      date.substr(0, 8),
      region,
      serviceName,
      v4Identifier
    ].join('/');
  },

  /**
   * @api private
   *
   * @param credentials [Credentials]
   * @param date [String]
   * @param region [String]
   * @param service [String]
   * @param shouldCache [Boolean]
   * @return [String]
   */
  getSigningKey: function getSigningKey(
    credentials,
    date,
    region,
    service,
    shouldCache
  ) {
    var credsIdentifier = AWS.util.crypto
      .hmac(credentials.secretAccessKey, credentials.accessKeyId, 'base64');
    var cacheKey = [credsIdentifier, date, region, service].join('_');
    shouldCache = shouldCache !== false;
    if (shouldCache && (cacheKey in cachedSecret)) {
      return cachedSecret[cacheKey];
    }

    var kDate = AWS.util.crypto.hmac(
      'AWS4' + credentials.secretAccessKey,
      date,
      'buffer'
    );
    var kRegion = AWS.util.crypto.hmac(kDate, region, 'buffer');
    var kService = AWS.util.crypto.hmac(kRegion, service, 'buffer');

    var signingKey = AWS.util.crypto.hmac(kService, v4Identifier, 'buffer');
    if (shouldCache) {
      cachedSecret[cacheKey] = signingKey;
      cacheQueue.push(cacheKey);
      if (cacheQueue.length > maxCacheEntries) {
        // remove the oldest entry (not the least recently used)
        delete cachedSecret[cacheQueue.shift()];
      }
    }

    return signingKey;
  },

  /**
   * @api private
   *
   * Empties the derived signing key cache. Made available for testing purposes
   * only.
   */
  emptyCache: function emptyCache() {
    cachedSecret = {};
    cacheQueue = [];
  }
};


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;

/**
 * @api private
 */
AWS.Signers.S3 = inherit(AWS.Signers.RequestSigner, {
  /**
   * When building the stringToSign, these sub resource params should be
   * part of the canonical resource string with their NON-decoded values
   */
  subResources: {
    'acl': 1,
    'accelerate': 1,
    'analytics': 1,
    'cors': 1,
    'lifecycle': 1,
    'delete': 1,
    'inventory': 1,
    'location': 1,
    'logging': 1,
    'metrics': 1,
    'notification': 1,
    'partNumber': 1,
    'policy': 1,
    'requestPayment': 1,
    'replication': 1,
    'restore': 1,
    'tagging': 1,
    'torrent': 1,
    'uploadId': 1,
    'uploads': 1,
    'versionId': 1,
    'versioning': 1,
    'versions': 1,
    'website': 1
  },

  // when building the stringToSign, these querystring params should be
  // part of the canonical resource string with their NON-encoded values
  responseHeaders: {
    'response-content-type': 1,
    'response-content-language': 1,
    'response-expires': 1,
    'response-cache-control': 1,
    'response-content-disposition': 1,
    'response-content-encoding': 1
  },

  addAuthorization: function addAuthorization(credentials, date) {
    if (!this.request.headers['presigned-expires']) {
      this.request.headers['X-Amz-Date'] = AWS.util.date.rfc822(date);
    }

    if (credentials.sessionToken) {
      // presigned URLs require this header to be lowercased
      this.request.headers['x-amz-security-token'] = credentials.sessionToken;
    }

    var signature = this.sign(credentials.secretAccessKey, this.stringToSign());
    var auth = 'AWS ' + credentials.accessKeyId + ':' + signature;

    this.request.headers['Authorization'] = auth;
  },

  stringToSign: function stringToSign() {
    var r = this.request;

    var parts = [];
    parts.push(r.method);
    parts.push(r.headers['Content-MD5'] || '');
    parts.push(r.headers['Content-Type'] || '');

    // This is the "Date" header, but we use X-Amz-Date.
    // The S3 signing mechanism requires us to pass an empty
    // string for this Date header regardless.
    parts.push(r.headers['presigned-expires'] || '');

    var headers = this.canonicalizedAmzHeaders();
    if (headers) parts.push(headers);
    parts.push(this.canonicalizedResource());

    return parts.join('\n');

  },

  canonicalizedAmzHeaders: function canonicalizedAmzHeaders() {

    var amzHeaders = [];

    AWS.util.each(this.request.headers, function (name) {
      if (name.match(/^x-amz-/i))
        amzHeaders.push(name);
    });

    amzHeaders.sort(function (a, b) {
      return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
    });

    var parts = [];
    AWS.util.arrayEach.call(this, amzHeaders, function (name) {
      parts.push(name.toLowerCase() + ':' + String(this.request.headers[name]));
    });

    return parts.join('\n');

  },

  canonicalizedResource: function canonicalizedResource() {

    var r = this.request;

    var parts = r.path.split('?');
    var path = parts[0];
    var querystring = parts[1];

    var resource = '';

    if (r.virtualHostedBucket)
      resource += '/' + r.virtualHostedBucket;

    resource += path;

    if (querystring) {

      // collect a list of sub resources and query params that need to be signed
      var resources = [];

      AWS.util.arrayEach.call(this, querystring.split('&'), function (param) {
        var name = param.split('=')[0];
        var value = param.split('=')[1];
        if (this.subResources[name] || this.responseHeaders[name]) {
          var subresource = { name: name };
          if (value !== undefined) {
            if (this.subResources[name]) {
              subresource.value = value;
            } else {
              subresource.value = decodeURIComponent(value);
            }
          }
          resources.push(subresource);
        }
      });

      resources.sort(function (a, b) { return a.name < b.name ? -1 : 1; });

      if (resources.length) {

        querystring = [];
        AWS.util.arrayEach(resources, function (res) {
          if (res.value === undefined) {
            querystring.push(res.name);
          } else {
            querystring.push(res.name + '=' + res.value);
          }
        });

        resource += '?' + querystring.join('&');
      }

    }

    return resource;

  },

  sign: function sign(secret, string) {
    return AWS.util.crypto.hmac(secret, string, 'base64', 'sha1');
  }
});

/**
 * @api private
 */
module.exports = AWS.Signers.S3;


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var inherit = AWS.util.inherit;

/**
 * @api private
 */
var expiresHeader = 'presigned-expires';

/**
 * @api private
 */
function signedUrlBuilder(request) {
  var expires = request.httpRequest.headers[expiresHeader];
  var signerClass = request.service.getSignerClass(request);

  delete request.httpRequest.headers['User-Agent'];
  delete request.httpRequest.headers['X-Amz-User-Agent'];

  if (signerClass === AWS.Signers.V4) {
    if (expires > 604800) { // one week expiry is invalid
      var message = 'Presigning does not support expiry time greater ' +
                    'than a week with SigV4 signing.';
      throw AWS.util.error(new Error(), {
        code: 'InvalidExpiryTime', message: message, retryable: false
      });
    }
    request.httpRequest.headers[expiresHeader] = expires;
  } else if (signerClass === AWS.Signers.S3) {
    var now = request.service ? request.service.getSkewCorrectedDate() : AWS.util.date.getDate();
    request.httpRequest.headers[expiresHeader] = parseInt(
      AWS.util.date.unixTimestamp(now) + expires, 10).toString();
  } else {
    throw AWS.util.error(new Error(), {
      message: 'Presigning only supports S3 or SigV4 signing.',
      code: 'UnsupportedSigner', retryable: false
    });
  }
}

/**
 * @api private
 */
function signedUrlSigner(request) {
  var endpoint = request.httpRequest.endpoint;
  var parsedUrl = AWS.util.urlParse(request.httpRequest.path);
  var queryParams = {};

  if (parsedUrl.search) {
    queryParams = AWS.util.queryStringParse(parsedUrl.search.substr(1));
  }

  var auth = request.httpRequest.headers['Authorization'].split(' ');
  if (auth[0] === 'AWS') {
    auth = auth[1].split(':');
    queryParams['AWSAccessKeyId'] = auth[0];
    queryParams['Signature'] = auth[1];

    AWS.util.each(request.httpRequest.headers, function (key, value) {
      if (key === expiresHeader) key = 'Expires';
      if (key.indexOf('x-amz-meta-') === 0) {
        // Delete existing, potentially not normalized key
        delete queryParams[key];
        key = key.toLowerCase();
      }
      queryParams[key] = value;
    });
    delete request.httpRequest.headers[expiresHeader];
    delete queryParams['Authorization'];
    delete queryParams['Host'];
  } else if (auth[0] === 'AWS4-HMAC-SHA256') { // SigV4 signing
    auth.shift();
    var rest = auth.join(' ');
    var signature = rest.match(/Signature=(.*?)(?:,|\s|\r?\n|$)/)[1];
    queryParams['X-Amz-Signature'] = signature;
    delete queryParams['Expires'];
  }

  // build URL
  endpoint.pathname = parsedUrl.pathname;
  endpoint.search = AWS.util.queryParamsToString(queryParams);
}

/**
 * @api private
 */
AWS.Signers.Presign = inherit({
  /**
   * @api private
   */
  sign: function sign(request, expireTime, callback) {
    request.httpRequest.headers[expiresHeader] = expireTime || 3600;
    request.on('build', signedUrlBuilder);
    request.on('sign', signedUrlSigner);
    request.removeListener('afterBuild',
      AWS.EventListeners.Core.SET_CONTENT_LENGTH);
    request.removeListener('afterBuild',
      AWS.EventListeners.Core.COMPUTE_SHA256);

    request.emit('beforePresign', [request]);

    if (callback) {
      request.build(function() {
        if (this.response.error) callback(this.response.error);
        else {
          callback(null, AWS.util.urlFormat(request.httpRequest.endpoint));
        }
      });
    } else {
      request.build();
      if (request.response.error) throw request.response.error;
      return AWS.util.urlFormat(request.httpRequest.endpoint);
    }
  }
});

/**
 * @api private
 */
module.exports = AWS.Signers.Presign;


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

/**
 * @api private
 */
AWS.ParamValidator = AWS.util.inherit({
  /**
   * Create a new validator object.
   *
   * @param validation [Boolean|map] whether input parameters should be
   *     validated against the operation description before sending the
   *     request. Pass a map to enable any of the following specific
   *     validation features:
   *
   *     * **min** [Boolean] &mdash; Validates that a value meets the min
   *       constraint. This is enabled by default when paramValidation is set
   *       to `true`.
   *     * **max** [Boolean] &mdash; Validates that a value meets the max
   *       constraint.
   *     * **pattern** [Boolean] &mdash; Validates that a string value matches a
   *       regular expression.
   *     * **enum** [Boolean] &mdash; Validates that a string value matches one
   *       of the allowable enum values.
   */
  constructor: function ParamValidator(validation) {
    if (validation === true || validation === undefined) {
      validation = {'min': true};
    }
    this.validation = validation;
  },

  validate: function validate(shape, params, context) {
    this.errors = [];
    this.validateMember(shape, params || {}, context || 'params');

    if (this.errors.length > 1) {
      var msg = this.errors.join('\n* ');
      msg = 'There were ' + this.errors.length +
        ' validation errors:\n* ' + msg;
      throw AWS.util.error(new Error(msg),
        {code: 'MultipleValidationErrors', errors: this.errors});
    } else if (this.errors.length === 1) {
      throw this.errors[0];
    } else {
      return true;
    }
  },

  fail: function fail(code, message) {
    this.errors.push(AWS.util.error(new Error(message), {code: code}));
  },

  validateStructure: function validateStructure(shape, params, context) {
    this.validateType(params, context, ['object'], 'structure');

    var paramName;
    for (var i = 0; shape.required && i < shape.required.length; i++) {
      paramName = shape.required[i];
      var value = params[paramName];
      if (value === undefined || value === null) {
        this.fail('MissingRequiredParameter',
          'Missing required key \'' + paramName + '\' in ' + context);
      }
    }

    // validate hash members
    for (paramName in params) {
      if (!Object.prototype.hasOwnProperty.call(params, paramName)) continue;

      var paramValue = params[paramName],
          memberShape = shape.members[paramName];

      if (memberShape !== undefined) {
        var memberContext = [context, paramName].join('.');
        this.validateMember(memberShape, paramValue, memberContext);
      } else {
        this.fail('UnexpectedParameter',
          'Unexpected key \'' + paramName + '\' found in ' + context);
      }
    }

    return true;
  },

  validateMember: function validateMember(shape, param, context) {
    switch (shape.type) {
      case 'structure':
        return this.validateStructure(shape, param, context);
      case 'list':
        return this.validateList(shape, param, context);
      case 'map':
        return this.validateMap(shape, param, context);
      default:
        return this.validateScalar(shape, param, context);
    }
  },

  validateList: function validateList(shape, params, context) {
    if (this.validateType(params, context, [Array])) {
      this.validateRange(shape, params.length, context, 'list member count');
      // validate array members
      for (var i = 0; i < params.length; i++) {
        this.validateMember(shape.member, params[i], context + '[' + i + ']');
      }
    }
  },

  validateMap: function validateMap(shape, params, context) {
    if (this.validateType(params, context, ['object'], 'map')) {
      // Build up a count of map members to validate range traits.
      var mapCount = 0;
      for (var param in params) {
        if (!Object.prototype.hasOwnProperty.call(params, param)) continue;
        // Validate any map key trait constraints
        this.validateMember(shape.key, param,
                            context + '[key=\'' + param + '\']');
        this.validateMember(shape.value, params[param],
                            context + '[\'' + param + '\']');
        mapCount++;
      }
      this.validateRange(shape, mapCount, context, 'map member count');
    }
  },

  validateScalar: function validateScalar(shape, value, context) {
    switch (shape.type) {
      case null:
      case undefined:
      case 'string':
        return this.validateString(shape, value, context);
      case 'base64':
      case 'binary':
        return this.validatePayload(value, context);
      case 'integer':
      case 'float':
        return this.validateNumber(shape, value, context);
      case 'boolean':
        return this.validateType(value, context, ['boolean']);
      case 'timestamp':
        return this.validateType(value, context, [Date,
          /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)?Z$/, 'number'],
          'Date object, ISO-8601 string, or a UNIX timestamp');
      default:
        return this.fail('UnkownType', 'Unhandled type ' +
                         shape.type + ' for ' + context);
    }
  },

  validateString: function validateString(shape, value, context) {
    var validTypes = ['string'];
    if (shape.isJsonValue) {
      validTypes = validTypes.concat(['number', 'object', 'boolean']);
    }
    if (value !== null && this.validateType(value, context, validTypes)) {
      this.validateEnum(shape, value, context);
      this.validateRange(shape, value.length, context, 'string length');
      this.validatePattern(shape, value, context);
      this.validateUri(shape, value, context);
    }
  },

  validateUri: function validateUri(shape, value, context) {
    if (shape['location'] === 'uri') {
      if (value.length === 0) {
        this.fail('UriParameterError', 'Expected uri parameter to have length >= 1,'
          + ' but found "' + value +'" for ' + context);
      }
    }
  },

  validatePattern: function validatePattern(shape, value, context) {
    if (this.validation['pattern'] && shape['pattern'] !== undefined) {
      if (!(new RegExp(shape['pattern'])).test(value)) {
        this.fail('PatternMatchError', 'Provided value "' + value + '" '
          + 'does not match regex pattern /' + shape['pattern'] + '/ for '
          + context);
      }
    }
  },

  validateRange: function validateRange(shape, value, context, descriptor) {
    if (this.validation['min']) {
      if (shape['min'] !== undefined && value < shape['min']) {
        this.fail('MinRangeError', 'Expected ' + descriptor + ' >= '
          + shape['min'] + ', but found ' + value + ' for ' + context);
      }
    }
    if (this.validation['max']) {
      if (shape['max'] !== undefined && value > shape['max']) {
        this.fail('MaxRangeError', 'Expected ' + descriptor + ' <= '
          + shape['max'] + ', but found ' + value + ' for ' + context);
      }
    }
  },

  validateEnum: function validateRange(shape, value, context) {
    if (this.validation['enum'] && shape['enum'] !== undefined) {
      // Fail if the string value is not present in the enum list
      if (shape['enum'].indexOf(value) === -1) {
        this.fail('EnumError', 'Found string value of ' + value + ', but '
          + 'expected ' + shape['enum'].join('|') + ' for ' + context);
      }
    }
  },

  validateType: function validateType(value, context, acceptedTypes, type) {
    // We will not log an error for null or undefined, but we will return
    // false so that callers know that the expected type was not strictly met.
    if (value === null || value === undefined) return false;

    var foundInvalidType = false;
    for (var i = 0; i < acceptedTypes.length; i++) {
      if (typeof acceptedTypes[i] === 'string') {
        if (typeof value === acceptedTypes[i]) return true;
      } else if (acceptedTypes[i] instanceof RegExp) {
        if ((value || '').toString().match(acceptedTypes[i])) return true;
      } else {
        if (value instanceof acceptedTypes[i]) return true;
        if (AWS.util.isType(value, acceptedTypes[i])) return true;
        if (!type && !foundInvalidType) acceptedTypes = acceptedTypes.slice();
        acceptedTypes[i] = AWS.util.typeName(acceptedTypes[i]);
      }
      foundInvalidType = true;
    }

    var acceptedType = type;
    if (!acceptedType) {
      acceptedType = acceptedTypes.join(', ').replace(/,([^,]+)$/, ', or$1');
    }

    var vowel = acceptedType.match(/^[aeiou]/i) ? 'n' : '';
    this.fail('InvalidParameterType', 'Expected ' + context + ' to be a' +
              vowel + ' ' + acceptedType);
    return false;
  },

  validateNumber: function validateNumber(shape, value, context) {
    if (value === null || value === undefined) return;
    if (typeof value === 'string') {
      var castedValue = parseFloat(value);
      if (castedValue.toString() === value) value = castedValue;
    }
    if (this.validateType(value, context, ['number'])) {
      this.validateRange(shape, value, context, 'numeric value');
    }
  },

  validatePayload: function validatePayload(value, context) {
    if (value === null || value === undefined) return;
    if (typeof value === 'string') return;
    if (value && typeof value.byteLength === 'number') return; // typed arrays
    if (AWS.util.isNode()) { // special check for buffer/stream in Node.js
      var Stream = AWS.util.stream.Stream;
      if (AWS.util.Buffer.isBuffer(value) || value instanceof Stream) return;
    } else {
      if (typeof Blob !== void 0 && value instanceof Blob) return;
    }

    var types = ['Buffer', 'Stream', 'File', 'Blob', 'ArrayBuffer', 'DataView'];
    if (value) {
      for (var i = 0; i < types.length; i++) {
        if (AWS.util.isType(value, types[i])) return;
        if (AWS.util.typeName(value.constructor) === types[i]) return;
      }
    }

    this.fail('InvalidParameterType', 'Expected ' + context + ' to be a ' +
      'string, Buffer, Stream, Blob, or typed array object');
  }
});


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

var v1 = __webpack_require__(88);
var v4 = __webpack_require__(89);

var uuid = v4;
uuid.v1 = v1;
uuid.v4 = v4;

module.exports = uuid;


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

var rng = __webpack_require__(39);
var bytesToUuid = __webpack_require__(40);

// **`v1()` - Generate time-based UUID**
//
// Inspired by https://github.com/LiosK/UUID.js
// and http://docs.python.org/library/uuid.html

var _nodeId;
var _clockseq;

// Previous uuid creation time
var _lastMSecs = 0;
var _lastNSecs = 0;

// See https://github.com/broofa/node-uuid for API details
function v1(options, buf, offset) {
  var i = buf && offset || 0;
  var b = buf || [];

  options = options || {};
  var node = options.node || _nodeId;
  var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq;

  // node and clockseq need to be initialized to random values if they're not
  // specified.  We do this lazily to minimize issues related to insufficient
  // system entropy.  See #189
  if (node == null || clockseq == null) {
    var seedBytes = rng();
    if (node == null) {
      // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
      node = _nodeId = [
        seedBytes[0] | 0x01,
        seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]
      ];
    }
    if (clockseq == null) {
      // Per 4.2.2, randomize (14 bit) clockseq
      clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;
    }
  }

  // UUID timestamps are 100 nano-second units since the Gregorian epoch,
  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
  var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime();

  // Per 4.2.1.2, use count of uuid's generated during the current clock
  // cycle to simulate higher resolution clock
  var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1;

  // Time since last uuid creation (in msecs)
  var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

  // Per 4.2.1.2, Bump clockseq on clock regression
  if (dt < 0 && options.clockseq === undefined) {
    clockseq = clockseq + 1 & 0x3fff;
  }

  // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
  // time interval
  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
    nsecs = 0;
  }

  // Per 4.2.1.2 Throw error if too many uuids are requested
  if (nsecs >= 10000) {
    throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
  }

  _lastMSecs = msecs;
  _lastNSecs = nsecs;
  _clockseq = clockseq;

  // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
  msecs += 12219292800000;

  // `time_low`
  var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
  b[i++] = tl >>> 24 & 0xff;
  b[i++] = tl >>> 16 & 0xff;
  b[i++] = tl >>> 8 & 0xff;
  b[i++] = tl & 0xff;

  // `time_mid`
  var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
  b[i++] = tmh >>> 8 & 0xff;
  b[i++] = tmh & 0xff;

  // `time_high_and_version`
  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
  b[i++] = tmh >>> 16 & 0xff;

  // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
  b[i++] = clockseq >>> 8 | 0x80;

  // `clock_seq_low`
  b[i++] = clockseq & 0xff;

  // `node`
  for (var n = 0; n < 6; ++n) {
    b[i + n] = node[n];
  }

  return buf ? buf : bytesToUuid(b);
}

module.exports = v1;


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

var rng = __webpack_require__(39);
var bytesToUuid = __webpack_require__(40);

function v4(options, buf, offset) {
  var i = buf && offset || 0;

  if (typeof(options) == 'string') {
    buf = options === 'binary' ? new Array(16) : null;
    options = null;
  }
  options = options || {};

  var rnds = options.random || (options.rng || rng)();

  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
  rnds[6] = (rnds[6] & 0x0f) | 0x40;
  rnds[8] = (rnds[8] & 0x3f) | 0x80;

  // Copy bytes to buffer, if provided
  if (buf) {
    for (var ii = 0; ii < 16; ++ii) {
      buf[i + ii] = rnds[ii];
    }
  }

  return buf || bytesToUuid(rnds);
}

module.exports = v4;


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

var Hmac = __webpack_require__(91);
var Md5 = __webpack_require__(95);
var Sha1 = __webpack_require__(96);
var Sha256 = __webpack_require__(97);

/**
 * @api private
 */
module.exports = exports = {
    createHash: function createHash(alg) {
      alg = alg.toLowerCase();
      if (alg === 'md5') {
        return new Md5();
      } else if (alg === 'sha256') {
        return new Sha256();
      } else if (alg === 'sha1') {
        return new Sha1();
      }

      throw new Error('Hash algorithm ' + alg + ' is not supported in the browser SDK');
    },
    createHmac: function createHmac(alg, key) {
      alg = alg.toLowerCase();
      if (alg === 'md5') {
        return new Hmac(Md5, key);
      } else if (alg === 'sha256') {
        return new Hmac(Sha256, key);
      } else if (alg === 'sha1') {
        return new Hmac(Sha1, key);
      }

      throw new Error('HMAC algorithm ' + alg + ' is not supported in the browser SDK');
    },
    createSign: function() {
      throw new Error('createSign is not implemented in the browser');
    }
  };


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

var hashUtils = __webpack_require__(14);

/**
 * @api private
 */
function Hmac(hashCtor, secret) {
    this.hash = new hashCtor();
    this.outer = new hashCtor();

    var inner = bufferFromSecret(hashCtor, secret);
    var outer = new Uint8Array(hashCtor.BLOCK_SIZE);
    outer.set(inner);

    for (var i = 0; i < hashCtor.BLOCK_SIZE; i++) {
        inner[i] ^= 0x36;
        outer[i] ^= 0x5c;
    }

    this.hash.update(inner);
    this.outer.update(outer);

    // Zero out the copied key buffer.
    for (var i = 0; i < inner.byteLength; i++) {
        inner[i] = 0;
    }
}

/**
 * @api private
 */
module.exports = exports = Hmac;

Hmac.prototype.update = function (toHash) {
    if (hashUtils.isEmptyData(toHash) || this.error) {
        return this;
    }

    try {
        this.hash.update(hashUtils.convertToBuffer(toHash));
    } catch (e) {
        this.error = e;
    }

    return this;
};

Hmac.prototype.digest = function (encoding) {
    if (!this.outer.finished) {
        this.outer.update(this.hash.digest());
    }

    return this.outer.digest(encoding);
};

function bufferFromSecret(hashCtor, secret) {
    var input = hashUtils.convertToBuffer(secret);
    if (input.byteLength > hashCtor.BLOCK_SIZE) {
        var bufferHash = new hashCtor;
        bufferHash.update(input);
        input = bufferHash.digest();
    }
    var buffer = new Uint8Array(hashCtor.BLOCK_SIZE);
    buffer.set(input);
    return buffer;
}


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.byteLength = byteLength
exports.toByteArray = toByteArray
exports.fromByteArray = fromByteArray

var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i]
  revLookup[code.charCodeAt(i)] = i
}

// Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications
revLookup['-'.charCodeAt(0)] = 62
revLookup['_'.charCodeAt(0)] = 63

function getLens (b64) {
  var len = b64.length

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42
  var validLen = b64.indexOf('=')
  if (validLen === -1) validLen = len

  var placeHoldersLen = validLen === len
    ? 0
    : 4 - (validLen % 4)

  return [validLen, placeHoldersLen]
}

// base64 is 4/3 + up to two characters of the original data
function byteLength (b64) {
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function _byteLength (b64, validLen, placeHoldersLen) {
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function toByteArray (b64) {
  var tmp
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]

  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen))

  var curByte = 0

  // if there are placeholders, only get up to the last complete 4 chars
  var len = placeHoldersLen > 0
    ? validLen - 4
    : validLen

  var i
  for (i = 0; i < len; i += 4) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 18) |
      (revLookup[b64.charCodeAt(i + 1)] << 12) |
      (revLookup[b64.charCodeAt(i + 2)] << 6) |
      revLookup[b64.charCodeAt(i + 3)]
    arr[curByte++] = (tmp >> 16) & 0xFF
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 2) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 2) |
      (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 1) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 10) |
      (revLookup[b64.charCodeAt(i + 1)] << 4) |
      (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  return arr
}

function tripletToBase64 (num) {
  return lookup[num >> 18 & 0x3F] +
    lookup[num >> 12 & 0x3F] +
    lookup[num >> 6 & 0x3F] +
    lookup[num & 0x3F]
}

function encodeChunk (uint8, start, end) {
  var tmp
  var output = []
  for (var i = start; i < end; i += 3) {
    tmp =
      ((uint8[i] << 16) & 0xFF0000) +
      ((uint8[i + 1] << 8) & 0xFF00) +
      (uint8[i + 2] & 0xFF)
    output.push(tripletToBase64(tmp))
  }
  return output.join('')
}

function fromByteArray (uint8) {
  var tmp
  var len = uint8.length
  var extraBytes = len % 3 // if we have 1 byte left, pad 2 bytes
  var parts = []
  var maxChunkLength = 16383 // must be multiple of 3

  // go through the array every three bytes, we'll deal with trailing stuff later
  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(
      uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)
    ))
  }

  // pad the end with zeros, but make sure to not forget the extra bytes
  if (extraBytes === 1) {
    tmp = uint8[len - 1]
    parts.push(
      lookup[tmp >> 2] +
      lookup[(tmp << 4) & 0x3F] +
      '=='
    )
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + uint8[len - 1]
    parts.push(
      lookup[tmp >> 10] +
      lookup[(tmp >> 4) & 0x3F] +
      lookup[(tmp << 2) & 0x3F] +
      '='
    )
  }

  return parts.join('')
}


/***/ }),
/* 93 */
/***/ (function(module, exports) {

exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = (nBytes * 8) - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = (e * 256) + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = (m * 256) + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = (nBytes * 8) - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = ((value * c) - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}


/***/ }),
/* 94 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

var hashUtils = __webpack_require__(14);
var Buffer = __webpack_require__(10).Buffer;

var BLOCK_SIZE = 64;

var DIGEST_LENGTH = 16;

var INIT = [
    0x67452301,
    0xefcdab89,
    0x98badcfe,
    0x10325476,
];

/**
 * @api private
 */
function Md5() {
    this.state = [
        0x67452301,
        0xefcdab89,
        0x98badcfe,
        0x10325476,
    ];
    this.buffer = new DataView(new ArrayBuffer(BLOCK_SIZE));
    this.bufferLength = 0;
    this.bytesHashed = 0;
    this.finished = false;
}

/**
 * @api private
 */
module.exports = exports = Md5;

Md5.BLOCK_SIZE = BLOCK_SIZE;

Md5.prototype.update = function (sourceData) {
    if (hashUtils.isEmptyData(sourceData)) {
        return this;
    } else if (this.finished) {
        throw new Error('Attempted to update an already finished hash.');
    }

    var data = hashUtils.convertToBuffer(sourceData);
    var position = 0;
    var byteLength = data.byteLength;
    this.bytesHashed += byteLength;
    while (byteLength > 0) {
        this.buffer.setUint8(this.bufferLength++, data[position++]);
        byteLength--;
        if (this.bufferLength === BLOCK_SIZE) {
            this.hashBuffer();
            this.bufferLength = 0;
        }
    }

    return this;
};

Md5.prototype.digest = function (encoding) {
    if (!this.finished) {
        var _a = this, buffer = _a.buffer, undecoratedLength = _a.bufferLength, bytesHashed = _a.bytesHashed;
        var bitsHashed = bytesHashed * 8;
        buffer.setUint8(this.bufferLength++, 128);
        // Ensure the final block has enough room for the hashed length
        if (undecoratedLength % BLOCK_SIZE >= BLOCK_SIZE - 8) {
            for (var i = this.bufferLength; i < BLOCK_SIZE; i++) {
                buffer.setUint8(i, 0);
            }
            this.hashBuffer();
            this.bufferLength = 0;
        }
        for (var i = this.bufferLength; i < BLOCK_SIZE - 8; i++) {
            buffer.setUint8(i, 0);
        }
        buffer.setUint32(BLOCK_SIZE - 8, bitsHashed >>> 0, true);
        buffer.setUint32(BLOCK_SIZE - 4, Math.floor(bitsHashed / 0x100000000), true);
        this.hashBuffer();
        this.finished = true;
    }
    var out = new DataView(new ArrayBuffer(DIGEST_LENGTH));
    for (var i = 0; i < 4; i++) {
        out.setUint32(i * 4, this.state[i], true);
    }
    var buff = new Buffer(out.buffer, out.byteOffset, out.byteLength);
    return encoding ? buff.toString(encoding) : buff;
};

Md5.prototype.hashBuffer = function () {
    var _a = this, buffer = _a.buffer, state = _a.state;
    var a = state[0], b = state[1], c = state[2], d = state[3];
    a = ff(a, b, c, d, buffer.getUint32(0, true), 7, 0xd76aa478);
    d = ff(d, a, b, c, buffer.getUint32(4, true), 12, 0xe8c7b756);
    c = ff(c, d, a, b, buffer.getUint32(8, true), 17, 0x242070db);
    b = ff(b, c, d, a, buffer.getUint32(12, true), 22, 0xc1bdceee);
    a = ff(a, b, c, d, buffer.getUint32(16, true), 7, 0xf57c0faf);
    d = ff(d, a, b, c, buffer.getUint32(20, true), 12, 0x4787c62a);
    c = ff(c, d, a, b, buffer.getUint32(24, true), 17, 0xa8304613);
    b = ff(b, c, d, a, buffer.getUint32(28, true), 22, 0xfd469501);
    a = ff(a, b, c, d, buffer.getUint32(32, true), 7, 0x698098d8);
    d = ff(d, a, b, c, buffer.getUint32(36, true), 12, 0x8b44f7af);
    c = ff(c, d, a, b, buffer.getUint32(40, true), 17, 0xffff5bb1);
    b = ff(b, c, d, a, buffer.getUint32(44, true), 22, 0x895cd7be);
    a = ff(a, b, c, d, buffer.getUint32(48, true), 7, 0x6b901122);
    d = ff(d, a, b, c, buffer.getUint32(52, true), 12, 0xfd987193);
    c = ff(c, d, a, b, buffer.getUint32(56, true), 17, 0xa679438e);
    b = ff(b, c, d, a, buffer.getUint32(60, true), 22, 0x49b40821);
    a = gg(a, b, c, d, buffer.getUint32(4, true), 5, 0xf61e2562);
    d = gg(d, a, b, c, buffer.getUint32(24, true), 9, 0xc040b340);
    c = gg(c, d, a, b, buffer.getUint32(44, true), 14, 0x265e5a51);
    b = gg(b, c, d, a, buffer.getUint32(0, true), 20, 0xe9b6c7aa);
    a = gg(a, b, c, d, buffer.getUint32(20, true), 5, 0xd62f105d);
    d = gg(d, a, b, c, buffer.getUint32(40, true), 9, 0x02441453);
    c = gg(c, d, a, b, buffer.getUint32(60, true), 14, 0xd8a1e681);
    b = gg(b, c, d, a, buffer.getUint32(16, true), 20, 0xe7d3fbc8);
    a = gg(a, b, c, d, buffer.getUint32(36, true), 5, 0x21e1cde6);
    d = gg(d, a, b, c, buffer.getUint32(56, true), 9, 0xc33707d6);
    c = gg(c, d, a, b, buffer.getUint32(12, true), 14, 0xf4d50d87);
    b = gg(b, c, d, a, buffer.getUint32(32, true), 20, 0x455a14ed);
    a = gg(a, b, c, d, buffer.getUint32(52, true), 5, 0xa9e3e905);
    d = gg(d, a, b, c, buffer.getUint32(8, true), 9, 0xfcefa3f8);
    c = gg(c, d, a, b, buffer.getUint32(28, true), 14, 0x676f02d9);
    b = gg(b, c, d, a, buffer.getUint32(48, true), 20, 0x8d2a4c8a);
    a = hh(a, b, c, d, buffer.getUint32(20, true), 4, 0xfffa3942);
    d = hh(d, a, b, c, buffer.getUint32(32, true), 11, 0x8771f681);
    c = hh(c, d, a, b, buffer.getUint32(44, true), 16, 0x6d9d6122);
    b = hh(b, c, d, a, buffer.getUint32(56, true), 23, 0xfde5380c);
    a = hh(a, b, c, d, buffer.getUint32(4, true), 4, 0xa4beea44);
    d = hh(d, a, b, c, buffer.getUint32(16, true), 11, 0x4bdecfa9);
    c = hh(c, d, a, b, buffer.getUint32(28, true), 16, 0xf6bb4b60);
    b = hh(b, c, d, a, buffer.getUint32(40, true), 23, 0xbebfbc70);
    a = hh(a, b, c, d, buffer.getUint32(52, true), 4, 0x289b7ec6);
    d = hh(d, a, b, c, buffer.getUint32(0, true), 11, 0xeaa127fa);
    c = hh(c, d, a, b, buffer.getUint32(12, true), 16, 0xd4ef3085);
    b = hh(b, c, d, a, buffer.getUint32(24, true), 23, 0x04881d05);
    a = hh(a, b, c, d, buffer.getUint32(36, true), 4, 0xd9d4d039);
    d = hh(d, a, b, c, buffer.getUint32(48, true), 11, 0xe6db99e5);
    c = hh(c, d, a, b, buffer.getUint32(60, true), 16, 0x1fa27cf8);
    b = hh(b, c, d, a, buffer.getUint32(8, true), 23, 0xc4ac5665);
    a = ii(a, b, c, d, buffer.getUint32(0, true), 6, 0xf4292244);
    d = ii(d, a, b, c, buffer.getUint32(28, true), 10, 0x432aff97);
    c = ii(c, d, a, b, buffer.getUint32(56, true), 15, 0xab9423a7);
    b = ii(b, c, d, a, buffer.getUint32(20, true), 21, 0xfc93a039);
    a = ii(a, b, c, d, buffer.getUint32(48, true), 6, 0x655b59c3);
    d = ii(d, a, b, c, buffer.getUint32(12, true), 10, 0x8f0ccc92);
    c = ii(c, d, a, b, buffer.getUint32(40, true), 15, 0xffeff47d);
    b = ii(b, c, d, a, buffer.getUint32(4, true), 21, 0x85845dd1);
    a = ii(a, b, c, d, buffer.getUint32(32, true), 6, 0x6fa87e4f);
    d = ii(d, a, b, c, buffer.getUint32(60, true), 10, 0xfe2ce6e0);
    c = ii(c, d, a, b, buffer.getUint32(24, true), 15, 0xa3014314);
    b = ii(b, c, d, a, buffer.getUint32(52, true), 21, 0x4e0811a1);
    a = ii(a, b, c, d, buffer.getUint32(16, true), 6, 0xf7537e82);
    d = ii(d, a, b, c, buffer.getUint32(44, true), 10, 0xbd3af235);
    c = ii(c, d, a, b, buffer.getUint32(8, true), 15, 0x2ad7d2bb);
    b = ii(b, c, d, a, buffer.getUint32(36, true), 21, 0xeb86d391);
    state[0] = (a + state[0]) & 0xFFFFFFFF;
    state[1] = (b + state[1]) & 0xFFFFFFFF;
    state[2] = (c + state[2]) & 0xFFFFFFFF;
    state[3] = (d + state[3]) & 0xFFFFFFFF;
};

function cmn(q, a, b, x, s, t) {
    a = (((a + q) & 0xFFFFFFFF) + ((x + t) & 0xFFFFFFFF)) & 0xFFFFFFFF;
    return (((a << s) | (a >>> (32 - s))) + b) & 0xFFFFFFFF;
}

function ff(a, b, c, d, x, s, t) {
    return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}

function gg(a, b, c, d, x, s, t) {
    return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}

function hh(a, b, c, d, x, s, t) {
    return cmn(b ^ c ^ d, a, b, x, s, t);
}

function ii(a, b, c, d, x, s, t) {
    return cmn(c ^ (b | (~d)), a, b, x, s, t);
}


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

var Buffer = __webpack_require__(10).Buffer;
var hashUtils = __webpack_require__(14);

var BLOCK_SIZE = 64;

var DIGEST_LENGTH = 20;

var KEY = new Uint32Array([
    0x5a827999,
    0x6ed9eba1,
    0x8f1bbcdc | 0,
    0xca62c1d6 | 0
]);

var INIT = [
    0x6a09e667,
    0xbb67ae85,
    0x3c6ef372,
    0xa54ff53a,
    0x510e527f,
    0x9b05688c,
    0x1f83d9ab,
    0x5be0cd19,
];

var MAX_HASHABLE_LENGTH = Math.pow(2, 53) - 1;

/**
 * @api private
 */
function Sha1() {
    this.h0 = 0x67452301;
    this.h1 = 0xEFCDAB89;
    this.h2 = 0x98BADCFE;
    this.h3 = 0x10325476;
    this.h4 = 0xC3D2E1F0;
    // The first 64 bytes (16 words) is the data chunk
    this.block = new Uint32Array(80);
    this.offset = 0;
    this.shift = 24;
    this.totalLength = 0;
}

/**
 * @api private
 */
module.exports = exports = Sha1;

Sha1.BLOCK_SIZE = BLOCK_SIZE;

Sha1.prototype.update = function (data) {
    if (this.finished) {
        throw new Error('Attempted to update an already finished hash.');
    }

    if (hashUtils.isEmptyData(data)) {
        return this;
    }

    data = hashUtils.convertToBuffer(data);

    var length = data.length;
    this.totalLength += length * 8;
    for (var i = 0; i < length; i++) {
        this.write(data[i]);
    }

    return this;
};

Sha1.prototype.write = function write(byte) {
    this.block[this.offset] |= (byte & 0xff) << this.shift;
    if (this.shift) {
        this.shift -= 8;
    } else {
        this.offset++;
        this.shift = 24;
    }

    if (this.offset === 16) this.processBlock();
};

Sha1.prototype.digest = function (encoding) {
    // Pad
    this.write(0x80);
    if (this.offset > 14 || (this.offset === 14 && this.shift < 24)) {
      this.processBlock();
    }
    this.offset = 14;
    this.shift = 24;

    // 64-bit length big-endian
    this.write(0x00); // numbers this big aren't accurate in javascript anyway
    this.write(0x00); // ..So just hard-code to zero.
    this.write(this.totalLength > 0xffffffffff ? this.totalLength / 0x10000000000 : 0x00);
    this.write(this.totalLength > 0xffffffff ? this.totalLength / 0x100000000 : 0x00);
    for (var s = 24; s >= 0; s -= 8) {
        this.write(this.totalLength >> s);
    }
    // The value in state is little-endian rather than big-endian, so flip
    // each word into a new Uint8Array
    var out = new Buffer(DIGEST_LENGTH);
    var outView = new DataView(out.buffer);
    outView.setUint32(0, this.h0, false);
    outView.setUint32(4, this.h1, false);
    outView.setUint32(8, this.h2, false);
    outView.setUint32(12, this.h3, false);
    outView.setUint32(16, this.h4, false);

    return encoding ? out.toString(encoding) : out;
};

Sha1.prototype.processBlock = function processBlock() {
    // Extend the sixteen 32-bit words into eighty 32-bit words:
    for (var i = 16; i < 80; i++) {
      var w = this.block[i - 3] ^ this.block[i - 8] ^ this.block[i - 14] ^ this.block[i - 16];
      this.block[i] = (w << 1) | (w >>> 31);
    }

    // Initialize hash value for this chunk:
    var a = this.h0;
    var b = this.h1;
    var c = this.h2;
    var d = this.h3;
    var e = this.h4;
    var f, k;

    // Main loop:
    for (i = 0; i < 80; i++) {
      if (i < 20) {
        f = d ^ (b & (c ^ d));
        k = 0x5A827999;
      }
      else if (i < 40) {
        f = b ^ c ^ d;
        k = 0x6ED9EBA1;
      }
      else if (i < 60) {
        f = (b & c) | (d & (b | c));
        k = 0x8F1BBCDC;
      }
      else {
        f = b ^ c ^ d;
        k = 0xCA62C1D6;
      }
      var temp = (a << 5 | a >>> 27) + f + e + k + (this.block[i]|0);
      e = d;
      d = c;
      c = (b << 30 | b >>> 2);
      b = a;
      a = temp;
    }

    // Add this chunk's hash to result so far:
    this.h0 = (this.h0 + a) | 0;
    this.h1 = (this.h1 + b) | 0;
    this.h2 = (this.h2 + c) | 0;
    this.h3 = (this.h3 + d) | 0;
    this.h4 = (this.h4 + e) | 0;

    // The block is now reusable.
    this.offset = 0;
    for (i = 0; i < 16; i++) {
        this.block[i] = 0;
    }
};


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

var Buffer = __webpack_require__(10).Buffer;
var hashUtils = __webpack_require__(14);

var BLOCK_SIZE = 64;

var DIGEST_LENGTH = 32;

var KEY = new Uint32Array([
    0x428a2f98,
    0x71374491,
    0xb5c0fbcf,
    0xe9b5dba5,
    0x3956c25b,
    0x59f111f1,
    0x923f82a4,
    0xab1c5ed5,
    0xd807aa98,
    0x12835b01,
    0x243185be,
    0x550c7dc3,
    0x72be5d74,
    0x80deb1fe,
    0x9bdc06a7,
    0xc19bf174,
    0xe49b69c1,
    0xefbe4786,
    0x0fc19dc6,
    0x240ca1cc,
    0x2de92c6f,
    0x4a7484aa,
    0x5cb0a9dc,
    0x76f988da,
    0x983e5152,
    0xa831c66d,
    0xb00327c8,
    0xbf597fc7,
    0xc6e00bf3,
    0xd5a79147,
    0x06ca6351,
    0x14292967,
    0x27b70a85,
    0x2e1b2138,
    0x4d2c6dfc,
    0x53380d13,
    0x650a7354,
    0x766a0abb,
    0x81c2c92e,
    0x92722c85,
    0xa2bfe8a1,
    0xa81a664b,
    0xc24b8b70,
    0xc76c51a3,
    0xd192e819,
    0xd6990624,
    0xf40e3585,
    0x106aa070,
    0x19a4c116,
    0x1e376c08,
    0x2748774c,
    0x34b0bcb5,
    0x391c0cb3,
    0x4ed8aa4a,
    0x5b9cca4f,
    0x682e6ff3,
    0x748f82ee,
    0x78a5636f,
    0x84c87814,
    0x8cc70208,
    0x90befffa,
    0xa4506ceb,
    0xbef9a3f7,
    0xc67178f2
]);

var INIT = [
    0x6a09e667,
    0xbb67ae85,
    0x3c6ef372,
    0xa54ff53a,
    0x510e527f,
    0x9b05688c,
    0x1f83d9ab,
    0x5be0cd19,
];

var MAX_HASHABLE_LENGTH = Math.pow(2, 53) - 1;

/**
 * @private
 */
function Sha256() {
    this.state = [
        0x6a09e667,
        0xbb67ae85,
        0x3c6ef372,
        0xa54ff53a,
        0x510e527f,
        0x9b05688c,
        0x1f83d9ab,
        0x5be0cd19,
    ];
    this.temp = new Int32Array(64);
    this.buffer = new Uint8Array(64);
    this.bufferLength = 0;
    this.bytesHashed = 0;
    /**
     * @private
     */
    this.finished = false;
}

/**
 * @api private
 */
module.exports = exports = Sha256;

Sha256.BLOCK_SIZE = BLOCK_SIZE;

Sha256.prototype.update = function (data) {
    if (this.finished) {
        throw new Error('Attempted to update an already finished hash.');
    }

    if (hashUtils.isEmptyData(data)) {
        return this;
    }

    data = hashUtils.convertToBuffer(data);

    var position = 0;
    var byteLength = data.byteLength;
    this.bytesHashed += byteLength;
    if (this.bytesHashed * 8 > MAX_HASHABLE_LENGTH) {
        throw new Error('Cannot hash more than 2^53 - 1 bits');
    }

    while (byteLength > 0) {
        this.buffer[this.bufferLength++] = data[position++];
        byteLength--;
        if (this.bufferLength === BLOCK_SIZE) {
            this.hashBuffer();
            this.bufferLength = 0;
        }
    }

    return this;
};

Sha256.prototype.digest = function (encoding) {
    if (!this.finished) {
        var bitsHashed = this.bytesHashed * 8;
        var bufferView = new DataView(this.buffer.buffer, this.buffer.byteOffset, this.buffer.byteLength);
        var undecoratedLength = this.bufferLength;
        bufferView.setUint8(this.bufferLength++, 0x80);
        // Ensure the final block has enough room for the hashed length
        if (undecoratedLength % BLOCK_SIZE >= BLOCK_SIZE - 8) {
            for (var i = this.bufferLength; i < BLOCK_SIZE; i++) {
                bufferView.setUint8(i, 0);
            }
            this.hashBuffer();
            this.bufferLength = 0;
        }
        for (var i = this.bufferLength; i < BLOCK_SIZE - 8; i++) {
            bufferView.setUint8(i, 0);
        }
        bufferView.setUint32(BLOCK_SIZE - 8, Math.floor(bitsHashed / 0x100000000), true);
        bufferView.setUint32(BLOCK_SIZE - 4, bitsHashed);
        this.hashBuffer();
        this.finished = true;
    }
    // The value in state is little-endian rather than big-endian, so flip
    // each word into a new Uint8Array
    var out = new Buffer(DIGEST_LENGTH);
    for (var i = 0; i < 8; i++) {
        out[i * 4] = (this.state[i] >>> 24) & 0xff;
        out[i * 4 + 1] = (this.state[i] >>> 16) & 0xff;
        out[i * 4 + 2] = (this.state[i] >>> 8) & 0xff;
        out[i * 4 + 3] = (this.state[i] >>> 0) & 0xff;
    }
    return encoding ? out.toString(encoding) : out;
};

Sha256.prototype.hashBuffer = function () {
    var _a = this,
        buffer = _a.buffer,
        state = _a.state;
    var state0 = state[0],
        state1 = state[1],
        state2 = state[2],
        state3 = state[3],
        state4 = state[4],
        state5 = state[5],
        state6 = state[6],
        state7 = state[7];
    for (var i = 0; i < BLOCK_SIZE; i++) {
        if (i < 16) {
            this.temp[i] = (((buffer[i * 4] & 0xff) << 24) |
                ((buffer[(i * 4) + 1] & 0xff) << 16) |
                ((buffer[(i * 4) + 2] & 0xff) << 8) |
                (buffer[(i * 4) + 3] & 0xff));
        }
        else {
            var u = this.temp[i - 2];
            var t1_1 = (u >>> 17 | u << 15) ^
                (u >>> 19 | u << 13) ^
                (u >>> 10);
            u = this.temp[i - 15];
            var t2_1 = (u >>> 7 | u << 25) ^
                (u >>> 18 | u << 14) ^
                (u >>> 3);
            this.temp[i] = (t1_1 + this.temp[i - 7] | 0) +
                (t2_1 + this.temp[i - 16] | 0);
        }
        var t1 = (((((state4 >>> 6 | state4 << 26) ^
            (state4 >>> 11 | state4 << 21) ^
            (state4 >>> 25 | state4 << 7))
            + ((state4 & state5) ^ (~state4 & state6))) | 0)
            + ((state7 + ((KEY[i] + this.temp[i]) | 0)) | 0)) | 0;
        var t2 = (((state0 >>> 2 | state0 << 30) ^
            (state0 >>> 13 | state0 << 19) ^
            (state0 >>> 22 | state0 << 10)) + ((state0 & state1) ^ (state0 & state2) ^ (state1 & state2))) | 0;
        state7 = state6;
        state6 = state5;
        state5 = state4;
        state4 = (state3 + t1) | 0;
        state3 = state2;
        state2 = state1;
        state1 = state0;
        state0 = (t1 + t2) | 0;
    }
    state[0] += state0;
    state[1] += state1;
    state[2] += state2;
    state[3] += state3;
    state[4] += state4;
    state[5] += state5;
    state[6] += state6;
    state[7] += state7;
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var punycode = __webpack_require__(99);
var util = __webpack_require__(101);

exports.parse = urlParse;
exports.resolve = urlResolve;
exports.resolveObject = urlResolveObject;
exports.format = urlFormat;

exports.Url = Url;

function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.host = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.query = null;
  this.pathname = null;
  this.path = null;
  this.href = null;
}

// Reference: RFC 3986, RFC 1808, RFC 2396

// define these here so at least they only have to be
// compiled once on the first module load.
var protocolPattern = /^([a-z0-9.+-]+:)/i,
    portPattern = /:[0-9]*$/,

    // Special case for a simple path URL
    simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,

    // RFC 2396: characters reserved for delimiting URLs.
    // We actually just auto-escape these.
    delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'],

    // RFC 2396: characters not allowed for various reasons.
    unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims),

    // Allowed by RFCs, but cause of XSS attacks.  Always escape these.
    autoEscape = ['\''].concat(unwise),
    // Characters that are never ever allowed in a hostname.
    // Note that any invalid chars are also handled, but these
    // are the ones that are *expected* to be seen, so we fast-path
    // them.
    nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape),
    hostEndingChars = ['/', '?', '#'],
    hostnameMaxLen = 255,
    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
    // protocols that can allow "unsafe" and "unwise" chars.
    unsafeProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that never have a hostname.
    hostlessProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that always contain a // bit.
    slashedProtocol = {
      'http': true,
      'https': true,
      'ftp': true,
      'gopher': true,
      'file': true,
      'http:': true,
      'https:': true,
      'ftp:': true,
      'gopher:': true,
      'file:': true
    },
    querystring = __webpack_require__(41);

function urlParse(url, parseQueryString, slashesDenoteHost) {
  if (url && util.isObject(url) && url instanceof Url) return url;

  var u = new Url;
  u.parse(url, parseQueryString, slashesDenoteHost);
  return u;
}

Url.prototype.parse = function(url, parseQueryString, slashesDenoteHost) {
  if (!util.isString(url)) {
    throw new TypeError("Parameter 'url' must be a string, not " + typeof url);
  }

  // Copy chrome, IE, opera backslash-handling behavior.
  // Back slashes before the query string get converted to forward slashes
  // See: https://code.google.com/p/chromium/issues/detail?id=25916
  var queryIndex = url.indexOf('?'),
      splitter =
          (queryIndex !== -1 && queryIndex < url.indexOf('#')) ? '?' : '#',
      uSplit = url.split(splitter),
      slashRegex = /\\/g;
  uSplit[0] = uSplit[0].replace(slashRegex, '/');
  url = uSplit.join(splitter);

  var rest = url;

  // trim before proceeding.
  // This is to support parse stuff like "  http://foo.com  \n"
  rest = rest.trim();

  if (!slashesDenoteHost && url.split('#').length === 1) {
    // Try fast path regexp
    var simplePath = simplePathPattern.exec(rest);
    if (simplePath) {
      this.path = rest;
      this.href = rest;
      this.pathname = simplePath[1];
      if (simplePath[2]) {
        this.search = simplePath[2];
        if (parseQueryString) {
          this.query = querystring.parse(this.search.substr(1));
        } else {
          this.query = this.search.substr(1);
        }
      } else if (parseQueryString) {
        this.search = '';
        this.query = {};
      }
      return this;
    }
  }

  var proto = protocolPattern.exec(rest);
  if (proto) {
    proto = proto[0];
    var lowerProto = proto.toLowerCase();
    this.protocol = lowerProto;
    rest = rest.substr(proto.length);
  }

  // figure out if it's got a host
  // user@server is *always* interpreted as a hostname, and url
  // resolution will treat //foo/bar as host=foo,path=bar because that's
  // how the browser resolves relative URLs.
  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    var slashes = rest.substr(0, 2) === '//';
    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }

  if (!hostlessProtocol[proto] &&
      (slashes || (proto && !slashedProtocol[proto]))) {

    // there's a hostname.
    // the first instance of /, ?, ;, or # ends the host.
    //
    // If there is an @ in the hostname, then non-host chars *are* allowed
    // to the left of the last @ sign, unless some host-ending character
    // comes *before* the @-sign.
    // URLs are obnoxious.
    //
    // ex:
    // http://a@b@c/ => user:a@b host:c
    // http://a@b?@c => user:a host:c path:/?@c

    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
    // Review our test case against browsers more comprehensively.

    // find the first instance of any hostEndingChars
    var hostEnd = -1;
    for (var i = 0; i < hostEndingChars.length; i++) {
      var hec = rest.indexOf(hostEndingChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }

    // at this point, either we have an explicit point where the
    // auth portion cannot go past, or the last @ char is the decider.
    var auth, atSign;
    if (hostEnd === -1) {
      // atSign can be anywhere.
      atSign = rest.lastIndexOf('@');
    } else {
      // atSign must be in auth portion.
      // http://a@b/c@d => host:b auth:a path:/c@d
      atSign = rest.lastIndexOf('@', hostEnd);
    }

    // Now we have a portion which is definitely the auth.
    // Pull that off.
    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = decodeURIComponent(auth);
    }

    // the host is the remaining to the left of the first non-host char
    hostEnd = -1;
    for (var i = 0; i < nonHostChars.length; i++) {
      var hec = rest.indexOf(nonHostChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }
    // if we still have not hit it, then the entire thing is a host.
    if (hostEnd === -1)
      hostEnd = rest.length;

    this.host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd);

    // pull out port.
    this.parseHost();

    // we've indicated that there is a hostname,
    // so even if it's empty, it has to be present.
    this.hostname = this.hostname || '';

    // if hostname begins with [ and ends with ]
    // assume that it's an IPv6 address.
    var ipv6Hostname = this.hostname[0] === '[' &&
        this.hostname[this.hostname.length - 1] === ']';

    // validate a little.
    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);
      for (var i = 0, l = hostparts.length; i < l; i++) {
        var part = hostparts[i];
        if (!part) continue;
        if (!part.match(hostnamePartPattern)) {
          var newpart = '';
          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              // we replace non-ASCII char with a temporary placeholder
              // we need this to make sure size of hostname is not
              // broken by replacing non-ASCII by nothing
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          }
          // we test again with ASCII char only
          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);
            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }
            if (notHost.length) {
              rest = '/' + notHost.join('.') + rest;
            }
            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }

    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    } else {
      // hostnames are always lower case.
      this.hostname = this.hostname.toLowerCase();
    }

    if (!ipv6Hostname) {
      // IDNA Support: Returns a punycoded representation of "domain".
      // It only converts parts of the domain name that
      // have non-ASCII characters, i.e. it doesn't matter if
      // you call it with a domain that already is ASCII-only.
      this.hostname = punycode.toASCII(this.hostname);
    }

    var p = this.port ? ':' + this.port : '';
    var h = this.hostname || '';
    this.host = h + p;
    this.href += this.host;

    // strip [ and ] from the hostname
    // the host field still retains them, though
    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
      if (rest[0] !== '/') {
        rest = '/' + rest;
      }
    }
  }

  // now rest is set to the post-host stuff.
  // chop off any delim chars.
  if (!unsafeProtocol[lowerProto]) {

    // First, make 100% sure that any "autoEscape" chars get
    // escaped, even if encodeURIComponent doesn't think they
    // need to be.
    for (var i = 0, l = autoEscape.length; i < l; i++) {
      var ae = autoEscape[i];
      if (rest.indexOf(ae) === -1)
        continue;
      var esc = encodeURIComponent(ae);
      if (esc === ae) {
        esc = escape(ae);
      }
      rest = rest.split(ae).join(esc);
    }
  }


  // chop off from the tail first.
  var hash = rest.indexOf('#');
  if (hash !== -1) {
    // got a fragment string.
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }
  var qm = rest.indexOf('?');
  if (qm !== -1) {
    this.search = rest.substr(qm);
    this.query = rest.substr(qm + 1);
    if (parseQueryString) {
      this.query = querystring.parse(this.query);
    }
    rest = rest.slice(0, qm);
  } else if (parseQueryString) {
    // no query string, but parseQueryString still requested
    this.search = '';
    this.query = {};
  }
  if (rest) this.pathname = rest;
  if (slashedProtocol[lowerProto] &&
      this.hostname && !this.pathname) {
    this.pathname = '/';
  }

  //to support http.request
  if (this.pathname || this.search) {
    var p = this.pathname || '';
    var s = this.search || '';
    this.path = p + s;
  }

  // finally, reconstruct the href based on what has been validated.
  this.href = this.format();
  return this;
};

// format a parsed object into a url string
function urlFormat(obj) {
  // ensure it's an object, and not a string url.
  // If it's an obj, this is a no-op.
  // this way, you can call url_format() on strings
  // to clean up potentially wonky urls.
  if (util.isString(obj)) obj = urlParse(obj);
  if (!(obj instanceof Url)) return Url.prototype.format.call(obj);
  return obj.format();
}

Url.prototype.format = function() {
  var auth = this.auth || '';
  if (auth) {
    auth = encodeURIComponent(auth);
    auth = auth.replace(/%3A/i, ':');
    auth += '@';
  }

  var protocol = this.protocol || '',
      pathname = this.pathname || '',
      hash = this.hash || '',
      host = false,
      query = '';

  if (this.host) {
    host = auth + this.host;
  } else if (this.hostname) {
    host = auth + (this.hostname.indexOf(':') === -1 ?
        this.hostname :
        '[' + this.hostname + ']');
    if (this.port) {
      host += ':' + this.port;
    }
  }

  if (this.query &&
      util.isObject(this.query) &&
      Object.keys(this.query).length) {
    query = querystring.stringify(this.query);
  }

  var search = this.search || (query && ('?' + query)) || '';

  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

  // only the slashedProtocols get the //.  Not mailto:, xmpp:, etc.
  // unless they had them to begin with.
  if (this.slashes ||
      (!protocol || slashedProtocol[protocol]) && host !== false) {
    host = '//' + (host || '');
    if (pathname && pathname.charAt(0) !== '/') pathname = '/' + pathname;
  } else if (!host) {
    host = '';
  }

  if (hash && hash.charAt(0) !== '#') hash = '#' + hash;
  if (search && search.charAt(0) !== '?') search = '?' + search;

  pathname = pathname.replace(/[?#]/g, function(match) {
    return encodeURIComponent(match);
  });
  search = search.replace('#', '%23');

  return protocol + host + pathname + search + hash;
};

function urlResolve(source, relative) {
  return urlParse(source, false, true).resolve(relative);
}

Url.prototype.resolve = function(relative) {
  return this.resolveObject(urlParse(relative, false, true)).format();
};

function urlResolveObject(source, relative) {
  if (!source) return relative;
  return urlParse(source, false, true).resolveObject(relative);
}

Url.prototype.resolveObject = function(relative) {
  if (util.isString(relative)) {
    var rel = new Url();
    rel.parse(relative, false, true);
    relative = rel;
  }

  var result = new Url();
  var tkeys = Object.keys(this);
  for (var tk = 0; tk < tkeys.length; tk++) {
    var tkey = tkeys[tk];
    result[tkey] = this[tkey];
  }

  // hash is always overridden, no matter what.
  // even href="" will remove it.
  result.hash = relative.hash;

  // if the relative url is empty, then there's nothing left to do here.
  if (relative.href === '') {
    result.href = result.format();
    return result;
  }

  // hrefs like //foo/bar always cut to the protocol.
  if (relative.slashes && !relative.protocol) {
    // take everything except the protocol from relative
    var rkeys = Object.keys(relative);
    for (var rk = 0; rk < rkeys.length; rk++) {
      var rkey = rkeys[rk];
      if (rkey !== 'protocol')
        result[rkey] = relative[rkey];
    }

    //urlParse appends trailing / to urls like http://www.example.com
    if (slashedProtocol[result.protocol] &&
        result.hostname && !result.pathname) {
      result.path = result.pathname = '/';
    }

    result.href = result.format();
    return result;
  }

  if (relative.protocol && relative.protocol !== result.protocol) {
    // if it's a known url protocol, then changing
    // the protocol does weird things
    // first, if it's not file:, then we MUST have a host,
    // and if there was a path
    // to begin with, then we MUST have a path.
    // if it is file:, then the host is dropped,
    // because that's known to be hostless.
    // anything else is assumed to be absolute.
    if (!slashedProtocol[relative.protocol]) {
      var keys = Object.keys(relative);
      for (var v = 0; v < keys.length; v++) {
        var k = keys[v];
        result[k] = relative[k];
      }
      result.href = result.format();
      return result;
    }

    result.protocol = relative.protocol;
    if (!relative.host && !hostlessProtocol[relative.protocol]) {
      var relPath = (relative.pathname || '').split('/');
      while (relPath.length && !(relative.host = relPath.shift()));
      if (!relative.host) relative.host = '';
      if (!relative.hostname) relative.hostname = '';
      if (relPath[0] !== '') relPath.unshift('');
      if (relPath.length < 2) relPath.unshift('');
      result.pathname = relPath.join('/');
    } else {
      result.pathname = relative.pathname;
    }
    result.search = relative.search;
    result.query = relative.query;
    result.host = relative.host || '';
    result.auth = relative.auth;
    result.hostname = relative.hostname || relative.host;
    result.port = relative.port;
    // to support http.request
    if (result.pathname || result.search) {
      var p = result.pathname || '';
      var s = result.search || '';
      result.path = p + s;
    }
    result.slashes = result.slashes || relative.slashes;
    result.href = result.format();
    return result;
  }

  var isSourceAbs = (result.pathname && result.pathname.charAt(0) === '/'),
      isRelAbs = (
          relative.host ||
          relative.pathname && relative.pathname.charAt(0) === '/'
      ),
      mustEndAbs = (isRelAbs || isSourceAbs ||
                    (result.host && relative.pathname)),
      removeAllDots = mustEndAbs,
      srcPath = result.pathname && result.pathname.split('/') || [],
      relPath = relative.pathname && relative.pathname.split('/') || [],
      psychotic = result.protocol && !slashedProtocol[result.protocol];

  // if the url is a non-slashed url, then relative
  // links like ../.. should be able
  // to crawl up to the hostname, as well.  This is strange.
  // result.protocol has already been set by now.
  // Later on, put the first path part into the host field.
  if (psychotic) {
    result.hostname = '';
    result.port = null;
    if (result.host) {
      if (srcPath[0] === '') srcPath[0] = result.host;
      else srcPath.unshift(result.host);
    }
    result.host = '';
    if (relative.protocol) {
      relative.hostname = null;
      relative.port = null;
      if (relative.host) {
        if (relPath[0] === '') relPath[0] = relative.host;
        else relPath.unshift(relative.host);
      }
      relative.host = null;
    }
    mustEndAbs = mustEndAbs && (relPath[0] === '' || srcPath[0] === '');
  }

  if (isRelAbs) {
    // it's absolute.
    result.host = (relative.host || relative.host === '') ?
                  relative.host : result.host;
    result.hostname = (relative.hostname || relative.hostname === '') ?
                      relative.hostname : result.hostname;
    result.search = relative.search;
    result.query = relative.query;
    srcPath = relPath;
    // fall through to the dot-handling below.
  } else if (relPath.length) {
    // it's relative
    // throw away the existing file, and take the new path instead.
    if (!srcPath) srcPath = [];
    srcPath.pop();
    srcPath = srcPath.concat(relPath);
    result.search = relative.search;
    result.query = relative.query;
  } else if (!util.isNullOrUndefined(relative.search)) {
    // just pull out the search.
    // like href='?foo'.
    // Put this after the other two cases because it simplifies the booleans
    if (psychotic) {
      result.hostname = result.host = srcPath.shift();
      //occationaly the auth can get stuck only in host
      //this especially happens in cases like
      //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
      var authInHost = result.host && result.host.indexOf('@') > 0 ?
                       result.host.split('@') : false;
      if (authInHost) {
        result.auth = authInHost.shift();
        result.host = result.hostname = authInHost.shift();
      }
    }
    result.search = relative.search;
    result.query = relative.query;
    //to support http.request
    if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
      result.path = (result.pathname ? result.pathname : '') +
                    (result.search ? result.search : '');
    }
    result.href = result.format();
    return result;
  }

  if (!srcPath.length) {
    // no path at all.  easy.
    // we've already handled the other stuff above.
    result.pathname = null;
    //to support http.request
    if (result.search) {
      result.path = '/' + result.search;
    } else {
      result.path = null;
    }
    result.href = result.format();
    return result;
  }

  // if a url ENDs in . or .., then it must get a trailing slash.
  // however, if it ends in anything else non-slashy,
  // then it must NOT get a trailing slash.
  var last = srcPath.slice(-1)[0];
  var hasTrailingSlash = (
      (result.host || relative.host || srcPath.length > 1) &&
      (last === '.' || last === '..') || last === '');

  // strip single dots, resolve double dots to parent dir
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = srcPath.length; i >= 0; i--) {
    last = srcPath[i];
    if (last === '.') {
      srcPath.splice(i, 1);
    } else if (last === '..') {
      srcPath.splice(i, 1);
      up++;
    } else if (up) {
      srcPath.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (!mustEndAbs && !removeAllDots) {
    for (; up--; up) {
      srcPath.unshift('..');
    }
  }

  if (mustEndAbs && srcPath[0] !== '' &&
      (!srcPath[0] || srcPath[0].charAt(0) !== '/')) {
    srcPath.unshift('');
  }

  if (hasTrailingSlash && (srcPath.join('/').substr(-1) !== '/')) {
    srcPath.push('');
  }

  var isAbsolute = srcPath[0] === '' ||
      (srcPath[0] && srcPath[0].charAt(0) === '/');

  // put the host back
  if (psychotic) {
    result.hostname = result.host = isAbsolute ? '' :
                                    srcPath.length ? srcPath.shift() : '';
    //occationaly the auth can get stuck only in host
    //this especially happens in cases like
    //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
    var authInHost = result.host && result.host.indexOf('@') > 0 ?
                     result.host.split('@') : false;
    if (authInHost) {
      result.auth = authInHost.shift();
      result.host = result.hostname = authInHost.shift();
    }
  }

  mustEndAbs = mustEndAbs || (result.host && srcPath.length);

  if (mustEndAbs && !isAbsolute) {
    srcPath.unshift('');
  }

  if (!srcPath.length) {
    result.pathname = null;
    result.path = null;
  } else {
    result.pathname = srcPath.join('/');
  }

  //to support request.http
  if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
    result.path = (result.pathname ? result.pathname : '') +
                  (result.search ? result.search : '');
  }
  result.auth = relative.auth || result.auth;
  result.slashes = result.slashes || relative.slashes;
  result.href = result.format();
  return result;
};

Url.prototype.parseHost = function() {
  var host = this.host;
  var port = portPattern.exec(host);
  if (port) {
    port = port[0];
    if (port !== ':') {
      this.port = port.substr(1);
    }
    host = host.substr(0, host.length - port.length);
  }
  if (host) this.hostname = host;
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, global) {var __WEBPACK_AMD_DEFINE_RESULT__;/*! https://mths.be/punycode v1.4.1 by @mathias */
;(function(root) {

	/** Detect free variables */
	var freeExports =  true && exports &&
		!exports.nodeType && exports;
	var freeModule =  true && module &&
		!module.nodeType && module;
	var freeGlobal = typeof global == 'object' && global;
	if (
		freeGlobal.global === freeGlobal ||
		freeGlobal.window === freeGlobal ||
		freeGlobal.self === freeGlobal
	) {
		root = freeGlobal;
	}

	/**
	 * The `punycode` object.
	 * @name punycode
	 * @type Object
	 */
	var punycode,

	/** Highest positive signed 32-bit float value */
	maxInt = 2147483647, // aka. 0x7FFFFFFF or 2^31-1

	/** Bootstring parameters */
	base = 36,
	tMin = 1,
	tMax = 26,
	skew = 38,
	damp = 700,
	initialBias = 72,
	initialN = 128, // 0x80
	delimiter = '-', // '\x2D'

	/** Regular expressions */
	regexPunycode = /^xn--/,
	regexNonASCII = /[^\x20-\x7E]/, // unprintable ASCII chars + non-ASCII chars
	regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g, // RFC 3490 separators

	/** Error messages */
	errors = {
		'overflow': 'Overflow: input needs wider integers to process',
		'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
		'invalid-input': 'Invalid input'
	},

	/** Convenience shortcuts */
	baseMinusTMin = base - tMin,
	floor = Math.floor,
	stringFromCharCode = String.fromCharCode,

	/** Temporary variable */
	key;

	/*--------------------------------------------------------------------------*/

	/**
	 * A generic error utility function.
	 * @private
	 * @param {String} type The error type.
	 * @returns {Error} Throws a `RangeError` with the applicable error message.
	 */
	function error(type) {
		throw new RangeError(errors[type]);
	}

	/**
	 * A generic `Array#map` utility function.
	 * @private
	 * @param {Array} array The array to iterate over.
	 * @param {Function} callback The function that gets called for every array
	 * item.
	 * @returns {Array} A new array of values returned by the callback function.
	 */
	function map(array, fn) {
		var length = array.length;
		var result = [];
		while (length--) {
			result[length] = fn(array[length]);
		}
		return result;
	}

	/**
	 * A simple `Array#map`-like wrapper to work with domain name strings or email
	 * addresses.
	 * @private
	 * @param {String} domain The domain name or email address.
	 * @param {Function} callback The function that gets called for every
	 * character.
	 * @returns {Array} A new string of characters returned by the callback
	 * function.
	 */
	function mapDomain(string, fn) {
		var parts = string.split('@');
		var result = '';
		if (parts.length > 1) {
			// In email addresses, only the domain name should be punycoded. Leave
			// the local part (i.e. everything up to `@`) intact.
			result = parts[0] + '@';
			string = parts[1];
		}
		// Avoid `split(regex)` for IE8 compatibility. See #17.
		string = string.replace(regexSeparators, '\x2E');
		var labels = string.split('.');
		var encoded = map(labels, fn).join('.');
		return result + encoded;
	}

	/**
	 * Creates an array containing the numeric code points of each Unicode
	 * character in the string. While JavaScript uses UCS-2 internally,
	 * this function will convert a pair of surrogate halves (each of which
	 * UCS-2 exposes as separate characters) into a single code point,
	 * matching UTF-16.
	 * @see `punycode.ucs2.encode`
	 * @see <https://mathiasbynens.be/notes/javascript-encoding>
	 * @memberOf punycode.ucs2
	 * @name decode
	 * @param {String} string The Unicode input string (UCS-2).
	 * @returns {Array} The new array of code points.
	 */
	function ucs2decode(string) {
		var output = [],
		    counter = 0,
		    length = string.length,
		    value,
		    extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
					output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// unmatched surrogate; only append this code unit, in case the next
					// code unit is the high surrogate of a surrogate pair
					output.push(value);
					counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	/**
	 * Creates a string based on an array of numeric code points.
	 * @see `punycode.ucs2.decode`
	 * @memberOf punycode.ucs2
	 * @name encode
	 * @param {Array} codePoints The array of numeric code points.
	 * @returns {String} The new Unicode string (UCS-2).
	 */
	function ucs2encode(array) {
		return map(array, function(value) {
			var output = '';
			if (value > 0xFFFF) {
				value -= 0x10000;
				output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
				value = 0xDC00 | value & 0x3FF;
			}
			output += stringFromCharCode(value);
			return output;
		}).join('');
	}

	/**
	 * Converts a basic code point into a digit/integer.
	 * @see `digitToBasic()`
	 * @private
	 * @param {Number} codePoint The basic numeric code point value.
	 * @returns {Number} The numeric value of a basic code point (for use in
	 * representing integers) in the range `0` to `base - 1`, or `base` if
	 * the code point does not represent a value.
	 */
	function basicToDigit(codePoint) {
		if (codePoint - 48 < 10) {
			return codePoint - 22;
		}
		if (codePoint - 65 < 26) {
			return codePoint - 65;
		}
		if (codePoint - 97 < 26) {
			return codePoint - 97;
		}
		return base;
	}

	/**
	 * Converts a digit/integer into a basic code point.
	 * @see `basicToDigit()`
	 * @private
	 * @param {Number} digit The numeric value of a basic code point.
	 * @returns {Number} The basic code point whose value (when used for
	 * representing integers) is `digit`, which needs to be in the range
	 * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
	 * used; else, the lowercase form is used. The behavior is undefined
	 * if `flag` is non-zero and `digit` has no uppercase form.
	 */
	function digitToBasic(digit, flag) {
		//  0..25 map to ASCII a..z or A..Z
		// 26..35 map to ASCII 0..9
		return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
	}

	/**
	 * Bias adaptation function as per section 3.4 of RFC 3492.
	 * https://tools.ietf.org/html/rfc3492#section-3.4
	 * @private
	 */
	function adapt(delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime ? floor(delta / damp) : delta >> 1;
		delta += floor(delta / numPoints);
		for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
			delta = floor(delta / baseMinusTMin);
		}
		return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
	}

	/**
	 * Converts a Punycode string of ASCII-only symbols to a string of Unicode
	 * symbols.
	 * @memberOf punycode
	 * @param {String} input The Punycode string of ASCII-only symbols.
	 * @returns {String} The resulting string of Unicode symbols.
	 */
	function decode(input) {
		// Don't use UCS-2
		var output = [],
		    inputLength = input.length,
		    out,
		    i = 0,
		    n = initialN,
		    bias = initialBias,
		    basic,
		    j,
		    index,
		    oldi,
		    w,
		    k,
		    digit,
		    t,
		    /** Cached calculation results */
		    baseMinusT;

		// Handle the basic code points: let `basic` be the number of input code
		// points before the last delimiter, or `0` if there is none, then copy
		// the first basic code points to the output.

		basic = input.lastIndexOf(delimiter);
		if (basic < 0) {
			basic = 0;
		}

		for (j = 0; j < basic; ++j) {
			// if it's not a basic code point
			if (input.charCodeAt(j) >= 0x80) {
				error('not-basic');
			}
			output.push(input.charCodeAt(j));
		}

		// Main decoding loop: start just after the last delimiter if any basic code
		// points were copied; start at the beginning otherwise.

		for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {

			// `index` is the index of the next character to be consumed.
			// Decode a generalized variable-length integer into `delta`,
			// which gets added to `i`. The overflow checking is easier
			// if we increase `i` as we go, then subtract off its starting
			// value at the end to obtain `delta`.
			for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

				if (index >= inputLength) {
					error('invalid-input');
				}

				digit = basicToDigit(input.charCodeAt(index++));

				if (digit >= base || digit > floor((maxInt - i) / w)) {
					error('overflow');
				}

				i += digit * w;
				t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

				if (digit < t) {
					break;
				}

				baseMinusT = base - t;
				if (w > floor(maxInt / baseMinusT)) {
					error('overflow');
				}

				w *= baseMinusT;

			}

			out = output.length + 1;
			bias = adapt(i - oldi, out, oldi == 0);

			// `i` was supposed to wrap around from `out` to `0`,
			// incrementing `n` each time, so we'll fix that now:
			if (floor(i / out) > maxInt - n) {
				error('overflow');
			}

			n += floor(i / out);
			i %= out;

			// Insert `n` at position `i` of the output
			output.splice(i++, 0, n);

		}

		return ucs2encode(output);
	}

	/**
	 * Converts a string of Unicode symbols (e.g. a domain name label) to a
	 * Punycode string of ASCII-only symbols.
	 * @memberOf punycode
	 * @param {String} input The string of Unicode symbols.
	 * @returns {String} The resulting Punycode string of ASCII-only symbols.
	 */
	function encode(input) {
		var n,
		    delta,
		    handledCPCount,
		    basicLength,
		    bias,
		    j,
		    m,
		    q,
		    k,
		    t,
		    currentValue,
		    output = [],
		    /** `inputLength` will hold the number of code points in `input`. */
		    inputLength,
		    /** Cached calculation results */
		    handledCPCountPlusOne,
		    baseMinusT,
		    qMinusT;

		// Convert the input in UCS-2 to Unicode
		input = ucs2decode(input);

		// Cache the length
		inputLength = input.length;

		// Initialize the state
		n = initialN;
		delta = 0;
		bias = initialBias;

		// Handle the basic code points
		for (j = 0; j < inputLength; ++j) {
			currentValue = input[j];
			if (currentValue < 0x80) {
				output.push(stringFromCharCode(currentValue));
			}
		}

		handledCPCount = basicLength = output.length;

		// `handledCPCount` is the number of code points that have been handled;
		// `basicLength` is the number of basic code points.

		// Finish the basic string - if it is not empty - with a delimiter
		if (basicLength) {
			output.push(delimiter);
		}

		// Main encoding loop:
		while (handledCPCount < inputLength) {

			// All non-basic code points < n have been handled already. Find the next
			// larger one:
			for (m = maxInt, j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue >= n && currentValue < m) {
					m = currentValue;
				}
			}

			// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
			// but guard against overflow
			handledCPCountPlusOne = handledCPCount + 1;
			if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
				error('overflow');
			}

			delta += (m - n) * handledCPCountPlusOne;
			n = m;

			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];

				if (currentValue < n && ++delta > maxInt) {
					error('overflow');
				}

				if (currentValue == n) {
					// Represent delta as a generalized variable-length integer
					for (q = delta, k = base; /* no condition */; k += base) {
						t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
						if (q < t) {
							break;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						output.push(
							stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
						);
						q = floor(qMinusT / baseMinusT);
					}

					output.push(stringFromCharCode(digitToBasic(q, 0)));
					bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
					delta = 0;
					++handledCPCount;
				}
			}

			++delta;
			++n;

		}
		return output.join('');
	}

	/**
	 * Converts a Punycode string representing a domain name or an email address
	 * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
	 * it doesn't matter if you call it on a string that has already been
	 * converted to Unicode.
	 * @memberOf punycode
	 * @param {String} input The Punycoded domain name or email address to
	 * convert to Unicode.
	 * @returns {String} The Unicode representation of the given Punycode
	 * string.
	 */
	function toUnicode(input) {
		return mapDomain(input, function(string) {
			return regexPunycode.test(string)
				? decode(string.slice(4).toLowerCase())
				: string;
		});
	}

	/**
	 * Converts a Unicode string representing a domain name or an email address to
	 * Punycode. Only the non-ASCII parts of the domain name will be converted,
	 * i.e. it doesn't matter if you call it with a domain that's already in
	 * ASCII.
	 * @memberOf punycode
	 * @param {String} input The domain name or email address to convert, as a
	 * Unicode string.
	 * @returns {String} The Punycode representation of the given domain name or
	 * email address.
	 */
	function toASCII(input) {
		return mapDomain(input, function(string) {
			return regexNonASCII.test(string)
				? 'xn--' + encode(string)
				: string;
		});
	}

	/*--------------------------------------------------------------------------*/

	/** Define the public API */
	punycode = {
		/**
		 * A string representing the current Punycode.js version number.
		 * @memberOf punycode
		 * @type String
		 */
		'version': '1.4.1',
		/**
		 * An object of methods to convert from JavaScript's internal character
		 * representation (UCS-2) to Unicode code points, and back.
		 * @see <https://mathiasbynens.be/notes/javascript-encoding>
		 * @memberOf punycode
		 * @type Object
		 */
		'ucs2': {
			'decode': ucs2decode,
			'encode': ucs2encode
		},
		'decode': decode,
		'encode': encode,
		'toASCII': toASCII,
		'toUnicode': toUnicode
	};

	/** Expose `punycode` */
	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		true
	) {
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
			return punycode;
		}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}

}(this));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(100)(module), __webpack_require__(8)))

/***/ }),
/* 100 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  isString: function(arg) {
    return typeof(arg) === 'string';
  },
  isObject: function(arg) {
    return typeof(arg) === 'object' && arg !== null;
  },
  isNull: function(arg) {
    return arg === null;
  },
  isNullOrUndefined: function(arg) {
    return arg == null;
  }
};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



// If obj.hasOwnProperty has been overridden, then calling
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707
function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

module.exports = function(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);

  var maxKeys = 1000;
  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length;
  // maxKeys <= 0 means that we should not limit keys count
  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr, vstr, k, v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var stringifyPrimitive = function(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

module.exports = function(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';
  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return map(objectKeys(obj), function(k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
      if (isArray(obj[k])) {
        return map(obj[k], function(v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);

  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq +
         encodeURIComponent(stringifyPrimitive(obj));
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

function map (xs, f) {
  if (xs.map) return xs.map(f);
  var res = [];
  for (var i = 0; i < xs.length; i++) {
    res.push(f(xs[i], i));
  }
  return res;
}

var objectKeys = Object.keys || function (obj) {
  var res = [];
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
  }
  return res;
};


/***/ }),
/* 104 */
/***/ (function(module, exports) {

module.exports = {
  //provide realtime clock for performance measurement
  now: function now() {
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
      return performance.now();
    }
    return Date.now();
  }
};


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

var eventMessageChunker = __webpack_require__(106).eventMessageChunker;
var parseEvent = __webpack_require__(107).parseEvent;

function createEventStream(body, parser, model) {
    var eventMessages = eventMessageChunker(body);

    var events = [];

    for (var i = 0; i < eventMessages.length; i++) {
        events.push(parseEvent(parser, eventMessages[i], model));
    }

    return events;
}

/**
 * @api private
 */
module.exports = {
    createEventStream: createEventStream
};


/***/ }),
/* 106 */
/***/ (function(module, exports) {

/**
 * Takes in a buffer of event messages and splits them into individual messages.
 * @param {Buffer} buffer
 * @api private
 */
function eventMessageChunker(buffer) {
    /** @type Buffer[] */
    var messages = [];
    var offset = 0;

    while (offset < buffer.length) {
        var totalLength = buffer.readInt32BE(offset);

        // create new buffer for individual message (shares memory with original)
        var message = buffer.slice(offset, totalLength + offset);
        // increment offset to it starts at the next message
        offset += totalLength;

        messages.push(message);
    }

    return messages;
}

/**
 * @api private
 */
module.exports = {
    eventMessageChunker: eventMessageChunker
};


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

var parseMessage = __webpack_require__(108).parseMessage;

/**
 *
 * @param {*} parser
 * @param {Buffer} message
 * @param {*} shape
 * @api private
 */
function parseEvent(parser, message, shape) {
    var parsedMessage = parseMessage(message);

    // check if message is an event or error
    var messageType = parsedMessage.headers[':message-type'];
    if (messageType) {
        if (messageType.value === 'error') {
            throw parseError(parsedMessage);
        } else if (messageType.value !== 'event') {
            // not sure how to parse non-events/non-errors, ignore for now
            return;
        }
    }

    // determine event type
    var eventType = parsedMessage.headers[':event-type'];
    // check that the event type is modeled
    var eventModel = shape.members[eventType.value];
    if (!eventModel) {
        return;
    }

    var result = {};
    // check if an event payload exists
    var eventPayloadMemberName = eventModel.eventPayloadMemberName;
    if (eventPayloadMemberName) {
        var payloadShape = eventModel.members[eventPayloadMemberName];
        // if the shape is binary, return the byte array
        if (payloadShape.type === 'binary') {
            result[eventPayloadMemberName] = parsedMessage.body;
        } else {
            result[eventPayloadMemberName] = parser.parse(parsedMessage.body.toString(), payloadShape);
        }
    }

    // read event headers
    var eventHeaderNames = eventModel.eventHeaderMemberNames;
    for (var i = 0; i < eventHeaderNames.length; i++) {
        var name = eventHeaderNames[i];
        if (parsedMessage.headers[name]) {
            // parse the header!
            result[name] = eventModel.members[name].toType(parsedMessage.headers[name].value);
        }
    }

    var output = {};
    output[eventType.value] = result;
    return output;
}

function parseError(message) {
    var errorCode = message.headers[':error-code'];
    var errorMessage = message.headers[':error-message'];
    var error = new Error(errorMessage.value || errorMessage);
    error.code = error.name = errorCode.value || errorCode;
    return error;
}

/**
 * @api private
 */
module.exports = {
    parseEvent: parseEvent
};


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

var Int64 = __webpack_require__(109).Int64;

var splitMessage = __webpack_require__(110).splitMessage;

var BOOLEAN_TAG = 'boolean';
var BYTE_TAG = 'byte';
var SHORT_TAG = 'short';
var INT_TAG = 'integer';
var LONG_TAG = 'long';
var BINARY_TAG = 'binary';
var STRING_TAG = 'string';
var TIMESTAMP_TAG = 'timestamp';
var UUID_TAG = 'uuid';

/**
 * @api private
 *
 * @param {Buffer} headers
 */
function parseHeaders(headers) {
    var out = {};
    var position = 0;
    while (position < headers.length) {
        var nameLength = headers.readUInt8(position++);
        var name = headers.slice(position, position + nameLength).toString();
        position += nameLength;
        switch (headers.readUInt8(position++)) {
            case 0 /* boolTrue */:
                out[name] = {
                    type: BOOLEAN_TAG,
                    value: true
                };
                break;
            case 1 /* boolFalse */:
                out[name] = {
                    type: BOOLEAN_TAG,
                    value: false
                };
                break;
            case 2 /* byte */:
                out[name] = {
                    type: BYTE_TAG,
                    value: headers.readInt8(position++)
                };
                break;
            case 3 /* short */:
                out[name] = {
                    type: SHORT_TAG,
                    value: headers.readInt16BE(position)
                };
                position += 2;
                break;
            case 4 /* integer */:
                out[name] = {
                    type: INT_TAG,
                    value: headers.readInt32BE(position)
                };
                position += 4;
                break;
            case 5 /* long */:
                out[name] = {
                    type: LONG_TAG,
                    value: new Int64(headers.slice(position, position + 8))
                };
                position += 8;
                break;
            case 6 /* byteArray */:
                var binaryLength = headers.readUInt16BE(position);
                position += 2;
                out[name] = {
                    type: BINARY_TAG,
                    value: headers.slice(position, position + binaryLength)
                };
                position += binaryLength;
                break;
            case 7 /* string */:
                var stringLength = headers.readUInt16BE(position);
                position += 2;
                out[name] = {
                    type: STRING_TAG,
                    value: headers.slice(
                        position,
                        position + stringLength
                    ).toString()
                };
                position += stringLength;
                break;
            case 8 /* timestamp */:
                out[name] = {
                    type: TIMESTAMP_TAG,
                    value: new Date(
                        new Int64(headers.slice(position, position + 8))
                            .valueOf()
                    )
                };
                position += 8;
                break;
            case 9 /* uuid */:
                var uuidChars = headers.slice(position, position + 16)
                    .toString('hex');
                position += 16;
                out[name] = {
                    type: UUID_TAG,
                    value: uuidChars.substr(0, 8) + '-' +
                        uuidChars.substr(8, 4) + '-' +
                        uuidChars.substr(12, 4) + '-' +
                        uuidChars.substr(16, 4) + '-' +
                        uuidChars.substr(20)
                };
                break;
            default:
                throw new Error('Unrecognized header type tag');
        }
    }
    return out;
}

function parseMessage(message) {
    var parsed = splitMessage(message);
    return { headers: parseHeaders(parsed.headers), body: parsed.body };
}

/**
 * @api private
 */
module.exports = {
    parseMessage: parseMessage
};


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(1).util;
var toBuffer = util.buffer.toBuffer;

/**
 * A lossless representation of a signed, 64-bit integer. Instances of this
 * class may be used in arithmetic expressions as if they were numeric
 * primitives, but the binary representation will be preserved unchanged as the
 * `bytes` property of the object. The bytes should be encoded as big-endian,
 * two's complement integers.
 * @param {Buffer} bytes
 *
 * @api private
 */
function Int64(bytes) {
    if (bytes.length !== 8) {
        throw new Error('Int64 buffers must be exactly 8 bytes');
    }
    if (!util.Buffer.isBuffer(bytes)) bytes = toBuffer(bytes);

    this.bytes = bytes;
}

/**
 * @param {number} number
 * @returns {Int64}
 *
 * @api private
 */
Int64.fromNumber = function(number) {
    if (number > 9223372036854775807 || number < -9223372036854775808) {
        throw new Error(
            number + ' is too large (or, if negative, too small) to represent as an Int64'
        );
    }

    var bytes = new Uint8Array(8);
    for (
        var i = 7, remaining = Math.abs(Math.round(number));
        i > -1 && remaining > 0;
        i--, remaining /= 256
    ) {
        bytes[i] = remaining;
    }

    if (number < 0) {
        negate(bytes);
    }

    return new Int64(bytes);
};

/**
 * @returns {number}
 *
 * @api private
 */
Int64.prototype.valueOf = function() {
    var bytes = this.bytes.slice(0);
    var negative = bytes[0] & 128;
    if (negative) {
        negate(bytes);
    }

    return parseInt(bytes.toString('hex'), 16) * (negative ? -1 : 1);
};

Int64.prototype.toString = function() {
    return String(this.valueOf());
};

/**
 * @param {Buffer} bytes
 *
 * @api private
 */
function negate(bytes) {
    for (var i = 0; i < 8; i++) {
        bytes[i] ^= 0xFF;
    }
    for (var i = 7; i > -1; i--) {
        bytes[i]++;
        if (bytes[i] !== 0) {
            break;
        }
    }
}

/**
 * @api private
 */
module.exports = {
    Int64: Int64
};


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(1).util;
var toBuffer = util.buffer.toBuffer;

// All prelude components are unsigned, 32-bit integers
var PRELUDE_MEMBER_LENGTH = 4;
// The prelude consists of two components
var PRELUDE_LENGTH = PRELUDE_MEMBER_LENGTH * 2;
// Checksums are always CRC32 hashes.
var CHECKSUM_LENGTH = 4;
// Messages must include a full prelude, a prelude checksum, and a message checksum
var MINIMUM_MESSAGE_LENGTH = PRELUDE_LENGTH + CHECKSUM_LENGTH * 2;

/**
 * @api private
 *
 * @param {Buffer} message
 */
function splitMessage(message) {
    if (!util.Buffer.isBuffer(message)) message = toBuffer(message);

    if (message.length < MINIMUM_MESSAGE_LENGTH) {
        throw new Error('Provided message too short to accommodate event stream message overhead');
    }

    if (message.length !== message.readUInt32BE(0)) {
        throw new Error('Reported message length does not match received message length');
    }

    var expectedPreludeChecksum = message.readUInt32BE(PRELUDE_LENGTH);

    if (
        expectedPreludeChecksum !== util.crypto.crc32(
            message.slice(0, PRELUDE_LENGTH)
        )
    ) {
        throw new Error(
            'The prelude checksum specified in the message (' +
            expectedPreludeChecksum +
            ') does not match the calculated CRC32 checksum.'
        );
    }

    var expectedMessageChecksum = message.readUInt32BE(message.length - CHECKSUM_LENGTH);

    if (
        expectedMessageChecksum !== util.crypto.crc32(
            message.slice(0, message.length - CHECKSUM_LENGTH)
        )
    ) {
        throw new Error(
            'The message checksum did not match the expected value of ' +
                expectedMessageChecksum
        );
    }

    var headersStart = PRELUDE_LENGTH + CHECKSUM_LENGTH;
    var headersEnd = headersStart + message.readUInt32BE(PRELUDE_MEMBER_LENGTH);

    return {
        headers: message.slice(headersStart, headersEnd),
        body: message.slice(headersEnd, message.length - CHECKSUM_LENGTH),
    };
}

/**
 * @api private
 */
module.exports = {
    splitMessage: splitMessage
};


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var STS = __webpack_require__(11);

/**
 * Represents temporary credentials retrieved from {AWS.STS}. Without any
 * extra parameters, credentials will be fetched from the
 * {AWS.STS.getSessionToken} operation. If an IAM role is provided, the
 * {AWS.STS.assumeRole} operation will be used to fetch credentials for the
 * role instead.
 *
 * @note AWS.TemporaryCredentials is deprecated, but remains available for
 *   backwards compatibility. {AWS.ChainableTemporaryCredentials} is the
 *   preferred class for temporary credentials.
 *
 * To setup temporary credentials, configure a set of master credentials
 * using the standard credentials providers (environment, EC2 instance metadata,
 * or from the filesystem), then set the global credentials to a new
 * temporary credentials object:
 *
 * ```javascript
 * // Note that environment credentials are loaded by default,
 * // the following line is shown for clarity:
 * AWS.config.credentials = new AWS.EnvironmentCredentials('AWS');
 *
 * // Now set temporary credentials seeded from the master credentials
 * AWS.config.credentials = new AWS.TemporaryCredentials();
 *
 * // subsequent requests will now use temporary credentials from AWS STS.
 * new AWS.S3().listBucket(function(err, data) { ... });
 * ```
 *
 * @!attribute masterCredentials
 *   @return [AWS.Credentials] the master (non-temporary) credentials used to
 *     get and refresh temporary credentials from AWS STS.
 * @note (see constructor)
 */
AWS.TemporaryCredentials = AWS.util.inherit(AWS.Credentials, {
  /**
   * Creates a new temporary credentials object.
   *
   * @note In order to create temporary credentials, you first need to have
   *   "master" credentials configured in {AWS.Config.credentials}. These
   *   master credentials are necessary to retrieve the temporary credentials,
   *   as well as refresh the credentials when they expire.
   * @param params [map] a map of options that are passed to the
   *   {AWS.STS.assumeRole} or {AWS.STS.getSessionToken} operations.
   *   If a `RoleArn` parameter is passed in, credentials will be based on the
   *   IAM role.
   * @param masterCredentials [AWS.Credentials] the master (non-temporary) credentials
   *  used to get and refresh temporary credentials from AWS STS.
   * @example Creating a new credentials object for generic temporary credentials
   *   AWS.config.credentials = new AWS.TemporaryCredentials();
   * @example Creating a new credentials object for an IAM role
   *   AWS.config.credentials = new AWS.TemporaryCredentials({
   *     RoleArn: 'arn:aws:iam::1234567890:role/TemporaryCredentials',
   *   });
   * @see AWS.STS.assumeRole
   * @see AWS.STS.getSessionToken
   */
  constructor: function TemporaryCredentials(params, masterCredentials) {
    AWS.Credentials.call(this);
    this.loadMasterCredentials(masterCredentials);
    this.expired = true;

    this.params = params || {};
    if (this.params.RoleArn) {
      this.params.RoleSessionName =
        this.params.RoleSessionName || 'temporary-credentials';
    }
  },

  /**
   * Refreshes credentials using {AWS.STS.assumeRole} or
   * {AWS.STS.getSessionToken}, depending on whether an IAM role ARN was passed
   * to the credentials {constructor}.
   *
   * @callback callback function(err)
   *   Called when the STS service responds (or fails). When
   *   this callback is called with no error, it means that the credentials
   *   information has been loaded into the object (as the `accessKeyId`,
   *   `secretAccessKey`, and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   * @see get
   */
  refresh: function refresh (callback) {
    this.coalesceRefresh(callback || AWS.util.fn.callback);
  },

  /**
   * @api private
   */
  load: function load (callback) {
    var self = this;
    self.createClients();
    self.masterCredentials.get(function () {
      self.service.config.credentials = self.masterCredentials;
      var operation = self.params.RoleArn ?
        self.service.assumeRole : self.service.getSessionToken;
      operation.call(self.service, function (err, data) {
        if (!err) {
          self.service.credentialsFrom(data, self);
        }
        callback(err);
      });
    });
  },

  /**
   * @api private
   */
  loadMasterCredentials: function loadMasterCredentials (masterCredentials) {
    this.masterCredentials = masterCredentials || AWS.config.credentials;
    while (this.masterCredentials.masterCredentials) {
      this.masterCredentials = this.masterCredentials.masterCredentials;
    }

    if (typeof this.masterCredentials.get !== 'function') {
      this.masterCredentials = new AWS.Credentials(this.masterCredentials);
    }
  },

  /**
   * @api private
   */
  createClients: function () {
    this.service = this.service || new STS({params: this.params});
  }

});


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var resolveRegionalEndpointsFlag = __webpack_require__(113);
var ENV_REGIONAL_ENDPOINT_ENABLED = 'AWS_STS_REGIONAL_ENDPOINTS';
var CONFIG_REGIONAL_ENDPOINT_ENABLED = 'sts_regional_endpoints';

AWS.util.update(AWS.STS.prototype, {
  /**
   * @overload credentialsFrom(data, credentials = null)
   *   Creates a credentials object from STS response data containing
   *   credentials information. Useful for quickly setting AWS credentials.
   *
   *   @note This is a low-level utility function. If you want to load temporary
   *     credentials into your process for subsequent requests to AWS resources,
   *     you should use {AWS.TemporaryCredentials} instead.
   *   @param data [map] data retrieved from a call to {getFederatedToken},
   *     {getSessionToken}, {assumeRole}, or {assumeRoleWithWebIdentity}.
   *   @param credentials [AWS.Credentials] an optional credentials object to
   *     fill instead of creating a new object. Useful when modifying an
   *     existing credentials object from a refresh call.
   *   @return [AWS.TemporaryCredentials] the set of temporary credentials
   *     loaded from a raw STS operation response.
   *   @example Using credentialsFrom to load global AWS credentials
   *     var sts = new AWS.STS();
   *     sts.getSessionToken(function (err, data) {
   *       if (err) console.log("Error getting credentials");
   *       else {
   *         AWS.config.credentials = sts.credentialsFrom(data);
   *       }
   *     });
   *   @see AWS.TemporaryCredentials
   */
  credentialsFrom: function credentialsFrom(data, credentials) {
    if (!data) return null;
    if (!credentials) credentials = new AWS.TemporaryCredentials();
    credentials.expired = false;
    credentials.accessKeyId = data.Credentials.AccessKeyId;
    credentials.secretAccessKey = data.Credentials.SecretAccessKey;
    credentials.sessionToken = data.Credentials.SessionToken;
    credentials.expireTime = data.Credentials.Expiration;
    return credentials;
  },

  assumeRoleWithWebIdentity: function assumeRoleWithWebIdentity(params, callback) {
    return this.makeUnauthenticatedRequest('assumeRoleWithWebIdentity', params, callback);
  },

  assumeRoleWithSAML: function assumeRoleWithSAML(params, callback) {
    return this.makeUnauthenticatedRequest('assumeRoleWithSAML', params, callback);
  },

  /**
   * @api private
   */
  setupRequestListeners: function setupRequestListeners(request) {
    request.addListener('validate', this.optInRegionalEndpoint, true);
  },

  /**
   * @api private
   */
  optInRegionalEndpoint: function optInRegionalEndpoint(req) {
    var service = req.service;
    var config = service.config;
    config.stsRegionalEndpoints = resolveRegionalEndpointsFlag(service._originalConfig, {
      env: ENV_REGIONAL_ENDPOINT_ENABLED,
      sharedConfig: CONFIG_REGIONAL_ENDPOINT_ENABLED,
      clientConfig: 'stsRegionalEndpoints'
    });
    if (
      config.stsRegionalEndpoints === 'regional' &&
      service.isGlobalEndpoint
    ) {
      //client will throw if region is not supplied; request will be signed with specified region
      if (!config.region) {
        throw AWS.util.error(new Error(),
          {code: 'ConfigError', message: 'Missing region in config'});
      }
      var insertPoint = config.endpoint.indexOf('.amazonaws.com');
      var regionalEndpoint = config.endpoint.substring(0, insertPoint) +
        '.' + config.region + config.endpoint.substring(insertPoint);
      req.httpRequest.updateEndpoint(regionalEndpoint);
      req.httpRequest.region = config.region;
    }
  }

});


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
/**
 * @api private
 */
function validateRegionalEndpointsFlagValue(configValue, errorOptions) {
  if (typeof configValue !== 'string') return undefined;
  else if (['legacy', 'regional'].indexOf(configValue.toLowerCase()) >= 0) {
    return configValue.toLowerCase();
  } else {
    throw AWS.util.error(new Error(), errorOptions);
  }
}

/**
 * Resolve the configuration value for regional endpoint from difference sources: client
 * config, environmental variable, shared config file. Value can be case-insensitive
 * 'legacy' or 'reginal'.
 * @param originalConfig user-supplied config object to resolve
 * @param options a map of config property names from individual configuration source
 *  - env: name of environmental variable that refers to the config
 *  - sharedConfig: name of shared configuration file property that refers to the config
 *  - clientConfig: name of client configuration property that refers to the config
 *
 * @api private
 */
function resolveRegionalEndpointsFlag(originalConfig, options) {
  originalConfig = originalConfig || {};
  //validate config value
  var resolved;
  if (originalConfig[options.clientConfig]) {
    resolved = validateRegionalEndpointsFlagValue(originalConfig[options.clientConfig], {
      code: 'InvalidConfiguration',
      message: 'invalid "' + options.clientConfig + '" configuration. Expect "legacy" ' +
      ' or "regional". Got "' + originalConfig[options.clientConfig] + '".'
    });
    if (resolved) return resolved;
  }
  if (!AWS.util.isNode()) return resolved;
  //validate environmental variable
  if (Object.prototype.hasOwnProperty.call(Object({"NODE_ENV":undefined}), options.env)) {
    var envFlag = Object({"NODE_ENV":undefined})[options.env];
    resolved = validateRegionalEndpointsFlagValue(envFlag, {
      code: 'InvalidEnvironmentalVariable',
      message: 'invalid ' + options.env + ' environmental variable. Expect "legacy" ' +
      ' or "regional". Got "' + Object({"NODE_ENV":undefined})[options.env] + '".'
    });
    if (resolved) return resolved;
  }
  //validate shared config file
  var profile = {};
  try {
    var profiles = AWS.util.getProfilesFromSharedConfig(AWS.util.iniLoader);
    profile = profiles[Object({"NODE_ENV":undefined}).AWS_PROFILE || AWS.util.defaultProfile];
  } catch (e) {};
  if (profile && Object.prototype.hasOwnProperty.call(profile, options.sharedConfig)) {
    var fileFlag = profile[options.sharedConfig];
    resolved = validateRegionalEndpointsFlagValue(fileFlag, {
      code: 'InvalidConfiguration',
      message: 'invalid ' + options.sharedConfig + ' profile config. Expect "legacy" ' +
      ' or "regional". Got "' + profile[options.sharedConfig] + '".'
    });
    if (resolved) return resolved;
  }
  return resolved;
}

module.exports = resolveRegionalEndpointsFlag;


/***/ }),
/* 114 */
/***/ (function(module) {

module.exports = JSON.parse("{\"version\":\"2.0\",\"metadata\":{\"apiVersion\":\"2011-06-15\",\"endpointPrefix\":\"sts\",\"globalEndpoint\":\"sts.amazonaws.com\",\"protocol\":\"query\",\"serviceAbbreviation\":\"AWS STS\",\"serviceFullName\":\"AWS Security Token Service\",\"serviceId\":\"STS\",\"signatureVersion\":\"v4\",\"uid\":\"sts-2011-06-15\",\"xmlNamespace\":\"https://sts.amazonaws.com/doc/2011-06-15/\"},\"operations\":{\"AssumeRole\":{\"input\":{\"type\":\"structure\",\"required\":[\"RoleArn\",\"RoleSessionName\"],\"members\":{\"RoleArn\":{},\"RoleSessionName\":{},\"PolicyArns\":{\"shape\":\"S4\"},\"Policy\":{},\"DurationSeconds\":{\"type\":\"integer\"},\"Tags\":{\"shape\":\"S8\"},\"TransitiveTagKeys\":{\"type\":\"list\",\"member\":{}},\"ExternalId\":{},\"SerialNumber\":{},\"TokenCode\":{}}},\"output\":{\"resultWrapper\":\"AssumeRoleResult\",\"type\":\"structure\",\"members\":{\"Credentials\":{\"shape\":\"Sh\"},\"AssumedRoleUser\":{\"shape\":\"Sm\"},\"PackedPolicySize\":{\"type\":\"integer\"}}}},\"AssumeRoleWithSAML\":{\"input\":{\"type\":\"structure\",\"required\":[\"RoleArn\",\"PrincipalArn\",\"SAMLAssertion\"],\"members\":{\"RoleArn\":{},\"PrincipalArn\":{},\"SAMLAssertion\":{},\"PolicyArns\":{\"shape\":\"S4\"},\"Policy\":{},\"DurationSeconds\":{\"type\":\"integer\"}}},\"output\":{\"resultWrapper\":\"AssumeRoleWithSAMLResult\",\"type\":\"structure\",\"members\":{\"Credentials\":{\"shape\":\"Sh\"},\"AssumedRoleUser\":{\"shape\":\"Sm\"},\"PackedPolicySize\":{\"type\":\"integer\"},\"Subject\":{},\"SubjectType\":{},\"Issuer\":{},\"Audience\":{},\"NameQualifier\":{}}}},\"AssumeRoleWithWebIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"RoleArn\",\"RoleSessionName\",\"WebIdentityToken\"],\"members\":{\"RoleArn\":{},\"RoleSessionName\":{},\"WebIdentityToken\":{},\"ProviderId\":{},\"PolicyArns\":{\"shape\":\"S4\"},\"Policy\":{},\"DurationSeconds\":{\"type\":\"integer\"}}},\"output\":{\"resultWrapper\":\"AssumeRoleWithWebIdentityResult\",\"type\":\"structure\",\"members\":{\"Credentials\":{\"shape\":\"Sh\"},\"SubjectFromWebIdentityToken\":{},\"AssumedRoleUser\":{\"shape\":\"Sm\"},\"PackedPolicySize\":{\"type\":\"integer\"},\"Provider\":{},\"Audience\":{}}}},\"DecodeAuthorizationMessage\":{\"input\":{\"type\":\"structure\",\"required\":[\"EncodedMessage\"],\"members\":{\"EncodedMessage\":{}}},\"output\":{\"resultWrapper\":\"DecodeAuthorizationMessageResult\",\"type\":\"structure\",\"members\":{\"DecodedMessage\":{}}}},\"GetAccessKeyInfo\":{\"input\":{\"type\":\"structure\",\"required\":[\"AccessKeyId\"],\"members\":{\"AccessKeyId\":{}}},\"output\":{\"resultWrapper\":\"GetAccessKeyInfoResult\",\"type\":\"structure\",\"members\":{\"Account\":{}}}},\"GetCallerIdentity\":{\"input\":{\"type\":\"structure\",\"members\":{}},\"output\":{\"resultWrapper\":\"GetCallerIdentityResult\",\"type\":\"structure\",\"members\":{\"UserId\":{},\"Account\":{},\"Arn\":{}}}},\"GetFederationToken\":{\"input\":{\"type\":\"structure\",\"required\":[\"Name\"],\"members\":{\"Name\":{},\"Policy\":{},\"PolicyArns\":{\"shape\":\"S4\"},\"DurationSeconds\":{\"type\":\"integer\"},\"Tags\":{\"shape\":\"S8\"}}},\"output\":{\"resultWrapper\":\"GetFederationTokenResult\",\"type\":\"structure\",\"members\":{\"Credentials\":{\"shape\":\"Sh\"},\"FederatedUser\":{\"type\":\"structure\",\"required\":[\"FederatedUserId\",\"Arn\"],\"members\":{\"FederatedUserId\":{},\"Arn\":{}}},\"PackedPolicySize\":{\"type\":\"integer\"}}}},\"GetSessionToken\":{\"input\":{\"type\":\"structure\",\"members\":{\"DurationSeconds\":{\"type\":\"integer\"},\"SerialNumber\":{},\"TokenCode\":{}}},\"output\":{\"resultWrapper\":\"GetSessionTokenResult\",\"type\":\"structure\",\"members\":{\"Credentials\":{\"shape\":\"Sh\"}}}}},\"shapes\":{\"S4\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"members\":{\"arn\":{}}}},\"S8\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"required\":[\"Key\",\"Value\"],\"members\":{\"Key\":{},\"Value\":{}}}},\"Sh\":{\"type\":\"structure\",\"required\":[\"AccessKeyId\",\"SecretAccessKey\",\"SessionToken\",\"Expiration\"],\"members\":{\"AccessKeyId\":{},\"SecretAccessKey\":{},\"SessionToken\":{},\"Expiration\":{\"type\":\"timestamp\"}}},\"Sm\":{\"type\":\"structure\",\"required\":[\"AssumedRoleId\",\"Arn\"],\"members\":{\"AssumedRoleId\":{},\"Arn\":{}}}}}");

/***/ }),
/* 115 */
/***/ (function(module) {

module.exports = JSON.parse("{\"pagination\":{}}");

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var STS = __webpack_require__(11);

/**
 * Represents temporary credentials retrieved from {AWS.STS}. Without any
 * extra parameters, credentials will be fetched from the
 * {AWS.STS.getSessionToken} operation. If an IAM role is provided, the
 * {AWS.STS.assumeRole} operation will be used to fetch credentials for the
 * role instead.
 *
 * AWS.ChainableTemporaryCredentials differs from AWS.TemporaryCredentials in
 * the way masterCredentials and refreshes are handled.
 * AWS.ChainableTemporaryCredentials refreshes expired credentials using the
 * masterCredentials passed by the user to support chaining of STS credentials.
 * However, AWS.TemporaryCredentials recursively collapses the masterCredentials
 * during instantiation, precluding the ability to refresh credentials which
 * require intermediate, temporary credentials.
 *
 * For example, if the application should use RoleA, which must be assumed from
 * RoleB, and the environment provides credentials which can assume RoleB, then
 * AWS.ChainableTemporaryCredentials must be used to support refreshing the
 * temporary credentials for RoleA:
 *
 * ```javascript
 * var roleACreds = new AWS.ChainableTemporaryCredentials({
 *   params: {RoleArn: 'RoleA'},
 *   masterCredentials: new AWS.ChainableTemporaryCredentials({
 *     params: {RoleArn: 'RoleB'},
 *     masterCredentials: new AWS.EnvironmentCredentials('AWS')
 *   })
 * });
 * ```
 *
 * If AWS.TemporaryCredentials had been used in the previous example,
 * `roleACreds` would fail to refresh because `roleACreds` would
 * use the environment credentials for the AssumeRole request.
 *
 * Another difference is that AWS.ChainableTemporaryCredentials creates the STS
 * service instance during instantiation while AWS.TemporaryCredentials creates
 * the STS service instance during the first refresh. Creating the service
 * instance during instantiation effectively captures the master credentials
 * from the global config, so that subsequent changes to the global config do
 * not affect the master credentials used to refresh the temporary credentials.
 *
 * This allows an instance of AWS.ChainableTemporaryCredentials to be assigned
 * to AWS.config.credentials:
 *
 * ```javascript
 * var envCreds = new AWS.EnvironmentCredentials('AWS');
 * AWS.config.credentials = envCreds;
 * // masterCredentials will be envCreds
 * AWS.config.credentials = new AWS.ChainableTemporaryCredentials({
 *   params: {RoleArn: '...'}
 * });
 * ```
 *
 * Similarly, to use the CredentialProviderChain's default providers as the
 * master credentials, simply create a new instance of
 * AWS.ChainableTemporaryCredentials:
 *
 * ```javascript
 * AWS.config.credentials = new ChainableTemporaryCredentials({
 *   params: {RoleArn: '...'}
 * });
 * ```
 *
 * @!attribute service
 *   @return [AWS.STS] the STS service instance used to
 *     get and refresh temporary credentials from AWS STS.
 * @note (see constructor)
 */
AWS.ChainableTemporaryCredentials = AWS.util.inherit(AWS.Credentials, {
  /**
   * Creates a new temporary credentials object.
   *
   * @param options [map] a set of options
   * @option options params [map] ({}) a map of options that are passed to the
   *   {AWS.STS.assumeRole} or {AWS.STS.getSessionToken} operations.
   *   If a `RoleArn` parameter is passed in, credentials will be based on the
   *   IAM role. If a `SerialNumber` parameter is passed in, {tokenCodeFn} must
   *   also be passed in or an error will be thrown.
   * @option options masterCredentials [AWS.Credentials] the master credentials
   *   used to get and refresh temporary credentials from AWS STS. By default,
   *   AWS.config.credentials or AWS.config.credentialProvider will be used.
   * @option options tokenCodeFn [Function] (null) Function to provide
   *   `TokenCode`, if `SerialNumber` is provided for profile in {params}. Function
   *   is called with value of `SerialNumber` and `callback`, and should provide
   *   the `TokenCode` or an error to the callback in the format
   *   `callback(err, token)`.
   * @example Creating a new credentials object for generic temporary credentials
   *   AWS.config.credentials = new AWS.ChainableTemporaryCredentials();
   * @example Creating a new credentials object for an IAM role
   *   AWS.config.credentials = new AWS.ChainableTemporaryCredentials({
   *     params: {
   *       RoleArn: 'arn:aws:iam::1234567890:role/TemporaryCredentials'
   *     }
   *   });
   * @see AWS.STS.assumeRole
   * @see AWS.STS.getSessionToken
   */
  constructor: function ChainableTemporaryCredentials(options) {
    AWS.Credentials.call(this);
    options = options || {};
    this.errorCode = 'ChainableTemporaryCredentialsProviderFailure';
    this.expired = true;
    this.tokenCodeFn = null;

    var params = AWS.util.copy(options.params) || {};
    if (params.RoleArn) {
      params.RoleSessionName = params.RoleSessionName || 'temporary-credentials';
    }
    if (params.SerialNumber) {
      if (!options.tokenCodeFn || (typeof options.tokenCodeFn !== 'function')) {
        throw new AWS.util.error(
          new Error('tokenCodeFn must be a function when params.SerialNumber is given'),
          {code: this.errorCode}
        );
      } else {
        this.tokenCodeFn = options.tokenCodeFn;
      }
    }
    var config = AWS.util.merge(
      {
        params: params,
        credentials: options.masterCredentials || AWS.config.credentials
      },
      options.stsConfig || {}
    );
    this.service = new STS(config);
  },

  /**
   * Refreshes credentials using {AWS.STS.assumeRole} or
   * {AWS.STS.getSessionToken}, depending on whether an IAM role ARN was passed
   * to the credentials {constructor}.
   *
   * @callback callback function(err)
   *   Called when the STS service responds (or fails). When
   *   this callback is called with no error, it means that the credentials
   *   information has been loaded into the object (as the `accessKeyId`,
   *   `secretAccessKey`, and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   * @see AWS.Credentials.get
   */
  refresh: function refresh(callback) {
    this.coalesceRefresh(callback || AWS.util.fn.callback);
  },

  /**
   * @api private
   * @param callback
   */
  load: function load(callback) {
    var self = this;
    var operation = self.service.config.params.RoleArn ? 'assumeRole' : 'getSessionToken';
    this.getTokenCode(function (err, tokenCode) {
      var params = {};
      if (err) {
        callback(err);
        return;
      }
      if (tokenCode) {
        params.TokenCode = tokenCode;
      }
      self.service[operation](params, function (err, data) {
        if (!err) {
          self.service.credentialsFrom(data, self);
        }
        callback(err);
      });
    });
  },

  /**
   * @api private
   */
  getTokenCode: function getTokenCode(callback) {
    var self = this;
    if (this.tokenCodeFn) {
      this.tokenCodeFn(this.service.config.params.SerialNumber, function (err, token) {
        if (err) {
          var message = err;
          if (err instanceof Error) {
            message = err.message;
          }
          callback(
            AWS.util.error(
              new Error('Error fetching MFA token: ' + message),
              { code: self.errorCode}
            )
          );
          return;
        }
        callback(null, token);
      });
    } else {
      callback(null);
    }
  }
});


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var STS = __webpack_require__(11);

/**
 * Represents credentials retrieved from STS Web Identity Federation support.
 *
 * By default this provider gets credentials using the
 * {AWS.STS.assumeRoleWithWebIdentity} service operation. This operation
 * requires a `RoleArn` containing the ARN of the IAM trust policy for the
 * application for which credentials will be given. In addition, the
 * `WebIdentityToken` must be set to the token provided by the identity
 * provider. See {constructor} for an example on creating a credentials
 * object with proper `RoleArn` and `WebIdentityToken` values.
 *
 * ## Refreshing Credentials from Identity Service
 *
 * In addition to AWS credentials expiring after a given amount of time, the
 * login token from the identity provider will also expire. Once this token
 * expires, it will not be usable to refresh AWS credentials, and another
 * token will be needed. The SDK does not manage refreshing of the token value,
 * but this can be done through a "refresh token" supported by most identity
 * providers. Consult the documentation for the identity provider for refreshing
 * tokens. Once the refreshed token is acquired, you should make sure to update
 * this new token in the credentials object's {params} property. The following
 * code will update the WebIdentityToken, assuming you have retrieved an updated
 * token from the identity provider:
 *
 * ```javascript
 * AWS.config.credentials.params.WebIdentityToken = updatedToken;
 * ```
 *
 * Future calls to `credentials.refresh()` will now use the new token.
 *
 * @!attribute params
 *   @return [map] the map of params passed to
 *     {AWS.STS.assumeRoleWithWebIdentity}. To update the token, set the
 *     `params.WebIdentityToken` property.
 * @!attribute data
 *   @return [map] the raw data response from the call to
 *     {AWS.STS.assumeRoleWithWebIdentity}. Use this if you want to get
 *     access to other properties from the response.
 */
AWS.WebIdentityCredentials = AWS.util.inherit(AWS.Credentials, {
  /**
   * Creates a new credentials object.
   * @param (see AWS.STS.assumeRoleWithWebIdentity)
   * @example Creating a new credentials object
   *   AWS.config.credentials = new AWS.WebIdentityCredentials({
   *     RoleArn: 'arn:aws:iam::1234567890:role/WebIdentity',
   *     WebIdentityToken: 'ABCDEFGHIJKLMNOP', // token from identity service
   *     RoleSessionName: 'web' // optional name, defaults to web-identity
   *   }, {
   *     // optionally provide configuration to apply to the underlying AWS.STS service client
   *     // if configuration is not provided, then configuration will be pulled from AWS.config
   *
   *     // specify timeout options
   *     httpOptions: {
   *       timeout: 100
   *     }
   *   });
   * @see AWS.STS.assumeRoleWithWebIdentity
   * @see AWS.Config
   */
  constructor: function WebIdentityCredentials(params, clientConfig) {
    AWS.Credentials.call(this);
    this.expired = true;
    this.params = params;
    this.params.RoleSessionName = this.params.RoleSessionName || 'web-identity';
    this.data = null;
    this._clientConfig = AWS.util.copy(clientConfig || {});
  },

  /**
   * Refreshes credentials using {AWS.STS.assumeRoleWithWebIdentity}
   *
   * @callback callback function(err)
   *   Called when the STS service responds (or fails). When
   *   this callback is called with no error, it means that the credentials
   *   information has been loaded into the object (as the `accessKeyId`,
   *   `secretAccessKey`, and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   * @see get
   */
  refresh: function refresh(callback) {
    this.coalesceRefresh(callback || AWS.util.fn.callback);
  },

  /**
   * @api private
   */
  load: function load(callback) {
    var self = this;
    self.createClients();
    self.service.assumeRoleWithWebIdentity(function (err, data) {
      self.data = null;
      if (!err) {
        self.data = data;
        self.service.credentialsFrom(data, self);
      }
      callback(err);
    });
  },

  /**
   * @api private
   */
  createClients: function() {
    if (!this.service) {
      var stsConfig = AWS.util.merge({}, this._clientConfig);
      stsConfig.params = this.params;
      this.service = new STS(stsConfig);
    }
  }

});


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var CognitoIdentity = __webpack_require__(119);
var STS = __webpack_require__(11);

/**
 * Represents credentials retrieved from STS Web Identity Federation using
 * the Amazon Cognito Identity service.
 *
 * By default this provider gets credentials using the
 * {AWS.CognitoIdentity.getCredentialsForIdentity} service operation, which
 * requires either an `IdentityId` or an `IdentityPoolId` (Amazon Cognito
 * Identity Pool ID), which is used to call {AWS.CognitoIdentity.getId} to
 * obtain an `IdentityId`. If the identity or identity pool is not configured in
 * the Amazon Cognito Console to use IAM roles with the appropriate permissions,
 * then additionally a `RoleArn` is required containing the ARN of the IAM trust
 * policy for the Amazon Cognito role that the user will log into. If a `RoleArn`
 * is provided, then this provider gets credentials using the
 * {AWS.STS.assumeRoleWithWebIdentity} service operation, after first getting an
 * Open ID token from {AWS.CognitoIdentity.getOpenIdToken}.
 *
 * In addition, if this credential provider is used to provide authenticated
 * login, the `Logins` map may be set to the tokens provided by the respective
 * identity providers. See {constructor} for an example on creating a credentials
 * object with proper property values.
 *
 * ## Refreshing Credentials from Identity Service
 *
 * In addition to AWS credentials expiring after a given amount of time, the
 * login token from the identity provider will also expire. Once this token
 * expires, it will not be usable to refresh AWS credentials, and another
 * token will be needed. The SDK does not manage refreshing of the token value,
 * but this can be done through a "refresh token" supported by most identity
 * providers. Consult the documentation for the identity provider for refreshing
 * tokens. Once the refreshed token is acquired, you should make sure to update
 * this new token in the credentials object's {params} property. The following
 * code will update the WebIdentityToken, assuming you have retrieved an updated
 * token from the identity provider:
 *
 * ```javascript
 * AWS.config.credentials.params.Logins['graph.facebook.com'] = updatedToken;
 * ```
 *
 * Future calls to `credentials.refresh()` will now use the new token.
 *
 * @!attribute params
 *   @return [map] the map of params passed to
 *     {AWS.CognitoIdentity.getId},
 *     {AWS.CognitoIdentity.getOpenIdToken}, and
 *     {AWS.STS.assumeRoleWithWebIdentity}. To update the token, set the
 *     `params.WebIdentityToken` property.
 * @!attribute data
 *   @return [map] the raw data response from the call to
 *     {AWS.CognitoIdentity.getCredentialsForIdentity}, or
 *     {AWS.STS.assumeRoleWithWebIdentity}. Use this if you want to get
 *     access to other properties from the response.
 * @!attribute identityId
 *   @return [String] the Cognito ID returned by the last call to
 *     {AWS.CognitoIdentity.getOpenIdToken}. This ID represents the actual
 *     final resolved identity ID from Amazon Cognito.
 */
AWS.CognitoIdentityCredentials = AWS.util.inherit(AWS.Credentials, {
  /**
   * @api private
   */
  localStorageKey: {
    id: 'aws.cognito.identity-id.',
    providers: 'aws.cognito.identity-providers.'
  },

  /**
   * Creates a new credentials object.
   * @example Creating a new credentials object
   *   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
   *
   *     // either IdentityPoolId or IdentityId is required
   *     // See the IdentityPoolId param for AWS.CognitoIdentity.getID (linked below)
   *     // See the IdentityId param for AWS.CognitoIdentity.getCredentialsForIdentity
   *     // or AWS.CognitoIdentity.getOpenIdToken (linked below)
   *     IdentityPoolId: 'us-east-1:1699ebc0-7900-4099-b910-2df94f52a030',
   *     IdentityId: 'us-east-1:128d0a74-c82f-4553-916d-90053e4a8b0f'
   *
   *     // optional, only necessary when the identity pool is not configured
   *     // to use IAM roles in the Amazon Cognito Console
   *     // See the RoleArn param for AWS.STS.assumeRoleWithWebIdentity (linked below)
   *     RoleArn: 'arn:aws:iam::1234567890:role/MYAPP-CognitoIdentity',
   *
   *     // optional tokens, used for authenticated login
   *     // See the Logins param for AWS.CognitoIdentity.getID (linked below)
   *     Logins: {
   *       'graph.facebook.com': 'FBTOKEN',
   *       'www.amazon.com': 'AMAZONTOKEN',
   *       'accounts.google.com': 'GOOGLETOKEN',
   *       'api.twitter.com': 'TWITTERTOKEN',
   *       'www.digits.com': 'DIGITSTOKEN'
   *     },
   *
   *     // optional name, defaults to web-identity
   *     // See the RoleSessionName param for AWS.STS.assumeRoleWithWebIdentity (linked below)
   *     RoleSessionName: 'web',
   *
   *     // optional, only necessary when application runs in a browser
   *     // and multiple users are signed in at once, used for caching
   *     LoginId: 'example@gmail.com'
   *
   *   }, {
   *      // optionally provide configuration to apply to the underlying service clients
   *      // if configuration is not provided, then configuration will be pulled from AWS.config
   *
   *      // region should match the region your identity pool is located in
   *      region: 'us-east-1',
   *
   *      // specify timeout options
   *      httpOptions: {
   *        timeout: 100
   *      }
   *   });
   * @see AWS.CognitoIdentity.getId
   * @see AWS.CognitoIdentity.getCredentialsForIdentity
   * @see AWS.STS.assumeRoleWithWebIdentity
   * @see AWS.CognitoIdentity.getOpenIdToken
   * @see AWS.Config
   * @note If a region is not provided in the global AWS.config, or
   *   specified in the `clientConfig` to the CognitoIdentityCredentials
   *   constructor, you may encounter a 'Missing credentials in config' error
   *   when calling making a service call.
   */
  constructor: function CognitoIdentityCredentials(params, clientConfig) {
    AWS.Credentials.call(this);
    this.expired = true;
    this.params = params;
    this.data = null;
    this._identityId = null;
    this._clientConfig = AWS.util.copy(clientConfig || {});
    this.loadCachedId();
    var self = this;
    Object.defineProperty(this, 'identityId', {
      get: function() {
        self.loadCachedId();
        return self._identityId || self.params.IdentityId;
      },
      set: function(identityId) {
        self._identityId = identityId;
      }
    });
  },

  /**
   * Refreshes credentials using {AWS.CognitoIdentity.getCredentialsForIdentity},
   * or {AWS.STS.assumeRoleWithWebIdentity}.
   *
   * @callback callback function(err)
   *   Called when the STS service responds (or fails). When
   *   this callback is called with no error, it means that the credentials
   *   information has been loaded into the object (as the `accessKeyId`,
   *   `secretAccessKey`, and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   * @see AWS.Credentials.get
   */
  refresh: function refresh(callback) {
    this.coalesceRefresh(callback || AWS.util.fn.callback);
  },

  /**
   * @api private
   * @param callback
   */
  load: function load(callback) {
    var self = this;
    self.createClients();
    self.data = null;
    self._identityId = null;
    self.getId(function(err) {
      if (!err) {
        if (!self.params.RoleArn) {
          self.getCredentialsForIdentity(callback);
        } else {
          self.getCredentialsFromSTS(callback);
        }
      } else {
        self.clearIdOnNotAuthorized(err);
        callback(err);
      }
    });
  },

  /**
   * Clears the cached Cognito ID associated with the currently configured
   * identity pool ID. Use this to manually invalidate your cache if
   * the identity pool ID was deleted.
   */
  clearCachedId: function clearCache() {
    this._identityId = null;
    delete this.params.IdentityId;

    var poolId = this.params.IdentityPoolId;
    var loginId = this.params.LoginId || '';
    delete this.storage[this.localStorageKey.id + poolId + loginId];
    delete this.storage[this.localStorageKey.providers + poolId + loginId];
  },

  /**
   * @api private
   */
  clearIdOnNotAuthorized: function clearIdOnNotAuthorized(err) {
    var self = this;
    if (err.code == 'NotAuthorizedException') {
      self.clearCachedId();
    }
  },

  /**
   * Retrieves a Cognito ID, loading from cache if it was already retrieved
   * on this device.
   *
   * @callback callback function(err, identityId)
   *   @param err [Error, null] an error object if the call failed or null if
   *     it succeeded.
   *   @param identityId [String, null] if successful, the callback will return
   *     the Cognito ID.
   * @note If not loaded explicitly, the Cognito ID is loaded and stored in
   *   localStorage in the browser environment of a device.
   * @api private
   */
  getId: function getId(callback) {
    var self = this;
    if (typeof self.params.IdentityId === 'string') {
      return callback(null, self.params.IdentityId);
    }

    self.cognito.getId(function(err, data) {
      if (!err && data.IdentityId) {
        self.params.IdentityId = data.IdentityId;
        callback(null, data.IdentityId);
      } else {
        callback(err);
      }
    });
  },


  /**
   * @api private
   */
  loadCredentials: function loadCredentials(data, credentials) {
    if (!data || !credentials) return;
    credentials.expired = false;
    credentials.accessKeyId = data.Credentials.AccessKeyId;
    credentials.secretAccessKey = data.Credentials.SecretKey;
    credentials.sessionToken = data.Credentials.SessionToken;
    credentials.expireTime = data.Credentials.Expiration;
  },

  /**
   * @api private
   */
  getCredentialsForIdentity: function getCredentialsForIdentity(callback) {
    var self = this;
    self.cognito.getCredentialsForIdentity(function(err, data) {
      if (!err) {
        self.cacheId(data);
        self.data = data;
        self.loadCredentials(self.data, self);
      } else {
        self.clearIdOnNotAuthorized(err);
      }
      callback(err);
    });
  },

  /**
   * @api private
   */
  getCredentialsFromSTS: function getCredentialsFromSTS(callback) {
    var self = this;
    self.cognito.getOpenIdToken(function(err, data) {
      if (!err) {
        self.cacheId(data);
        self.params.WebIdentityToken = data.Token;
        self.webIdentityCredentials.refresh(function(webErr) {
          if (!webErr) {
            self.data = self.webIdentityCredentials.data;
            self.sts.credentialsFrom(self.data, self);
          }
          callback(webErr);
        });
      } else {
        self.clearIdOnNotAuthorized(err);
        callback(err);
      }
    });
  },

  /**
   * @api private
   */
  loadCachedId: function loadCachedId() {
    var self = this;

    // in the browser we source default IdentityId from localStorage
    if (AWS.util.isBrowser() && !self.params.IdentityId) {
      var id = self.getStorage('id');
      if (id && self.params.Logins) {
        var actualProviders = Object.keys(self.params.Logins);
        var cachedProviders =
          (self.getStorage('providers') || '').split(',');

        // only load ID if at least one provider used this ID before
        var intersect = cachedProviders.filter(function(n) {
          return actualProviders.indexOf(n) !== -1;
        });
        if (intersect.length !== 0) {
          self.params.IdentityId = id;
        }
      } else if (id) {
        self.params.IdentityId = id;
      }
    }
  },

  /**
   * @api private
   */
  createClients: function() {
    var clientConfig = this._clientConfig;
    this.webIdentityCredentials = this.webIdentityCredentials ||
      new AWS.WebIdentityCredentials(this.params, clientConfig);
    if (!this.cognito) {
      var cognitoConfig = AWS.util.merge({}, clientConfig);
      cognitoConfig.params = this.params;
      this.cognito = new CognitoIdentity(cognitoConfig);
    }
    this.sts = this.sts || new STS(clientConfig);
  },

  /**
   * @api private
   */
  cacheId: function cacheId(data) {
    this._identityId = data.IdentityId;
    this.params.IdentityId = this._identityId;

    // cache this IdentityId in browser localStorage if possible
    if (AWS.util.isBrowser()) {
      this.setStorage('id', data.IdentityId);

      if (this.params.Logins) {
        this.setStorage('providers', Object.keys(this.params.Logins).join(','));
      }
    }
  },

  /**
   * @api private
   */
  getStorage: function getStorage(key) {
    return this.storage[this.localStorageKey[key] + this.params.IdentityPoolId + (this.params.LoginId || '')];
  },

  /**
   * @api private
   */
  setStorage: function setStorage(key, val) {
    try {
      this.storage[this.localStorageKey[key] + this.params.IdentityPoolId + (this.params.LoginId || '')] = val;
    } catch (_) {}
  },

  /**
   * @api private
   */
  storage: (function() {
    try {
      var storage = AWS.util.isBrowser() && window.localStorage !== null && typeof window.localStorage === 'object' ?
          window.localStorage : {};

      // Test set/remove which would throw an error in Safari's private browsing
      storage['aws.test-storage'] = 'foobar';
      delete storage['aws.test-storage'];

      return storage;
    } catch (_) {
      return {};
    }
  })()
});


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(12);
var AWS = __webpack_require__(1);
var Service = AWS.Service;
var apiLoader = AWS.apiLoader;

apiLoader.services['cognitoidentity'] = {};
AWS.CognitoIdentity = Service.defineService('cognitoidentity', ['2014-06-30']);
__webpack_require__(120);
Object.defineProperty(apiLoader.services['cognitoidentity'], '2014-06-30', {
  get: function get() {
    var model = __webpack_require__(121);
    model.paginators = __webpack_require__(122).pagination;
    return model;
  },
  enumerable: true,
  configurable: true
});

module.exports = AWS.CognitoIdentity;


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);

AWS.util.update(AWS.CognitoIdentity.prototype, {
  getOpenIdToken: function getOpenIdToken(params, callback) {
    return this.makeUnauthenticatedRequest('getOpenIdToken', params, callback);
  },

  getId: function getId(params, callback) {
    return this.makeUnauthenticatedRequest('getId', params, callback);
  },

  getCredentialsForIdentity: function getCredentialsForIdentity(params, callback) {
    return this.makeUnauthenticatedRequest('getCredentialsForIdentity', params, callback);
  }
});


/***/ }),
/* 121 */
/***/ (function(module) {

module.exports = JSON.parse("{\"version\":\"2.0\",\"metadata\":{\"apiVersion\":\"2014-06-30\",\"endpointPrefix\":\"cognito-identity\",\"jsonVersion\":\"1.1\",\"protocol\":\"json\",\"serviceFullName\":\"Amazon Cognito Identity\",\"serviceId\":\"Cognito Identity\",\"signatureVersion\":\"v4\",\"targetPrefix\":\"AWSCognitoIdentityService\",\"uid\":\"cognito-identity-2014-06-30\"},\"operations\":{\"CreateIdentityPool\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolName\",\"AllowUnauthenticatedIdentities\"],\"members\":{\"IdentityPoolName\":{},\"AllowUnauthenticatedIdentities\":{\"type\":\"boolean\"},\"AllowClassicFlow\":{\"type\":\"boolean\"},\"SupportedLoginProviders\":{\"shape\":\"S5\"},\"DeveloperProviderName\":{},\"OpenIdConnectProviderARNs\":{\"shape\":\"S9\"},\"CognitoIdentityProviders\":{\"shape\":\"Sb\"},\"SamlProviderARNs\":{\"shape\":\"Sg\"},\"IdentityPoolTags\":{\"shape\":\"Sh\"}}},\"output\":{\"shape\":\"Sk\"}},\"DeleteIdentities\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityIdsToDelete\"],\"members\":{\"IdentityIdsToDelete\":{\"type\":\"list\",\"member\":{}}}},\"output\":{\"type\":\"structure\",\"members\":{\"UnprocessedIdentityIds\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{},\"ErrorCode\":{}}}}}}},\"DeleteIdentityPool\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\"],\"members\":{\"IdentityPoolId\":{}}}},\"DescribeIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityId\"],\"members\":{\"IdentityId\":{}}},\"output\":{\"shape\":\"Sv\"}},\"DescribeIdentityPool\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\"],\"members\":{\"IdentityPoolId\":{}}},\"output\":{\"shape\":\"Sk\"}},\"GetCredentialsForIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityId\"],\"members\":{\"IdentityId\":{},\"Logins\":{\"shape\":\"S10\"},\"CustomRoleArn\":{}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{},\"Credentials\":{\"type\":\"structure\",\"members\":{\"AccessKeyId\":{},\"SecretKey\":{},\"SessionToken\":{},\"Expiration\":{\"type\":\"timestamp\"}}}}}},\"GetId\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\"],\"members\":{\"AccountId\":{},\"IdentityPoolId\":{},\"Logins\":{\"shape\":\"S10\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{}}}},\"GetIdentityPoolRoles\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\"],\"members\":{\"IdentityPoolId\":{}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityPoolId\":{},\"Roles\":{\"shape\":\"S1c\"},\"RoleMappings\":{\"shape\":\"S1e\"}}}},\"GetOpenIdToken\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityId\"],\"members\":{\"IdentityId\":{},\"Logins\":{\"shape\":\"S10\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{},\"Token\":{}}}},\"GetOpenIdTokenForDeveloperIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\",\"Logins\"],\"members\":{\"IdentityPoolId\":{},\"IdentityId\":{},\"Logins\":{\"shape\":\"S10\"},\"TokenDuration\":{\"type\":\"long\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{},\"Token\":{}}}},\"ListIdentities\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\",\"MaxResults\"],\"members\":{\"IdentityPoolId\":{},\"MaxResults\":{\"type\":\"integer\"},\"NextToken\":{},\"HideDisabled\":{\"type\":\"boolean\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityPoolId\":{},\"Identities\":{\"type\":\"list\",\"member\":{\"shape\":\"Sv\"}},\"NextToken\":{}}}},\"ListIdentityPools\":{\"input\":{\"type\":\"structure\",\"required\":[\"MaxResults\"],\"members\":{\"MaxResults\":{\"type\":\"integer\"},\"NextToken\":{}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityPools\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"members\":{\"IdentityPoolId\":{},\"IdentityPoolName\":{}}}},\"NextToken\":{}}}},\"ListTagsForResource\":{\"input\":{\"type\":\"structure\",\"required\":[\"ResourceArn\"],\"members\":{\"ResourceArn\":{}}},\"output\":{\"type\":\"structure\",\"members\":{\"Tags\":{\"shape\":\"Sh\"}}}},\"LookupDeveloperIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\"],\"members\":{\"IdentityPoolId\":{},\"IdentityId\":{},\"DeveloperUserIdentifier\":{},\"MaxResults\":{\"type\":\"integer\"},\"NextToken\":{}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{},\"DeveloperUserIdentifierList\":{\"type\":\"list\",\"member\":{}},\"NextToken\":{}}}},\"MergeDeveloperIdentities\":{\"input\":{\"type\":\"structure\",\"required\":[\"SourceUserIdentifier\",\"DestinationUserIdentifier\",\"DeveloperProviderName\",\"IdentityPoolId\"],\"members\":{\"SourceUserIdentifier\":{},\"DestinationUserIdentifier\":{},\"DeveloperProviderName\":{},\"IdentityPoolId\":{}}},\"output\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{}}}},\"SetIdentityPoolRoles\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\",\"Roles\"],\"members\":{\"IdentityPoolId\":{},\"Roles\":{\"shape\":\"S1c\"},\"RoleMappings\":{\"shape\":\"S1e\"}}}},\"TagResource\":{\"input\":{\"type\":\"structure\",\"required\":[\"ResourceArn\",\"Tags\"],\"members\":{\"ResourceArn\":{},\"Tags\":{\"shape\":\"Sh\"}}},\"output\":{\"type\":\"structure\",\"members\":{}}},\"UnlinkDeveloperIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityId\",\"IdentityPoolId\",\"DeveloperProviderName\",\"DeveloperUserIdentifier\"],\"members\":{\"IdentityId\":{},\"IdentityPoolId\":{},\"DeveloperProviderName\":{},\"DeveloperUserIdentifier\":{}}}},\"UnlinkIdentity\":{\"input\":{\"type\":\"structure\",\"required\":[\"IdentityId\",\"Logins\",\"LoginsToRemove\"],\"members\":{\"IdentityId\":{},\"Logins\":{\"shape\":\"S10\"},\"LoginsToRemove\":{\"shape\":\"Sw\"}}}},\"UntagResource\":{\"input\":{\"type\":\"structure\",\"required\":[\"ResourceArn\",\"TagKeys\"],\"members\":{\"ResourceArn\":{},\"TagKeys\":{\"type\":\"list\",\"member\":{}}}},\"output\":{\"type\":\"structure\",\"members\":{}}},\"UpdateIdentityPool\":{\"input\":{\"shape\":\"Sk\"},\"output\":{\"shape\":\"Sk\"}}},\"shapes\":{\"S5\":{\"type\":\"map\",\"key\":{},\"value\":{}},\"S9\":{\"type\":\"list\",\"member\":{}},\"Sb\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"members\":{\"ProviderName\":{},\"ClientId\":{},\"ServerSideTokenCheck\":{\"type\":\"boolean\"}}}},\"Sg\":{\"type\":\"list\",\"member\":{}},\"Sh\":{\"type\":\"map\",\"key\":{},\"value\":{}},\"Sk\":{\"type\":\"structure\",\"required\":[\"IdentityPoolId\",\"IdentityPoolName\",\"AllowUnauthenticatedIdentities\"],\"members\":{\"IdentityPoolId\":{},\"IdentityPoolName\":{},\"AllowUnauthenticatedIdentities\":{\"type\":\"boolean\"},\"AllowClassicFlow\":{\"type\":\"boolean\"},\"SupportedLoginProviders\":{\"shape\":\"S5\"},\"DeveloperProviderName\":{},\"OpenIdConnectProviderARNs\":{\"shape\":\"S9\"},\"CognitoIdentityProviders\":{\"shape\":\"Sb\"},\"SamlProviderARNs\":{\"shape\":\"Sg\"},\"IdentityPoolTags\":{\"shape\":\"Sh\"}}},\"Sv\":{\"type\":\"structure\",\"members\":{\"IdentityId\":{},\"Logins\":{\"shape\":\"Sw\"},\"CreationDate\":{\"type\":\"timestamp\"},\"LastModifiedDate\":{\"type\":\"timestamp\"}}},\"Sw\":{\"type\":\"list\",\"member\":{}},\"S10\":{\"type\":\"map\",\"key\":{},\"value\":{}},\"S1c\":{\"type\":\"map\",\"key\":{},\"value\":{}},\"S1e\":{\"type\":\"map\",\"key\":{},\"value\":{\"type\":\"structure\",\"required\":[\"Type\"],\"members\":{\"Type\":{},\"AmbiguousRoleResolution\":{},\"RulesConfiguration\":{\"type\":\"structure\",\"required\":[\"Rules\"],\"members\":{\"Rules\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"required\":[\"Claim\",\"MatchType\",\"Value\",\"RoleARN\"],\"members\":{\"Claim\":{},\"MatchType\":{},\"Value\":{},\"RoleARN\":{}}}}}}}}}}}");

/***/ }),
/* 122 */
/***/ (function(module) {

module.exports = JSON.parse("{\"pagination\":{}}");

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var STS = __webpack_require__(11);

/**
 * Represents credentials retrieved from STS SAML support.
 *
 * By default this provider gets credentials using the
 * {AWS.STS.assumeRoleWithSAML} service operation. This operation
 * requires a `RoleArn` containing the ARN of the IAM trust policy for the
 * application for which credentials will be given, as well as a `PrincipalArn`
 * representing the ARN for the SAML identity provider. In addition, the
 * `SAMLAssertion` must be set to the token provided by the identity
 * provider. See {constructor} for an example on creating a credentials
 * object with proper `RoleArn`, `PrincipalArn`, and `SAMLAssertion` values.
 *
 * ## Refreshing Credentials from Identity Service
 *
 * In addition to AWS credentials expiring after a given amount of time, the
 * login token from the identity provider will also expire. Once this token
 * expires, it will not be usable to refresh AWS credentials, and another
 * token will be needed. The SDK does not manage refreshing of the token value,
 * but this can be done through a "refresh token" supported by most identity
 * providers. Consult the documentation for the identity provider for refreshing
 * tokens. Once the refreshed token is acquired, you should make sure to update
 * this new token in the credentials object's {params} property. The following
 * code will update the SAMLAssertion, assuming you have retrieved an updated
 * token from the identity provider:
 *
 * ```javascript
 * AWS.config.credentials.params.SAMLAssertion = updatedToken;
 * ```
 *
 * Future calls to `credentials.refresh()` will now use the new token.
 *
 * @!attribute params
 *   @return [map] the map of params passed to
 *     {AWS.STS.assumeRoleWithSAML}. To update the token, set the
 *     `params.SAMLAssertion` property.
 */
AWS.SAMLCredentials = AWS.util.inherit(AWS.Credentials, {
  /**
   * Creates a new credentials object.
   * @param (see AWS.STS.assumeRoleWithSAML)
   * @example Creating a new credentials object
   *   AWS.config.credentials = new AWS.SAMLCredentials({
   *     RoleArn: 'arn:aws:iam::1234567890:role/SAMLRole',
   *     PrincipalArn: 'arn:aws:iam::1234567890:role/SAMLPrincipal',
   *     SAMLAssertion: 'base64-token', // base64-encoded token from IdP
   *   });
   * @see AWS.STS.assumeRoleWithSAML
   */
  constructor: function SAMLCredentials(params) {
    AWS.Credentials.call(this);
    this.expired = true;
    this.params = params;
  },

  /**
   * Refreshes credentials using {AWS.STS.assumeRoleWithSAML}
   *
   * @callback callback function(err)
   *   Called when the STS service responds (or fails). When
   *   this callback is called with no error, it means that the credentials
   *   information has been loaded into the object (as the `accessKeyId`,
   *   `secretAccessKey`, and `sessionToken` properties).
   *   @param err [Error] if an error occurred, this value will be filled
   * @see get
   */
  refresh: function refresh(callback) {
    this.coalesceRefresh(callback || AWS.util.fn.callback);
  },

  /**
   * @api private
   */
  load: function load(callback) {
    var self = this;
    self.createClients();
    self.service.assumeRoleWithSAML(function (err, data) {
      if (!err) {
        self.service.credentialsFrom(data, self);
      }
      callback(err);
    });
  },

  /**
   * @api private
   */
  createClients: function() {
    this.service = this.service || new STS({params: this.params});
  }

});


/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

var util = __webpack_require__(2);
var Shape = __webpack_require__(9);

function DomXmlParser() { }

DomXmlParser.prototype.parse = function(xml, shape) {
  if (xml.replace(/^\s+/, '') === '') return {};

  var result, error;
  try {
    if (window.DOMParser) {
      try {
        var parser = new DOMParser();
        result = parser.parseFromString(xml, 'text/xml');
      } catch (syntaxError) {
        throw util.error(new Error('Parse error in document'),
          {
            originalError: syntaxError,
            code: 'XMLParserError',
            retryable: true
          });
      }

      if (result.documentElement === null) {
        throw util.error(new Error('Cannot parse empty document.'),
          {
            code: 'XMLParserError',
            retryable: true
          });
      }

      var isError = result.getElementsByTagName('parsererror')[0];
      if (isError && (isError.parentNode === result ||
          isError.parentNode.nodeName === 'body' ||
          isError.parentNode.parentNode === result ||
          isError.parentNode.parentNode.nodeName === 'body')) {
        var errorElement = isError.getElementsByTagName('div')[0] || isError;
        throw util.error(new Error(errorElement.textContent || 'Parser error in document'),
          {
            code: 'XMLParserError',
            retryable: true
          });
      }
    } else if (window.ActiveXObject) {
      result = new window.ActiveXObject('Microsoft.XMLDOM');
      result.async = false;

      if (!result.loadXML(xml)) {
        throw util.error(new Error('Parse error in document'),
          {
            code: 'XMLParserError',
            retryable: true
          });
      }
    } else {
      throw new Error('Cannot load XML parser');
    }
  } catch (e) {
    error = e;
  }

  if (result && result.documentElement && !error) {
    var data = parseXml(result.documentElement, shape);
    var metadata = getElementByTagName(result.documentElement, 'ResponseMetadata');
    if (metadata) {
      data.ResponseMetadata = parseXml(metadata, {});
    }
    return data;
  } else if (error) {
    throw util.error(error || new Error(), {code: 'XMLParserError', retryable: true});
  } else { // empty xml document
    return {};
  }
};

function getElementByTagName(xml, tag) {
  var elements = xml.getElementsByTagName(tag);
  for (var i = 0, iLen = elements.length; i < iLen; i++) {
    if (elements[i].parentNode === xml) {
      return elements[i];
    }
  }
}

function parseXml(xml, shape) {
  if (!shape) shape = {};
  switch (shape.type) {
    case 'structure': return parseStructure(xml, shape);
    case 'map': return parseMap(xml, shape);
    case 'list': return parseList(xml, shape);
    case undefined: case null: return parseUnknown(xml);
    default: return parseScalar(xml, shape);
  }
}

function parseStructure(xml, shape) {
  var data = {};
  if (xml === null) return data;

  util.each(shape.members, function(memberName, memberShape) {
    if (memberShape.isXmlAttribute) {
      if (Object.prototype.hasOwnProperty.call(xml.attributes, memberShape.name)) {
        var value = xml.attributes[memberShape.name].value;
        data[memberName] = parseXml({textContent: value}, memberShape);
      }
    } else {
      var xmlChild = memberShape.flattened ? xml :
        getElementByTagName(xml, memberShape.name);
      if (xmlChild) {
        data[memberName] = parseXml(xmlChild, memberShape);
      } else if (
        !memberShape.flattened &&
        memberShape.type === 'list' &&
        !shape.api.xmlNoDefaultLists) {
        data[memberName] = memberShape.defaultValue;
      }
    }
  });

  return data;
}

function parseMap(xml, shape) {
  var data = {};
  var xmlKey = shape.key.name || 'key';
  var xmlValue = shape.value.name || 'value';
  var tagName = shape.flattened ? shape.name : 'entry';

  var child = xml.firstElementChild;
  while (child) {
    if (child.nodeName === tagName) {
      var key = getElementByTagName(child, xmlKey).textContent;
      var value = getElementByTagName(child, xmlValue);
      data[key] = parseXml(value, shape.value);
    }
    child = child.nextElementSibling;
  }
  return data;
}

function parseList(xml, shape) {
  var data = [];
  var tagName = shape.flattened ? shape.name : (shape.member.name || 'member');

  var child = xml.firstElementChild;
  while (child) {
    if (child.nodeName === tagName) {
      data.push(parseXml(child, shape.member));
    }
    child = child.nextElementSibling;
  }
  return data;
}

function parseScalar(xml, shape) {
  if (xml.getAttribute) {
    var encoding = xml.getAttribute('encoding');
    if (encoding === 'base64') {
      shape = new Shape.create({type: encoding});
    }
  }

  var text = xml.textContent;
  if (text === '') text = null;
  if (typeof shape.toType === 'function') {
    return shape.toType(text);
  } else {
    return text;
  }
}

function parseUnknown(xml) {
  if (xml === undefined || xml === null) return '';

  // empty object
  if (!xml.firstElementChild) {
    if (xml.parentNode.parentNode === null) return {};
    if (xml.childNodes.length === 0) return '';
    else return xml.textContent;
  }

  // object, parse as structure
  var shape = {type: 'structure', members: {}};
  var child = xml.firstElementChild;
  while (child) {
    var tag = child.nodeName;
    if (Object.prototype.hasOwnProperty.call(shape.members, tag)) {
      // multiple tags of the same name makes it a list
      shape.members[tag].type = 'list';
    } else {
      shape.members[tag] = {name: tag};
    }
    child = child.nextElementSibling;
  }
  return parseStructure(xml, shape);
}

/**
 * @api private
 */
module.exports = DomXmlParser;


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

var AWS = __webpack_require__(1);
var EventEmitter = __webpack_require__(126).EventEmitter;
__webpack_require__(37);

/**
 * @api private
 */
AWS.XHRClient = AWS.util.inherit({
  handleRequest: function handleRequest(httpRequest, httpOptions, callback, errCallback) {
    var self = this;
    var endpoint = httpRequest.endpoint;
    var emitter = new EventEmitter();
    var href = endpoint.protocol + '//' + endpoint.hostname;
    if (endpoint.port !== 80 && endpoint.port !== 443) {
      href += ':' + endpoint.port;
    }
    href += httpRequest.path;

    var xhr = new XMLHttpRequest(), headersEmitted = false;
    httpRequest.stream = xhr;

    xhr.addEventListener('readystatechange', function() {
      try {
        if (xhr.status === 0) return; // 0 code is invalid
      } catch (e) { return; }

      if (this.readyState >= this.HEADERS_RECEIVED && !headersEmitted) {
        emitter.statusCode = xhr.status;
        emitter.headers = self.parseHeaders(xhr.getAllResponseHeaders());
        emitter.emit(
          'headers',
          emitter.statusCode,
          emitter.headers,
          xhr.statusText
        );
        headersEmitted = true;
      }
      if (this.readyState === this.DONE) {
        self.finishRequest(xhr, emitter);
      }
    }, false);
    xhr.upload.addEventListener('progress', function (evt) {
      emitter.emit('sendProgress', evt);
    });
    xhr.addEventListener('progress', function (evt) {
      emitter.emit('receiveProgress', evt);
    }, false);
    xhr.addEventListener('timeout', function () {
      errCallback(AWS.util.error(new Error('Timeout'), {code: 'TimeoutError'}));
    }, false);
    xhr.addEventListener('error', function () {
      errCallback(AWS.util.error(new Error('Network Failure'), {
        code: 'NetworkingError'
      }));
    }, false);
    xhr.addEventListener('abort', function () {
      errCallback(AWS.util.error(new Error('Request aborted'), {
        code: 'RequestAbortedError'
      }));
    }, false);

    callback(emitter);
    xhr.open(httpRequest.method, href, httpOptions.xhrAsync !== false);
    AWS.util.each(httpRequest.headers, function (key, value) {
      if (key !== 'Content-Length' && key !== 'User-Agent' && key !== 'Host') {
        xhr.setRequestHeader(key, value);
      }
    });

    if (httpOptions.timeout && httpOptions.xhrAsync !== false) {
      xhr.timeout = httpOptions.timeout;
    }

    if (httpOptions.xhrWithCredentials) {
      xhr.withCredentials = true;
    }
    try { xhr.responseType = 'arraybuffer'; } catch (e) {}

    try {
      if (httpRequest.body) {
        xhr.send(httpRequest.body);
      } else {
        xhr.send();
      }
    } catch (err) {
      if (httpRequest.body && typeof httpRequest.body.buffer === 'object') {
        xhr.send(httpRequest.body.buffer); // send ArrayBuffer directly
      } else {
        throw err;
      }
    }

    return emitter;
  },

  parseHeaders: function parseHeaders(rawHeaders) {
    var headers = {};
    AWS.util.arrayEach(rawHeaders.split(/\r?\n/), function (line) {
      var key = line.split(':', 1)[0];
      var value = line.substring(key.length + 2);
      if (key.length > 0) headers[key.toLowerCase()] = value;
    });
    return headers;
  },

  finishRequest: function finishRequest(xhr, emitter) {
    var buffer;
    if (xhr.responseType === 'arraybuffer' && xhr.response) {
      var ab = xhr.response;
      buffer = new AWS.util.Buffer(ab.byteLength);
      var view = new Uint8Array(ab);
      for (var i = 0; i < buffer.length; ++i) {
        buffer[i] = view[i];
      }
    }

    try {
      if (!buffer && typeof xhr.responseText === 'string') {
        buffer = new AWS.util.Buffer(xhr.responseText);
      }
    } catch (e) {}

    if (buffer) emitter.emit('data', buffer);
    emitter.emit('end');
  }
});

/**
 * @api private
 */
AWS.HttpClient.prototype = AWS.XHRClient.prototype;

/**
 * @api private
 */
AWS.HttpClient.streamsApiVersion = 1;


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),
/* 127 */
/***/ (function(module) {

module.exports = JSON.parse("{\"version\":\"2.0\",\"metadata\":{\"apiVersion\":\"2016-11-28\",\"endpointPrefix\":\"runtime.lex\",\"jsonVersion\":\"1.1\",\"protocol\":\"rest-json\",\"serviceFullName\":\"Amazon Lex Runtime Service\",\"serviceId\":\"Lex Runtime Service\",\"signatureVersion\":\"v4\",\"signingName\":\"lex\",\"uid\":\"runtime.lex-2016-11-28\"},\"operations\":{\"DeleteSession\":{\"http\":{\"method\":\"DELETE\",\"requestUri\":\"/bot/{botName}/alias/{botAlias}/user/{userId}/session\"},\"input\":{\"type\":\"structure\",\"required\":[\"botName\",\"botAlias\",\"userId\"],\"members\":{\"botName\":{\"location\":\"uri\",\"locationName\":\"botName\"},\"botAlias\":{\"location\":\"uri\",\"locationName\":\"botAlias\"},\"userId\":{\"location\":\"uri\",\"locationName\":\"userId\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"botName\":{},\"botAlias\":{},\"userId\":{},\"sessionId\":{}}}},\"GetSession\":{\"http\":{\"method\":\"GET\",\"requestUri\":\"/bot/{botName}/alias/{botAlias}/user/{userId}/session/\"},\"input\":{\"type\":\"structure\",\"required\":[\"botName\",\"botAlias\",\"userId\"],\"members\":{\"botName\":{\"location\":\"uri\",\"locationName\":\"botName\"},\"botAlias\":{\"location\":\"uri\",\"locationName\":\"botAlias\"},\"userId\":{\"location\":\"uri\",\"locationName\":\"userId\"},\"checkpointLabelFilter\":{\"location\":\"querystring\",\"locationName\":\"checkpointLabelFilter\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"recentIntentSummaryView\":{\"shape\":\"Sa\"},\"sessionAttributes\":{\"shape\":\"Sd\"},\"sessionId\":{},\"dialogAction\":{\"shape\":\"Sh\"}}}},\"PostContent\":{\"http\":{\"requestUri\":\"/bot/{botName}/alias/{botAlias}/user/{userId}/content\"},\"input\":{\"type\":\"structure\",\"required\":[\"botName\",\"botAlias\",\"userId\",\"contentType\",\"inputStream\"],\"members\":{\"botName\":{\"location\":\"uri\",\"locationName\":\"botName\"},\"botAlias\":{\"location\":\"uri\",\"locationName\":\"botAlias\"},\"userId\":{\"location\":\"uri\",\"locationName\":\"userId\"},\"sessionAttributes\":{\"shape\":\"Sl\",\"jsonvalue\":true,\"location\":\"header\",\"locationName\":\"x-amz-lex-session-attributes\"},\"requestAttributes\":{\"shape\":\"Sl\",\"jsonvalue\":true,\"location\":\"header\",\"locationName\":\"x-amz-lex-request-attributes\"},\"contentType\":{\"location\":\"header\",\"locationName\":\"Content-Type\"},\"accept\":{\"location\":\"header\",\"locationName\":\"Accept\"},\"inputStream\":{\"shape\":\"So\"}},\"payload\":\"inputStream\"},\"output\":{\"type\":\"structure\",\"members\":{\"contentType\":{\"location\":\"header\",\"locationName\":\"Content-Type\"},\"intentName\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-intent-name\"},\"slots\":{\"jsonvalue\":true,\"location\":\"header\",\"locationName\":\"x-amz-lex-slots\"},\"sessionAttributes\":{\"jsonvalue\":true,\"location\":\"header\",\"locationName\":\"x-amz-lex-session-attributes\"},\"sentimentResponse\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-sentiment\"},\"message\":{\"shape\":\"Si\",\"location\":\"header\",\"locationName\":\"x-amz-lex-message\"},\"messageFormat\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-message-format\"},\"dialogState\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-dialog-state\"},\"slotToElicit\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-slot-to-elicit\"},\"inputTranscript\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-input-transcript\"},\"audioStream\":{\"shape\":\"So\"},\"sessionId\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-session-id\"}},\"payload\":\"audioStream\"},\"authtype\":\"v4-unsigned-body\"},\"PostText\":{\"http\":{\"requestUri\":\"/bot/{botName}/alias/{botAlias}/user/{userId}/text\"},\"input\":{\"type\":\"structure\",\"required\":[\"botName\",\"botAlias\",\"userId\",\"inputText\"],\"members\":{\"botName\":{\"location\":\"uri\",\"locationName\":\"botName\"},\"botAlias\":{\"location\":\"uri\",\"locationName\":\"botAlias\"},\"userId\":{\"location\":\"uri\",\"locationName\":\"userId\"},\"sessionAttributes\":{\"shape\":\"Sd\"},\"requestAttributes\":{\"shape\":\"Sd\"},\"inputText\":{\"shape\":\"Si\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"intentName\":{},\"slots\":{\"shape\":\"Sd\"},\"sessionAttributes\":{\"shape\":\"Sd\"},\"message\":{\"shape\":\"Si\"},\"sentimentResponse\":{\"type\":\"structure\",\"members\":{\"sentimentLabel\":{},\"sentimentScore\":{}}},\"messageFormat\":{},\"dialogState\":{},\"slotToElicit\":{},\"responseCard\":{\"type\":\"structure\",\"members\":{\"version\":{},\"contentType\":{},\"genericAttachments\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"members\":{\"title\":{},\"subTitle\":{},\"attachmentLinkUrl\":{},\"imageUrl\":{},\"buttons\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"required\":[\"text\",\"value\"],\"members\":{\"text\":{},\"value\":{}}}}}}}}},\"sessionId\":{}}}},\"PutSession\":{\"http\":{\"requestUri\":\"/bot/{botName}/alias/{botAlias}/user/{userId}/session\"},\"input\":{\"type\":\"structure\",\"required\":[\"botName\",\"botAlias\",\"userId\"],\"members\":{\"botName\":{\"location\":\"uri\",\"locationName\":\"botName\"},\"botAlias\":{\"location\":\"uri\",\"locationName\":\"botAlias\"},\"userId\":{\"location\":\"uri\",\"locationName\":\"userId\"},\"sessionAttributes\":{\"shape\":\"Sd\"},\"dialogAction\":{\"shape\":\"Sh\"},\"recentIntentSummaryView\":{\"shape\":\"Sa\"},\"accept\":{\"location\":\"header\",\"locationName\":\"Accept\"}}},\"output\":{\"type\":\"structure\",\"members\":{\"contentType\":{\"location\":\"header\",\"locationName\":\"Content-Type\"},\"intentName\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-intent-name\"},\"slots\":{\"jsonvalue\":true,\"location\":\"header\",\"locationName\":\"x-amz-lex-slots\"},\"sessionAttributes\":{\"jsonvalue\":true,\"location\":\"header\",\"locationName\":\"x-amz-lex-session-attributes\"},\"message\":{\"shape\":\"Si\",\"location\":\"header\",\"locationName\":\"x-amz-lex-message\"},\"messageFormat\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-message-format\"},\"dialogState\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-dialog-state\"},\"slotToElicit\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-slot-to-elicit\"},\"audioStream\":{\"shape\":\"So\"},\"sessionId\":{\"location\":\"header\",\"locationName\":\"x-amz-lex-session-id\"}},\"payload\":\"audioStream\"}}},\"shapes\":{\"Sa\":{\"type\":\"list\",\"member\":{\"type\":\"structure\",\"required\":[\"dialogActionType\"],\"members\":{\"intentName\":{},\"checkpointLabel\":{},\"slots\":{\"shape\":\"Sd\"},\"confirmationStatus\":{},\"dialogActionType\":{},\"fulfillmentState\":{},\"slotToElicit\":{}}}},\"Sd\":{\"type\":\"map\",\"key\":{},\"value\":{},\"sensitive\":true},\"Sh\":{\"type\":\"structure\",\"required\":[\"type\"],\"members\":{\"type\":{},\"intentName\":{},\"slots\":{\"shape\":\"Sd\"},\"slotToElicit\":{},\"fulfillmentState\":{},\"message\":{\"shape\":\"Si\"},\"messageFormat\":{}}},\"Si\":{\"type\":\"string\",\"sensitive\":true},\"Sl\":{\"type\":\"string\",\"sensitive\":true},\"So\":{\"type\":\"blob\",\"streaming\":true}}}");

/***/ }),
/* 128 */
/***/ (function(module) {

module.exports = JSON.parse("{\"pagination\":{}}");

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _microphone = __webpack_require__(130);

var _microphone2 = _interopRequireDefault(_microphone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var defaultConfig = {
  nFrequencyBars: 255,
  onAnalysed: null
};

var Recorder = function () {
  function Recorder(audioContext) {
    var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Recorder);

    this.config = Object.assign({}, defaultConfig, config);

    this.audioContext = audioContext;
    this.audioInput = null;
    this.realAudioInput = null;
    this.inputPoint = null;
    this.audioRecorder = null;
    this.rafID = null;
    this.analyserContext = null;
    this.recIndex = 0;
    this.stream = null;

    this.updateAnalysers = this.updateAnalysers.bind(this);
  }

  _createClass(Recorder, [{
    key: 'init',
    value: function init(stream) {
      var _this = this;

      return new Promise(function (resolve) {
        _this.inputPoint = _this.audioContext.createGain();

        _this.stream = stream;

        _this.realAudioInput = _this.audioContext.createMediaStreamSource(stream);
        _this.audioInput = _this.realAudioInput;
        _this.audioInput.connect(_this.inputPoint);

        _this.analyserNode = _this.audioContext.createAnalyser();
        _this.analyserNode.fftSize = 2048;
        _this.inputPoint.connect(_this.analyserNode);

        _this.audioRecorder = new _microphone2.default(_this.inputPoint);

        var zeroGain = _this.audioContext.createGain();
        zeroGain.gain.value = 0.0;

        _this.inputPoint.connect(zeroGain);
        zeroGain.connect(_this.audioContext.destination);

        _this.updateAnalysers();

        resolve();
      });
    }
  }, {
    key: 'start',
    value: function start() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        if (!_this2.audioRecorder) {
          reject('Not currently recording');
          return;
        }

        _this2.audioRecorder.clear();
        _this2.audioRecorder.record();

        resolve(_this2.stream);
      });
    }
  }, {
    key: 'stop',
    value: function stop() {
      var _this3 = this;

      return new Promise(function (resolve) {
        _this3.audioRecorder.stop();

        _this3.audioRecorder.getBuffer(function (buffer) {
          _this3.audioRecorder.exportWAV(function (blob) {
            return resolve({ buffer: buffer, blob: blob });
          });
        });
      });
    }
  }, {
    key: 'updateAnalysers',
    value: function updateAnalysers() {
      if (this.config.onAnalysed) {
        requestAnimationFrame(this.updateAnalysers);

        var freqByteData = new Uint8Array(this.analyserNode.frequencyBinCount);

        this.analyserNode.getByteFrequencyData(freqByteData);

        var data = new Array(255);
        var lastNonZero = 0;
        var datum = void 0;

        for (var idx = 0; idx < 255; idx += 1) {
          datum = Math.floor(freqByteData[idx]) - Math.floor(freqByteData[idx]) % 5;

          if (datum !== 0) {
            lastNonZero = idx;
          }

          data[idx] = datum;
        }

        this.config.onAnalysed({ data: data, lineTo: lastNonZero });
      }
    }
  }, {
    key: 'setOnAnalysed',
    value: function setOnAnalysed(handler) {
      this.config.onAnalysed = handler;
    }
  }]);

  return Recorder;
}();

Recorder.download = function download(blob) {
  var filename = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'audio';

  _microphone2.default.forceDownload(blob, filename + '.wav');
};

exports.default = Recorder;

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable */
/**
 * License (MIT)
 *
 * Copyright © 2013 Matt Diamond
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


var _inlineWorker = __webpack_require__(131);

var _inlineWorker2 = _interopRequireDefault(_inlineWorker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var defaultConfig = {
  bufferLen: 4096,
  numChannels: 2,
  mimeType: 'audio/wav'
};

var Microphone = function () {
  function Microphone(source, config) {
    var _this = this;

    _classCallCheck(this, Microphone);

    this.config = Object.assign({}, defaultConfig, config);

    this.recording = false;

    this.callbacks = {
      getBuffer: [],
      exportWAV: []
    };

    this.context = source.context;
    this.node = (this.context.createScriptProcessor || this.context.createJavaScriptNode).call(this.context, this.config.bufferLen, this.config.numChannels, this.config.numChannels);

    this.node.onaudioprocess = function (e) {
      if (!_this.recording) return;

      var buffer = [];
      for (var channel = 0; channel < _this.config.numChannels; channel++) {
        buffer.push(e.inputBuffer.getChannelData(channel));
      }
      _this.worker.postMessage({
        command: 'record',
        buffer: buffer
      });
    };

    source.connect(this.node);
    this.node.connect(this.context.destination); //this should not be necessary

    var self = {};
    this.worker = new _inlineWorker2.default(function () {
      var recLength = 0,
          recBuffers = [],
          sampleRate = void 0,
          numChannels = void 0;

      this.onmessage = function (e) {
        switch (e.data.command) {
          case 'init':
            init(e.data.config);
            break;
          case 'record':
            record(e.data.buffer);
            break;
          case 'exportWAV':
            exportWAV(e.data.type);
            break;
          case 'getBuffer':
            getBuffer();
            break;
          case 'clear':
            clear();
            break;
        }
      };

      function init(config) {
        sampleRate = config.sampleRate;
        numChannels = config.numChannels;
        initBuffers();
      }

      function record(inputBuffer) {
        for (var channel = 0; channel < numChannels; channel++) {
          recBuffers[channel].push(inputBuffer[channel]);
        }
        recLength += inputBuffer[0].length;
      }

      function exportWAV(type) {
        var buffers = [];
        for (var channel = 0; channel < numChannels; channel++) {
          buffers.push(mergeBuffers(recBuffers[channel], recLength));
        }
        var interleaved = void 0;
        if (numChannels === 2) {
          interleaved = interleave(buffers[0], buffers[1]);
        } else {
          interleaved = buffers[0];
        }
        var dataview = encodeWAV(interleaved);
        var audioBlob = new Blob([dataview], { type: type });

        this.postMessage({ command: 'exportWAV', data: audioBlob });
      }

      function getBuffer() {
        var buffers = [];
        for (var channel = 0; channel < numChannels; channel++) {
          buffers.push(mergeBuffers(recBuffers[channel], recLength));
        }
        this.postMessage({ command: 'getBuffer', data: buffers });
      }

      function clear() {
        recLength = 0;
        recBuffers = [];
        initBuffers();
      }

      function initBuffers() {
        for (var channel = 0; channel < numChannels; channel++) {
          recBuffers[channel] = [];
        }
      }

      function mergeBuffers(recBuffers, recLength) {
        var result = new Float32Array(recLength);
        var offset = 0;
        for (var i = 0; i < recBuffers.length; i++) {
          result.set(recBuffers[i], offset);
          offset += recBuffers[i].length;
        }
        return result;
      }

      function interleave(inputL, inputR) {
        var length = inputL.length + inputR.length;
        var result = new Float32Array(length);

        var index = 0,
            inputIndex = 0;

        while (index < length) {
          result[index++] = inputL[inputIndex];
          result[index++] = inputR[inputIndex];
          inputIndex++;
        }
        return result;
      }

      function floatTo16BitPCM(output, offset, input) {
        for (var i = 0; i < input.length; i++, offset += 2) {
          var s = Math.max(-1, Math.min(1, input[i]));
          output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
        }
      }

      function writeString(view, offset, string) {
        for (var i = 0; i < string.length; i += 1) {
          view.setUint8(offset + i, string.charCodeAt(i));
        }
      }

      function encodeWAV(samples) {
        var buffer = new ArrayBuffer(44 + samples.length * 2);
        var view = new DataView(buffer);

        /* RIFF identifier */
        writeString(view, 0, 'RIFF');
        /* RIFF chunk length */
        view.setUint32(4, 36 + samples.length * 2, true);
        /* RIFF type */
        writeString(view, 8, 'WAVE');
        /* format chunk identifier */
        writeString(view, 12, 'fmt ');
        /* format chunk length */
        view.setUint32(16, 16, true);
        /* sample format (raw) */
        view.setUint16(20, 1, true);
        /* channel count */
        view.setUint16(22, numChannels, true);
        /* sample rate */
        view.setUint32(24, sampleRate, true);
        /* byte rate (sample rate * block align) */
        view.setUint32(28, sampleRate * 4, true);
        /* block align (channel count * bytes per sample) */
        view.setUint16(32, numChannels * 2, true);
        /* bits per sample */
        view.setUint16(34, 16, true);
        /* data chunk identifier */
        writeString(view, 36, 'data');
        /* data chunk length */
        view.setUint32(40, samples.length * 2, true);

        floatTo16BitPCM(view, 44, samples);

        return view;
      }
    }, self);

    this.worker.postMessage({
      command: 'init',
      config: {
        sampleRate: this.context.sampleRate,
        numChannels: this.config.numChannels
      }
    });

    this.worker.onmessage = function (e) {
      var cb = _this.callbacks[e.data.command].pop();
      if (typeof cb === 'function') {
        cb(e.data.data);
      }
    };
  }

  _createClass(Microphone, [{
    key: 'record',
    value: function record() {
      this.recording = true;
    }
  }, {
    key: 'stop',
    value: function stop() {
      this.recording = false;
    }
  }, {
    key: 'clear',
    value: function clear() {
      this.worker.postMessage({ command: 'clear' });
    }
  }, {
    key: 'getBuffer',
    value: function getBuffer(cb) {
      cb = cb || this.config.callback;

      if (!cb) throw new Error('Callback not set');

      this.callbacks.getBuffer.push(cb);

      this.worker.postMessage({ command: 'getBuffer' });
    }
  }, {
    key: 'exportWAV',
    value: function exportWAV(cb, mimeType) {
      mimeType = mimeType || this.config.mimeType;
      cb = cb || this.config.callback;

      if (!cb) throw new Error('Callback not set');

      this.callbacks.exportWAV.push(cb);

      this.worker.postMessage({
        command: 'exportWAV',
        type: mimeType
      });
    }
  }]);

  return Microphone;
}();

Microphone.forceDownload = function forceDownload(blob, filename) {
  var a = document.createElement('a');

  a.style = 'display: none';
  document.body.appendChild(a);

  var url = window.URL.createObjectURL(blob);

  a.href = url;
  a.download = filename;
  a.click();

  window.URL.revokeObjectURL(url);

  document.body.removeChild(a);
};

exports.default = Microphone;

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var WORKER_ENABLED = !!(global === global.window && global.URL && global.Blob && global.Worker);

function InlineWorker(func, self) {
  var _this = this;
  var functionBody;

  self = self || {};

  if (WORKER_ENABLED) {
    functionBody = func.toString().trim().match(
      /^function\s*\w*\s*\([\w\s,]*\)\s*{([\w\W]*?)}$/
    )[1];

    return new global.Worker(global.URL.createObjectURL(
      new global.Blob([ functionBody ], { type: "text/javascript" })
    ));
  }

  function postMessage(data) {
    setTimeout(function() {
      _this.onmessage({ data: data });
    }, 0);
  }

  this.self = self;
  this.self.postMessage = postMessage;

  setTimeout(func.bind(self, self), 0);
}

InlineWorker.prototype.postMessage = function postMessage(data) {
  var _this = this;

  setTimeout(function() {
    _this.self.onmessage({ data: data });
  }, 0);
};

module.exports = InlineWorker;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(8)))

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(5);
            var content = __webpack_require__(133);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(6);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".audio-recorder-container .record-button {\r\n    outline: none;\r\n    stroke: currentColor;\r\n}\r\n\r\n.audio-recorder-container .record-button:active {\r\n    /* background-color: #d14210; */\r\n    stroke: black;\r\n}\r\n\r\n.audio-recorder-container .record-button:hover {\r\n    cursor: pointer;\r\n    cursor: hand;\r\n    stroke: #d14210;\r\n}\r\n\r\n.audio-recorder-container .record-button:focus {\r\n    outline-color: #d14210;\r\n    outline-width: 1px;\r\n    outline-style: dotted;\r\n}\r\n", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.13.0
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?
Symbol.for("react.suspense_list"):60120,r=b?Symbol.for("react.memo"):60115,t=b?Symbol.for("react.lazy"):60116,v=b?Symbol.for("react.block"):60121,w=b?Symbol.for("react.fundamental"):60117,x=b?Symbol.for("react.responder"):60118,y=b?Symbol.for("react.scope"):60119;
function z(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case t:case r:case h:return a;default:return u}}case d:return u}}}function A(a){return z(a)===m}exports.AsyncMode=l;exports.ConcurrentMode=m;exports.ContextConsumer=k;exports.ContextProvider=h;exports.Element=c;exports.ForwardRef=n;exports.Fragment=e;exports.Lazy=t;exports.Memo=r;exports.Portal=d;
exports.Profiler=g;exports.StrictMode=f;exports.Suspense=p;exports.isAsyncMode=function(a){return A(a)||z(a)===l};exports.isConcurrentMode=A;exports.isContextConsumer=function(a){return z(a)===k};exports.isContextProvider=function(a){return z(a)===h};exports.isElement=function(a){return"object"===typeof a&&null!==a&&a.$$typeof===c};exports.isForwardRef=function(a){return z(a)===n};exports.isFragment=function(a){return z(a)===e};exports.isLazy=function(a){return z(a)===t};
exports.isMemo=function(a){return z(a)===r};exports.isPortal=function(a){return z(a)===d};exports.isProfiler=function(a){return z(a)===g};exports.isStrictMode=function(a){return z(a)===f};exports.isSuspense=function(a){return z(a)===p};
exports.isValidElementType=function(a){return"string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||a===q||"object"===typeof a&&null!==a&&(a.$$typeof===t||a.$$typeof===r||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n||a.$$typeof===w||a.$$typeof===x||a.$$typeof===y||a.$$typeof===v)};exports.typeOf=z;


/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.13.0
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */





if (undefined !== "production") {
  (function() {
'use strict';

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;
var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace; // TODO: We don't use AsyncMode or ConcurrentMode anymore. They were temporary
// (unstable) APIs that have been removed. Can we remove the symbols?

var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for('react.suspense_list') : 0xead8;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;
var REACT_BLOCK_TYPE = hasSymbol ? Symbol.for('react.block') : 0xead9;
var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for('react.fundamental') : 0xead5;
var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for('react.responder') : 0xead6;
var REACT_SCOPE_TYPE = hasSymbol ? Symbol.for('react.scope') : 0xead7;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE || type.$$typeof === REACT_SCOPE_TYPE || type.$$typeof === REACT_BLOCK_TYPE);
}

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;

    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;

          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_LAZY_TYPE:
              case REACT_MEMO_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;

              default:
                return $$typeof;
            }

        }

      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
} // AsyncMode is deprecated along with isAsyncMode

var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;
var hasWarnedAboutDeprecatedIsAsyncMode = false; // AsyncMode should be deprecated

function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true; // Using console['warn'] to evade Babel and ESLint

      console['warn']('The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }

  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
exports.isValidElementType = isValidElementType;
exports.typeOf = typeOf;
  })();
}


/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactIs = __webpack_require__(42);
var assign = __webpack_require__(137);

var ReactPropTypesSecret = __webpack_require__(22);
var checkPropTypes = __webpack_require__(138);

var has = Function.call.bind(Object.prototype.hasOwnProperty);
var printWarning = function() {};

if (undefined !== 'production') {
  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

function emptyFunctionThatReturnsNull() {
  return null;
}

module.exports = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    elementType: createElementTypeTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (undefined !== 'production') {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          var err = new Error(
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
          err.name = 'Invariant Violation';
          throw err;
        } else if (undefined !== 'production' && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            printWarning(
              'You are manually calling a React.PropTypes validation ' +
              'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!ReactIs.isValidElementType(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      if (undefined !== 'production') {
        if (arguments.length > 1) {
          printWarning(
            'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
            'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
          );
        } else {
          printWarning('Invalid argument supplied to oneOf, expected an array.');
        }
      }
      return emptyFunctionThatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
        var type = getPreciseType(value);
        if (type === 'symbol') {
          return String(value);
        }
        return value;
      });
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (has(propValue, key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
      undefined !== 'production' ? printWarning('Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
      return emptyFunctionThatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        printWarning(
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
        );
        return emptyFunctionThatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = assign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // falsy value can't be a Symbol
    if (!propValue) {
      return false;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes;
  ReactPropTypes.resetWarningCache = checkPropTypes.resetWarningCache;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var printWarning = function() {};

if (undefined !== 'production') {
  var ReactPropTypesSecret = __webpack_require__(22);
  var loggedTypeFailures = {};
  var has = Function.call.bind(Object.prototype.hasOwnProperty);

  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (undefined !== 'production') {
    for (var typeSpecName in typeSpecs) {
      if (has(typeSpecs, typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error(
              (componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' +
              'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.'
            );
            err.name = 'Invariant Violation';
            throw err;
          }
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
        } catch (ex) {
          error = ex;
        }
        if (error && !(error instanceof Error)) {
          printWarning(
            (componentName || 'React class') + ': type specification of ' +
            location + ' `' + typeSpecName + '` is invalid; the type checker ' +
            'function must return `null` or an `Error` but returned a ' + typeof error + '. ' +
            'You may have forgotten to pass an argument to the type checker ' +
            'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' +
            'shape all require an argument).'
          );
        }
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          printWarning(
            'Failed ' + location + ' type: ' + error.message + (stack != null ? stack : '')
          );
        }
      }
    }
  }
}

/**
 * Resets warning cache when testing.
 *
 * @private
 */
checkPropTypes.resetWarningCache = function() {
  if (undefined !== 'production') {
    loggedTypeFailures = {};
  }
}

module.exports = checkPropTypes;


/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = __webpack_require__(22);

function emptyFunction() {}
function emptyFunctionWithReset() {}
emptyFunctionWithReset.resetWarningCache = emptyFunction;

module.exports = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  };
  shim.isRequired = shim;
  function getShim() {
    return shim;
  };
  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    elementType: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim,

    checkPropTypes: emptyFunctionWithReset,
    resetWarningCache: emptyFunction
  };

  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),
/* 140 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external {"commonjs":"react","commonjs2":"react","amd":"React","root":"React"}
var external_commonjs_react_commonjs2_react_amd_React_root_React_ = __webpack_require__(0);
var external_commonjs_react_commonjs2_react_amd_React_root_React_default = /*#__PURE__*/__webpack_require__.n(external_commonjs_react_commonjs2_react_amd_React_root_React_);

// EXTERNAL MODULE: ./src/components/DGovChatBot/DGovChatBot.css
var DGovChatBot = __webpack_require__(44);

// CONCATENATED MODULE: ./src/components/Svg/SvgWAFavicon.js


function SvgWAFavicon(props) {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    version: "1.1",
    id: "Layer_1",
    x: "0px",
    y: "0px",
    width: props.width,
    height: props.height,
    viewBox: "0 0 80 72"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("rect", {
    x: "15",
    y: "15.5",
    fill: "#060606",
    width: "59.7",
    height: "59.7"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("g", null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    fill: "#FFFFFF",
    d: "M48.5,33.8l-4.9,23.8h-3.8h-2.6l-2.9-12.8l-2.7,12.8h-3.3h-3.1l-4.9-23.8h6l2.6,14.5l2.9-14.5h2.9H37\r l3.1,14.6l2.6-14.6H48.5z"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    fill: "#FFFFFF",
    d: "M62.6,53.5h-7.7l-1.3,3.8h-6.6l8.6-23.8h4.2h1.6l8.8,23.8h-6.6L62.6,53.5z M61,48.6l-2.4-7.5l-2.2,7.5H61z"
  })));
}

/* harmony default export */ var Svg_SvgWAFavicon = (SvgWAFavicon);
// EXTERNAL MODULE: ./src/components/ResponseCardButtonStack/ResponseCardButtonStack.css
var ResponseCardButtonStack = __webpack_require__(47);

// CONCATENATED MODULE: ./src/components/ResponseCardButtonStack/ResponseCardButtonStack.js



function ResponseCardButtonStack_ResponseCardButtonStack(props) {
  var firstButtonText = props.buttons[0].text.toLowerCase();
  var isYesNoResponse = firstButtonText === 'yes' || firstButtonText === 'no';
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: !isYesNoResponse ? 'response-card-button-stack' : 'response-card-button-stack yesno'
  }, props.buttons.map(function (btn, index) {
    return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("button", {
      key: index,
      className: "chat-button",
      onClick: function onClick() {
        return props.onButtonClick(btn.value, btn.text);
      },
      tabIndex: 5 + index
    }, btn.text);
  }));
}

/* harmony default export */ var components_ResponseCardButtonStack_ResponseCardButtonStack = (ResponseCardButtonStack_ResponseCardButtonStack);
// CONCATENATED MODULE: ./src/components/ResponseCard/ResponseCard.js



function ResponseCard(props) {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, props.genericAttachment.title && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("h3", null, props.genericAttachment.title), props.genericAttachment.imageUrl && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("img", {
    src: props.genericAttachment.imageUrl,
    alt: ""
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(components_ResponseCardButtonStack_ResponseCardButtonStack, {
    buttons: props.genericAttachment.buttons,
    onButtonClick: props.onButtonClick
  }));
}

/* harmony default export */ var ResponseCard_ResponseCard = (ResponseCard);
// EXTERNAL MODULE: ./src/components/ResponseCarousel/ResponseCarousel.css
var ResponseCarousel = __webpack_require__(49);

// CONCATENATED MODULE: ./src/components/ResponseCarousel/ResponseCarousel.js
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





function ResponseCarousel_ResponseCarousel(props) {
  var _useState = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(props.genericAttachments.length),
      _useState2 = _slicedToArray(_useState, 1),
      pageCount = _useState2[0];

  var _useState3 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      currentPage = _useState4[0],
      setCurrentpage = _useState4[1];

  function onPreviousClicked() {
    setCurrentpage(currentPage > 0 ? currentPage - 1 : pageCount - 1);
  }

  function onNextClicked() {
    setCurrentpage(currentPage < pageCount - 1 ? currentPage + 1 : 0);
  }

  function onPageIndicatorClicked(index) {
    setCurrentpage(index);
  }

  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "response-carousel"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "carousel-top"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "previous-button",
    onClick: onPreviousClicked,
    title: 'previous response page'
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    alt: "previous option page",
    width: "30",
    height: "30",
    viewBox: "0 0 24 24",
    fill: "none",
    stroke: "currentColor",
    strokeWidth: "2",
    strokeLinecap: "round",
    strokeLinejoin: "round",
    className: "feather feather-chevron-left"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("polyline", {
    points: "15 18 9 12 15 6"
  }))), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "page-container"
  }, props.genericAttachments.map(function (genericAttachment, index) {
    return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
      className: currentPage === index ? "response-carousel-page active" : "response-carousel-page"
    }, genericAttachment.buttons.length > 0 && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(ResponseCard_ResponseCard, {
      carousel: true,
      genericAttachment: genericAttachment,
      onButtonClick: props.onButtonClick
    }));
  })), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "next-button",
    onClick: onNextClicked,
    title: "next response page"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    alt: "next option page",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24",
    fill: "none",
    stroke: "currentColor",
    strokeWidth: "2",
    strokeLinecap: "round",
    strokeLinejoin: "round",
    className: "feather feather-chevron-right"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("polyline", {
    points: "9 18 15 12 9 6"
  })))), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "carousel-indicators"
  }, props.genericAttachments.map(function (genericAttachment, index) {
    return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
      title: 'response page '.concat(index + 1),
      onClick: function onClick() {
        return onPageIndicatorClicked(index);
      },
      className: index === currentPage ? 'page-indicator active' : 'page-indicator'
    });
  })));
}

/* harmony default export */ var components_ResponseCarousel_ResponseCarousel = (ResponseCarousel_ResponseCarousel);
// CONCATENATED MODULE: ./src/util/types.js
var ResponseMode = {
  TEXT: 0,
  BUTTON: 1,
  SPEECH: 2,
  MINISTER_CONTACT_CARD: 3,
  AGENCY_CONTACT_CARD: 4
};
var MessageType = {
  OUT: 0,
  IN: 1,
  BOTACTIVITY: 2,
  ERROR: 3
};
// EXTERNAL MODULE: ./src/components/MinisterContactCard/MinisterContactCard.css
var MinisterContactCard = __webpack_require__(51);

// CONCATENATED MODULE: ./src/components/MinisterContactCard/MinisterContactCard.js



function MinisterContactCard_MinisterContactCard(props) {
  var contactDetails = JSON.parse(props.contactDetails);
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: 'minister-contact-card-container'
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-summary"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-photo-container"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("img", {
    className: "minister-photo",
    src: contactDetails.ImageURL,
    alt: "minister photo"
  })), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-header"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-name"
  }, contactDetails.Name), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", null, contactDetails.Portfolio))), (contactDetails.PostalAddress !== null || contactDetails.PostalSuburb !== null) && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-row"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    alt: "address",
    title: "address"
  }, "\uF003"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-item"
  }, contactDetails.PostalAddress, contactDetails.PostalSuburb)), contactDetails.Phone !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-row"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    alt: "phone number",
    title: "phone number"
  }, "\uF098"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-item"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: "tel:".concat(contactDetails.Phone)
  }, contactDetails.Phone))), contactDetails.Fax !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-row"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    alt: "fax number",
    title: "fax number"
  }, "\uF1AC"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-item"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: "fax:".concat(contactDetails.Fax)
  }, contactDetails.Fax))), contactDetails.EmailAddress !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-row"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    alt: "email address",
    title: "email address"
  }, "\uF1FA"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-item"
  }, "send ", external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: "mailto:".concat(contactDetails.EmailAddress),
    title: contactDetails.EmailAddress
  }, "email"))), contactDetails.Website !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-row"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    alt: "website address",
    title: "website address"
  }, "\uE9C9"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minister-details-item"
  }, "visit ", external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: contactDetails.Website,
    title: contactDetails.Website
  }, "website"))));
}

/* harmony default export */ var components_MinisterContactCard_MinisterContactCard = (MinisterContactCard_MinisterContactCard);
// EXTERNAL MODULE: ./src/components/AgencyContactCard/AgencyContactCard.css
var AgencyContactCard = __webpack_require__(53);

// CONCATENATED MODULE: ./src/components/AgencyContactCard/AgencyContactCard.js



function AgencyContactCard_AgencyContactCard(props) {
  var contactDetails = JSON.parse(props.contactDetails);
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: 'agency-contact-card-container'
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-row": "1",
    "grid-column": "1"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "agency-header",
    "grid-row": "1",
    "grid-column": "2"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "contact-card-name"
  }, contactDetails.Name)), (contactDetails.PostalAddress !== null || contactDetails.PostalSuburb !== null) && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    "grid-row": "2",
    "grid-column": "1",
    alt: "address",
    title: "address"
  }, "\uF003"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "agency-address",
    "grid-row": "2",
    "grid-column": "2"
  }, contactDetails.PostalAddress, ", ", contactDetails.PostalSuburb)), contactDetails.Phone !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    "grid-row": "3",
    "grid-column": "1",
    alt: "phone number",
    title: "phone number"
  }, "\uF098"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-row": "3",
    "grid-column": "2"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: "tel:".concat(contactDetails.Phone)
  }, contactDetails.Phone))), contactDetails.Fax !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    "grid-row": "4",
    "grid-column": "1",
    alt: "fax number",
    title: "fax number"
  }, "\uF1AC"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-row": "4",
    "grid-column": "2"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: "fax:".concat(contactDetails.Fax)
  }, contactDetails.Fax))), contactDetails.EmailAddress !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    "grid-row": "5",
    "grid-column": "1",
    alt: "email address",
    title: "email address"
  }, "\uF1FA"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-row": "5",
    "grid-column": "2"
  }, "send ", external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: "mailto:".concat(contactDetails.EmailAddress),
    title: contactDetails.EmailAddress
  }, "email"))), contactDetails.ContactForm !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    "grid-row": "6",
    "grid-column": "1",
    alt: "contact form",
    title: "contact form"
  }, "\uF044"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-row": "6",
    "grid-column": "2"
  }, "submit ", external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: contactDetails.ContactForm,
    title: contactDetails.ContactForm
  }, "contact form"))), contactDetails.Website !== null && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-item",
    "grid-row": "7",
    "grid-column": "1",
    alt: "website address",
    title: "website address"
  }, "\uE9C9"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-row": "7",
    "grid-column": "2"
  }, "visit ", external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("a", {
    href: contactDetails.Website,
    title: contactDetails.Website
  }, "website"))));
}

/* harmony default export */ var components_AgencyContactCard_AgencyContactCard = (AgencyContactCard_AgencyContactCard);
// CONCATENATED MODULE: ./src/components/MessageBubbles/SpeechBubble.js








function SpeechBubble(props) {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    key: props.index,
    className: 'messagecontainer '.concat(props.message.type === MessageType.IN ? 'in' : 'out')
  }, props.message.type === MessageType.IN && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-sender",
    alt: "WA",
    "grid-column": 1,
    "grid-row": 1
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgWAFavicon, {
    width: 40,
    height: 40
  })), props.message.responseMode === ResponseMode.MINISTER_CONTACT_CARD ? external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: 'messagecontent '.concat(props.message.type === MessageType.IN ? 'in' : 'out')
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(components_MinisterContactCard_MinisterContactCard, {
    contactDetails: props.message.fullResponse.sessionAttributes.ContactDetails
  })) : props.message.responseMode === ResponseMode.AGENCY_CONTACT_CARD ? external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: 'messagecontent '.concat(props.message.type === MessageType.IN ? 'in' : 'out')
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(components_AgencyContactCard_AgencyContactCard, {
    contactDetails: props.message.fullResponse.sessionAttributes.ContactDetails
  })) : external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: props.message.formattedMessage
    },
    className: 'messagecontent '.concat(props.message.type === MessageType.IN ? 'in' : 'out')
  }), props.message.responseMode === ResponseMode.BUTTON && props.message.genericAttachments !== undefined && props.displayResponseCards && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, props.message.genericAttachments.length > 1 ? external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-column": 1,
    "grid-row": 2
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(components_ResponseCarousel_ResponseCarousel, {
    genericAttachments: props.message.genericAttachments,
    onButtonClick: props.onClick
  })) : external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    "grid-column": 1,
    "grid-row": 2
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    style: {
      marginLeft: '10px'
    }
  }, props.message.genericAttachments.map(function (genericAttachment, index) {
    return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(ResponseCard_ResponseCard, {
      key: index,
      genericAttachment: genericAttachment,
      onButtonClick: props.onClick
    });
  })))));
}

/* harmony default export */ var MessageBubbles_SpeechBubble = (SpeechBubble);
// CONCATENATED MODULE: ./src/components/MessageBubbles/BotActivityBubble.js



function BotActivityBubble(props) {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    key: props.index,
    className: 'messagecontainer in'
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-sender",
    alt: "WA",
    title: "wa.gov.au logo"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgWAFavicon, {
    width: 40,
    height: 40
  })), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "messagecontent in"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("span", {
    className: "dot one"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("span", {
    className: "dot two"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("span", {
    className: "dot three"
  })));
}

/* harmony default export */ var MessageBubbles_BotActivityBubble = (BotActivityBubble);
// CONCATENATED MODULE: ./src/components/MessageBubbles/ErrorBubble.js



function ErrorBubble(props) {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    key: props.index,
    className: 'messagecontainer in'
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "icon-sender",
    alt: "WA",
    title: "wa.gov.au logo"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgWAFavicon, {
    width: 40,
    height: 40
  })), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "messagecontent in"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("span", {
    className: "dot one"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("span", {
    className: "dot two"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("span", {
    className: "dot three"
  })));
}

/* harmony default export */ var MessageBubbles_ErrorBubble = (ErrorBubble);
// CONCATENATED MODULE: ./node_modules/@babel/runtime/helpers/esm/extends.js
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime/helpers/esm/objectWithoutPropertiesLoose.js
function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = _objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}
// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(3);
var prop_types_default = /*#__PURE__*/__webpack_require__.n(prop_types);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/utils/debounce.js
// Corresponds to 10 frames at 60 Hz.
// A few bytes payload overhead when lodash/debounce is ~3 kB and debounce ~300 B.
function debounce(func) {
  var wait = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 166;
  var timeout;

  function debounced() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    // eslint-disable-next-line consistent-this
    var that = this;

    var later = function later() {
      func.apply(that, args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  }

  debounced.clear = function () {
    clearTimeout(timeout);
  };

  return debounced;
}
// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/utils/setRef.js
// TODO v5: make it private
function setRef(ref, value) {
  if (typeof ref === 'function') {
    ref(value);
  } else if (ref) {
    ref.current = value;
  }
}
// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/utils/useForkRef.js


function useForkRef(refA, refB) {
  /**
   * This will create a new function if the ref props change and are defined.
   * This means react will call the old forkRef with `null` and the new forkRef
   * with the ref. Cleanup naturally emerges from this behavior
   */
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useMemo(function () {
    if (refA == null && refB == null) {
      return null;
    }

    return function (refValue) {
      setRef(refA, refValue);
      setRef(refB, refValue);
    };
  }, [refA, refB]);
}
// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/TextareaAutosize/TextareaAutosize.js







function getStyleValue(computedStyle, property) {
  return parseInt(computedStyle[property], 10) || 0;
}

var useEnhancedEffect = typeof window !== 'undefined' ? external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useLayoutEffect : external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useEffect;
var styles = {
  /* Styles applied to the shadow textarea element. */
  shadow: {
    // Visibility needed to hide the extra text area on iPads
    visibility: 'hidden',
    // Remove from the content flow
    position: 'absolute',
    // Ignore the scrollbar width
    overflow: 'hidden',
    height: 0,
    top: 0,
    left: 0,
    // Create a new layer, increase the isolation of the computed values
    transform: 'translateZ(0)'
  }
};
var TextareaAutosize_TextareaAutosize = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.forwardRef(function TextareaAutosize(props, ref) {
  var onChange = props.onChange,
      rows = props.rows,
      rowsMax = props.rowsMax,
      _props$rowsMin = props.rowsMin,
      rowsMinProp = _props$rowsMin === void 0 ? 1 : _props$rowsMin,
      style = props.style,
      value = props.value,
      other = _objectWithoutProperties(props, ["onChange", "rows", "rowsMax", "rowsMin", "style", "value"]);

  var rowsMin = rows || rowsMinProp;

  var _React$useRef = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useRef(value != null),
      isControlled = _React$useRef.current;

  var inputRef = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useRef(null);
  var handleRef = useForkRef(ref, inputRef);
  var shadowRef = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useRef(null);
  var renders = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useRef(0);

  var _React$useState = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useState({}),
      state = _React$useState[0],
      setState = _React$useState[1];

  var syncHeight = external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useCallback(function () {
    var input = inputRef.current;
    var computedStyle = window.getComputedStyle(input);
    var inputShallow = shadowRef.current;
    inputShallow.style.width = computedStyle.width;
    inputShallow.value = input.value || props.placeholder || 'x';
    var boxSizing = computedStyle['box-sizing'];
    var padding = getStyleValue(computedStyle, 'padding-bottom') + getStyleValue(computedStyle, 'padding-top');
    var border = getStyleValue(computedStyle, 'border-bottom-width') + getStyleValue(computedStyle, 'border-top-width'); // The height of the inner content

    var innerHeight = inputShallow.scrollHeight - padding; // Measure height of a textarea with a single row

    inputShallow.value = 'x';
    var singleRowHeight = inputShallow.scrollHeight - padding; // The height of the outer content

    var outerHeight = innerHeight;

    if (rowsMin) {
      outerHeight = Math.max(Number(rowsMin) * singleRowHeight, outerHeight);
    }

    if (rowsMax) {
      outerHeight = Math.min(Number(rowsMax) * singleRowHeight, outerHeight);
    }

    outerHeight = Math.max(outerHeight, singleRowHeight); // Take the box sizing into account for applying this value as a style.

    var outerHeightStyle = outerHeight + (boxSizing === 'border-box' ? padding + border : 0);
    var overflow = Math.abs(outerHeight - innerHeight) <= 1;
    setState(function (prevState) {
      // Need a large enough difference to update the height.
      // This prevents infinite rendering loop.
      if (renders.current < 20 && (outerHeightStyle > 0 && Math.abs((prevState.outerHeightStyle || 0) - outerHeightStyle) > 1 || prevState.overflow !== overflow)) {
        renders.current += 1;
        return {
          overflow: overflow,
          outerHeightStyle: outerHeightStyle
        };
      }

      if (undefined !== 'production') {
        if (renders.current === 20) {
          console.error(['Material-UI: too many re-renders. The layout is unstable.', 'TextareaAutosize limits the number of renders to prevent an infinite loop.'].join('\n'));
        }
      }

      return prevState;
    });
  }, [rowsMax, rowsMin, props.placeholder]);
  external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useEffect(function () {
    var handleResize = debounce(function () {
      renders.current = 0;
      syncHeight();
    });
    window.addEventListener('resize', handleResize);
    return function () {
      handleResize.clear();
      window.removeEventListener('resize', handleResize);
    };
  }, [syncHeight]);
  useEnhancedEffect(function () {
    syncHeight();
  });
  external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.useEffect(function () {
    renders.current = 0;
  }, [value]);

  var handleChange = function handleChange(event) {
    renders.current = 0;

    if (!isControlled) {
      syncHeight();
    }

    if (onChange) {
      onChange(event);
    }
  };

  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("textarea", _extends({
    value: value,
    onChange: handleChange,
    ref: handleRef // Apply the rows prop to get a "correct" first SSR paint
    ,
    rows: rowsMin,
    style: _extends({
      height: state.outerHeightStyle,
      // Need a large enough difference to allow scrolling.
      // This prevents infinite rendering loop.
      overflow: state.overflow ? 'hidden' : null
    }, style)
  }, other)), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("textarea", {
    "aria-hidden": true,
    className: props.className,
    readOnly: true,
    ref: shadowRef,
    tabIndex: -1,
    style: _extends({}, styles.shadow, {}, style)
  }));
});
undefined !== "production" ? TextareaAutosize_TextareaAutosize.propTypes = {
  /**
   * @ignore
   */
  className: prop_types_default.a.string,

  /**
   * @ignore
   */
  onChange: prop_types_default.a.func,

  /**
   * @ignore
   */
  placeholder: prop_types_default.a.string,

  /**
   * Use `rowsMin` instead. The prop will be removed in v5.
   *
   * @deprecated
   */
  rows: prop_types_default.a.oneOfType([prop_types_default.a.string, prop_types_default.a.number]),

  /**
   * Maximum number of rows to display.
   */
  rowsMax: prop_types_default.a.oneOfType([prop_types_default.a.string, prop_types_default.a.number]),

  /**
   * Minimum number of rows to display.
   */
  rowsMin: prop_types_default.a.oneOfType([prop_types_default.a.string, prop_types_default.a.number]),

  /**
   * @ignore
   */
  style: prop_types_default.a.object,

  /**
   * @ignore
   */
  value: prop_types_default.a.any
} : void 0;
/* harmony default export */ var esm_TextareaAutosize_TextareaAutosize = (TextareaAutosize_TextareaAutosize);
// EXTERNAL MODULE: ./node_modules/aws-sdk/clients/lexruntime.js
var clients_lexruntime = __webpack_require__(23);
var lexruntime_default = /*#__PURE__*/__webpack_require__.n(clients_lexruntime);

// EXTERNAL MODULE: ./node_modules/aws-sdk/browser.js
var browser = __webpack_require__(4);
var browser_default = /*#__PURE__*/__webpack_require__.n(browser);

// CONCATENATED MODULE: ./src/components/Svg/SvgChatIcon.js


function SvgChatIcon() {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    className: "chat-icon-svg",
    version: "1.1",
    id: "Layer_1",
    x: "0px",
    y: "0px",
    width: "82.5px",
    height: "82.5px",
    viewBox: "0 0 170 170"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("circle", {
    cx: "85.8",
    cy: "85.7",
    r: "67.8"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    fill: "#FFFFFF",
    d: "M131.9,85.2c0-19.2-20.6-34.7-46.1-34.7S39.7,66,39.7,85.2s20.6,34.7,46.1,34.7c2.4,0,4.7-0.1,7-0.4l6.2,7.9\r l4-9.9l0.1-0.1C120,112.3,131.9,99.8,131.9,85.2z"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("g", null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    d: "M89.7,72.7l-5.6,27h-4.3h-2.9l-3.3-14.6l-3.2,14.6h-3.6h-3.6l-5.6-27h6.9l2.9,16.5l3.4-16.5H74h2.6L80,89.3\r l2.9-16.5h6.8V72.7z"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    d: "M105.6,95.2h-8.7l-1.4,4.5h-7.4l9.8-27h4.7h1.9l9.9,27H107L105.6,95.2z M103.9,89.6l-2.6-8.5l-2.6,8.5\r H103.9z"
  })));
}

/* harmony default export */ var Svg_SvgChatIcon = (SvgChatIcon);
// CONCATENATED MODULE: ./src/components/Svg/SvgMinimise.js


function SvgMinimise() {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24",
    fill: "#2D2F32",
    strokeWidth: "2",
    strokeLinecap: "round",
    strokeLinejoin: "round",
    className: "feather feather-chevron-down"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("polyline", {
    points: "6 9 12 15 18 9"
  }));
}

/* harmony default export */ var Svg_SvgMinimise = (SvgMinimise);
// CONCATENATED MODULE: ./src/components/Svg/SvgSend.js


function SvgSend() {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24",
    fill: "none",
    strokeWidth: "2",
    strokeLinecap: "round",
    strokeLinejoin: "round",
    className: "feather feather-send"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("line", {
    transform: "translate(7 -5) rotate(45 0 0)",
    x1: "22",
    y1: "2",
    x2: "11",
    y2: "13"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("polygon", {
    transform: "translate(7 -5) rotate(45 0 0)",
    points: "22 2 15 22 11 13 2 9 22 2"
  }));
}

/* harmony default export */ var Svg_SvgSend = (SvgSend);
// CONCATENATED MODULE: ./src/components/Svg/SvgMicrophone.js


function SvgMicrophone() {
  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24",
    fill: "none",
    strokeWidth: "2",
    strokeLinecap: "round",
    strokeLinejoin: "round",
    className: "feather feather-mic"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    d: "M12 1a3 3 0 0 0-3 3v8a3 3 0 0 0 6 0V4a3 3 0 0 0-3-3z"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("path", {
    d: "M19 10v2a7 7 0 0 1-14 0v-2"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("line", {
    x1: "12",
    y1: "19",
    x2: "12",
    y2: "23"
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("line", {
    x1: "8",
    y1: "23",
    x2: "16",
    y2: "23"
  }));
}

/* harmony default export */ var Svg_SvgMicrophone = (SvgMicrophone);
// EXTERNAL MODULE: ./node_modules/recorder-js/index.js
var recorder_js = __webpack_require__(43);
var recorder_js_default = /*#__PURE__*/__webpack_require__.n(recorder_js);

// EXTERNAL MODULE: ./src/components/Audio/AudioRecorder.css
var AudioRecorder = __webpack_require__(132);

// CONCATENATED MODULE: ./src/components/Audio/audio.js
/**
 * Get access to the users microphone through the browser.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia#Using_the_new_API_in_older_browsers
 */
function getAudioStream() {
  // Older browsers might not implement mediaDevices at all, so we set an empty object first
  if (navigator.mediaDevices === undefined) {
    navigator.mediaDevices = {};
  } // Some browsers partially implement mediaDevices. We can't just assign an object
  // with getUserMedia as it would overwrite existing properties.
  // Here, we will just add the getUserMedia property if it's missing.


  if (navigator.mediaDevices.getUserMedia === undefined) {
    navigator.mediaDevices.getUserMedia = function (constraints) {
      // First get ahold of the legacy getUserMedia, if present
      var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia; // Some browsers just don't implement it - return a rejected promise with an error
      // to keep a consistent interface

      if (!getUserMedia) {
        return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
      } // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise


      return new Promise(function (resolve, reject) {
        getUserMedia.call(navigator, constraints, resolve, reject);
      });
    };
  }

  var params = {
    audio: true,
    video: false
  };
  return navigator.mediaDevices.getUserMedia(params);
}
/**
 * Snippets taken from:
 * https://aws.amazon.com/blogs/machine-learning/capturing-voice-input-in-a-browser/
 */


var recordSampleRate = 44100;
/**
 * Samples the buffer at 16 kHz.
 */

function downsampleBuffer(buffer, exportSampleRate) {
  if (exportSampleRate === recordSampleRate) {
    return buffer;
  }

  var sampleRateRatio = recordSampleRate / exportSampleRate;
  var newLength = Math.round(buffer.length / sampleRateRatio);
  var result = new Float32Array(newLength);
  var offsetResult = 0;
  var offsetBuffer = 0;

  while (offsetResult < result.length) {
    var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
    var accum = 0;
    var count = 0;

    for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
      accum += buffer[i];
      count++;
    }

    result[offsetResult] = accum / count;
    offsetResult++;
    offsetBuffer = nextOffsetBuffer;
  }

  return result;
}

function floatTo16BitPCM(output, offset, input) {
  for (var i = 0; i < input.length; i++, offset += 2) {
    var s = Math.max(-1, Math.min(1, input[i]));
    output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7fff, true);
  }
}

function writeString(view, offset, string) {
  for (var i = 0; i < string.length; i++) {
    view.setUint8(offset + i, string.charCodeAt(i));
  }
}
/**
 * Encodes the buffer as a WAV file.
 */


function encodeWAV(samples) {
  var buffer = new ArrayBuffer(44 + samples.length * 2);
  var view = new DataView(buffer);
  writeString(view, 0, 'RIFF');
  view.setUint32(4, 32 + samples.length * 2, true);
  writeString(view, 8, 'WAVE');
  writeString(view, 12, 'fmt ');
  view.setUint32(16, 16, true);
  view.setUint16(20, 1, true);
  view.setUint16(22, 1, true);
  view.setUint32(24, recordSampleRate, true);
  view.setUint32(28, recordSampleRate * 2, true);
  view.setUint16(32, 2, true);
  view.setUint16(34, 16, true);
  writeString(view, 36, 'data');
  view.setUint32(40, samples.length * 2, true);
  floatTo16BitPCM(view, 44, samples);
  return view;
}
/**
 * Samples the buffer at 16 kHz.
 * Encodes the buffer as a WAV file.
 * Returns the encoded audio as a Blob.
 */


function exportBuffer(recBuffer) {
  var downsampledBuffer = downsampleBuffer(recBuffer, 16000);
  var encodedWav = encodeWAV(downsampledBuffer);
  var audioBlob = new Blob([encodedWav], {
    type: 'application/octet-stream'
  });
  return audioBlob;
}


// CONCATENATED MODULE: ./src/components/Audio/AudioRecorder.js
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function AudioRecorder_slicedToArray(arr, i) { return AudioRecorder_arrayWithHoles(arr) || AudioRecorder_iterableToArrayLimit(arr, i) || AudioRecorder_nonIterableRest(); }

function AudioRecorder_nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function AudioRecorder_iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function AudioRecorder_arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







function AudioRecorder_AudioRecorder(props) {
  var _useState = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(false),
      _useState2 = AudioRecorder_slicedToArray(_useState, 2),
      recording = _useState2[0],
      setRecording = _useState2[1];

  var _useState3 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(null),
      _useState4 = AudioRecorder_slicedToArray(_useState3, 2),
      audioRecorder = _useState4[0],
      setAudioRecorder = _useState4[1];

  var _useState5 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(null),
      _useState6 = AudioRecorder_slicedToArray(_useState5, 2),
      timer = _useState6[0],
      setTimer = _useState6[1];

  var audioRecorderStateRef = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useRef"])(audioRecorder);

  var setAudioRecorderState = function setAudioRecorderState(data) {
    audioRecorderStateRef.current = data;
    setAudioRecorder(data);
  };

  function start() {
    return _start.apply(this, arguments);
  }

  function _start() {
    _start = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var _audioRecorderStateRe;

      var audioContext, audioStream, recorder;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              audioContext = new (window.AudioContext || window.webkitAudioContext)();
              if (audioContext && audioContext.state === "suspended") audioContext.resume();
              audioStream = undefined;
              _context.next = 5;
              return getAudioStream().then(function (stream) {
                return audioStream = stream;
              })["catch"](function (error) {
                console.log("Could not get audio stream ".concat(error));
                return;
              });

            case 5:
              if (!(audioStream === undefined || audioStream === null)) {
                _context.next = 8;
                break;
              }

              console.log("No valid audio stream");
              return _context.abrupt("return");

            case 8:
              setRecording(true);
              props.onRecordingStarted();
              recorder = new recorder_js_default.a(audioContext, {// An array of 255 Numbers
                // You can use this to visualize the audio stream
                // If you use react, check out react-wave-stream
                //onAnalysed: data => console.log(data),
              });
              recorder.init(audioStream);
              setAudioRecorderState(recorder);
              setTimer(setExactTimeout(stop, 14000, 20));
              (_audioRecorderStateRe = audioRecorderStateRef.current) === null || _audioRecorderStateRe === void 0 ? void 0 : _audioRecorderStateRe.start();

            case 15:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    return _start.apply(this, arguments);
  }

  function stop() {
    return _stop.apply(this, arguments);
  }

  function _stop() {
    _stop = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
      var _audioRecorderStateRe2;

      var _ref, buffer, audio;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (recording) {
                _context2.next = 2;
                break;
              }

              return _context2.abrupt("return");

            case 2:
              if (timer) clearExactTimeout(timer);
              setRecording(false);
              _context2.next = 6;
              return audioRecorderStateRef === null || audioRecorderStateRef === void 0 ? void 0 : (_audioRecorderStateRe2 = audioRecorderStateRef.current) === null || _audioRecorderStateRe2 === void 0 ? void 0 : _audioRecorderStateRe2.stop()["catch"](function (error) {
                console.log(error);
                return;
              });

            case 6:
              _ref = _context2.sent;
              buffer = _ref.buffer;
              audio = exportBuffer(buffer[0]);
              props.onRecordingComplete(audio);

            case 10:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));
    return _stop.apply(this, arguments);
  }

  var setExactTimeout = function setExactTimeout(callback, duration, resolution) {
    var start = new Date().getTime();
    var timeout = setInterval(function () {
      var elapsed = new Date().getTime() - start;

      if (elapsed > duration) {
        callback();
        clearInterval(timeout);
      }
    }, resolution);
    return timeout;
  };

  var clearExactTimeout = function clearExactTimeout(timeout) {
    clearInterval(timeout);
  };

  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "audio-recorder-container"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("button", {
    className: "record-button",
    title: "Hold to record a message",
    onMouseDown: start,
    onMouseUp: stop,
    onTouchStart: start,
    onTouchEnd: stop,
    disabled: props.disabled,
    tabIndex: 10
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgMicrophone, null, "start"))));
}

/* harmony default export */ var Audio_AudioRecorder = (AudioRecorder_AudioRecorder);
// CONCATENATED MODULE: ./src/components/DGovChatBot/DGovChatBot.js
function DGovChatBot_slicedToArray(arr, i) { return DGovChatBot_arrayWithHoles(arr) || DGovChatBot_iterableToArrayLimit(arr, i) || DGovChatBot_nonIterableRest(); }

function DGovChatBot_nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function DGovChatBot_iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function DGovChatBot_arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }















function DGovChatBot_DGovChatBot(props) {
  var textAreaPlaceholderDefault = "Start typing or hold the mic button to record..";
  var textAreaPlaceholderStopRecording = "Listening..";
  var textAreaPlaceholderDefaultIE = "Send a message..";

  var _useState = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(''),
      _useState2 = DGovChatBot_slicedToArray(_useState, 2),
      input = _useState2[0],
      setInput = _useState2[1];

  var _useState3 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(false),
      _useState4 = DGovChatBot_slicedToArray(_useState3, 2),
      controlsEnabled = _useState4[0],
      setControlsEnabled = _useState4[1];

  var _useState5 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])([{
    type: MessageType.IN,
    formattedMessage: "Hi there! I've been trained to help you find agencies and Ministers' contact details.<br><br>I'm sorry I can't help you with all your queries. I seek your understanding and patience as I am new and still learning. I will continue to learn more topics to improve my knowledge."
  }]),
      _useState6 = DGovChatBot_slicedToArray(_useState5, 2),
      messages = _useState6[0],
      setMessages = _useState6[1];

  var _useState7 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(ResponseMode.TEXT),
      _useState8 = DGovChatBot_slicedToArray(_useState7, 2),
      lastResponseMode = _useState8[0],
      setLastResponseMode = _useState8[1];

  var _useState9 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(false),
      _useState10 = DGovChatBot_slicedToArray(_useState9, 2),
      initialised = _useState10[0],
      setInitialised = _useState10[1];

  var _useState11 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(true),
      _useState12 = DGovChatBot_slicedToArray(_useState11, 2),
      minimised = _useState12[0],
      setMinimised = _useState12[1];

  var _useState13 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])({
    'Accept-Language:': 'en-AU'
  }),
      _useState14 = DGovChatBot_slicedToArray(_useState13, 2),
      sessionAttributes = _useState14[0],
      setSessionAttributes = _useState14[1];

  var _useState15 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])('Contact-bot' + Date.now()),
      _useState16 = DGovChatBot_slicedToArray(_useState15, 1),
      userId = _useState16[0];

  var _useState17 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(false),
      _useState18 = DGovChatBot_slicedToArray(_useState17, 2),
      fulfilled = _useState18[0],
      setFulfilled = _useState18[1];

  var _useState19 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(false),
      _useState20 = DGovChatBot_slicedToArray(_useState19, 2),
      errorState = _useState20[0],
      setErrorState = _useState20[1];

  var _useState21 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(true),
      _useState22 = DGovChatBot_slicedToArray(_useState21, 2),
      lastRequestSuccessful = _useState22[0],
      setLastRequestSuccessful = _useState22[1];

  var _useState23 = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useState"])(null),
      _useState24 = DGovChatBot_slicedToArray(_useState23, 2),
      textAreaPlaceholder = _useState24[0],
      setTextAreaPlaceholder = _useState24[1];

  var dummyScrollElement = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useRef"])(null);

  function onInput(e) {
    setInput(e.target.value);
  }

  function resetSessionAttributes() {
    var tempSessionAttributes = {};
    tempSessionAttributes["IntentStatus"] = "UnknownStatus";
    tempSessionAttributes["appContext"] = sessionAttributes.appContext;
    tempSessionAttributes["InputType"] = sessionAttributes.InputType;
    tempSessionAttributes["Accept-Language"] = "en-AU";
    setSessionAttributes(tempSessionAttributes);
  }

  function submitTextMessage(messageContent, display, buttonclicked) {
    var displayText = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    setControlsEnabled(false);
    var trimmedMessageContent = messageContent.trim();

    if (trimmedMessageContent.length === 0) {
      setInput('');
      return;
    }

    var outgoingMessage = {
      type: MessageType.OUT,
      formattedMessage: buttonclicked ? displayText : trimmedMessageContent
    };
    var tempMessages = display ? messages.concat(outgoingMessage) : messages;
    setMessages(tempMessages); // Create a bogus message for the '...' content

    var botActivityMessage = {
      type: MessageType.BOTACTIVITY
    };
    tempMessages = tempMessages.concat(botActivityMessage);
    setMessages(tempMessages);
    browser_default.a.config.region = props.config.awsRegionId;
    browser_default.a.config.credentials = new browser_default.a.CognitoIdentityCredentials({
      IdentityPoolId: props.config.awsCognitoIdentityPoolId
    });
    var lexruntime = new lexruntime_default.a();
    var params = {
      botAlias: props.config.awsBotAlias,
      botName: props.config.awsBotName,
      inputText: trimmedMessageContent,
      userId: userId,
      sessionAttributes: sessionAttributes
    };

    if (buttonclicked) {
      var tempAttributes = sessionAttributes;
      tempAttributes["clicked"] = "true";
      params.sessionAttributes = tempAttributes;
    }

    try {
      lexruntime.postText(params, function (err, response) {
        if (err) {
          setErrorState(true);
          console.log(err);
        } else {
          setLastRequestSuccessful(true);
          var responseMessage = {
            type: MessageType.IN,
            formattedMessage: response.message,
            fullResponse: response
          };

          if (containsResponseCardButtons(response)) {
            responseMessage.genericAttachments = response.responseCard.genericAttachments;
            setLastResponseMode(ResponseMode.BUTTON);
            responseMessage.responseMode = ResponseMode.BUTTON;
          } else {
            setLastResponseMode(ResponseMode.TEXT);
            responseMessage.responseMode = ResponseMode.TEXT;
          } // Remove the '...' message now that the bot has responded


          tempMessages.pop();
          tempMessages = tempMessages.concat(responseMessage);
          setMessages(tempMessages);

          if (containsContactDetails(response)) {
            var contactCardResponseMessage = {
              type: MessageType.IN,
              formattedMessage: response.message,
              fullResponse: response
            };

            switch (response.sessionAttributes.IntentTarget.toLowerCase()) {
              case 'minister':
              case 'premier':
                setLastResponseMode(ResponseMode.MINISTER_CONTACT_CARD);
                contactCardResponseMessage.responseMode = ResponseMode.MINISTER_CONTACT_CARD;
                break;

              case 'agency':
                setLastResponseMode(ResponseMode.AGENCY_CONTACT_CARD);
                contactCardResponseMessage.responseMode = ResponseMode.AGENCY_CONTACT_CARD;
                break;
            }

            setMessages(tempMessages.concat(contactCardResponseMessage));
          }

          if (response.sessionAttributes.appContext) {
            response.sessionAttributes.appContext = null;
          }

          setControlsEnabled(true);

          if (response.dialogState === 'Fulfilled') {
            resetSessionAttributes();
            setFulfilled(true);
          } else {
            setSessionAttributes(response.sessionAttributes);
          }
        }
      });
    } catch (e) {
      setErrorState(true);
    }
  }

  var submitTextMessageCallback = Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useCallback"])(function (messageContent, display, buttonclicked, displayText) {
    submitTextMessage(messageContent, display, buttonclicked, displayText);
  }, [messages, props.config.awsBotAlias, props.config.awsBotName, props.config.awsCognitoIdentityPoolId, props.config.awsRegionId, sessionAttributes, userId]);
  Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useEffect"])(function () {
    var isExplorer = isIE();
    isExplorer ? setTextAreaPlaceholder(textAreaPlaceholderDefaultIE) : setTextAreaPlaceholder(textAreaPlaceholderDefault);
  }, []);
  Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useEffect"])(function () {
    if (!minimised && !initialised) {
      submitTextMessageCallback("Welcome", false, false);
      setInitialised(true);
    }
  }, [minimised, initialised, submitTextMessageCallback]);
  Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useEffect"])(function () {
    if (fulfilled) {
      submitTextMessageCallback("continueconversation", false, false);
      setFulfilled(false);
    }
  }, [fulfilled, submitTextMessageCallback]);
  Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useEffect"])(function () {
    if (errorState && lastRequestSuccessful) {
      var tempMessages = messages;

      if (messages[messages.length - 1].type === MessageType.BOTACTIVITY) {
        tempMessages.pop();
        var errorMessage = {
          type: MessageType.ERROR,
          formattedMessage: "Sorry, an error has occurred.  Please try again later."
        };
        tempMessages = tempMessages.concat(errorMessage);
        setMessages(tempMessages);
        setFulfilled(true);
      }

      setErrorState(false);
      setLastRequestSuccessful(false);
    }
  }, [errorState, lastRequestSuccessful, messages]);
  Object(external_commonjs_react_commonjs2_react_amd_React_root_React_["useEffect"])(function () {
    dummyScrollElement.current.scrollIntoView({
      block: 'end',
      behaviour: 'smooth'
    });
  });

  function onResponseCardButtonClicked(value, text) {
    if (value === '') return;
    submitTextMessage(value, true, true, text);
  }

  function onSendTypedMessage() {
    if (input === '') return;
    submitTextMessage(input, true, false);
    setInput('');
  }

  function onSendKeyPress(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
      submitTextMessage(input, true, false);
      setInput('');
    }
  }

  function onOpenChatBotKeyPress(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
      setMinimised(false);
    }
  }

  function onCloseChatBotKeyPress(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
      setMinimised(true);
    }
  }

  function onRecordingStarted() {
    setTextAreaPlaceholder(textAreaPlaceholderStopRecording);
  }

  function onRecordingStopped(blob) {
    setTextAreaPlaceholder(textAreaPlaceholderDefault);
    submitAudioMessage(blob);
  }

  function submitAudioMessage(audioBlob) {
    if (!audioBlob) return;
    setControlsEnabled(false); // Create a bogus message for the '...' content

    var botActivityMessage = {
      type: MessageType.BOTACTIVITY
    };
    var tempMessages = messages.concat(botActivityMessage);
    setMessages(tempMessages);
    browser_default.a.config.region = props.config.awsRegionId;
    browser_default.a.config.credentials = new browser_default.a.CognitoIdentityCredentials({
      IdentityPoolId: props.config.awsCognitoIdentityPoolId
    });
    var lexruntime = new lexruntime_default.a();
    var params = {
      accept: "audio/mpeg",
      botAlias: props.config.awsBotAlias,
      botName: props.config.awsBotName,
      contentType: "audio/l16; rate=16000; channels=1",
      inputStream: audioBlob,
      userId: userId,
      sessionAttributes: sessionAttributes
    };
    lexruntime.postContent(params, function (err, response) {
      if (err) {
        setErrorState(true);
        console.log(err);
      } else {
        tempMessages.pop();
        setLastRequestSuccessful(true);
        var outgoingMessage = {
          type: MessageType.OUT,
          formattedMessage: response.inputTranscript
        };
        tempMessages = tempMessages.concat(outgoingMessage);
        setMessages(tempMessages);
        var responseMessage = {
          type: MessageType.IN,
          formattedMessage: response.message,
          fullResponse: response
        };

        try {
          var audioUrl = window.URL.createObjectURL(new Blob([response.audioStream], {
            type: "audio/mpeg"
          }));
          var audio = new Audio(audioUrl);
          audio.play();
        } catch (e) {
          console.log(e);
        } // Remove the '...' message now that the bot has responded


        tempMessages = tempMessages.concat(responseMessage);
        setMessages(tempMessages);

        if (response.sessionAttributes.appContext) {
          try {
            var appContext = JSON.parse(response.sessionAttributes.appContext);

            if (appContext && appContext.contentType === "application/vnd.amazonaws.card.generic") {
              responseMessage.genericAttachments = appContext.genericAttachments;
              setLastResponseMode(ResponseMode.BUTTON);
              responseMessage.responseMode = ResponseMode.BUTTON;
              delete response.sessionAttributes.appContext;
            }
          } catch (e) {
            console.log("Could not parse app context: " + e);
          }
        }

        if (containsContactDetails(response)) {
          var contactCardResponseMessage = {
            type: MessageType.IN,
            formattedMessage: response.message,
            fullResponse: response
          };

          switch (response.sessionAttributes.IntentTarget.toLowerCase()) {
            case 'minister':
            case 'premier':
              setLastResponseMode(ResponseMode.MINISTER_CONTACT_CARD);
              contactCardResponseMessage.responseMode = ResponseMode.MINISTER_CONTACT_CARD;
              break;

            case 'agency':
              setLastResponseMode(ResponseMode.AGENCY_CONTACT_CARD);
              contactCardResponseMessage.responseMode = ResponseMode.AGENCY_CONTACT_CARD;
              break;
          }

          setMessages(tempMessages.concat(contactCardResponseMessage));
        }

        setControlsEnabled(true);

        if (response.dialogState === 'Fulfilled') {
          resetSessionAttributes();
          setFulfilled(true);
        } else {
          setSessionAttributes(response.sessionAttributes);
        }
      }
    });
  }

  ;

  function isIE() {
    return document.documentMode !== undefined;
  }

  function containsResponseCardButtons(response) {
    var returnVal = false;
    if (response.responseCard === undefined || response.responseCard === null) return false;
    response.responseCard.genericAttachments.forEach(function (genericAttachment) {
      if (genericAttachment.buttons.length > 0) {
        returnVal = true;
      }
    });
    return returnVal;
  }

  function containsContactDetails(response) {
    var _response$sessionAttr;

    return ((_response$sessionAttr = response.sessionAttributes) === null || _response$sessionAttr === void 0 ? void 0 : _response$sessionAttr.ContactDetails) !== undefined;
  }

  return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.Fragment, null, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: minimised ? 'chatbot minimised' : 'chatbot'
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "header"
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "header-text"
  }, "WA.gov.au chatbot pilot"), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "minimise-icon",
    alt: "minimise",
    title: "minimise chatbot",
    tabIndex: 13,
    onKeyPress: onCloseChatBotKeyPress,
    onClick: function onClick() {
      return setMinimised(true);
    }
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgMinimise, null))), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "chat-feed",
    id: "chatfeed"
  }, messages.map(function (msg, index) {
    switch (msg.type) {
      case MessageType.IN:
      case MessageType.OUT:
        return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(MessageBubbles_SpeechBubble, {
          key: index,
          index: index,
          displayResponseCards: index === messages.length - 1,
          message: msg,
          responseMode: lastResponseMode,
          onClick: onResponseCardButtonClicked
        });

      case MessageType.BOTACTIVITY:
        return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(MessageBubbles_BotActivityBubble, {
          key: index
        });

      case MessageType.ERROR:
        return external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(MessageBubbles_ErrorBubble, {
          key: index,
          message: msg.formattedMessage
        });

      default:
        return null;
    }
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    id: 'el',
    ref: dummyScrollElement
  })), (lastResponseMode === ResponseMode.TEXT || lastResponseMode === ResponseMode.BUTTON) && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: "text-input-panel"
  }, !isIE() && external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Audio_AudioRecorder, {
    disabled: !controlsEnabled,
    onRecordingComplete: onRecordingStopped,
    onRecordingStarted: onRecordingStarted
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(esm_TextareaAutosize_TextareaAutosize, {
    className: "input-control",
    "aria-label": "Enter message here",
    title: "Enter message here",
    id: "text-input",
    rowsMax: 5,
    placeholder: textAreaPlaceholder,
    maxLength: 1024,
    autoFocus: true,
    onChange: onInput,
    value: input,
    onKeyPress: onSendKeyPress // ref={input => input && input.focus()}
    ,
    tabIndex: 11
  }), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    alt: "Send",
    title: "Send",
    className: "send-button",
    onClick: controlsEnabled ? onSendTypedMessage : undefined,
    tabIndex: 12,
    onKeyPress: onSendKeyPress
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgSend, null)))), external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement("div", {
    className: minimised ? 'chat-icon no-print' : 'chat-icon hidden no-print',
    title: "open chatbot",
    onClick: function onClick() {
      return setMinimised(false);
    },
    tabIndex: 0,
    onKeyPress: onOpenChatBotKeyPress
  }, external_commonjs_react_commonjs2_react_amd_React_root_React_default.a.createElement(Svg_SvgChatIcon, null)));
}

/* harmony default export */ var components_DGovChatBot_DGovChatBot = (DGovChatBot_DGovChatBot);
// CONCATENATED MODULE: ./src/index.js

/* harmony default export */ var src = __webpack_exports__["default"] = (components_DGovChatBot_DGovChatBot);

/***/ })
/******/ ]);