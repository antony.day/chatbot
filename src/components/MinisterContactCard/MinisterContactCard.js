import React from 'react';
import './MinisterContactCard.css';

function MinisterContactCard(props) {

    let contactDetails = JSON.parse(props.contactDetails);

    return (
        <div className={'minister-contact-card-container'}>

            <div className='minister-summary'>
                <div className="minister-photo-container">
                    <img className="minister-photo" src={contactDetails.ImageURL} alt="minister photo" />
                </div>
                <div className="minister-header">
                    <div className="minister-name">{contactDetails.Name}</div>
                    <div>{contactDetails.Portfolio}</div>
                </div>
            </div>

            {(contactDetails.PostalAddress !== null || contactDetails.PostalSuburb !== null) &&
                <div className='minister-details-row'>
                    <div className='icon-item' alt='address' title='address'>&#xf003;</div>
                    <div className='minister-details-item'>
                        {contactDetails.PostalAddress}
                        {contactDetails.PostalSuburb}
                    </div>
                </div>
            }

            {contactDetails.Phone !== null &&
                <div className='minister-details-row'>
                    <div className='icon-item' alt='phone number' title='phone number'>&#xf098;</div>
                    <div className='minister-details-item'>
                        <a href={"tel:".concat(contactDetails.Phone)}>{contactDetails.Phone}</a>
                    </div>
                </div>
            }


            {contactDetails.Fax !== null &&
                <div className='minister-details-row'>
                    <div className='icon-item' alt='fax number' title='fax number'>&#xf1ac;</div>
                    <div className='minister-details-item'>
                        <a href={"fax:".concat(contactDetails.Fax)}>{contactDetails.Fax}</a>
                    </div>
                </div>
            }

            {contactDetails.EmailAddress !== null &&
                <div className='minister-details-row'>
                    <div className='icon-item' alt='email address' title='email address'>&#xf1fa;</div>
                    <div className='minister-details-item'>
                        send <a href={"mailto:".concat(contactDetails.EmailAddress)} title={contactDetails.EmailAddress}>email</a>
                    </div>
                </div>
            }

            {contactDetails.Website !== null &&
                <div className='minister-details-row'>
                    <div className='icon-item' alt='website address' title='website address'>&#xe9c9;</div>
                    <div className='minister-details-item'>
                        visit <a href={contactDetails.Website} title={contactDetails.Website}>website</a>
                    </div>
                </div>
            }

        </div>
    )
}

export default MinisterContactCard;