import React, { useState, useRef } from 'react';
import SvgMicrophone from '../Svg/SvgMicrophone';
import Recorder from 'recorder-js';
import './AudioRecorder.css';
import { getAudioStream, exportBuffer } from './audio';

function AudioRecorder(props) {

    const [recording, setRecording] = useState(false);
    const [audioRecorder, setAudioRecorder] = useState(null);
    const [timer, setTimer] = useState(null);

    const audioRecorderStateRef = useRef(audioRecorder);
    const setAudioRecorderState = data => {
        audioRecorderStateRef.current = data;
        setAudioRecorder(data);
    }

    async function start() {

        var audioContext = new (window.AudioContext || window.webkitAudioContext)();

        if (audioContext && audioContext.state === "suspended")
            audioContext.resume();

        let audioStream = undefined;
        await getAudioStream().then(stream => audioStream = stream).catch(error => { console.log("Could not get audio stream ".concat(error)); return });
        
        if (audioStream === undefined || audioStream === null) { console.log("No valid audio stream"); return; }

        setRecording(true);
        props.onRecordingStarted();

        let recorder = new Recorder(audioContext, {
            // An array of 255 Numbers
            // You can use this to visualize the audio stream
            // If you use react, check out react-wave-stream
            //onAnalysed: data => console.log(data),
        });
        recorder.init(audioStream);

        setAudioRecorderState(recorder);
        setTimer(setExactTimeout(stop, 14000, 20));
        audioRecorderStateRef.current?.start();
    }

    async function stop() {

        if (!recording) return;

        if (timer) clearExactTimeout(timer);
        setRecording(false);

        const { buffer } = await audioRecorderStateRef?.current?.stop().catch((error) => { console.log(error); return; });

        let audio = exportBuffer(buffer[0]);
        props.onRecordingComplete(audio);
    }

    const setExactTimeout = function (callback, duration, resolution) {
        const start = (new Date()).getTime();
        const timeout = setInterval(function () {
            let elapsed = new Date().getTime() - start;
            if (elapsed > duration) {
                callback();
                clearInterval(timeout);
            }
        }, resolution);

        return timeout;
    };

    const clearExactTimeout = function (timeout) {
        clearInterval(timeout);
    };

    return (
        <React.Fragment>
            <div className='audio-recorder-container'>
                <button className='record-button'
                    title='Hold to record a message'
                    onMouseDown={start}
                    onMouseUp={stop}
                    onTouchStart={start}
                    onTouchEnd={stop}
                    disabled={props.disabled}
                    tabIndex={10}>
                    <SvgMicrophone>start</SvgMicrophone>
                </button>
            </div>
        </React.Fragment>
    )
}

export default AudioRecorder;