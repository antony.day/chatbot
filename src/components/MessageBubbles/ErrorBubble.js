import React from 'react';
import SvgWAFavicon from '../Svg/SvgWAFavicon';

function ErrorBubble(props) {

    return (
        <div key={props.index} className={'messagecontainer in'}>
            <div className='icon-sender' alt='WA' title="wa.gov.au logo">
                <SvgWAFavicon width={40} height={40}></SvgWAFavicon>
            </div>
            <div className='messagecontent in'>
                <span className='dot one'></span>
                <span className='dot two'></span>
                <span className='dot three'></span>
            </div>
        </div>
    )
}

export default ErrorBubble;