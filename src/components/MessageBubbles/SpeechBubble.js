import React from 'react';
import SvgWAFavicon from '../Svg/SvgWAFavicon';
import ResponseCard from '../ResponseCard/ResponseCard';
import ResponseCarousel from '../ResponseCarousel/ResponseCarousel';
import { MessageType, ResponseMode } from '../../util/types.js';
import MinisterContactCard from '../MinisterContactCard/MinisterContactCard';
import AgencyContactCard from '../AgencyContactCard/AgencyContactCard';

function SpeechBubble(props) {

    return (
        <div key={props.index} className={'messagecontainer '.concat(props.message.type === MessageType.IN ? 'in' : 'out')}>
            {props.message.type === MessageType.IN &&
                <div className='icon-sender' alt='WA' grid-column={1} grid-row={1}>
                    <SvgWAFavicon width={40} height={40}></SvgWAFavicon>
                </div>
            }

            {props.message.responseMode === ResponseMode.MINISTER_CONTACT_CARD ?
                <div className={'messagecontent '.concat(props.message.type === MessageType.IN ? 'in' : 'out')} >
                    <MinisterContactCard contactDetails={props.message.fullResponse.sessionAttributes.ContactDetails} />
                </div>
                : props.message.responseMode === ResponseMode.AGENCY_CONTACT_CARD ?
                    <div className={'messagecontent '.concat(props.message.type === MessageType.IN ? 'in' : 'out')} >
                        <AgencyContactCard contactDetails={props.message.fullResponse.sessionAttributes.ContactDetails} />
                    </div>
                    :
                    <div dangerouslySetInnerHTML={{ __html: props.message.formattedMessage }} className={'messagecontent '.concat(props.message.type === MessageType.IN ? 'in' : 'out')} />
            }

            {(props.message.responseMode === ResponseMode.BUTTON &&
                props.message.genericAttachments !== undefined &&
                props.displayResponseCards) &&
                <React.Fragment>
                    {props.message.genericAttachments.length > 1 ?
                        <React.Fragment>
                            <div grid-column={1} grid-row={2}>
                            </div>
                            <ResponseCarousel genericAttachments={props.message.genericAttachments} onButtonClick={props.onClick} />
                        </React.Fragment> :
                        <React.Fragment>
                            <div grid-column={1} grid-row={2}>
                            </div>
                            <div style={{marginLeft: '10px'}}>
                                {props.message.genericAttachments.map((genericAttachment, index) => {
                                    return <ResponseCard key={index} genericAttachment={genericAttachment} onButtonClick={props.onClick} />
                                })
                                }
                            </div>
                        </React.Fragment>
                    }
                </React.Fragment>
            }
        </div>
    )
}

export default SpeechBubble;