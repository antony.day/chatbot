import React from 'react';
import './AgencyContactCard.css';

function AgencyContactCard(props) {
    let contactDetails = JSON.parse(props.contactDetails);

    return (
        <div className={'agency-contact-card-container'}>
            <div grid-row="1" grid-column="1"></div>
            <div className="agency-header" grid-row="1" grid-column="2">
                <div className="contact-card-name">{contactDetails.Name}</div>
            </div>

            {(contactDetails.PostalAddress !== null || contactDetails.PostalSuburb !== null) &&
                <React.Fragment>
                    <div className='icon-item' grid-row="2" grid-column="1" alt='address' title='address'>&#xf003;</div>
                    <div className="agency-address" grid-row="2" grid-column="2">
                        {contactDetails.PostalAddress}, {contactDetails.PostalSuburb}
                    </div>
                </React.Fragment>
            }

            {contactDetails.Phone !== null &&
                <React.Fragment>
                    <div className='icon-item' grid-row="3" grid-column="1" alt='phone number' title='phone number'>&#xf098;</div>
                    <div grid-row="3" grid-column="2">
                        <a href={"tel:".concat(contactDetails.Phone)}>{contactDetails.Phone}</a>
                    </div>
                </React.Fragment>
            }

            {contactDetails.Fax !== null &&
                <React.Fragment>
                    <div className='icon-item' grid-row="4" grid-column="1" alt='fax number' title='fax number'>&#xf1ac;</div>
                    <div grid-row="4" grid-column="2">
                        <a href={"fax:".concat(contactDetails.Fax)}>{contactDetails.Fax}</a>
                    </div>
                </React.Fragment>
            }

            {contactDetails.EmailAddress !== null &&
                <React.Fragment>
                    <div className='icon-item' grid-row="5" grid-column="1" alt='email address' title='email address'>&#xf1fa;</div>
                    <div grid-row="5" grid-column="2">
                        send <a href={"mailto:".concat(contactDetails.EmailAddress)} title={contactDetails.EmailAddress}>email</a>
                    </div>
                </React.Fragment>
            }

            {contactDetails.ContactForm !== null &&
                <React.Fragment>
                    <div className='icon-item' grid-row="6" grid-column="1" alt='contact form' title='contact form'>&#xf044;</div>
                    <div grid-row="6" grid-column="2">
                        submit <a href={contactDetails.ContactForm} title={contactDetails.ContactForm}>contact form</a>
                    </div>
                </React.Fragment>
            }

            {contactDetails.Website !== null &&
                <React.Fragment>
                    <div className='icon-item' grid-row="7" grid-column="1" alt='website address' title='website address'>&#xe9c9;</div>
                    <div grid-row="7" grid-column="2">
                        visit <a href={contactDetails.Website} title={contactDetails.Website}>website</a>
                    </div>
                </React.Fragment>
            }
        </div>
    )
}

export default AgencyContactCard;