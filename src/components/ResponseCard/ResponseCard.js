import React from 'react';
import ResponseCardButtonStack from '../ResponseCardButtonStack/ResponseCardButtonStack';

function ResponseCard(props) {

    return (
        <React.Fragment>
            {props.genericAttachment.title && <h3>{props.genericAttachment.title}</h3>}
            {props.genericAttachment.imageUrl && <img src={props.genericAttachment.imageUrl} alt="" />}
            <ResponseCardButtonStack buttons={props.genericAttachment.buttons} onButtonClick={props.onButtonClick} />
        </React.Fragment>
    )
}

export default ResponseCard;