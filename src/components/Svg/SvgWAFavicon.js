import React from 'react';

function SvgWAFavicon(props) {
  return (
      <svg version="1.1" id="Layer_1" x="0px" y="0px" width={props.width} height={props.height} viewBox="0 0 80 72" >
        <rect x="15" y="15.5" fill="#060606" width="59.7" height="59.7"/>
        <g>
          <path fill="#FFFFFF" d="M48.5,33.8l-4.9,23.8h-3.8h-2.6l-2.9-12.8l-2.7,12.8h-3.3h-3.1l-4.9-23.8h6l2.6,14.5l2.9-14.5h2.9H37
            l3.1,14.6l2.6-14.6H48.5z"/>
          <path fill="#FFFFFF" d="M62.6,53.5h-7.7l-1.3,3.8h-6.6l8.6-23.8h4.2h1.6l8.8,23.8h-6.6L62.6,53.5z M61,48.6l-2.4-7.5l-2.2,7.5H61z" />
        </g>
      </svg>
  );
}

export default SvgWAFavicon;
