import React from 'react';

function SvgChatIcon() {
  return (<svg className='chat-icon-svg' version="1.1" id="Layer_1" x="0px" y="0px"
	 width="82.5px" height="82.5px" viewBox="0 0 170 170" >
<circle cx="85.8" cy="85.7" r="67.8"/>
<path fill="#FFFFFF" d="M131.9,85.2c0-19.2-20.6-34.7-46.1-34.7S39.7,66,39.7,85.2s20.6,34.7,46.1,34.7c2.4,0,4.7-0.1,7-0.4l6.2,7.9
	l4-9.9l0.1-0.1C120,112.3,131.9,99.8,131.9,85.2z"/>
<g>
	<path d="M89.7,72.7l-5.6,27h-4.3h-2.9l-3.3-14.6l-3.2,14.6h-3.6h-3.6l-5.6-27h6.9l2.9,16.5l3.4-16.5H74h2.6L80,89.3
		l2.9-16.5h6.8V72.7z"/>
	<path d="M105.6,95.2h-8.7l-1.4,4.5h-7.4l9.8-27h4.7h1.9l9.9,27H107L105.6,95.2z M103.9,89.6l-2.6-8.5l-2.6,8.5
		H103.9z"/>
</g>
</svg>

  );
}

export default SvgChatIcon;
