import React from 'react';

function SvgMinimise() {
  return (
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="#2D2F32" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-chevron-down">
          <polyline points="6 9 12 15 18 9"></polyline>
        </svg>
  );
}

export default SvgMinimise;
