import React from 'react';

function SvgSend() {
  return (
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-send">
        <line transform="translate(7 -5) rotate(45 0 0)" x1="22" y1="2" x2="11" y2="13"></line>
        <polygon transform="translate(7 -5) rotate(45 0 0)" points="22 2 15 22 11 13 2 9 22 2"></polygon>
      </svg>
  );
}

export default SvgSend;
