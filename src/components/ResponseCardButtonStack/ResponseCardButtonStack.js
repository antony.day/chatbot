import React from 'react';
import './ResponseCardButtonStack.css';

function ResponseCardButtonStack(props) {

    let firstButtonText = props.buttons[0].text.toLowerCase();
    let isYesNoResponse = (
        (firstButtonText === 'yes' || (firstButtonText === 'no')));

    return (
        <div className={!isYesNoResponse ? 'response-card-button-stack' : 'response-card-button-stack yesno'}>
            {props.buttons.map((btn, index) => {
                return <button
                    key={index}
                    className='chat-button'
                    onClick={() => props.onButtonClick(btn.value, btn.text)}
                    tabIndex={5 + index}>
                    {btn.text}
                </button>
            })}
        </div>
    )
}

export default ResponseCardButtonStack;