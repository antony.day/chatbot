import React, { useState } from 'react';
import ResponseCard from '../ResponseCard/ResponseCard';
import './ResponseCarousel.css';

function ResponseCarousel(props) {

    const [pageCount] = useState(props.genericAttachments.length);
    const [currentPage, setCurrentpage] = useState(0);

    function onPreviousClicked() {
        setCurrentpage(currentPage > 0 ? currentPage - 1 : pageCount - 1);
    }

    function onNextClicked() {
        setCurrentpage(currentPage < pageCount - 1 ? currentPage + 1 : 0);
    }

    function onPageIndicatorClicked(index) {
        setCurrentpage(index);
    }

    return (
        <div className="response-carousel">

            <div className="carousel-top">
                <div className="previous-button" onClick={onPreviousClicked} title={'previous response page'}>
                    <svg xmlns='http://www.w3.org/2000/svg' alt="previous option page" width='30' height='30' viewBox='0 0 24 24' fill='none' stroke='currentColor' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' className='feather feather-chevron-left'><polyline points='15 18 9 12 15 6'></polyline></svg>
                </div>
                <div className="page-container">
                    {props.genericAttachments.map((genericAttachment, index) => {
                        return <div className={currentPage === index ? "response-carousel-page active" : "response-carousel-page"}>{genericAttachment.buttons.length > 0 &&
                            <ResponseCard carousel={true} genericAttachment={genericAttachment} onButtonClick={props.onButtonClick} />
                        }</div>
                    })}
                </div>
                <div className="next-button" onClick={onNextClicked} title='next response page'>
                    <svg xmlns='http://www.w3.org/2000/svg' alt="next option page" width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' className='feather feather-chevron-right'><polyline points='9 18 15 12 9 6'></polyline></svg>
                </div>
            </div>
            <div className="carousel-indicators">
                {props.genericAttachments.map((genericAttachment, index) => {
                    return <div title={'response page '.concat(index + 1)} onClick={() => onPageIndicatorClicked(index)} className={index === currentPage ? 'page-indicator active' : 'page-indicator'}></div>
                })}
            </div>
        </div>
    )
}

export default ResponseCarousel;