import React, { useState, useEffect, useRef, useCallback } from 'react';
import './DGovChatBot.css';
import SpeechBubble from '../MessageBubbles/SpeechBubble';
import BotActivityBubble from '../MessageBubbles/BotActivityBubble';
import ErrorBubble from '../MessageBubbles/ErrorBubble';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import LexRuntime from 'aws-sdk/clients/lexruntime';
import AWS from 'aws-sdk/global';
import SvgChatIcon from '../Svg/SvgChatIcon';
import SvgMinimise from '../Svg/SvgMinimise';
import SvgSend from '../Svg/SvgSend';
import AudioRecorder from '../Audio/AudioRecorder';
import { MessageType, ResponseMode } from '../../util/types.js';

function DGovChatBot(props) {

    const textAreaPlaceholderDefault = "Start typing or hold the mic button to record..";
    const textAreaPlaceholderStopRecording = "Listening..";
    const textAreaPlaceholderDefaultIE = "Send a message..";

    const [input, setInput] = useState('');
    const [controlsEnabled, setControlsEnabled] = useState(false);
    const [messages, setMessages] = useState([{
        type: MessageType.IN,
        formattedMessage: "Hi there! I've been trained to help you find agencies and Ministers' contact details.<br><br>I'm sorry I can't help you with all your queries. I seek your understanding and patience as I am new and still learning. I will continue to learn more topics to improve my knowledge."
    }]);
    const [lastResponseMode, setLastResponseMode] = useState(ResponseMode.TEXT);
    const [initialised, setInitialised] = useState(false);
    const [minimised, setMinimised] = useState(true);
    const [sessionAttributes, setSessionAttributes] = useState({ 'Accept-Language:': 'en-AU' });
    const [userId] = useState('Contact-bot' + Date.now());
    const [fulfilled, setFulfilled] = useState(false);
    const [errorState, setErrorState] = useState(false);
    const [lastRequestSuccessful, setLastRequestSuccessful] = useState(true);
    const [textAreaPlaceholder, setTextAreaPlaceholder] = useState(null);
    const dummyScrollElement = useRef(null);

    function onInput(e) {
        setInput(e.target.value);
    }

    function resetSessionAttributes() {
        let tempSessionAttributes = {};
        tempSessionAttributes["IntentStatus"] = "UnknownStatus";
        tempSessionAttributes["appContext"] = sessionAttributes.appContext;
        tempSessionAttributes["InputType"] = sessionAttributes.InputType;
        tempSessionAttributes["Accept-Language"] = "en-AU";

        setSessionAttributes(tempSessionAttributes);

    }

    function submitTextMessage(messageContent, display, buttonclicked, displayText = null) {
        setControlsEnabled(false);

        let trimmedMessageContent = messageContent.trim();
        if (trimmedMessageContent.length === 0) {
            setInput('');
            return;
        }

        const outgoingMessage = {
            type: MessageType.OUT,
            formattedMessage: buttonclicked ? displayText : trimmedMessageContent
        };

        let tempMessages = display ? messages.concat(outgoingMessage) : messages;

        setMessages(tempMessages);

        // Create a bogus message for the '...' content
        const botActivityMessage = {
            type: MessageType.BOTACTIVITY,
        };

        tempMessages = tempMessages.concat(botActivityMessage);
        setMessages(tempMessages);

        AWS.config.region = props.config.awsRegionId;
        AWS.config.credentials = new AWS.CognitoIdentityCredentials(
            {
                IdentityPoolId: props.config.awsCognitoIdentityPoolId
            }
        );

        var lexruntime = new LexRuntime();

        var params =
        {
            botAlias: props.config.awsBotAlias,
            botName: props.config.awsBotName,
            inputText: trimmedMessageContent,
            userId: userId,
            sessionAttributes: sessionAttributes
        };

        if (buttonclicked) {
            let tempAttributes = sessionAttributes;
            tempAttributes["clicked"] = "true";
            params.sessionAttributes = tempAttributes;
        }

        try {
            lexruntime.postText(params, function (err, response) {

                if (err) {
                    setErrorState(true);
                    console.log(err);
                }
                else {
                    setLastRequestSuccessful(true);

                    const responseMessage = {
                        type: MessageType.IN,
                        formattedMessage: response.message,
                        fullResponse: response
                    };

                    if (containsResponseCardButtons(response)) {
                        responseMessage.genericAttachments = response.responseCard.genericAttachments;
                        setLastResponseMode(ResponseMode.BUTTON);
                        responseMessage.responseMode = ResponseMode.BUTTON;
                    }
                    else {
                        setLastResponseMode(ResponseMode.TEXT);
                        responseMessage.responseMode = ResponseMode.TEXT;
                    }

                    // Remove the '...' message now that the bot has responded
                    tempMessages.pop();
                    tempMessages = tempMessages.concat(responseMessage)
                    setMessages(tempMessages);

                    if (containsContactDetails(response)) {

                        const contactCardResponseMessage = {
                            type: MessageType.IN,
                            formattedMessage: response.message,
                            fullResponse: response
                        };

                        switch (response.sessionAttributes.IntentTarget.toLowerCase()) {
                            case 'minister':
                            case 'premier':
                                setLastResponseMode(ResponseMode.MINISTER_CONTACT_CARD);
                                contactCardResponseMessage.responseMode = ResponseMode.MINISTER_CONTACT_CARD;
                                break;
                            case 'agency':
                                setLastResponseMode(ResponseMode.AGENCY_CONTACT_CARD);
                                contactCardResponseMessage.responseMode = ResponseMode.AGENCY_CONTACT_CARD;
                                break;
                        }

                        setMessages(tempMessages.concat(contactCardResponseMessage));
                    }

                    if (response.sessionAttributes.appContext) {
                        response.sessionAttributes.appContext = null;
                    }

                    setControlsEnabled(true);

                    if (response.dialogState === 'Fulfilled') {
                        resetSessionAttributes();
                        setFulfilled(true);
                    }
                    else {
                        setSessionAttributes(response.sessionAttributes);
                    }
                }
            }
            )
        } catch (e) {
            setErrorState(true);
        }
    }

    const submitTextMessageCallback = useCallback((messageContent, display, buttonclicked, displayText) => {
        submitTextMessage(messageContent, display, buttonclicked, displayText);
    }, [messages, props.config.awsBotAlias, props.config.awsBotName, props.config.awsCognitoIdentityPoolId, props.config.awsRegionId, sessionAttributes, userId])

    useEffect(() => {
        let isExplorer = isIE();
        isExplorer ? setTextAreaPlaceholder(textAreaPlaceholderDefaultIE) : setTextAreaPlaceholder(textAreaPlaceholderDefault);
    }, [])

    useEffect(() => {
        if (!minimised && !initialised) {
            submitTextMessageCallback("Welcome", false, false);
            setInitialised(true);
        }
    }, [minimised, initialised, submitTextMessageCallback]);

    useEffect(() => {
        if (fulfilled) {
            submitTextMessageCallback("continueconversation", false, false);
            setFulfilled(false);
        }
    }, [fulfilled, submitTextMessageCallback]);

    useEffect(() => {
        if (errorState && lastRequestSuccessful) {
            let tempMessages = messages;
            if (messages[messages.length - 1].type === MessageType.BOTACTIVITY) {
                tempMessages.pop();
                const errorMessage = {
                    type: MessageType.ERROR,
                    formattedMessage: "Sorry, an error has occurred.  Please try again later."
                };

                tempMessages = tempMessages.concat(errorMessage);
                setMessages(tempMessages);
                setFulfilled(true);
            }

            setErrorState(false);
            setLastRequestSuccessful(false);
        }
    }, [errorState, lastRequestSuccessful, messages]);

    useEffect(() => {
        dummyScrollElement.current.scrollIntoView({ block: 'end', behaviour: 'smooth' });
    });

    function onResponseCardButtonClicked(value, text) {
        if (value === '') return
        submitTextMessage(value, true, true, text);
    }

    function onSendTypedMessage() {
        if (input === '') return

        submitTextMessage(input, true, false);
        setInput('');
    }

    function onSendKeyPress(e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            submitTextMessage(input, true, false);
            setInput('');
        }
    }

    function onOpenChatBotKeyPress(e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            setMinimised(false);
        }
    }

    function onCloseChatBotKeyPress(e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            setMinimised(true);
        }
    }

    function onRecordingStarted() {
        setTextAreaPlaceholder(textAreaPlaceholderStopRecording);
    }

    function onRecordingStopped(blob) {
        setTextAreaPlaceholder(textAreaPlaceholderDefault);
        submitAudioMessage(blob);
    }

    function submitAudioMessage(audioBlob) {

        if (!audioBlob) return;
        setControlsEnabled(false);

        // Create a bogus message for the '...' content
        const botActivityMessage = {
            type: MessageType.BOTACTIVITY,
        };

        let tempMessages = messages.concat(botActivityMessage);
        setMessages(tempMessages);

        AWS.config.region = props.config.awsRegionId;
        AWS.config.credentials = new AWS.CognitoIdentityCredentials(
            {
                IdentityPoolId: props.config.awsCognitoIdentityPoolId
            }
        );

        var lexruntime = new LexRuntime();

        let params =
        {
            accept: "audio/mpeg",
            botAlias: props.config.awsBotAlias,
            botName: props.config.awsBotName,
            contentType: "audio/l16; rate=16000; channels=1",
            inputStream: audioBlob,
            userId: userId,
            sessionAttributes: sessionAttributes
        };

        lexruntime.postContent(params, function (err, response) {
            if (err) {
                setErrorState(true);
                console.log(err);
            }
            else {
                tempMessages.pop();

                setLastRequestSuccessful(true);

                const outgoingMessage = {
                    type: MessageType.OUT,
                    formattedMessage: response.inputTranscript
                };

                tempMessages = tempMessages.concat(outgoingMessage);

                setMessages(tempMessages);

                const responseMessage = {
                    type: MessageType.IN,
                    formattedMessage: response.message,
                    fullResponse: response,
                };

                try {
                    let audioUrl = window.URL.createObjectURL(new Blob([response.audioStream], { type: "audio/mpeg" }));
                    let audio = new Audio(audioUrl);
                    audio.play();
                }
                catch (e) {
                    console.log(e);
                }

                // Remove the '...' message now that the bot has responded
                tempMessages = tempMessages.concat(responseMessage);
                setMessages(tempMessages);

                if (response.sessionAttributes.appContext) {
                    try {
                        let appContext = JSON.parse(response.sessionAttributes.appContext);

                        if (appContext && appContext.contentType === "application/vnd.amazonaws.card.generic") {
                            responseMessage.genericAttachments = appContext.genericAttachments;
                            setLastResponseMode(ResponseMode.BUTTON);
                            responseMessage.responseMode = ResponseMode.BUTTON;
                            delete response.sessionAttributes.appContext;
                        }
                    }
                    catch (e) {
                        console.log("Could not parse app context: " + e);
                    }
                }

                if (containsContactDetails(response)) {

                    const contactCardResponseMessage = {
                        type: MessageType.IN,
                        formattedMessage: response.message,
                        fullResponse: response,
                    };

                    switch (response.sessionAttributes.IntentTarget.toLowerCase()) {
                        case 'minister':
                        case 'premier':
                            setLastResponseMode(ResponseMode.MINISTER_CONTACT_CARD);
                            contactCardResponseMessage.responseMode = ResponseMode.MINISTER_CONTACT_CARD;
                            break;
                        case 'agency':
                            setLastResponseMode(ResponseMode.AGENCY_CONTACT_CARD);
                            contactCardResponseMessage.responseMode = ResponseMode.AGENCY_CONTACT_CARD;
                            break;
                    }

                    setMessages(tempMessages.concat(contactCardResponseMessage));
                }

                setControlsEnabled(true);

                if (response.dialogState === 'Fulfilled') {
                    resetSessionAttributes();
                    setFulfilled(true);
                }
                else {
                    setSessionAttributes(response.sessionAttributes);
                }
            }
        }
        );
    };

    function isIE() {
        return document.documentMode !== undefined;
    }

    function containsResponseCardButtons(response) {
        let returnVal = false;

        if (response.responseCard === undefined || response.responseCard === null) return false;

        response.responseCard.genericAttachments.forEach(genericAttachment => {
            if (genericAttachment.buttons.length > 0) {
                returnVal = true;
            }
        })

        return returnVal;
    }

    function containsContactDetails(response) {

        return response.sessionAttributes?.ContactDetails !== undefined;
    }

    return (
        <React.Fragment>
            <div className={minimised ? 'chatbot minimised' : 'chatbot'}>

                <div className='header'>
                    <div className='header-text'>WA.gov.au chatbot pilot</div>
                    <div
                        className='minimise-icon'
                        alt='minimise'
                        title='minimise chatbot'
                        tabIndex={13}
                        onKeyPress={onCloseChatBotKeyPress}
                        onClick={() => setMinimised(true)}>
                        <SvgMinimise></SvgMinimise>
                    </div>
                </div>

                <div className='chat-feed' id='chatfeed'>
                    {messages.map((msg, index) => {
                        switch (msg.type) {
                            case MessageType.IN:
                            case MessageType.OUT:
                                return <SpeechBubble
                                    key={index}
                                    index={index}
                                    displayResponseCards={index === messages.length - 1}
                                    message={msg}
                                    responseMode={lastResponseMode}
                                    onClick={onResponseCardButtonClicked}>
                                </SpeechBubble>

                            case MessageType.BOTACTIVITY:
                                return <BotActivityBubble key={index} />

                            case MessageType.ERROR:
                                return <ErrorBubble key={index} message={msg.formattedMessage} />

                            default:
                                return null;
                        }
                    })
                    }

                    <div id={'el'} ref={dummyScrollElement} />
                </div>

                {(lastResponseMode === ResponseMode.TEXT || lastResponseMode === ResponseMode.BUTTON) &&
                    <div className='text-input-panel'>
                        {(!isIE()) &&
                            <AudioRecorder disabled={!controlsEnabled} onRecordingComplete={onRecordingStopped} onRecordingStarted={onRecordingStarted}></AudioRecorder>
                        }
                        <TextareaAutosize
                            className='input-control'
                            aria-label='Enter message here'
                            title='Enter message here'
                            id='text-input'
                            rowsMax={5}
                            placeholder={textAreaPlaceholder}
                            maxLength={1024}
                            autoFocus={true}
                            onChange={onInput}
                            value={input}
                            onKeyPress={onSendKeyPress}
                            // ref={input => input && input.focus()}
                            tabIndex={11} />
                        <div
                            alt='Send'
                            title='Send'
                            className='send-button'
                            onClick={controlsEnabled ? onSendTypedMessage : undefined}
                            tabIndex={12}
                            onKeyPress={onSendKeyPress}>
                            <SvgSend ></SvgSend>
                        </div>
                    </div>
                }
            </div>
            <div className={minimised ? 'chat-icon no-print' : 'chat-icon hidden no-print'}
                title='open chatbot'
                onClick={() => setMinimised(false)}
                tabIndex={0}
                onKeyPress={onOpenChatBotKeyPress}>
                <SvgChatIcon></SvgChatIcon>
            </div>
        </React.Fragment>
    )
}

export default DGovChatBot;