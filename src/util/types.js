export const ResponseMode = {
    TEXT: 0,
    BUTTON: 1,
    SPEECH: 2,
    MINISTER_CONTACT_CARD: 3,
    AGENCY_CONTACT_CARD: 4
}

export const MessageType = {
    OUT: 0,
    IN: 1,
    BOTACTIVITY: 2,
    ERROR: 3
}